clear;
close all;
%load('E:/ADQUISITIONS/PNEUMONIA/HEAD73616943906TK1');
folder_headers = '/home/gustavo/Documents/MATLAB/pneumonia_EI/HEAD';
% folder_headers = '/home/gustavo/Documents/MATLAB/pneumonia_EI/HEADS_8dic';

folder_IMG_files = '/media/gustavo/A076C2B676C28C8A/IMG';
% prefijos={'73629342650','73617351669','73618242071','73628040217','73630243821','73628040881','73628042056','73617353084','73629245402','73640346680','73636943406','73630744059','73643343058','73643447235'};

prefijos = {'73623843603'};
for jj=1:length(prefijos)
    
    mkdir(['videos_IMG/' char(prefijos(jj))]);
    
    list_takes = dir ([folder_IMG_files '/IMG' char(prefijos(jj)) '*']);
    
    
    
    for zz=1:length(list_takes)
        
        
        k1 = strfind(list_takes(zz).name,'TK');
        k2 = strfind(list_takes(zz).name,'CL0');
        
        num_take = list_takes(zz).name (k1+2:k2-1);
        
        header_file =[folder_headers '/' 'HEAD' char(prefijos(jj)) 'TK' num_take '.mat'];
        if (exist(header_file,'file')==0);
            disp('No hay header')
            continue
        end
        
        
        load(header_file)
        %fi=fopen('RCV73616955706TK3CL0');
        % fi=fopen('RCV73638641221TK1CL0');
        
        
        % [Raw,n]=fread(fi,Inf,'int16=>int16');
        
        nombre_save = ['videos_IMG/',char(prefijos(jj)),'/',sprintf('VIDI_zone%d_%dTK%d',UIValues.zone,FILE.PatientID,FILE.Take) '.avi'];
        if (exist(nombre_save,'file')==2);
            disp('Ya existe el archivo')
            continue
        end
        

        %FILE.RcvHandle=fopen([FILE.Directory ...
        %     sprintf('RCV%dTK%dCL%d',FILE.PatientID,FILE.Take,FILE.Cluster)],'r');
        handle = [folder_IMG_files '/' sprintf('IMG%dTK%dCL%d',FILE.PatientID,FILE.Take,FILE.Cluster)];
        FILE.ImgHandle=fopen(handle,'r');

        %fi=fopen('RCV73616955706TK3CL0');
        %[Raw,n]=fread(fi,'int16');

        %fclose(FILE.RcvHandle);
        %Resource.RcvBuffer.rowsPerFrame=1179648;
        %Resource.RcvBuffer.colsPerFrame=64;
        % i=Resource.RcvBuffer.rowsPerFrame;%Resource.RcvBuffer.rowsPerFrame;
        % j=Resource.RcvBuffer.colsPerFrame;%Resource.RcvBuffer.colsPerFrame;
        % k=n/i/j;
        % sizes=[i j k];

        % Raw2=reshape(Raw,sizes);

        l2mm=1540/Trans.frequency/1e3;
        % convb_recon_gapli

        [video, n]=fread(FILE.ImgHandle,Inf,'double=>double');
        i=2*(SFormat.endDepth-SFormat.startDepth)*4;
        j=128;
        k=n/i/j;
        sizes=[i j k];
        video=reshape(video,sizes);

        xl=linspace(-j/2*0.3,j/2*0.3,j);
        zl=linspace(SFormat.startDepth*l2mm,SFormat.endDepth*l2mm,j);

    %     aviobj = avifile(['videos_IMG/',sprintf('VIDI%dTK%d',FILE.PatientID,FILE.Take)],'compression','None');
    %     aviobj.Fps= 10;

        writerObj = VideoWriter(nombre_save);
        writerObj.FrameRate = 1;
            open(writerObj)
        CMAP= repmat(((0:1/64:1).^UIValues.gamma)',[1 3]);

        video(video<UIValues.reject)=0;

        for i=1:sizes(3)
            figure(89);
            % video(:,:,i)=video(:,:,i)-max(max(video(:,:,i)));



            imagesc(xl,zl,video(:,:,i));
            title(['Zone ' num2str(num2str(UIValues.zone)) ' - Frame ' num2str(i)])
    %         title('Verasonics Beamforming & Processing')
            set(gca,'clim',[UIValues.reject UIValues.DynRng]);
            xlabel('azimuth (mm)'),ylabel('depth (mm)')
            set(gcf,'Colormap',CMAP);
            set(gca,'XTick',[-15 -10 -5 0 5 10 15])
            axis image;

            a=figure(89);
            h = getframe(a);
                writeVideo(writerObj,h);
    %         aviobj = addframe(aviobj,gcf);
            pause(0.01);
        end
    %     viobj = close(aviobj); %Close avi object
                close(writerObj);
            clear('writerObj')
        fclose('all');
        
        clear header_file
        clear FILE
        
    end
end