function [ medians ] = block_median( X, w )
%BLOCK_MEDIAN Summary of this function goes here
%   Detailed explanation goes here
    
    num_cols = size(X,2);
    
    for ii=1:num_cols
        lim_inf = max(ii-w,1);
        lim_sup = min(ii+w,num_cols);
        aa = X(:,lim_inf:lim_sup);
        medians(ii)=median(aa(:));
    end


end

