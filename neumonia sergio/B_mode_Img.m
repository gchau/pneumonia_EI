%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Vizualizacion de B-mode images
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;
home;

% Interfaz de lectura de datos (apuntar a la ruta con las carpetas
% "Enfermos" y "Sanos"
input_folder = uigetdir;
cd(input_folder)

code = pwd;

folder = dir;
directorio = {folder.isdir};
directorio = find(cell2mat(directorio));

for i = directorio(3:end)
    fprintf('FileName = %s , id = %i \n',folder(i).name,i)
end
    
% lectura del archivo
cd(code);
folder_loop = strcat(input_folder,'\',folder(id).name,'\');
cd(folder_loop);
images  = dir('*.rf');
    
for fileid = 1:length(images)
    
    %fileCompleto = strcat(folder_loop,'\',images(fileid).name);
    fileCompleto = strcat(folder_loop,images(fileid).name);
    cd(code);
    RF = RPread(fileCompleto, '6.0.5');
    
    fprintf('fileid = %i\n',fileid);
    
    for frame = 1%:size(RF,3)
        frame
        
        F2p= RF(:,:,frame);
        
        iUS = 20*log10(abs(hilbert(F2p)));
        iUS1 = iUS - max(iUS(:));
        iUS2 = max(iUS(:)) - iUS;
        
        figure(1);imagesc(iUS1);colormap gray; set(gca, 'clim', [-60 0]);
        figure(2);imagesc(iUS2);colormap gray; set(gca, 'clim', [0 60]);
    end
    pause
    clear RF
end

