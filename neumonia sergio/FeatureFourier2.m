function [Angulo,I01,I2] = FeatureFourier2(Region,head,N,dim,overlap,umbral)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% Features Inclinacion %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% N = 512;
% dim = 100;
% overlap=25;


tam = 1:ceil(dim*(100-overlap)/100):size(Region,1)-ceil(dim*(100-overlap)/100);

I1 = zeros(length(tam),N/2);
cnt = 1;

for ky = tam                        % numero de muestra
    Reg1 = Region(ky:min(ky+dim-1,size(Region,1)),:);
    Reg1a = bsxfun(@times,Reg1,hamming(size(Reg1,1)));
%     Reg1a = bsxfun(@times,Reg1,rectwin(size(Reg1,1)));

    FReg = abs(fft(Reg1a,N,1));
    
%     h = spectrum.welch('Hann',size(Reg1a,1)/2);
%     FReg = psd(h,Reg1a);

%     FReg2 = sum(FReg.Data,2)/size(Region,2);
%         figure(1),plot(10*log10(FReg2)),pause(0.1)
%     I1(cnt,:) = FReg2;
    
    FReg2 = sum(FReg,2)/size(Region,2);
    I1(cnt,:) = FReg2(1:N/2)';
    cnt = cnt+1;
end

I01 = bsxfun(@rdivide,I1,max(I1,[],2));

I2 = 10*log(I01(1:end-5,:))> umbral; %-10;%%error
[eti_1,neti_1] = bwlabel(I2);

temp = 0;
for indd = 1:neti_1
    BW = eti_1 == indd;
    if sum(BW(:))>temp
        temp = sum(BW(:));
        BW2 = BW;
    end
end

y = zeros(1,size(BW2,1)); x = 1:size(BW2,1);
for indd = 1:size(BW2,1)
    
    lol = find(BW2(indd,:)==1,1,'last');
    
    if isempty(lol)
        y(indd) = 1;
    else
        y(indd) = lol;
    end
end

% depth=linspace(0,size(Region,1)*1540/head.sf/2*100,size(BW2,1));
% freq=linspace(0,head.sf/2/1e6,size(I1,2));

% p = polyfit(depth,medfilt1(freq(y),5),1);
% yfit = polyval(p,depth);
% Angulo = (yfit(1)-yfit(end)/(depth(end)-depth(1)));

p = polyfit(x,medfilt1(y,5),1);
yfit = polyval(p,x);
Angulo = (yfit(1)-yfit(end))/(x(end)-x(1));


end