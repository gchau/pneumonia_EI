function[d, fr, fo, flw, fup] = applyfir(In, H, Nfrec, fo, flw, fup, flag_fr)

% convolve
if isstruct(H) % separable filters

  L_cols = uint16( round( length(H.cols)/2 ) );
  offL_cols = rem(length(H.cols),2);
  if(L_cols == 1) offL_cols = 0; end

  L_rows = uint16( round( length(H.rows)/2 ) );
  offL_rows = rem(length(H.rows),2);
  if(L_rows == 1) offL_rows = 0; end

  y = conv2(H.rows, H.cols, In); 

  %------------------------------------

  if(flag_fr == 1) % frequency response

    fr = struct('cols', [], 'rows', []);
    fr.cols = freqz(H.cols, 1, Nfrec);
    fr.rows = freqz(H.rows, 1, Nfrec);
  else
    fr = 0;
  end

else % non-separable

  [Lrows, Lcols] = size(h);

  L_cols = uint16( round( Lcols/2 ) );
  offL_cols = rem(Lcols,2);

  L_rows = uint16( round( Lrows/2 ) );
  offL_rows = rem(Lrows,2);

  y = conv2(h, In); 

end


[Nrows Ncols] = size(In);
d = y(L_rows + offL_rows : Nrows + L_rows + offL_rows - 1, ...
      L_cols + offL_cols : Ncols + L_cols + offL_cols - 1); 





% copy data
fo  = fo;
flw = flw;
fup = fup; 
