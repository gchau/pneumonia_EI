function[am_est, fm_est, phi_est] = amfm_vak2D(In, HilFIR, LPF, FBank, figN)
%
%


if nargin < 4
    figN = 0;
end

% ------------------------------
% Sanity Checks & Initialization
% ------------------------------

% Check argument LPF (low pass filter)
if ~isempty(LPF),
    % LPF function handle
end

% Number of filters in the filter bank
N_fb = length(FBank);

% Check argument LPF (low pass filter)
if ~isempty(FBank),
    if ~(iscell(FBank) && isa(FBank{1}, 'function_handle') ),
        warning('argument FBank must be a cell array {FBank} of function handle');
        return;
    end
end


% Number of input elements
[Nrows, Ncols] = size(In);



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    --- Low pass input image ---  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% input parameter


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   --- Apply filter bank ---  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In_hil = HilFIR.cols(In);

y = zeros(Nrows, Ncols, N_fb);

phi = zeros(Nrows, Ncols, N_fb);
f_rows_sign = zeros(Nrows, Ncols, N_fb);
f_cols_sign = zeros(Nrows, Ncols, N_fb);


H = cell(1, N_fb);      % Freq. response
Fo = cell(1, N_fb);     % central freq.
Flw = cell(1, N_fb);    % lower cut-off
Fup = cell(1, N_fb);    % upper cut-off


for k=1:N_fb
    
    FB = FBank{k};
    
    [d fr fo flw fup] = FB(In);   % The filter is assumed to be a band-pass FIR filter
    % and therefore d is (assumed to be) equivalent to conv(In, filter{k})
    %
    % fr : frequency response of filter (with Nfrec elements)
    % fo : central frequency of filter (a scalar)
    % flw, fup are the lower/upper cut-off frequencies
    
    y(:,:,k) = d;
    
    % ------------------
    
    d_hil = FB(In_hil);
    
    phi(:,:,k) = atan2(d_hil, y(:,:,k));
    
    if 1
        
        den = 2*( y(:,:,k).*y(:,:,k) + d_hil(:,:).*d_hil(:,:) );
        
        %% sign along rows
        num_r = y(3:Nrows,:,k) - y(1:Nrows-2,:,k);
        num_i = d_hil(3:Nrows,:) - d_hil(1:Nrows-2,:);
        
        % No need to compute asin, since we only need the sign.
        %    f_rows_sign(2:Nrows-1,:,k) = sign( real( asin( ( num_i.*y(2:Nrows-1,:,k) - ...
        %                                                  num_r.*d_hil(2:Nrows-1,:) )./den(2:Nrows-1,:) ) ) );
        
        f_rows_sign(2:Nrows-1,:,k) = sign(num_i.*y(2:Nrows-1,:,k) - num_r.*d_hil(2:Nrows-1,:) );
        f_rows_sign(2:Nrows-1,:,k) = f_rows_sign(2:Nrows-1,:,k) + ( f_rows_sign(2:Nrows-1,:,k)==0 );
        
        %% sign along columns
        num_r = y(:,3:Ncols,k) - y(:,1:Ncols-2,k);
        num_i = d_hil(:,3:Ncols) - d_hil(:,1:Ncols-2);
        
        % No need to compute asin, since we only need the sign.
        %    f_cols_sign(:,2:Ncols-1,k) = sign( real( asin( ( num_i.*y(:,2:Ncols-1,k) - ...
        %                                                     num_r.*d_hil(:,2:Ncols-1) )./den(:,2:Ncols-1) ) ) );
        
        f_cols_sign(:,2:Ncols-1,k) = sign(num_i.*y(:,2:Ncols-1,k) - num_r.*d_hil(:,2:Ncols-1) );
        f_cols_sign(:,2:Ncols-1,k) = f_cols_sign(:,2:Ncols-1,k) + (f_cols_sign(:,2:Ncols-1,k) == 1);
    end
    
    % ------------------
    
    H{k}.rows = abs(fr.rows(:));
    H{k}.cols = abs(fr.cols(:));
    
    Fo{k}.rows = fo.rows;
    Fo{k}.cols = fo.cols;
    
    Flw{k}.rows = flw.rows;
    Flw{k}.cols = flw.cols;
    
    Fup{k}.rows = fup.rows;
    Fup{k}.cols = fup.cols;
    
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Frequency estimation                       %
% Vakman method                              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

f_rows   = zeros(Nrows, Ncols, N_fb);
f_cols   = zeros(Nrows, Ncols, N_fb);
amp  = zeros(Nrows, Ncols, N_fb);

for k = 1:N_fb,
    
    %    FB = FBank{k};
    %    [d fr fo flw fup] = FB(In);
    
    
    
    %% intermediate results
    
    z1_cols = LPF.cols( 2*y(:,1:Ncols-2,k).*y(:,3:Ncols,k) );
    z2_cols = LPF.cols( (y(:,2:Ncols-1,k).*y(:,3:Ncols,k) + ...
        y(:,2:Ncols-1,k).*y(:,1:Ncols-2,k)) );
    
    z1_rows = LPF.rows( 2*y(1:Nrows-2,:,k).*y(3:Nrows,:,k) );
    z2_rows = LPF.rows( (y(2:Nrows-1,:,k).*y(3:Nrows,:,k) + ...
        y(2:Nrows-1,:,k).*y(1:Nrows-2,:,k)) );
    
    z3 = LPF.full( 2*y(:,:,k).*y(:,:,k) );
    
    R_rows = z1_rows./z2_rows;
    R_cols = z1_cols./z2_cols;
    
    % ------------------------------------
    % Compute instantaneous frequency (IF)  FIXME: write an intrenal function
    % ------------------------------------
    
    if( Fo{k}.rows < 0.25 )	% Original Vakman's approach can handle on frequencies between [0 0.25]
        % The extension was first proposed in [rodriguez-2005-phdthesis - REF]
        f_rows(2:Nrows-1,:,k) = real( acos( (R_rows + sqrt( R_rows.*R_rows + 8 ))/4 ) );
    else
        f_rows(2:Nrows-1,:,k) = pi - real( acos( (-R_rows + sqrt( R_rows.*R_rows + 8 ))/4 ) );
    end
    
    % Normalized instantaneous frequency ( in [0 0.5] )
    f_rows(:,:,k) = f_rows(:,:,k) / (2*pi);
    
    % NOTE: use some kind of extrapolation
    f_rows(1,:,k) = f_rows(2,:,k);
    f_rows(end,:,k) = f_rows(end-1,:,k);
    
    % ------------------------------------
    
    if( Fo{k}.cols < 0.25 ) % Original Vakman's approach can handle on frequencies between [0 0.25]
        % The extension was first proposed in [rodriguez-2005-phdthesis - REF]
        f_cols(:,2:Ncols-1,k) = real( acos( (R_cols + sqrt( R_cols.*R_cols + 8 ))/4 ) );
    else
        f_cols(:,2:Ncols-1,k) = pi - real( acos( (-R_cols + sqrt( R_cols.*R_cols + 8 ))/4 ) );
    end
    
    % Normalized instantaneous frequency ( in [0 0.5] )
    f_cols(:,:,k) = f_cols(:,:,k) / (2*pi);
    
    % NOTE: use some kind of extrapolation
    f_cols(:,1,k) = f_cols(:,2,k);
    f_cols(:,end,k) = f_cols(:,end-1,k);
    
    
    
    % instantaneous amplitude
    amp(:,:,k) = sqrt(z3);
    
    
    % ---
    % amplitude adjustment
    % ---
    
    % the IF is considered only if it lays between the filter support
    cond_f_rows = ( f_rows(:,:,k) < Fup{k}.rows ).*( f_rows(:,:,k) > Flw{k}.rows );
    NZ_rows = find( cond_f_rows );      % NZR: non-zero rows
    f_rows(:,:,k)   = cond_f_rows.*f_rows(:,:,k);
    
    cond_f_cols = ( f_cols(:,:,k) < Fup{k}.cols ).*( f_cols(:,:,k) > Flw{k}.cols );
    NZ_cols = find( cond_f_cols );      % NZC: non-zero cols
    f_cols(:,:,k)   = cond_f_cols.*f_cols(:,:,k);
    
    
    %    amp(:,:,k) = cond_f_rows.*cond_f_cols.*amp(:,:,k);
    
    
    
    % Given the freq. response (H{k} in [0 0.5]) of the filter and the IF, compute the corresponding
    % index of the IF with respect to H{k}
    Nfrec_rows = length( H{k}.rows );
    Nfrec_cols = length( H{k}.cols );
    
    tmp = f_rows(:,:,k); tmp = tmp(:); % NOTE: f_rows <= 0.5
    index_rows = uint16( floor(2*Nfrec_rows*tmp(NZ_rows) + 1) );
    
    tmp = f_cols(:,:,k); tmp = tmp(:);
    index_cols = uint16( floor(2*Nfrec_cols*tmp(NZ_cols) + 1) );
    
    % paranoia checks
    % fixing = find( abs( H{k}(index) ) );
    
    tmp = amp(:,:,k); tmp = tmp(:);
    %    amp(nonzero,k) = amp(nonzero,k) ./ abs( H{k}(index) );
    tmp(NZ_rows) = tmp(NZ_rows) ./ abs( H{k}.rows(index_rows) );
    tmp(NZ_cols) = tmp(NZ_cols) ./ abs( H{k}.cols(index_cols) );
    
    amp(:,:,k) = reshape(tmp, Nrows, Ncols);
    
end % _END_ FOR k = 1:N_fb


% ===========================================
% Dominant component analysis

[am_est ind_am] = max(amp, [], 3);

fm_est.rows = zeros(Nrows, Ncols);
fm_est.cols = zeros(Nrows, Ncols);
phi_est     = zeros(Nrows, Ncols);

ind = 1:Nrows*Ncols;
ind = ind(:) + (ind_am(:)-1)*Nrows*Ncols;

tmp = f_rows(:);
tmp_s = f_rows_sign(:);
fm_est.rows = reshape(tmp(ind), Nrows, Ncols).*reshape(tmp_s(ind), Nrows, Ncols);

tmp = f_cols(:);
tmp_s = f_cols_sign(:);
fm_est.cols = reshape(tmp(ind), Nrows, Ncols).*reshape(tmp_s(ind), Nrows, Ncols);

tmp = phi(:);
phi_est = reshape(tmp(ind), Nrows, Ncols);


%Show results
if( figN > 0 )
    
    figure(figN); imagesc(In); axis image; colormap gray
    
    
    figure(figN+1); imagesc(am_est); axis image; colormap gray
    
    
    figure(figN+2); imagesc(In);  axis image; colormap gray
    [xx yy] = meshgrid(1:8:Ncols, 1:8:Nrows);
    hold on; quiver(xx,yy,fm_est.cols(1:8:Nrows,1:8:Ncols), ...
        fm_est.rows(1:8:Nrows,1:8:Ncols), 4);
    hold off;
    
    
end