%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETECCION DE NEUMONIA PARA DATA DEL VERASONICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
close all;
home;

% data_folder = '/media/gustavo/TOSHIBA EXT1/RF_Data/'; %direccion en donde está la data RF
data_folder = '/media/gustavo/TOSHIBA EXT/prueba/';
folder_pleura = '/home/gustavo/Pleuras_verasonics/'; %dirección en donde está la carpeta con las segmentaciones de pleura
output_folder = ['Resultados_AMFM_vera' ]; % donde grabar la salida del algoritmo

% Si no existe el folder de salida, entonces crearlo
if exist(output_folder,'dir')==7;
else
    mkdir(output_folder);
end

% Lista de archivos
folder = dir(data_folder);
directorio = {folder.isdir};
directorio = find(cell2mat(directorio));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PROGRAMA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N = 512;
overlap=50;
umbral = -16;
dim = 50;
factor=10;

images  = dir([data_folder '/*.mat']);

for fileid = 1:length(images)
    disp(['archivo ' num2str(fileid) '/' num2str(length(images))])
    
    
    filename=images(fileid).name;
    fileCompleto = strcat(data_folder,filename);
    
    archivo=images(fileid).name;
    archivo=archivo(1:end-4);
    
    nombre_rf=[data_folder archivo '.mat'];
    nombre_pleura=[folder_pleura archivo '.mat'];
    nombre_output=[output_folder '/output_amfm_' archivo '.mat'];
    
    % Chequear si ya se procesó. Si es así, continuar con el siguiente
    % archivo
    if (exist(nombre_output,'file')==2);
        disp('Ya existe el archivo')
        continue
    end
    
    %Chequear si existe segmentación de pleura, si no existe, continuar con
    %el siguiente
    
    
    
    if ~(exist(nombre_pleura,'file')==2);
        disp('No hay segmentacion de pleura')
    else
        
        
        load(nombre_rf); % cargar data RF
        RF = video_final;
        clear video_final;
        load(nombre_pleura); %cargar pleura
        
        % Parametros de todas las adquisiciones
        freq = 7.5*10^6;% head.sf=40e6;
        sf = 4*freq;
        xw=linspace(0,0.038,128);
        
        for frame = 1:size(RF,3)
            tic
            disp (['Procesndo ' num2str(frame) '/' num2str(size(RF,3))])
            
            
            iRF= RF(:,:,frame);
            
            % Filtrado de bajas frecuencias
            [B] = fir1(100,1.5e6/sf/2,'high');
            for indexFIR=1:size(iRF,2)
                iRF(:,indexFIR)=filter(B,1,iRF(:,indexFIR));
            end
            
            %Recorte de la imagen segun la segmentacion de la pleura
            for indice=1:size(iRF,2);
                iRF2(:,indice)= iRF(ini(indice,frame):fin(indice,frame),indice);
            end
            
            [ia_est if_est phi_est] = vakman2D_2(iRF2, 0);
            
            Mask1(:,frame,1) = mean(ia_est{1},1);
            Mask1(:,frame,2) = median(ia_est{1},1);
            Mask1(:,frame,3) = std(ia_est{1},1);
            
            grad1 = sqrt(if_est{1}.rows.^2 +if_est{1}.cols.^2);
            
            Mask1(:,frame,4) = mean(phi_est{1},1);
            Mask1(:,frame,5) = median(phi_est{1},1);
            Mask1(:,frame,6) = std(phi_est{1},1);
            
            Mask1(:,frame,7) = mean(grad1,1);
            Mask1(:,frame,8) = median(grad1,1);
            Mask1(:,frame,9) = std(grad1,1);
            
            scale = 2;
            
            Mask2(:,frame,1) = mean(ia_est{scale},1);
            Mask2(:,frame,2) = median(ia_est{scale},1);
            Mask2(:,frame,3) = std(ia_est{scale},1);
            
            grad1 = sqrt(if_est{scale}.rows.^2 +if_est{scale}.cols.^2);
            
            Mask2(:,frame,4) = mean(phi_est{scale},1);
            Mask2(:,frame,5) = median(phi_est{scale},1);
            Mask2(:,frame,6) = std(phi_est{scale},1);
            
            Mask2(:,frame,7) = mean(grad1,1);
            Mask2(:,frame,8) = median(grad1,1);
            Mask2(:,frame,9) = std(grad1,1);
            
            
            clear ia_est if_est ia_est;
            clear iRF2;
            
            %Imagen(:,:,frame)=iUS;
            clear iRF2;
            clear Mascara;
          
            
            toc
            
        end
        
       
        save(nombre_output,'Mask1','Mask2','ini','fin');
        clear ini;
        clear fin;
        clear nombre Mask1 Mask2
    end
    
end

