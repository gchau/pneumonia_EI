function[ia_est if_est phi_est] = vakman2D_2(In, flag_show)

if nargin < 2
  flag_show = 0;
end

FBank = gen_filterbank();
LPF   = vak_lowpass();
HIL   = gen_hilbert();


% ---------------------
% AM-FM extended Vakman
% ---------------------

FB1{1} = FBank{1};
FB2{1} = FBank{2};
FB2{2} = FBank{3};
FB2{3} = FBank{4};

[ia if_1 phi_1] = amfm2D_vak(In, HIL, LPF, FB1, flag_show);
    
ia_est{1} = ia;
if_est{1} = if_1;
phi_est{1} = phi_1;

[ia if_1 phi] = amfm2D_vak(In, HIL, LPF,FB2 , flag_show);

ia_est{2} = ia;
if_est{2} = if_1;
phi_est{2} = phi_1;



%% -------------------------------------------------------------

function[FBank] = gen_filterbank()

% very simple filter-bank 
L=15;
Nfrec= 256;

h1 = firpm(30, [0 0.2 0.26 0.5]*2, [ 1 1 0 0]);
h2 = firpm(30, [0 0.24 0.3 0.5]*2, [ 0 0 1 1 ]);


% Generate filter-bank + data
H1 = struct('cols', h1, 'rows', h1);
f1o = struct('cols', 0.125, 'rows', 0.125);
flw1 = struct('cols', 0.0, 'rows', 0.0);
fup1 = struct('cols', 0.25, 'rows', 0.25);

H2 = struct('cols', h1, 'rows', h2);
f2o = struct('cols', 0.125, 'rows', 0.375);
flw2 = struct('cols', 0.0, 'rows', 0.25);
fup2 = struct('cols', 0.25, 'rows', 0.5);

H3 = struct('cols', h2, 'rows', h1);
f3o = struct('cols', 0.375, 'rows', 0.125);
flw3 = struct('cols', 0.25, 'rows', 0.0);
fup3 = struct('cols', 0.5, 'rows', 0.25);

H4 = struct('cols', h2, 'rows', h2);
f4o = struct('cols', 0.375, 'rows', 0.375);
flw4 = struct('cols', 0.25, 'rows', 0.25);
fup4 = struct('cols', 0.5, 'rows', 0.5);


F11 = @(x) applyfir2D(x, H1, 256, f1o, flw1, fup1, 1);
F12 = @(x) applyfir2D(x, H2, 256, f2o, flw2, fup2, 1);
F21 = @(x) applyfir2D(x, H3, 256, f3o, flw3, fup3, 1);
F22 = @(x) applyfir2D(x, H4, 256, f4o, flw4, fup4, 1);

FBank = {F11, F12, F21, F22};

%% -------------------------------------------------------------
%% -------------------------------------------------------------


% -------------------
% low-pass for vakman
% -------------------
function[LPF] = vak_lowpass()

L = 15;
WF = @(x) hamming(x);
lpf1 = genlpf(L, 0, 0.1, 256, 0.5, WF);  % Window method

%  lpf1 = firpm(50, [0 0.09 0.11 0.5]*2, [ 1 1 0 0]);

LPF.rows = @(x) applyfir2D(x, struct('cols', 1, 'rows', lpf1), 256, 0, 0, 0.1, 0);
LPF.cols = @(x) applyfir2D(x, struct('cols', lpf1, 'rows', 1), 256, 0, 0, 0.1, 0);

lpf_full = struct('cols', lpf1, 'rows', lpf1);
LPF.full = @(x) applyfir2D(x, lpf_full, 256, 0, 0, 0.1, 0);

%% -------------------------------------------------------------
%% -------------------------------------------------------------


% -------------------
%     Hilbert
% -------------------

function[HIL] = gen_hilbert()
%  L = 15;
%  Hil = hilbertfir(L);
Hil = firpm(51,[.05 .95],[1 1],'Hilbert');

HIL.rows = @(x) applyfir2D(x, struct('cols', 1, 'rows', Hil), 0, 0, 0, 0, 0);
HIL.cols = @(x) applyfir2D(x, struct('cols', Hil, 'rows', 1), 0, 0, 0, 0, 0);

%% -------------------------------------------------------------
%% -------------------------------------------------------------


