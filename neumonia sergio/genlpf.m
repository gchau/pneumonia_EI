function[hx, Hx, Hxc, ind] = genlpf(L, fo, bw, N, am_max, WF);

if nargin < 6
  WF = @(x) rectwin(x);   % window function
end

f1 = fo - bw/2;
f2 = fo + bw/2;

n = -(N-1):N;
k = 0:2*L-1;
M = exp(-j*2*pi*(n')*k/(2*N));
ind = n/(2*N);

l = -(L-1):L; ntaps = length(l);
Ml = exp(-j*2*pi*(n')*l/(2*N));


iMl = pinv(Ml);


[dummy pos1] = max(f1<=ind);
[dummy pos2] = max(f2<=ind);

neg1 = N - (pos1-N);
neg2 = N - (pos2-N);

%[ind(neg1) ind(pos1) ] 

n_Hx = zeros(2*N,1);
%  n_Hx(pos1:pos2) = hamming(pos2-pos1+1);
%  n_Hx(neg2:neg1) = hamming(neg1-neg2+1);
n_Hx(pos1:pos2) = WF(pos2-pos1+1);
n_Hx(neg2:neg1) = WF(neg1-neg2+1);


hx = real(iMl*n_Hx);
frx = M*hx; mfx = max(abs(frx)); hx = hx/mfx;
Hx = M*hx;

%figure(1); plot(ind, n_Hx);
%hold on; plot(ind, abs(Hx)); hold off

tmp = abs(Hx) > am_max;
Hxc = Hx.*tmp;

