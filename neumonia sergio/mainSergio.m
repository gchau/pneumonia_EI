%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETECCION DE NEUMONIA(SERGIO)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
close all;
home;

% Interfaz de lectura de datos
data_folder = '/home/gustavo/Documents/MATLAB/pneumonia_EI/Sonix_data/Dataset/'; %direccion en donde esten carpetas "enfermos" y "sanos"
folder_pleura = '/home/gustavo/Documents/MATLAB/pneumonia_EI/Sonix_data/Pleura/'
% cd(input_folder)

code = pwd;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPARACION DE LA MUESTRA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%output_folder = [code '\Output ' date '\ ']
output_folder = ['/media/gustavo/G_HD/pneumonia_results/Output_ ' date ]
% cd(input_folder);

if exist(output_folder,'dir')==7;
else
%     mkdir(output_folder);
end

folder = dir(data_folder);
directorio = {folder.isdir};
directorio = find(cell2mat(directorio));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PROGRAMA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 N = 512;
 overlap=50;
 umbral = -16;
 dim = 50;
 factor=10;

for i = directorio(3:end)
    fprintf('FileName = %s , id = %i \n',folder(i).name,i)
end
 
for id = directorio(3:end)
    
    % lectura del archivo
%     cd(code);
    
    %Omitir la lectura de la carpeta "Pleura" para lectura de archivos .rf
%     if folder(id).name ~= 'Pleura';
        
        folder_loop = strcat(data_folder,'/',folder(id).name,'/');
%         cd(folder_loop);
        

        
        
        images  = dir([folder_loop '/*.rf']);
        
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         images  = dir('*.mat');
%         
%         for fileid = 2
%             archivo=images(fileid).name;
% 
%             nombre=strcat(input_folder,'\Pleura\',archivo(1:end-3));
% 
%             load(nombre);
%         end
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        for fileid = 1:length(images)

            filename=images(fileid).name;
            fileCompleto = strcat(folder_loop,filename);
            cd(code);


            

            % Analisis frame por frame:

            archivo=images(fileid).name

            nombre=strcat(folder_pleura,archivo(1:end-3),'.mat')
            
            if ~(exist(nombre,'file')==2);
                disp('No hay segmentacion de pleura')
            else
            
                        [RF,head] = RPread(fileCompleto, '6.0.5');
            xw=linspace(0,0.038,head.w);
            
            load(nombre); %cargar pleura

            for frame = 1:size(RF,3)
                tic
                disp (['Procesndo ' num2str(frame) '/' num2str(size(RF,3))])

                iRF= RF(:,:,frame);
                % Filtrado de bajas frecuencias
                [B] = fir1(100,1.5e6/head.sf/2,'high');
                for indexFIR=1:size(iRF,2)
                    iRF(:,indexFIR)=filter(B,1,iRF(:,indexFIR));
                end
                %Imagen en Modo-B
                iUS = 20*log10(abs(hilbert(iRF)));
                iUS = iUS - max(iUS(:));
%                 figure(1);imagesc(iUS);colormap gray; set(gca, 'clim', [-60 0]);

                %Recorte de la imagen segun la segmentacion de la pleura
                for indice=1:size(iRF,2);
                    iRF2(:,indice)= iRF(ini(indice,frame):fin(indice,frame),indice);
                end
                iUSshort = 20*log10(abs(hilbert(iRF2)));
                iUSshort = iUSshort - max(iUSshort(:));
%                 figure(3);imagesc(iUSshort);colormap gray; set(gca, 'clim', [-60 0]);
                
%                 for indice=1:size(iRF,2);
%                     iUSshort2(:,indice)= iUS(ini(indice,frame):fin(indice,frame),indice);
%                 end
%                 figure(4);imagesc(iUSshort);colormap gray; set(gca, 'clim', [-60 0]);
                
                %factor=10;

                parfor ind=1:size(iRF2,2)-factor+1;

                    Region = iRF2(:,ind:ind+factor-1);%Region = vector de 10 RF-lines

                    [Angulo,I01,I2] = FeatureFourierOrig(Region,head,N,dim,overlap,umbral);

                    AngMask{ind} = Angulo;
                    Ind01{ind} = I01;
                    Ind02{ind} = I2;
                    
                end

                Mascara=cell2mat(AngMask);
                    

                Mascara=[mean(Mascara)*ones(1,ceil(factor/2)-1) Mascara mean(Mascara)*ones(1,ceil(factor/2))];
                                
                Mask(:,frame)=Mascara;
                
                Imagen(:,:,frame)=iUS;

                clear iRF2;
                toc
            end

            
            filename = strcat(output_folder,'/','output_orig2_',images(fileid).name(1:end-3));
%             filename = strcat(output_folder,folder(id).name,'-',images(fileid).name(1:end-3));

            save(filename,'Imagen','Mask','ini','fin');

            clear Mask;
            clear Imagen;
            clear ini;
            clear fin;
            clear nombre
            end
            
        end
        
%     end
    
    
end

% load(filename)
% 
%     figure(5), plot(Mask(:,40)')
%     grid on; axis([0 512 0 inf])
% %     figure(6), plot(Mask1(:,1)')
% %     grid on;axis([0 512 0 inf])
%     BinMask = Mask(:,40)' < 0.40;
%     BinMask1 = Mask1(:,40)' < 0.40;
%     figure(7), plot(BinMask)
%     grid on; axis([0 512 -1 2])
%     figure(8), plot(BinMask1)
%     grid on; axis([0 512 -1 2])
    
