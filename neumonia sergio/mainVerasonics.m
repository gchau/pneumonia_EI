%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETECCION DE NEUMONIA PARA DATA DEL VERASONICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
close all;
home;

data_folder = '/media/gustavo/TOSHIBA EXT1/RF_Data/'; %direccion en donde está la data RF
folder_pleura = '/home/gustavo/Pleuras_verasonics/'; %dirección en donde está la carpeta con las segmentaciones de pleura
output_folder = ['Resultados' ]; % donde grabar la salida del algoritmo

% Si no existe el folder de salida, entonces crearlo
if exist(output_folder,'dir')==7;
else
    mkdir(output_folder);
end

% Lista de archivos
folder = dir(data_folder);
directorio = {folder.isdir};
directorio = find(cell2mat(directorio));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PROGRAMA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N = 512;
overlap=50;
umbral = -16;
dim = 50;
factor=10;

images  = dir([data_folder '/*.mat']);

for fileid = 1:length(images)
    disp(['archivo ' num2str(fileid) '/' num2str(length(images))])
    
    
    filename=images(fileid).name;
    fileCompleto = strcat(data_folder,filename);
    
    archivo=images(fileid).name;
    archivo=archivo(1:end-4);
    
    nombre_rf=[data_folder archivo '.mat'];
    nombre_pleura=[folder_pleura archivo '.mat'];
    nombre_output=[output_folder '/output_' archivo '.mat'];
    
    % Chequear si ya se procesó. Si es así, continuar con el siguiente
    % archivo
    if (exist(nombre_output,'file')==2);
        disp('Ya existe el archivo')
        continue
    end
    
    %Chequear si existe segmentación de pleura, si no existe, continuar con
    %el siguiente
    
    
    
    if ~(exist(nombre_pleura,'file')==2);
        disp('No hay segmentacion de pleura')
    else
   
        
        load(nombre_rf); % cargar data RF
        RF = video_final;
        clear video_final;
        load(nombre_pleura); %cargar pleura
        
        % Parametros de todas las adquisiciones
        freq = 7.5*10^6;% head.sf=40e6;
        sf = 4*freq;
        xw=linspace(0,0.038,128);

        for frame = 1:size(RF,3)
            tic
            disp (['Procesndo ' num2str(frame) '/' num2str(size(RF,3))])
            
            
            iRF= RF(:,:,frame);
    
            % Filtrado de bajas frecuencias
            [B] = fir1(100,1.5e6/sf/2,'high');
            for indexFIR=1:size(iRF,2)
                iRF(:,indexFIR)=filter(B,1,iRF(:,indexFIR));
            end
            
            %Recorte de la imagen segun la segmentacion de la pleura
            for indice=1:size(iRF,2);
                iRF2(:,indice)= iRF(ini(indice,frame):fin(indice,frame),indice);
            end
            
            parfor ind=1:size(iRF2,2)-factor+1;
                
                Region = iRF2(:,ind:ind+factor-1);
                [Angulo,I01,I2] = FeatureFourierOrig_verasonics(Region,N,dim,overlap,umbral,sf);
                AngMask{ind}=Angulo;
                Ind01{ind}=I01;
                Ind02{ind}=I2;
            end
            
            Mascara=cell2mat(AngMask);
            Mascara=[mean(Mascara)*ones(1,ceil(factor/2)-1) Mascara mean(Mascara)*ones(1,ceil(factor/2)-1)];
            Mask(:,frame)=Mascara;
            %Imagen(:,:,frame)=iUS;
            clear iRF2;
            clear Mascara;
            toc
        end

                        
        save(nombre_output,'Mask','ini','fin');
        
        clear Mask;
        clear Imagen;
        clear ini;
        clear fin;
        clear nombre
    end
    
end

