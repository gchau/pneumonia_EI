function Movie_from_frames(input_file, frameRate, workingDir)

    imageNames = dir(fullfile(workingDir,'*.jpg'));
    imageNames = {imageNames.name}';
    
    videoName = strcat(input_file(1:find(input_file=='.')-1),'.avi');

    outputVideo = VideoWriter(fullfile(workingDir,videoName));
    outputVideo.FrameRate = frameRate;
    open(outputVideo)

    for ii = 1:length(imageNames)
       img = imread(fullfile(workingDir,imageNames{ii}));
       writeVideo(outputVideo,img)
    end
    
    close(outputVideo)

end
