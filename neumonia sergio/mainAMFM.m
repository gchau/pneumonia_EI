%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETECCION DE NEUMONIA(SERGIO)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear;
close all;
home;

% Interfaz de lectura de datos
data_folder = '/home/gustavo/Documents/MATLAB/pneumonia_EI/Sonix_data/Dataset/'; %direccion en donde esten carpetas "enfermos" y "sanos"
folder_pleura = '/home/gustavo/Documents/MATLAB/pneumonia_EI/Sonix_data/Pleura/'
% cd(input_folder)

code = pwd;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPARACION DE LA MUESTRA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%output_folder = [code '\Output ' date '\ ']
output_folder = ['/media/gustavo/A076C2B676C28C8A/Output_AMFM_sinsegm' ]
% cd(input_folder);

if exist(output_folder,'dir')==7;
else
    mkdir(output_folder);
end

folder = dir(data_folder);
directorio = {folder.isdir};
directorio = find(cell2mat(directorio));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PROGRAMA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 N = 512;
 overlap=50;
 umbral = -16;
 dim = 50;
 factor=10;
dynran=-65;
for i = directorio(3:end)
    fprintf('FileName = %s , id = %i \n',folder(i).name,i)
end
 
for id = directorio(3:end)
    
    % lectura del archivo
%     cd(code);
    
    %Omitir la lectura de la carpeta "Pleura" para lectura de archivos .rf
%     if folder(id).name ~= 'Pleura';
        
        folder_loop = strcat(data_folder,'/',folder(id).name,'/');
%         cd(folder_loop);
        

        
        
        images  = dir([folder_loop '/*.rf']);
        
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         images  = dir('*.mat');
%         
%         for fileid = 2
%             archivo=images(fileid).name;
% 
%             nombre=strcat(input_folder,'\Pleura\',archivo(1:end-3));
% 
%             load(nombre);
%         end
%         %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        Mask_ia = {};
        Mask_if={};
        Mask_phi={};
                
        for fileid = 1:length(images)

            filename=images(fileid).name;
            fileCompleto = strcat(folder_loop,filename);
            cd(code);


            

            % Analisis frame por frame:

            archivo=images(fileid).name

            nombre=strcat(folder_pleura,archivo(1:end-3),'.mat')
            
            if ~(exist(nombre,'file')==2);
                disp('No hay segmentacion de pleura')
            else
            
            [RF,head] = RPread(fileCompleto, '6.0.5');
            xw=linspace(0,0.038,head.w);
            
            load(nombre); %cargar pleura

            for frame = 1:size(RF,3)
                tic
                disp (['Procesndo ' num2str(frame) '/' num2str(size(RF,3))])

                iRF= RF(:,:,frame);
                % Filtrado de bajas frecuencias
                [B] = fir1(100,1.5e6/head.sf/2,'high');
                for indexFIR=1:size(iRF,2)
                    iRF(:,indexFIR)=filter(B,1,iRF(:,indexFIR));
                end

%                 %Recorte de la imagen segun la segmentacion de la pleura
%                 for indice=1:size(iRF,2);
%                     iRF2(:,indice)= iRF(ini(indice,frame):fin(indice,frame),indice);
%                 end
%                 
%                 env = abs(hilbert(iRF2));
%                 env = env/max(env(:));
%                 iRF2 = 20*log10(env);
%                 iRF2(iRF2<dynran) = dynran;
%                 iRF2 = (iRF2-dynran)/(-dynran);
%                 iRF2 = conv2(iRF2, ones(3)/9);
                
                
                [ia_est if_est phi_est] = vakman2D_2(iRF, 0);
                
                Mask1(:,frame,1) = mean(ia_est{1},1);
                Mask1(:,frame,2) = median(ia_est{1},1);
%                 Mask1(:,frame,2) = block_median(ia_est{1},2);
                Mask1(:,frame,3) = std(ia_est{1},1);
                
                grad1 = sqrt(if_est{1}.rows.^2 +if_est{1}.cols.^2);
                
                Mask1(:,frame,4) = mean(phi_est{1},1);
                Mask1(:,frame,5) = median(phi_est{1},1);
                Mask1(:,frame,6) = std(phi_est{1},1);
                
                Mask1(:,frame,7) = mean(grad1,1);
                Mask1(:,frame,8) = median(grad1,1);
%                 Mask1(:,frame,8) = block_median(grad1,2);

                Mask1(:,frame,9) = std(grad1,1);
                
                scale = 2;
                
                Mask2(:,frame,1) = mean(ia_est{scale},1);
                Mask2(:,frame,2) = median(ia_est{scale},1);
                Mask2(:,frame,3) = std(ia_est{scale},1);
                
                grad1 = sqrt(if_est{scale}.rows.^2 +if_est{scale}.cols.^2);
                
                Mask2(:,frame,4) = mean(phi_est{scale},1);
                Mask2(:,frame,5) = median(phi_est{scale},1);
                Mask2(:,frame,6) = std(phi_est{scale},1);
                
                Mask2(:,frame,7) = mean(grad1,1);
                Mask2(:,frame,8) = median(grad1,1);
                Mask2(:,frame,9) = std(grad1,1);
                               
                
                clear ia_est if_est ia_est;
                clear iRF2;

                toc
            end

            if strcmp(folder(id).name,'Enfermos')
                clase = 'Caso';
            else
                clase = 'Sano';
            end
            
            
            filename = strcat(output_folder,'/', clase,'_',images(fileid).name(1:end-3));
%             filename = strcat(output_folder,folder(id).name,'-',images(fileid).name(1:end-3));

%             save(filename,'Mask_ia','Mask_if','Mask_phi','ini','fin','-v7.3');

            save(filename,'Mask1','Mask2','ini','fin');
            clear ini;
            clear fin;
            clear nombre Mask1 Mask2
            end
            
        end
        
%     end
    
    
end

% load(filename)
% 
%     figure(5), plot(Mask(:,40)')
%     grid on; axis([0 512 0 inf])
% %     figure(6), plot(Mask1(:,1)')
% %     grid on;axis([0 512 0 inf])
%     BinMask = Mask(:,40)' < 0.40;
%     BinMask1 = Mask1(:,40)' < 0.40;
%     figure(7), plot(BinMask)
%     grid on; axis([0 512 -1 2])
%     figure(8), plot(BinMask1)
%     grid on; axis([0 512 -1 2])
    
