function [Angulo,I01,I2] = FeatureFourier3(Region,head,N,dim,overlap,umbral)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% Features Inclinacion %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% N = 512;
% dim = 100;
% overlap=25;


tam = 1:ceil(dim*(100-overlap)/100):size(Region,1)-ceil(dim*(100-overlap)/100);
RS = 0; %vectores para remover del fondo de la imagen para mejorar el ajuste de la recta
I1 = zeros(length(tam),N/2+1); % +1 por calculo de potencia con metodo de Welch
cnt = 1;

for ky = tam                        % numero de muestra
    Reg1 = Region(ky:min(ky+dim-1,size(Region,1)),:);
    %Reg1a = bsxfun(@times,Reg1,hamming(size(Reg1,1)));
    
%     [pxx,f] = pwelch(Reg1,[],[],N,head.sf);
    [pxx,f] = pwelch(Reg1,hanning(ceil(length(Reg1)/4)),[],N,head.sf);%7 segmenos de 50% de overlap
%     figure(5), plot(f,10*log10(pxx)) %potencia a dB
%     xlabel('Frequency (Hz)')
%     ylabel('Magnitude (dB)')
    
    Prompxx = sum(pxx,2)/size(Region,2);
%     figure(6), plot(f,10*log10(Prompxx)) %potencia a dB
%     xlabel('Frequency (Hz)')
%     ylabel('Magnitude (dB)')
        
    I1(cnt,:) = Prompxx';
    cnt = cnt+1;
end

I01 = bsxfun(@rdivide,I1,max(I1,[],2)); %para normalizar %Imagesc
% figure(7), plot(f/1e6,10*log10(I01(1,:))) %potencia a dB
% xlabel('Frequency (Hz)')
% ylabel('Magnitude (dB)')
% 
% figure(8), plot(f/1e6,10*log10(I1(1,:))) %potencia a dB
% xlabel('Frequency (Hz)')
% ylabel('Magnitude (dB)')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % % % % I2 = 10*log(I01(1:end-5,:))> umbral; %-10;%%error
I2 = 10*log(I01(1:end-RS,:))> -16;%umbral;
% figure(9), imagesc(I2)

[eti_1,neti_1] = bwlabel(I2);

temp = 0;
for indd = 1:neti_1
    BW = eti_1 == indd;
    if sum(BW(:))>temp
        temp = sum(BW(:));
        BW2 = BW;
    end
end

y = zeros(1,size(BW2,1)); x = 1:size(BW2,1);
for indd = 1:size(BW2,1)
    
    lol = find(BW2(indd,:)==1,1,'last');%busca la pos del ultimo "1" de la 
                                        %fila indd del objeto
    
    if isempty(lol)
        y(indd) = 1;
    else
        y(indd) = lol;
    end
end

%Para calcular el ajuste si no se toman algunas lineas del fondo
depthShort=linspace((head.h - size(Region,1))*1540/head.sf/2*100,head.h*1540/head.sf/2*100-(RS*ceil(dim*(100-overlap)/100)+ceil(dim*overlap/100))*1540/head.sf/2*100,size(BW2,1));

%Para grafico de depth vs. freq vs. dB
% depth=linspace((head.h - size(Region,1))*1540/head.sf/2*100,head.h*1540/head.sf/2*100,size(I01,1));

% % % % % % % depth=linspace(0,size(Region,1)*1540/head.sf/2*100,size(BW2,1));
% % % % % % % freq=linspace(0,head.sf/2/1e6,size(I1,2));

f0 = f/1e6;
% figure(10), imagesc(f0,depth,10*log10(I01))
% hb = colorbar;
% set(gca,'fontsize',10,'fontname','times','fontweight','b');
% ylabel(hb,'dB');
% set(gca,'fontsize',10,'fontname','times','fontweight','b');
% xlabel('Frequency (MHz)'); ylabel('Depth(cm)');


p = polyfit(depthShort,medfilt1(f0(y)',5),1);
% yfit = polyval(p,depth);
yfit = polyval(p,depthShort);
% Angulo = (yfit(1)-yfit(end))/(depth(1)-depth(end));
Angulo = (yfit(1)-yfit(end))/(depthShort(1)-depthShort(end));

% figure(11), plot(depth,yfit,'r'); hold on, plot(depth, medfilt1(f0(y)',3))
% axis([min(depth) max(depth) 5 10])
% xlabel('Depth(cm)'); ylabel('Frequency (MHz)');

% text(2,6,['y = ',num2str(p(1)),'*x + ',num2str(p(2))]);hold off


% p1 = polyfit(x,medfilt1(y,5),1);
% yfit1 = polyval(p1,x);
% Angulo1 = ((yfit1(1)-yfit1(end))/((x(1)-x(end))));

% figure(12), plot(x,yfit1,'r'); hold on, plot(x, medfilt1(y,5))
% xlabel('x'); ylabel('y');
% 
% text(10,90,['y = ',num2str(p1(1)),'*x + ',num2str(p1(2))]);hold off
            
end