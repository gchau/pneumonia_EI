clear all;
close all;

[input_file,path_file] = uigetfile;
ori_folder=pwd;
cd (path_file)
load(input_file);

for i=1:size(Mask,2)
    fig=figure;
    set(gcf, 'Position', get(0, 'Screensize'));
    subplot(221),imagesc(Imagen(:,:,i));colormap gray;set(gca, 'clim', [-60 0]);
    title(['Frame ',num2str(i)])
    hold on
    plot(ini(:,i),'r','LineWidth',2);
    plot(fin(:,i),'r','LineWidth',2);
    xlabel('# de Linea','FontSize', 10)
    ylabel('# de Muestra','FontSize', 10)
    set(gca,'FontSize',8)
        
    temp=squeeze(Mask(:,i: min(i+4,size(Mask,2))));

    subplot(224),plot(double(median(temp,2)<0.41));
    xlabel('# de Linea','FontSize', 10)
    ylabel('Estimado','FontSize', 10)
    set(gca,'FontSize',8)
    axis([1 512 -1 2]);
    
    
    subplot(222),plot((temp));
%     subplot(222),plot(double(median(temp,2)));
    xlabel('# de Linea','FontSize', 10)
    ylabel('Umbralizacion','FontSize', 10)
    set(gca,'FontSize',8)
    legend(strcat('Frame ',num2str(i)),strcat('Frame ',num2str(i+1)),...
        strcat('Frame ',num2str(i+2)),strcat('Frame ',num2str(i+3)),...
        strcat('Frame ',num2str(i+4)),'Location','southeast')
    axis([1 512 -1 2]);

    subplot(223),plot(medfilt1(double(median(temp,2)<0.41),25));
    axis([1 512 -1 2]);
%     subplot(212),plot(medfilt1(Mask(1,:,i),50)); axis([1 512 -100 100]);
    
%     hold on; plot(max(Mask(1,:,i))*ones(1,size(Mask,2)),'r');
%     
%     med(i)=max(Mask(1,:,i));
%     plot(mean(med)*ones(1,size(Mask,2)),'k');
    xlabel('# de Linea','FontSize', 10)
    ylabel('Detecci�n','FontSize', 10)
    set(gca,'FontSize',8)
    
    truesize(fig,[260 400])
    imagen=getframe(fig);
    img=imagen.cdata;
    imwrite(img,sprintf('Frame%d.jpg',i))
%     
%     pause
    close
    
end

close all;
cd(ori_folder);
Movie_from_frames(input_file,4,path_file);
cd (path_file)
delete('*.jpg');
cd(ori_folder);