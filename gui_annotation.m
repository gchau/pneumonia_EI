function varargout = gui_annotation(varargin)
% GUI_ANNOTATION MATLAB code for gui_annotation.fig
%      GUI_ANNOTATION, by itself, creates a new GUI_ANNOTATION or raises the existing
%      singleton*.
%
%      H = GUI_ANNOTATION returns the handle to a new GUI_ANNOTATION or the handle to
%      the existing singleton*.
%
%      GUI_ANNOTATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_ANNOTATION.M with the given input arguments.
%
%      GUI_ANNOTATION('Property','Value',...) creates a new GUI_ANNOTATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_annotation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_annotation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui_annotation

% Last Modified by GUIDE v2.5 30-Nov-2016 18:39:20

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_annotation_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_annotation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gui_annotation is made visible.
function gui_annotation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui_annotation (see VARARGIN)

% Choose default command line output for gui_annotation
handles.output = hObject;
handles.videos_folder = 'videos_puno';

% load list of videos
handles.video_list = dir([handles.videos_folder '/*.avi']);

if (exist('annotation_file.mat','file')==2) % if exist annotation file
    load('annotation_file.mat')
    handles.annotation = annotation_file;
    
    last_vid_num = annotation_file{end,1}.videonum;
    last_duration = annotation_file{end,1}.vidduratiopn;
    last_frame = annotation_file{end,1}.frame;
    
    if (last_frame == last_duration) % if end of video
        handles.cur_vid_num=last_vid_num+1; %next video
        handles.cur_frame_num = 1;
    else %if there are still frames
        handles.cur_vid_num=last_vid_num;
        handles.cur_frame_num = last_frame+1;
    end
    
else
    handles.annotation = {};
    % compare with annotation and define the latest video
    handles.cur_vid_num = 1; % current video number
    handles.cur_frame_num = 1; % current frame number

end

handles = load_video(handles);



%initializae some variables
handles.current_regions = []; % where to save regions
handles.state = 0;


% Update handles structure
guidata(hObject, handles);



% UIWAIT makes gui_annotation wait for user response (see UIRESUME)
% uiwait(handles.btn_reset);


% --- Outputs from this function are returned to the command line.
function varargout = gui_annotation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in btn_region.
function btn_region_Callback(hObject, eventdata, handles)
% hObject    handle to btn_region (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

rect = getrect(handles.ax_image );
rectangle('Position',rect,'LineStyle','--','EdgeColor','red')

index = size(handles.current_regions,1)+1;
handles.current_regions(index,:) = rect;
handles.state = 1;

set(handles.btn_quality, 'Enable', 'Off'); % clean comments

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in btn_quality.
function btn_quality_Callback(hObject, eventdata, handles)
% hObject    handle to btn_quality (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.state = -1;
set(handles.btn_region, 'Enable', 'Off'); % clean comments
set(handles.txt_quality, 'Visible', 'On'); % clean comments


% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in btn_next.
function btn_next_Callback(hObject, eventdata, handles)
% hObject    handle to btn_next (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% if last frame then load next video


% save current state
handles = save_annotation(handles);

% go to next frame/or video
if (handles.cur_vid_num==length(handles.video_list)) %last video
    if (handles.duration == handles.cur_frame_num) % if end of video
        set(handles.btn_next, 'Enable', 'Off'); % clean comments
        h = msgbox('There are no more videos');
    else %if there are still frames
        handles.cur_frame_num = handles.cur_frame_num+1;
        draw_frame(handles);
    end
else
    if (handles.duration == handles.cur_frame_num) % if end of video
        handles.cur_vid_num=handles.cur_vid_num+1; %next video
        handles.cur_frame_num = 1;
        handles = load_video(handles);
    else %if there are still frames
        handles.cur_frame_num = handles.cur_frame_num+1;
        draw_frame(handles);
    end
end

%clear
handles.current_regions = [];
handles.state = 0;
set(handles.txt_comments, 'String', ''); % clean comments
set(handles.txt_quality, 'Visible', 'Off'); % clean comments
set(handles.btn_quality, 'Enable', 'On'); % clean comments
set(handles.btn_region, 'Enable', 'On'); % clean comments

% Update handles structure
guidata(hObject, handles);



function txt_comments_Callback(hObject, eventdata, handles)
% hObject    handle to txt_comments (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_comments as text
%        str2double(get(hObject,'String')) returns contents of txt_comments as a double


% --- Executes during object creation, after setting all properties.
function txt_comments_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_comments (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function handles_out = load_video(handles)

handles_out = handles;


% open video and show
current_vid_name = handles.video_list(handles.cur_vid_num).name; %name of current video
current_vid_name = current_vid_name(1:end-4); % remove extension

% extract zone, patient ID and # take from the name
handles_out.zone = current_vid_name(10);
handles_out.patient_ID = current_vid_name(12:22);
handles_out.take = current_vid_name(25:end);

% change labels in gui
set(handles.txt_patient, 'String', handles_out.patient_ID);
set(handles.txt_take, 'String', handles_out.take);
set(handles.txt_zone, 'String', handles_out.zone);
set(handles.txt_frame_num, 'String', num2str(handles.cur_frame_num));

% load video
vr = VideoReader([handles.videos_folder '/' current_vid_name '.avi']);
current_video_matrix = [];zeros(vr.Height,vr.Width,vr.Duration);
for ii=1:vr.Duration
    image_actual = mean(double(readFrame(vr)),3)/255;
    image_actual = image_actual(31:374,184:404);
    current_video_matrix(:,:,ii) = image_actual ;
end
handles_out.video = current_video_matrix;
handles_out.x_axis = linspace (-19.2,19.2,size(image_actual,2));
handles_out.z_axis = linspace (0,59.96,size(image_actual,1));
handles_out.duration = vr.Duration;
draw_frame(handles_out)


function draw_frame(handles)

% show frame in the GUI
axes(handles.ax_image);
imagesc(handles.x_axis,handles.z_axis,handles.video(:,:,handles.cur_frame_num));
set(handles.txt_frame_num, 'String', num2str(handles.cur_frame_num));
            set(gca,'XTick',[-15 -11 -8 -5 0 5 8 11 15])
set(gca, 'XColor', [1 0 0]);

% clear variable
handles.state = 0;

colormap gray
axis image


function handles= save_annotation(handles)

%create structure
frame_annotation = struct;
frame_annotation.name_video = handles.video_list(handles.cur_vid_num).name;
frame_annotation.patient = handles.patient_ID;
frame_annotation.zone = handles.zone;
frame_annotation.take = handles.take;
frame_annotation.frame = handles.cur_frame_num;
frame_annotation.state = handles.state;
frame_annotation.comment = get(handles.txt_comments, 'string');
frame_annotation.regions = handles.current_regions;
frame_annotation.videonum = handles.cur_vid_num;
frame_annotation.vidduratiopn = handles.duration;
index = length(handles.annotation)+1;
handles.annotation{index,1} = frame_annotation; % add to annotation cell
annotation_file = handles.annotation;
save('annotation_file.mat','annotation_file')
% Update handles structure


% --- Executes on button press in txt_reset.
function txt_reset_Callback(hObject, eventdata, handles)
% hObject    handle to txt_reset (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%clear
draw_frame(handles)
handles.state = 0;
handles.current_regions = [];
set(handles.txt_comments, 'String', ''); % clean comments
set(handles.txt_quality, 'Visible', 'Off'); % clean comments
set(handles.btn_quality, 'Enable', 'On'); % clean comments
set(handles.btn_region, 'Enable', 'On'); % clean comments

% Update handles structure
guidata(hObject, handles);
