% Copyright 2001-2013 Verasonics, Inc.  All world-wide rights and remedies under all intellectual property laws and industrial property laws are reserved.  Verasonics Registered U.S. Patent and Trademark Office.
%
% File name SetUpP4_2vFlash.m:
% Generate .mat Sequence Object file for P4-2v phased array
% virtual apex format, flash transmit scan. 
%  - The curvature of the transmit pulse is set to match a circle with the same
%    radius as the distance to the virtual apex.
%  - The P4-2v is a 64 element probe that is wired to the scanhead connector with
%    elements 1-64 connected to inputs 33-96. We therefore need a Trans.Connector array
%    to specify the connector channels used, which will be defined by the computeTrans function.
%
% Last update 04-07-2013

clear all

% Define system parameters.
Resource.Parameters.numTransmit = 128;  % number of transmit channels.
Resource.Parameters.numRcvChannels = 64;  % number of receive channels.
Resource.Parameters.speedOfSound = 1540;
Resource.Parameters.speedCorrectionFactor = 1.0;
Resource.Parameters.simulateMode = 0;
%  Resource.Parameters.simulateMode = 1 forces simulate mode, even if hardware is present.
%  Resource.Parameters.simulateMode = 2 stops sequence and processes RcvData continuously.

% Specify Trans structure array.
Trans.name = 'P4-2v';
Trans = computeTrans(Trans);

% Set up SFormat structure array.
aperture = 64*Trans.spacing; % aperture in wavelengths
SFormat.transducer = 'P4-2v';
SFormat.scanFormat = 'VAPX';
SFormat.theta = -pi/4;
SFormat.radius = (aperture/2)/tan(-SFormat.theta); % dist. to virt. apex
SFormat.numRays = 1;      % no. of Rays (1 for Flash transmit)
SFormat.FirstRayLoc = [0,0,0];   % x,y,z
SFormat.rayDelta = 2*(-SFormat.theta);  % spacing in radians(sector) or dist. between rays
SFormat.startDepth = 0;
SFormat.endDepth = 160;   % Acquisition depth in wavelengths

% Set up PData structure.
PData.sFormat = 1;
PData.pdeltaX = 0.875;
PData.pdeltaZ = 0.5;
PData.Size(1) = 10 + ceil((SFormat.endDepth-SFormat.startDepth)/PData.pdeltaZ);
PData.Size(2) = 10 + ceil(2*(SFormat.endDepth + SFormat.radius)*sin(-SFormat.theta)/PData.pdeltaX);
PData.Size(3) = 1;
PData.Origin = [-(PData.Size(2)/2)*PData.pdeltaX,0,0];

% Specify Media.  Use point targets in middle of PData.
% Set up Media points
% - Uncomment for speckle
% Media.MP = rand(40000,4);
% Media.MP(:,2) = 0;
% Media.MP(:,4) = 0.04*Media.MP(:,4) + 0.04;  % Random amplitude 
% Media.MP(:,1) = 2*halfwidth*(Media.MP(:,1)-0.5);
% Media.MP(:,3) = SFormat.acqDepth*Media.MP(:,3);
Media.MP(1,:) = [-45,0,30,1.0];
Media.MP(2,:) = [-15,0,30,1.0];
Media.MP(3,:) = [15,0,30,1.0];
Media.MP(4,:) = [45,0,30,1.0];
Media.MP(5,:) = [-15,0,60,1.0];
Media.MP(6,:) = [-15,0,90,1.0];
Media.MP(7,:) = [-15,0,120,1.0];
Media.MP(8,:) = [-15,0,150,1.0];
Media.MP(9,:) = [-45,0,120,1.0];
Media.MP(10,:) = [15,0,120,1.0];
Media.MP(11,:) = [45,0,120,1.0];
Media.MP(12,:) = [-10,0,69,1.0];
Media.MP(13,:) = [-5,0,75,1.0];
Media.MP(14,:) = [0,0,78,1.0];
Media.MP(15,:) = [5,0,80,1.0];
Media.MP(16,:) = [10,0,81,1.0];
Media.MP(17,:) = [-75,0,120,1.0];
Media.MP(18,:) = [75,0,120,1.0];
Media.MP(19,:) = [-15,0,180,1.0];
Media.numPoints = 19;
Media.function = 'movePoints';

% Specify Resources.
Resource.RcvBuffer(1).datatype = 'int16';
Resource.RcvBuffer(1).rowsPerFrame = 4096;
Resource.RcvBuffer(1).colsPerFrame = Resource.Parameters.numRcvChannels;
Resource.RcvBuffer(1).numFrames = 100;    % 100 frames used for RF cineloop.
Resource.InterBuffer(1).datatype = 'complex';
Resource.InterBuffer(1).numFrames = 1;  % one intermediate buffer defined but not used.
Resource.InterBuffer(1).rowsPerFrame = 1024;
Resource.InterBuffer(1).colsPerFrame = PData.Size(2);
Resource.ImageBuffer(1).datatype = 'double';
Resource.ImageBuffer(1).rowsPerFrame = 1024; % this is for maximum depth
Resource.ImageBuffer(1).colsPerFrame = PData.Size(2);
Resource.ImageBuffer(1).numFrames = 10;
Resource.DisplayWindow(1).Title = 'P4-2vFlash';
Resource.DisplayWindow(1).pdelta = 0.3;
Resource.DisplayWindow(1).Position = [200,200, ...
    ceil(PData.Size(2)*(PData.pdeltaX/Resource.DisplayWindow(1).pdelta)), ...
    ceil(PData.Size(1)*PData.pdeltaZ/Resource.DisplayWindow(1).pdelta)];    % height
Resource.DisplayWindow(1).ReferencePt = [PData.Origin(1),PData.Origin(3)]; % 2D imaging is in the X,Z plane
Resource.DisplayWindow(1).Colormap = gray(256);

% Specify Transmit waveform structure.
TW.type = 'parametric';
TW.Parameters = [36,17,2,1];   % A, B, C, D

txFocus = 40;  % Initial transmit focus.
txFNum = 2;  % set to desired f-number value for transmit (range: 1.0 - 20)
txNumEl=round((txFocus/txFNum)/Trans.spacing/2); % no. of elements in 1/2 aperture.
if txNumEl > (Trans.numelements/2 - 1), txNumEl = floor(Trans.numelements/2 - 1); end   

% Set up transmit delays in TX structure.
TX.waveform = 1;
TX.Origin = [0,0,0];            % set origin to 0,0,0 for flat focus.
TX.focus = txFocus;     % set focus to negative for concave TX.Delay profile.
TX.Steer = [0,0];
TX.Apod = zeros(1,64);  % set TX.Apod for 64 elements
lft = 32 - txNumEl;
rt = 32 + txNumEl;
TX.Apod(lft:rt) = 1.0;
TX.Delay = computeTXDelays(TX);

% Specify Receive structure arrays. 
maxAcqLength = sqrt(aperture^2 + SFormat.endDepth^2 - 2*aperture*SFormat.endDepth*cos(SFormat.theta-pi/2)) - SFormat.startDepth;
wlsPer128 = 128/(4*2); % wavelengths in 128 samples for 4 samplesPerWave
Receive = repmat(struct('Apod', ones(1,64), ...
                        'startDepth', SFormat.startDepth, ...
                        'endDepth', SFormat.startDepth + wlsPer128*ceil(maxAcqLength/wlsPer128), ...
                        'TGC', 1, ...
                        'bufnum', 1, ...
                        'framenum', 1, ...
                        'acqNum', 1, ...
                        'samplesPerWave', 4, ...
                        'mode', 0, ...
                        'InputFilter', [0.0036,0.0127,0.0066,-0.0881,-0.2595,0.6494], ...
                        'callMediaFunc',1),1,Resource.RcvBuffer(1).numFrames);
% - Set event specific Receive attributes.
for i = 1:Resource.RcvBuffer(1).numFrames
    Receive(i).framenum = i;
end

% Specify TGC Waveform structure.
TGC.CntrlPts = [450,550,650,710,770,830,890,950];
TGC.rangeMax = SFormat.endDepth;
TGC.Waveform = computeTGCWaveform(TGC);

% Specify Recon structure arrays.
Recon = struct('senscutoff', 0.5, ...
               'pdatanum', 1, ...
               'rcvBufFrame', -1, ...
               'ImgBufDest', [1,-1], ...
               'RINums', 1);

% Define ReconInfo structure.
ReconInfo = struct('mode', 0, ...  % replace.
                   'txnum', 1, ...
                   'rcvnum', 1, ...
                   'regionnum', 0);

% Specify Process structure array.
pers = 30;
Process(1).classname = 'Image';
Process(1).method = 'imageDisplay';
Process(1).Parameters = {'imgbufnum',1,...   % number of buffer to process.
                         'framenum',-1,...   % (-1 => lastFrame)
                         'pdatanum',1,...    % number of PData structure to use
                         'norm',1,...        % normalization method(1 means fixed)
                         'pgain',1.0,...            % pgain is image processing gain
                         'persistMethod','simple',...
                         'persistLevel',pers,...
                         'interp',1,...      % method of interpolation (1=4pt interp)
                         'compression',0.5,...      % X^0.5 normalized to output word size
                         'reject',2,...
                         'mappingMode','full',...
                         'display',1,...      % display image after processing
                         'displayWindow',1};

% Specify SeqControl structure arrays.  Missing fields are set to NULL.
SeqControl(1).command = 'jump'; %  - Jump back to start.
SeqControl(1).argument = 1;
SeqControl(2).command = 'timeToNextAcq';  % set time between frames
SeqControl(2).argument = 10000; % 10msec (~100fps)
SeqControl(3).command = 'returnToMatlab';
nsc = 4; % nsc is count of SeqControl objects

n = 1; % n is count of Events

% Acquire all frames defined in RcvBuffer
for i = 1:Resource.RcvBuffer(1).numFrames
    Event(n).info = 'Acquire full aperture';
    Event(n).tx = 1;         % use 1st TX structure.
    Event(n).rcv = i; 
    Event(n).recon = 0;      % no reconstruction.
    Event(n).process = 0;    % no processing
    Event(n).seqControl = [2,nsc]; % time between frames & transferToHostuse
       SeqControl(nsc).command = 'transferToHost';
       nsc = nsc + 1;
    n = n+1;
    
    Event(n).info = 'Reconstruct'; 
    Event(n).tx = 0;         % no transmit
    Event(n).rcv = 0;        % no rcv
    Event(n).recon = 1;      % reconstruction
    Event(n).process = 1;    % process
    Event(n).seqControl = 0;
    if floor(i/3) == i/3     % Exit to Matlab every 3rd frame 
        Event(n).seqControl = 3;
    end
    n = n+1;
end

Event(n).info = 'Jump back to first event';
Event(n).tx = 0;        % no TX
Event(n).rcv = 0;       % no Rcv
Event(n).recon = 0;     % no Recon
Event(n).process = 0; 
Event(n).seqControl = 1; % jump command


% User specified UI Control Elements
% - Sensitivity Cutoff
UI(1).Control =  {'UserB7','Style','VsSlider','Label','Sens. Cutoff',...
                  'SliderMinMaxVal',[0,1.0,Recon(1).senscutoff],...
                  'SliderStep',[0.025,0.1],'ValueFormat','%1.3f'};
UI(1).Callback = text2cell('%-UI#1Callback');

% - Range Change
UI(2).Control = {'UserA1','Style','VsSlider','Label','Range',...
                 'SliderMinMaxVal',[64,320,SFormat.endDepth],'SliderStep',[0.1,0.2],'ValueFormat','%3.0f'};
UI(2).Callback = text2cell('%-UI#2Callback');
             
UI(3).Control = {'UserB4','Style','VsSlider','Label','TX Focus',...
                 'SliderMinMaxVal',[0,320,100],'SliderStep',[0.1,0.2],'ValueFormat','%3.0f'};
UI(3).Callback = text2cell('%-UI#3Callback');

UI(4).Control = {'UserB3','Style','VsSlider','Label','F Number',...
                 'SliderMinMaxVal',[1,20,2],'SliderStep',[0.1,0.2],'ValueFormat','%2.0f'};
UI(4).Callback = text2cell('%-UI#4Callback');

% Specify factor for converting sequenceRate to frameRate.
frameRateFactor = 3;

% Save all the structures to a .mat file.
save('P4-2vFlash');

return


% **** Callback routines to be converted by text2cell function. ****
%-UI#1Callback - Sensitivity cutoff change
ReconL = evalin('base', 'Recon');
for i = 1:size(ReconL,2)
    ReconL(i).senscutoff = UIValue;
end
assignin('base','Recon',ReconL);
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'Recon'};
assignin('base','Control', Control);
return
%-UI#1Callback

%-UI#2Callback - Range change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No range change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','SFormat.endDepth'));
    return
end
range = UIValue;
assignin('base','range',range);
SFormat = evalin('base','SFormat');
SFormat.endDepth = range;
assignin('base','SFormat',SFormat);
evalin('base','PData.Size(1) = ceil((SFormat.endDepth-SFormat.startDepth)/PData.pdeltaZ);');
evalin('base','[PData.Region,PData.numRegions] = createRegions(PData);');
evalin('base','Resource.DisplayWindow(1).Position(4) = ceil(PData.Size(1)*PData.pdeltaZ/Resource.DisplayWindow(1).pdelta);');
Receive = evalin('base', 'Receive');
aperture = evalin('base','aperture');
maxAcqLength = sqrt(aperture^2 + SFormat.endDepth^2 - 2*aperture*SFormat.endDepth*cos(SFormat.theta-pi/2)) - SFormat.startDepth;
wlsPer128 = 128/(4*2);
for i = 1:size(Receive,2)
    Receive(i).endDepth = SFormat.startDepth + wlsPer128*ceil(maxAcqLength/wlsPer128);
end
assignin('base','Receive',Receive);
evalin('base','TGC.rangeMax = SFormat.endDepth;');
evalin('base','TGC.Waveform = computeTGCWaveform(TGC);');
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'SFormat','PData','Receive','Recon','DisplayWindow','ImageBuffer'};
assignin('base','Control', Control);
assignin('base', 'action', 'displayChange');
return
%-UI#2Callback

%-UI#3Callback - TX focus changel
simMode = evalin('base','Resource.Parameters.simulateMode');
% No focus change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','txFocus'));
    return
end
txFocus = UIValue;
assignin('base','txFocus',txFocus);
% Determine TX aperture based on focal point and desired f number.
Trans = evalin('base', 'Trans');
txFNum = evalin('base', 'txFNum'); % get f-number value for transmit (range: 1.0 - 20)
% txNumEl is the number of elements to include on each side of the center element, for the specified
%    focus and sensitivity cutoff.  Thus the full transmit aperture will be 2*txNumEl + 1 elements.
txNumEl = round((txFocus/txFNum)/Trans.spacing/2); % no. of elements in 1/2 aperture.
if txNumEl > (Trans.numelements/2 - 1), txNumEl = floor(Trans.numelements/2 - 1); end  
assignin('base','txNumEl',txNumEl);
% - Redefine event specific TX attributes for the new focus.
TX = evalin('base', 'TX');
    % Set Apod vector back to all zeros
     % write new focus value to TX
    TX(1).focus = txFocus;
    % Set transmit Apodization so (1 + 2*TXnumel) transmitters are active.
    TX.Apod = zeros(1,64);  % set TX.Apod for 64 elements
    lft = 32 - txNumEl;
    rt = 32 + txNumEl;
    TX.Apod(lft:rt) = 1.0;
    TX(1).Delay = computeTXDelays(TX(1));
    
assignin('base','TX', TX);
% Set Control command to update TX
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'TX'};
assignin('base','Control', Control);
return
%-UI#3Callback

%-UI#4Callback - F number change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No F number change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','txFNum'));
    return
end
txFNum = UIValue;
assignin('base','txFNum',txFNum);
% Determine TX aperture based on focal point and desired f number.
Trans = evalin('base', 'Trans');
txFocus = evalin('base', 'txFocus'); % get txFocus value for transmit
% txNumEl is the number of elements to include on each side of the center element, for the specified
%    focus and sensitivity cutoff.  Thus the full transmit aperture will be 2*txNumEl + 1 elements.
txNumEl = round((txFocus/txFNum)/Trans.spacing/2); % no. of elements in 1/2 aperture.
if txNumEl > (Trans.numelements/2 - 1), txNumEl = floor(Trans.numelements/2 - 1); end  
assignin('base','txNumEl',txNumEl);
% - Redefine event specific TX attributes for the new focus.
TX = evalin('base', 'TX');
TX = evalin('base', 'TX');
    % Set Apod vector back to all zeros
    TX(1).Apod(:) = 0;
    % write new focus value to TX
    TX(1).focus = txFocus;
    % Set transmit Apodization so (1 + 2*TXnumel) transmitters are active.
    lft = 32 - txNumEl;
    rt = 32 + txNumEl;
    TX(1).Apod(lft:rt) = 1.0;
    TX(1).Delay = computeTXDelays(TX(1));
 assignin('base','TX', TX);
% Set Control command to update TX
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'TX'};
assignin('base','Control', Control);
return
%-UI#4Callback
