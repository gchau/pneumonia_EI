function vsx_gui
%
% Copyright 2001-2013 Verasonics, Inc.  All world-wide rights and remedies under all intellectual property laws and industrial property laws are reserved.  Verasonics Registered U.S. Patent and Trademark Office.
%
% VSX_GUI  This gui is opened by the main VSX program and allows control of various
% acquisition and processing parameters.

% Close any previously opened GUI windows.


delete(findobj('tag','UI'));
% Initialize and hide the GUI as it is being constructed.
ScrnSize = get(0,'ScreenSize');
f = figure('Visible','off',...  %'Units','normalized',...
            'Position',[ScrnSize(3)-500,(ScrnSize(4)-620)/2,450,620],... %'Position',[0.7,0.25,0.25,0.50],...
            'Name','VSX Control',...
            'NumberTitle','off',...
            'MenuBar','none', ...
            'Resize','on', ...
            'tag','UI');
set(f,'CloseRequestFcn',{@closefunc});
bkgrnd = get(f,'Color');
set(f,'DefaultUicontrolBackgroundColor',bkgrnd)

% Are we running with hardware? Get state of VDAS variable and simulateMode.
if evalin('base','exist(''VDAS'',''var'')')
    VDAS = evalin('base','VDAS');
else VDAS = 0;
end

% Determine simulateMode value and whether there are ImageBuffer frames.
simMode = 0; % set default
nfrms = 0;
if evalin('base','exist(''Resource'',''var'')')
    if evalin('base','isfield(Resource,''Parameters'')');
        if evalin('base','isfield(Resource.Parameters,''simulateMode'')');
            simMode = evalin('base','Resource.Parameters.simulateMode');
        end
    end
    if evalin('base','isfield(Resource,''ImageBuffer'')')
        nfrms = evalin('base','Resource.ImageBuffer(1).numFrames');
    end
end

% ***** Create the GUI components *****
% Define UIPos, which contains the default GUI positions - three columns of 10 controls. The x,y 
%    locations increment up columns, with each column being a separate page. The origin 
%    specified by UIPos is the lower left corner of a virtual box that encloses the control.
UIPos = zeros(10,2,3);
UIPos(:,1,1) = 0.0625;
UIPos(:,1,2) = 0.375;
UIPos(:,1,3) = 0.6875;
UIPos(:,2,1) = 0.0:0.1:0.9;
UIPos(:,2,2) = 0.0:0.1:0.9;
UIPos(:,2,3) = 0.0:0.1:0.9;
assignin('base','UIPos',UIPos);
% Define slider group offsets and sizes. All units are normalized.
SG = struct('TO',[0.0,0.0975],...   % title offset
            'TS',[0.25,0.025],...   % title size
            'TF',0.8,...            % title font size
            'SO',[0.0,0.06],...     % slider offset
            'SS',[0.25,0.031],...   % slider size
            'EO',[0.075,0.031],...   % edit box offset
            'ES',[0.11,0.031]);     % edit box size
assignin('base','SG',SG);
% Define pushbutton offsets and sizes.
PB = struct('FS',0.3,...            % font size
            'BO',[0.025,0.04],...   % button offset
            'BS',[0.2,0.07]);       % button size
assignin('base','PB',PB);
% Define buttonGroup offsets and sizes for a single button.
BG = struct('TFS',0.25,...          % button group title font size
            'BGO',[-0.025,0.025],... % button group offset (BGO(2) is 0.1 - BGS(2))
            'BGS',[0.3,0.075],...   % button group size
            'BI',0.030,...          % button increment (in units of full window)
            'BO',[0.1,0.2],...      % button offset (units relative to BGS box)
            'BS',[0.9,0.4],...      % button size      "    "    "    "     "
            'BFS',0.7);             % button font size
assignin('base','BG',BG);

%
% Titles
Pos = UIPos(10,:,1);
uicontrol('Style','text','String',...
          'Front End',...
          'Units','normalized',...
          'Position',[Pos+[0.0 0.06],0.25,0.03],...
          'FontUnits','normalized',...
          'FontSize',0.8,...
          'FontWeight','bold');
uicontrol('Style','text','String',...
          'Processing',...
          'Units','normalized',...
          'Position',[Pos+[0.3175 0.06],0.25,0.03],...
          'FontUnits','normalized',...
          'FontSize',0.8,...
          'FontWeight','bold');
uicontrol('Style','text','String',...
          'Display',...
          'Units','normalized',...
          'Position',[Pos+[0.625 0.06],0.25,0.03],...
          'FontUnits','normalized',...
          'FontSize',0.8,...
          'FontWeight','bold');

% Transducer connector
if evalin('base','exist(''numBoards'',''var'')') 
    nb = evalin('base','numBoards');
    if nb == 4
        tcx = 0.09;
        tcy = 0.935;
        tc = 1;
        if evalin('base','exist(''Resource'',''var'')')&&evalin('base','isfield(Resource.Parameters,''connector'')')
            tc = evalin('base','Resource.Parameters.connector');
        end
        TcString = ['Using Connector ',num2str(tc,'%1.0f')]; 
        tchndl = uicontrol('Style','text','String',TcString,...
            'Units','normalized',...
            'Position',[tcx tcy 0.2 0.02],...
            'FontUnits','normalized',...
            'FontSize',0.8,...
            'BackgroundColor',[1.0,0.8,0.4]);
    end
end

% TGC Controls
Pos = UIPos(9,:,1);
tgcSldrInc = 0.0325; %Increment between TGC sliders
% - set sliders to positions specified in TGC.CntlPts, if it exists.
if evalin('base','exist(''TGC'',''var'')')
    SP = double(evalin('base','TGC(1).CntrlPts'))/1023;
else
    SP = [0.5,0.5,0.5,0.5,0.5,0.5,0.5,0.5];
end
% SPAll = 1.0; % default overall gain
% tgctxt = uicontrol('Style','text','String','TGC',...
%             'Units','normalized',...
%             'Position',[Pos+SG.TO,SG.TS],...
%             'FontUnits','normalized',...
%             'FontSize',0.8,...
%             'FontWeight','bold');
% Pos = Pos+SG.SO;
% tgc1 = uicontrol(f,'Style','slider',...
%             'Max',1.0,'Min',0,'Value',SP(1),...
%             'SliderStep',[0.05 0.2],...
%             'Units','normalized',...
%             'Position',[Pos,SG.SS],...
%             'BackgroundColor',bkgrnd-0.05,...
%             'Callback',{@tgc1_Callback});
% tgc2 = uicontrol(f,'Style','slider',...
%             'Max',1.0,'Min',0,'Value',SP(2),...
%             'SliderStep',[0.05 0.2],...
%             'Units','normalized',...
%             'Position',[Pos+[0 -tgcSldrInc],SG.SS],...
%             'BackgroundColor',bkgrnd-0.05,...
%             'Callback',{@tgc2_Callback});
% tgc3 = uicontrol(f,'Style','slider',...
%             'Max',1.0,'Min',0,'Value',SP(3),...
%             'SliderStep',[0.05 0.2],...
%             'Units','normalized',...
%             'Position',[Pos+[0 -2*tgcSldrInc],SG.SS],...
%             'BackgroundColor',bkgrnd-0.05,...
%             'Callback',{@tgc3_Callback});
% tgc4 = uicontrol(f,'Style','slider',...
%             'Max',1.0,'Min',0,'Value',SP(4),...
%             'SliderStep',[0.05 0.2],...
%             'Units','normalized',...
%             'Position',[Pos+[0 -3*tgcSldrInc],SG.SS],...
%             'BackgroundColor',bkgrnd-0.05,...
%             'Callback',{@tgc4_Callback});
% tgc5 = uicontrol(f,'Style','slider',...
%             'Max',1.0,'Min',0,'Value',SP(5),...
%             'SliderStep',[0.05 0.2],...
%             'Units','normalized',...
%             'Position',[Pos+[0 -4*tgcSldrInc],SG.SS],...
%             'BackgroundColor',bkgrnd-0.05,...
%             'Callback',{@tgc5_Callback});
% tgc6 = uicontrol(f,'Style','slider',...
%             'Max',1.0,'Min',0,'Value',SP(6),...
%             'SliderStep',[0.05 0.2],...
%             'Units','normalized',...
%             'Position',[Pos+[0 -5*tgcSldrInc],SG.SS],...
%             'BackgroundColor',bkgrnd-0.05,...
%             'Callback',{@tgc6_Callback});
% tgc7 = uicontrol(f,'Style','slider',...
%             'Max',1.0,'Min',0,'Value',SP(7),...
%             'SliderStep',[0.05 0.2],...
%             'Units','normalized',...
%             'Position',[Pos+[0 -6*tgcSldrInc],SG.SS],...
%             'BackgroundColor',bkgrnd-0.05,...
%             'Callback',{@tgc7_Callback});
% tgc8 = uicontrol(f,'Style','slider',...
%             'Max',1.0,'Min',0,'Value',SP(8),...
%             'SliderStep',[0.05 0.2],...
%             'Units','normalized',...
%             'Position',[Pos+[0 -7*tgcSldrInc],SG.SS],...
%             'BackgroundColor',bkgrnd-0.05,...
%             'Callback',{@tgc8_Callback});
% Pos = UIPos(6,:,1);
% tgcAllTxt = uicontrol(f,'Style','text','String','TGC All Gain',...
%             'Units','normalized',...
%             'Position',[Pos+SG.TO,SG.TS],...
%             'FontUnits','normalized',...
%             'FontSize',0.8,...
%             'FontWeight','bold');            
% tgcAll = uicontrol(f,'Style','slider',...
%             'Max',1.0,'Min',-1.0,'Value',0,...
%             'SliderStep',[0.01,0.04],...
%             'Units','normalized',...
%             'Position',[Pos+SG.SO,SG.SS],...
%             'BackgroundColor',bkgrnd-0.05,...
%             'Callback',{@tgcAll_Callback});

% - PGain slider.
pgn = 0.0; % set default
if evalin('base','exist(''Process'',''var'')')
    Process = evalin('base','Process');
    % Find first Process structure with 'Image' classname and 'imageDisplay' method.
    i = 1;
    while i <= size(Process,2)
        if (strcmp(Process(i).classname,'Image'))&&(strcmp(Process(i).method,'imageDisplay'))
            break;
        end
        i = i+1;
    end
    if i <= size(Process,2)
        for j = 1:2:size(Process(i).Parameters,2)
            if strcmp(Process(i).Parameters{j},'pgain')
                pgn = Process(i).Parameters{j+1};
            end
        end
    end
end
assignin('base', 'pgain', pgn);
% Add PGain slider only if we have an imageDisplay method in a Process structure.
if pgn ~= 0.0
    Pos = UIPos(5,:,1);
    pgaintxt = uicontrol('Style','text',...
                'String','Digital Gain',...
                'Units','normalized',...
                'Position',[Pos+SG.TO,SG.TS],...
                'FontUnits','normalized',...
                'FontSize',0.8,...
                'FontWeight','bold');
    pgainSldr = uicontrol(f,'Style','slider',...
                'Max',20.1,'Min',0.1,'Value',pgn,...
                'SliderStep',[0.01 0.05],...
                'Units','normalized',...
                'Position',[Pos+SG.SO,SG.SS],...
                'BackgroundColor',bkgrnd-0.05,...
                'Callback',{@pgainSldr_Callback});
    pgainValue = uicontrol('Style','edit','String',num2str(pgn,'%1.3f'),...
                'Units','normalized',...
                'Position',[Pos+SG.EO,SG.ES],...
                'BackgroundColor',bkgrnd+0.1,...
                'Callback',{@pgainValue_Callback,pgainSldr});        
end

% High Voltage slider.
%   The slider's min and max range is determined by the limits of the Verasonics 
%   TPC and a max voltage limit that can be set in the user's setup script.  
%   A HAL call is made to get the hardware limits.  The call is safe to make
%   when hardware is not opened or present, such as when in Simulate Mode. When
%   no hardware is present, the TPC-L ("light") range (1 - 56.4 volts) is returned.
% 
%   The user script may override the "Max" attribute of this slider to impose
%   a high voltage maximum limit that is less than the capability of the detected
%   Verasonics TPC.
% 
%   We also get the rangeGranularity so we can avoid double precision errors.
%   For example:
%      set(hivoltsSldr,'Value',56.40001);
%   yields:  "Warning: slider control can not have a Value outside of Min/Max
%   range.  Control will not be rendered until all of its parameter values
%   are valid." Eventhough the value we get from the Verasonics HAL call may 
%   display as 56.4000, the digits after that may be non zero due to the nature
%   of using doubles.  So, we add one percent of rangeGranularity as a buffer
%   to the slider Max (and minus one percent from Min) to avoid such errors.
%
% - Check to see if the user has defined one or more TPC profiles.  If more than one,
%   these would be referenced in SeqControl objects with command 'setTPCProfile'.
%   For each profile, check for an associated TPC structure for defining
%   maxHighVoltage and set in Profile array.  If no TPC structure, use Trans.maxHighVoltage.
%
% - Check for user supplied highVoltage limit in Trans or TPC structures.
if (evalin('base','exist(''Trans'',''var'')'))&&(evalin('base','isfield(Trans, ''maxHighVoltage'')'))
    transHV = evalin('base','Trans.maxHighVoltage');
else
    transHV = 56.4;
end
% - Define an array of each profile's high voltage to indicate which profiles are referenced.  A
%   zero value means the profile is not referenced.
Profiles = zeros(1,5);  
Profiles(1) = transHV;  % Set profile 1 on as default.
if evalin('base','exist(''SeqControl'',''var'')')
    SC = evalin('base','SeqControl');
    for i = 1:size(SC,2)
        if strcmp(SC(i).command,'setTPCProfile')
            Profiles(SC(i).argument) = transHV; % use transHV as default maxHighVoltage
        end
    end
end
% - Determine TPC voltage range and set maxHighVoltage limit in active Profiles.
% -- 'getTpcHighVoltage' returns default values if hardware not present.  
[rangeMin, rangeMax, rangeGranularity] = getTpcHighVoltageRange();
% -- Check for maxHighVoltage set in a TPC structure.
if (evalin('base','exist(''TPC'',''var'')'))
    TPC = evalin('base','TPC');
    for i = 1:size(TPC,2)
        if (Profiles(i)~=0)
            if isfield(TPC(i),'maxHighVoltage')&&~isempty(TPC(i).maxHighVoltage)
                if TPC(i).maxHighVoltage <= Profiles(i), Profiles(i) = TPC(i).maxHighVoltage; end
            end
            if isfield(TPC(i),'highVoltageLimit')&&~isempty(TPC(i).highVoltageLimit)
                if TPC(i).highVoltageLimit <= Profiles(i), Profiles(i) = TPC(i).highVoltageLimit; end
            end
        end
    end
end
% -- If maxHighVoltage limit greater than rangeMax, set limit to rangeMax.
for i = 1:5
    if Profiles(i) ~= 0
        if (Profiles(i) > rangeMax), Profiles(i) = rangeMax; end
    end
end
% - For more than one profile, determine the number to use for the 2nd slider. Profile 5 has
%   priority over profiles 2-4.
hv2 = 0;     % hv2 will get the profile to use for 2nd slider.
if (size(find(Profiles),2) > 1)&&(Profiles(5)==0)
    for i = 2:4
        if Profiles(i) ~= 0, hv2 = i; break, end;
    end
elseif Profiles(5) ~= 0
    hv2 = 5;
end
if evalin('base','exist(''profile5ena'',''var'')')
    profile5ena = evalin('base','profile5ena');
else
    profile5ena = 0;
end

trackP5 = 0; % set default value for hv1 slider in case there is no P5

% - Render HV control number 1
Pos = UIPos(4,:,1);
hv1txt = uicontrol('Style','text','String','High Voltage P1',...
            'Units','normalized',...
            'Position',[Pos+SG.TO,SG.TS],...
            'FontUnits','normalized',...
            'FontSize',0.8,...
            'FontWeight','bold');
% Set slider 1's min value higher than rangeMin by rangeGranularity if profile 5 is also active.
if (hv2 == 5)&&(VDAS == 1)
    % Check for old TPC and profile 5 voltage limiting.
    enforceVLim = vdasTpcProfile5RequiresVoltageRelationshipEnforcementGet();
    if enforceVLim
        % increase the slider minimum and initial voltage setting, to
        % conform to the enforceVlim constraint
        sldrMin = rangeMin + rangeGranularity;
        [result, sldrMin] = setTpcProfileHighVoltage(sldrMin,1);
        if ~strcmpi(result, 'Success') && ~strcmpi(result, 'Hardware Not Open')
            % ERROR!  Failed to set high voltage.
            error('ERROR!  Failed to set Verasonics TPC high voltage for profile 1 because \"%s\".', result);
        end
    else
        sldrMin = rangeMin; % - rangeGranularity/100;
    end
else
    sldrMin = rangeMin; % - rangeGranularity/100; 
end
hv1Sldr = uicontrol(f,'Style','slider',...
           'Max',Profiles(1),...
           'Min',30,...
           'Value',45,...
           'SliderStep',[1 1],...
           'Units','normalized',...
           'Position',[Pos+SG.SO,SG.SS],...
           'BackgroundColor',bkgrnd-0.05,...
           'Interruptible','off',...
           'BusyAction','cancel',...
           'Tag','hv1Sldr',...
           'Callback',{@hv1Sldr_Callback,rangeGranularity});
hv1Value = uicontrol('Style','edit','String',num2str(45,'%.1f'),...
           'Units','normalized',...
           'Position',[Pos+SG.EO,SG.ES],...
           'Tag','hv1Value',...
           'Callback',{@hv1Value_Callback,hv1Sldr,rangeGranularity},...
           'BackgroundColor',bkgrnd+0.1);        
assignin('base','HV1maxHighVoltage',Profiles(1));

% - If more than one profile, create 2nd high voltage control
%hv2=5; % uncomment to force render.
Profiles(5)=50;
if hv2 ~= 0
    Pos = UIPos(3,:,1);
    string = ['High Voltage P' num2str(hv2)];
    hv2txt = uicontrol('Style','text',...
                'String',string,...
                'Units','normalized',...
                'Position',[Pos+SG.TO,SG.TS],...
                'FontUnits','normalized',...
                'FontSize',0.8,...
                'FontWeight','bold');
    hv2Sldr = uicontrol(f,'Style','slider',...
               'Max',Profiles(hv2),...
               'Min',rangeMin,...
               'Value',rangeMin,...
               'SliderStep',[0.05 0.2],...
               'Units','normalized',...
               'Position',[Pos+SG.SO,SG.SS],...
               'BackgroundColor',bkgrnd-0.05,...
               'Interruptible','off',...
               'BusyAction','cancel',...
               'Tag','hv2Sldr',...
               'Callback',{@hv2Sldr_Callback,rangeGranularity});
    assignin('base','HV2maxHighVoltage',Profiles(hv2)); % set profile maxHV in base for TXEventCheck
    if hv2 == 5
        % check for profile voltage tracking parameter
        if evalin('base','isfield(Resource,''HIFU'')')&&evalin('base','isfield(Resource.HIFU,''voltageTrackP5'')')...
                &&evalin('base','~isempty(Resource.HIFU.voltageTrackP5)')
            trackP5 = evalin('base','Resource.HIFU.voltageTrackP5');
        else
            trackP5 = 0; % default to off if not specified by user
        end
        hv2Value = uicontrol('Style','edit','String',num2str(rangeMin,'%.1f'),...
               'Units','normalized',...
               'Position',[Pos+[0.045 0.03],SG.ES],...
               'Tag','hv2Value',...
               'Callback',{@hv2Value_Callback,hv2Sldr,rangeGranularity},...
               'BackgroundColor',bkgrnd+0.1);        
        hv2Actual = uicontrol('Style','edit','String',num2str(rangeMin,'%.1f'),...
               'Units','normalized',...
               'Position',[Pos+[0.125 0.03],SG.ES],...
               'Enable','inactive',...
               'Tag','hv2Actual',...
               'BackgroundColor',bkgrnd+0.05);
        if (VDAS==1)&&(simMode==0)
            % Create a timer object that will update the actual push capacitor voltage string.
            hvtmr = timer('TimerFcn',@hvTimerCallback, 'Period', 1.5,'ExecutionMode','fixedSpacing');
            start(hvtmr);
        end
    else
        hv2Value = uicontrol('Style','edit','String',num2str(rangeMin,'%.1f'),...
               'Units','normalized',...
               'Position',[Pos+SG.EO,SG.ES],...
               'Tag','hv2Value',...
               'Callback',{@hv2Value_Callback,hv2Sldr,rangeGranularity},...
               'BackgroundColor',bkgrnd+0.1);
    end
end
% JE Eliminar simulacion y RcvData lOop
% - RcvData loop control
% rcvdataloop = uicontrol('Style','togglebutton',...
%             'String','Rcv Data Loop',...
%             'Units','normalized',...
%             'Position',[UIPos(1,:,1)+[0.0175 0.077],0.20,0.05],...
%             'FontUnits','normalized',...
%             'FontSize',0.4,...
%             'BackgroundColor',bkgrnd+0.05,...
%             'Callback',{@rcvdataloop_Callback});
% 
% % - Simulate control
% simulate = uicontrol('Style','togglebutton',...
%             'String','Simulate',...
%             'Units','normalized',...
%             'Position',[UIPos(1,:,1)+[0.0175 0.027],0.20,0.05],...
%             'FontUnits','normalized',...
%             'FontSize',0.4,...
%             'BackgroundColor',bkgrnd+0.05,...
%             'Callback',{@simulate_Callback});

% - Add Speed Correction slider only if a Recon is defined.
if evalin('base','exist(''Recon'',''var'')')
%     Pos = UIPos(9,:,2);
%     speedtxt = uicontrol('Style','text','String','Speed Of Sound',...
%                 'Units','normalized',...
%                 'Position',[Pos+SG.TO,SG.TS],...
%                 'FontUnits','normalized',...
%                 'FontSize',0.8,...
%                 'FontWeight','bold');
%     speedSldr = uicontrol(f,'Style','slider',...
%                 'Max',1.4,'Min',0.6,'Value',1.0,...
%                 'SliderStep',[0.00125 0.0125],...
%                 'Units','normalized',...
%                 'Position',[Pos+SG.SO,SG.SS],...
%                 'BackgroundColor',bkgrnd-0.05,...
%                 'Interruptible','off',...
%                 'BusyAction','cancel',...
%                 'Callback',{@speedSldr_Callback});
%     speedValue = uicontrol('Style','edit','String',num2str(1.0,'%1.3f'),...
%                 'Units','normalized',...
%                 'Position',[Pos+SG.EO,SG.ES],...
%                 'Callback',{@speedValue_Callback,speedSldr},...
%                 'BackgroundColor',bkgrnd+0.1);        
end

% - Freeze control
freeze = uicontrol('Style','togglebutton',...
            'String','Freeze',...
            'Units','normalized',...
            'Position',[UIPos(1,:,2)+PB.BO,PB.BS],...
            'FontUnits','normalized',...
            'FontSize',PB.FS,...
            'BackgroundColor',bkgrnd+0.05,...
            'Callback',{@freeze_Callback});

% Add the following GUI controls only if we have an ImageBuffer specification.
if nfrms*0 ~= 0  %JE eliminar controles de display
    % - Zoom controls
    Pos = UIPos(9,:,3);
    zoomtxt = uicontrol('Style','text','String','Zoom',...
                'Units','normalized',...
                'Position',[Pos+SG.TO,SG.TS],...
                'FontUnits','normalized',...
                'FontSize',0.8,...
                'FontWeight','bold');
    zoomin = uicontrol('Style','pushbutton',...
                'String','In',...
                'Units','normalized',...
                'Position',[Pos+[0.045 0.055],0.075,0.0375],...
                'FontUnits','normalized',...
                'FontSize',0.5,...
                'BackgroundColor',bkgrnd+0.05,...
                'Interruptible','off',...
                'BusyAction','cancel',...
                'Callback',{@zoomin_Callback});
    zoomout = uicontrol('Style','pushbutton',...
                'String','Out',...
                'Units','normalized',...
                'Position',[Pos+[0.135 0.055],0.075,0.0375],...
                'FontUnits','normalized',...
                'FontSize',0.5,...
                'BackgroundColor',bkgrnd+0.05,...
                'Interruptible','off',...
                'BusyAction','cancel',...
                'Callback',{@zoomout_Callback});
    % - Pan controls
    Pos = UIPos(8,:,3)+[0.0,0.01];
    pantxt = uicontrol('Style','text','String','Pan',...
                'Units','normalized',...
                'Position',[Pos+SG.TO,SG.TS],...
                'FontUnits','normalized',...
                'FontSize',0.8,...
                'FontWeight','bold');
    panlft = uicontrol('Style','pushbutton','String','Lft',...
                'Units','normalized',...
                'Position',[Pos+[0.0425 0.045],0.05,0.035],...
                'FontUnits','normalized',...
                'FontSize',0.5,...
                'BackgroundColor',bkgrnd+0.05,...
                'Interruptible','off',...
                'BusyAction','cancel',...
                'Callback',{@panlft_Callback});
    panrt = uicontrol('Style','pushbutton','String','Rt',...
                'Units','normalized',...
                'Position',[Pos+[0.1625 0.045],0.05,0.035],...
                'FontUnits','normalized',...
                'FontSize',0.5,...
                'BackgroundColor',bkgrnd+0.05,...
                'Interruptible','off',...
                'BusyAction','cancel',...
                'Callback',{@panrt_Callback});
    panup = uicontrol('Style','pushbutton','String','Up',...
                'Units','normalized',...
                'Position',[Pos+[0.1025 0.062],0.05,0.032],...
                'FontUnits','normalized',...
                'FontSize',0.5,...
                'BackgroundColor',bkgrnd+0.05,...
                'Interruptible','off',...
                'BusyAction','cancel',...
                'Callback',{@panup_Callback});
    pandn = uicontrol('Style','pushbutton','String','Dn',...
                'Units','normalized',...
                'Position',[Pos+[0.1025 0.03],0.05,0.032],...
                'FontUnits','normalized',...
                'FontSize',0.5,...
                'BackgroundColor',bkgrnd+0.05,...
                'Interruptible','off',...
                'BusyAction','cancel',...
                'Callback',{@pandn_Callback});


    % - Get values of compression, reject, and persistence defined by user script for displayWindow 1.
    % If no displayWindow 1 found in Process definition, use defaults for slider initial values.
    cmp = 0.5; % set default
    rjt = 0;   %default value of reject
    Prs = zeros(1,4);   %default values of persistence
    if evalin('base','exist(''Process'',''var'')')
        Process = evalin('base','Process');
        % Find values used in the 'Image/imageDisplay' Process structure for displayWindow 1
        for i = 1:size(Process,2)
            if (strcmp(Process(i).classname,'Image'))&&(strcmp(Process(i).method,'imageDisplay'))
                for j = 1:2:size(Process(i).Parameters,2)
                    if (strcmp(Process(i).Parameters{j},'displayWindow'))&&(Process(i).Parameters{j+1}==1)
                        for k = 1:2:size(Process(i).Parameters,2)
                            if strcmp(Process(i).Parameters{k},'compression')
                                cmp = Process(i).Parameters{k+1};
                            end
                            if strcmp(Process(i).Parameters{k},'reject')
                                rjt = Process(i).Parameters{k+1};
                            end
                            if strcmp(Process(i).Parameters{k},'persistLevel')||strcmp(Process(i).Parameters{k},'PersistLevel')
                                for n = 1:size(Process(i).Parameters{k+1},2)
                                    Prs(n) = Process(i).Parameters{k+1}(n);
                                end
                            end
                        end
                        break
                    end
                end
            end
        end
    end

    % - Compression slider
    Pos = UIPos(7,:,3);
    compresstxt = uicontrol('Style','text','String','Compression',...
                'Units','normalized',...
                'Position',[Pos+SG.TO,SG.TS],...
                'FontUnits','normalized',...
                'FontSize',0.8,...
                'FontWeight','bold');
    compress = uicontrol(f,'Style','slider',...
                'Max',1.0,'Min',0.0,'Value',cmp,...
                'SliderStep',[0.05, 0.1],...
                'Units','normalized',...
                'Position',[Pos+SG.SO,SG.SS],...
                'BackgroundColor',bkgrnd-0.05,...
                'Interruptible','off',...
                'BusyAction','cancel',...
                'Callback',{@compress_Callback});
    compValue = uicontrol('Style','edit','String',num2str(cmp,'%1.3f'),...
                'Units','normalized',...
                'Position',[Pos+SG.EO,SG.ES],...
                'BackgroundColor',bkgrnd+0.1,...        
                'Callback',{@compValue_Callback});
 
    % - Reject slider
    Pos = UIPos(6,:,3);
    rejecttxt = uicontrol('Style','text','String','Reject',...
                'Units','normalized',...
                'Position',[Pos+SG.TO,SG.TS],...
                'FontUnits','normalized',...
                'FontSize',0.8,...
                'FontWeight','bold');
    reject = uicontrol(f,'Style','slider',...
                'Max',100,'Min',0,'Value',rjt,...
                'SliderStep',[0.01, 0.05],...
                'Units','normalized',...
                'Position',[Pos+SG.SO,SG.SS],...
                'BackgroundColor',bkgrnd-0.05,...
                'Interruptible','off',...
                'BusyAction','cancel',...
                'Callback',{@reject_Callback});
    rejectValue = uicontrol('Style','edit','String',num2str(rjt,'%3.0f'),...
                'Units','normalized',...
                'Position',[Pos+SG.EO,SG.ES],...
                'BackgroundColor',bkgrnd+0.1,...        
                'Callback',{@rejectValue_Callback});

    % - Persistence slider
    Pos = UIPos(5,:,3);
    persisttxt = uicontrol('Style','text','String','Persistence',...
                'Units','normalized',...
                'Position',[Pos+SG.TO,SG.TS],...
                'FontUnits','normalized',...
                'FontSize',0.8,...
                'FontWeight','bold');
    persist = uicontrol(f,'Style','slider',...
                'Max',100,'Min',0,'Value',Prs(1),...
                'SliderStep',[0.05, 0.1],...
                'Units','normalized',...
                'Position',[Pos+SG.SO,SG.SS],...
                'BackgroundColor',bkgrnd-0.05,...
                'Tag','persistSlider',...
                'Interruptible','off',...
                'BusyAction','cancel',...
                'Callback',{@persist_Callback});
    persValue = uicontrol('Style','edit','String',num2str(Prs(1),'%3.0f'),...
                'Units','normalized',...
                'Position',[Pos+SG.EO,SG.ES],...
                'BackgroundColor',bkgrnd+0.1,...        
                'Callback',{@persistValue_Callback});
end

% - Add Cineloop control if ImageBuffer contains more than one frame.
if nfrms*0 > 1
    Pos = UIPos(1,:,3);
    cinex = 0.6875;
    ciney = 0.06;
    cineNum = nfrms;
    cinetxt = uicontrol('Style','text','String','CineLoop',...
                'Units','normalized',...
                'Position',[Pos+SG.TO,SG.TS],...
                'FontUnits','normalized',...
                'FontSize',0.8,...
                'FontWeight','bold');
    cine = uicontrol('Style','slider',...
                'Max',nfrms,'Min',1,'Value',nfrms,...
                'SliderStep',[1/(nfrms-1) 1/(nfrms-1)],...
                'Units','normalized',...
                'Position',[Pos+SG.SO,SG.SS],...
                'BackgroundColor',bkgrnd-0.05,...
                'Interruptible','off',...
                'BusyAction','cancel',...
                'Tag','CLSlider',...
                'Callback',{@cine_Callback});
    % -- Cineloop number
    cineValue = uicontrol('Style','edit','String',num2str(cineNum,'%2.0f'),...
                'Units','normalized',...
                'Position',[Pos+[0.02 0.03],0.08,0.03],...
                'Tag','CLValue',...
                'Callback',{@cineValue_Callback,cine},...
                'BackgroundColor',bkgrnd+0.1);        
    % -- Cineloop save button
    simulate = uicontrol('Style','togglebutton',...
                'String','Save',...
                'Units','normalized',...
                'Position',[Pos+[0.12 0.023],0.10,0.04],...
                'FontUnits','normalized',...
                'FontSize',0.5,...
                'Callback',{@cineSave_Callback});
end

% Make the GUI visible, unless the call has requested that it be hidden.
visibility = 'on';
if((true == evalin('base', 'exist(''Mcr_GuiHide'', ''var'')')) && (1 == evalin('base', 'Mcr_GuiHide')))
    % Caller has requested that we do NOT show the GUI window.
    visibility = 'off';
end
set(f,'Visible', visibility);
axes('Position',[0.4,0.4,0.5,0.5]);
imagen=imread('lung zones.png');
image(imagen)
axis off

function closefunc(source,eventdata)
    assignin('base', 'exit', 1);
    if exist('hvtmr','var'), stop(hvtmr); delete(hvtmr); end
    delete(f);
end

% TGC Callback functions
%   The array SP keeps track of the slider positions before applying the SPAll gain factor.
%   This allows returning saturated TGC sliders to original gain curve values if the SPAll
%   gain is lowered.
function tgc1_Callback(source,eventdata)
    SP(1) = get(tgc1,'Value')/SPAll;
    assignin('base', 'tgc1',min(1023,1023*SP(1)*SPAll));
    assignin('base', 'action', 'tgc');
end
function tgc2_Callback(source,eventdata)
    SP(2) = get(tgc2,'Value')/SPAll;
    assignin('base', 'tgc2',min(1023,1023*SP(2)*SPAll));
    assignin('base', 'action', 'tgc');
end
function tgc3_Callback(source,eventdata)
    SP(3) = get(tgc3,'Value')/SPAll;
    assignin('base', 'tgc3',min(1023,1023*SP(3)*SPAll));
    assignin('base', 'action', 'tgc');
end
function tgc4_Callback(source,eventdata)
    SP(4) = get(tgc4,'Value')/SPAll;
    assignin('base', 'tgc4',min(1023,1023*SP(4)*SPAll));
    assignin('base', 'action', 'tgc');
end
function tgc5_Callback(source,eventdata)
    SP(5) = get(tgc5,'Value')/SPAll;
    assignin('base', 'tgc5',min(1023,1023*SP(5)*SPAll));
    assignin('base', 'action', 'tgc');
end
function tgc6_Callback(source,eventdata)
    SP(6) = get(tgc6,'Value')/SPAll;
    assignin('base', 'tgc6',min(1023,1023*SP(6)*SPAll));
    assignin('base', 'action', 'tgc');
end
function tgc7_Callback(source,eventdata)
    SP(7) = get(tgc7,'Value')/SPAll;
    assignin('base', 'tgc7',min(1023,1023*SP(7)*SPAll));
    assignin('base', 'action', 'tgc');
end
function tgc8_Callback(source,eventdata)
    SP(8) = get(tgc8,'Value')/SPAll;
    assignin('base', 'tgc8',min(1023,1023*SP(8)*SPAll));
    assignin('base', 'action', 'tgc');
end
function tgcAll_Callback(source,eventdata)
    SPAll = get(tgcAll,'Value');
    SPAll = 1.05*SPAll*SPAll + 1.95*SPAll + 1;  % Convert to gain factor between 0.25 and 4.0
    set(tgc1,'Value',min(1.0,SP(1)*SPAll));
    set(tgc2,'Value',min(1.0,SP(2)*SPAll));
    set(tgc3,'Value',min(1.0,SP(3)*SPAll));
    set(tgc4,'Value',min(1.0,SP(4)*SPAll));
    set(tgc5,'Value',min(1.0,SP(5)*SPAll));
    set(tgc6,'Value',min(1.0,SP(6)*SPAll));
    set(tgc7,'Value',min(1.0,SP(7)*SPAll));
    set(tgc8,'Value',min(1.0,SP(8)*SPAll));
    assignin('base', 'tgc1',min(1023,1023*SP(1)*SPAll));
    assignin('base', 'tgc2',min(1023,1023*SP(2)*SPAll));
    assignin('base', 'tgc3',min(1023,1023*SP(3)*SPAll));
    assignin('base', 'tgc4',min(1023,1023*SP(4)*SPAll));
    assignin('base', 'tgc5',min(1023,1023*SP(5)*SPAll));
    assignin('base', 'tgc6',min(1023,1023*SP(6)*SPAll));
    assignin('base', 'tgc7',min(1023,1023*SP(7)*SPAll));
    assignin('base', 'tgc8',min(1023,1023*SP(8)*SPAll));
    assignin('base', 'action', 'tgc');
end

% RcvData Loop Callback
function rcvdataloop_Callback(source,eventdata)
    assignin('base','rloopButton',get(source,'Value'));
    assignin('base', 'action', 'rcvloop');
end

% Simulate Callback
function simulate_Callback(source,eventdata)
    assignin('base','simButton',get(source,'Value'));
    assignin('base', 'action', 'simulate');
end

% Freeze Callback
function freeze_Callback(source,eventdata)
    frzstate = get(source,'Value');
    assignin('base','freeze',frzstate);
    if (frzstate == 0)&&(exist('cine','var'))
        set(cine, 'Value', nfrms);
        set(cineValue,'String',num2str(nfrms,'%2.0f'));
    end
end

% Zoom Callbacks
function zoomin_Callback(source,eventdata)
    assignin('base', 'action', 'zoomin');
end

function zoomout_Callback(source,eventdata)
    assignin('base', 'action', 'zoomout');
end

% Pan Callbacks
function panlft_Callback(source,eventdata)
    assignin('base', 'action', 'panlft');
end

function panrt_Callback(source,eventdata)
    assignin('base', 'action', 'panrt');
end

function panup_Callback(source,eventdata)
    assignin('base', 'action', 'panup');
end

function pandn_Callback(source,eventdata)
    assignin('base', 'action', 'pandn');
end

% Compress Callback
function compress_Callback(src,eventdata)
    cv = get(src,'Value');
    handle = evalin('base', 'Resource.DisplayWindow(1).figureHandle');
    rej = evalin('base', 'Image(1).reject');
    sp = evalin('base', 'Resource.DisplayWindow(1).splitPalette');
    changeMap(handle,2*(cv-0.5),rej,sp);
    set(compValue,'String',num2str(cv,'%1.3f'));
    evalin('base',['Image(1).compression = ',num2str(cv),';']);
end

% CompValue Callback
function compValue_Callback(src,eventdata)
    cv = str2num(get(src,'String'));
    if (0<cv) && (cv<=1.0)
        handle = evalin('base', 'Resource.DisplayWindow(1).figureHandle');
        rej = evalin('base', 'Image(1).reject');
        sp = evalin('base', 'Resource.DisplayWindow(1).splitPalette');
        changeMap(handle,2*(cv-0.5),rej,sp);
        set(compress, 'Value', cv);
        evalin('base',['Image(1).compression = ',num2str(cv),';']);
    else
        cv = get(compress,'Value');
        set(src,'String',num2str(cv,'%1.3f'));
    end
end

% Reject Callback
function reject_Callback(reject,eventdata)
    rej = get(reject,'Value');
    handle = evalin('base', 'Resource.DisplayWindow(1).figureHandle');
    cv = evalin('base', 'Image(1).compression');
    sp = evalin('base', 'Resource.DisplayWindow(1).splitPalette');
    changeMap(handle,2*(cv-0.5),rej,sp);
    set(rejectValue,'String',num2str(rej,'%3.0f'));
    evalin('base',['Image(1).reject = ',num2str(rej),';']);
end

% RejectValue Callback
function rejectValue_Callback(src,eventdata)
    rej = str2num(get(src,'String'));
    if (0<=rej) && (rej<=100)
        handle = evalin('base', 'Resource.DisplayWindow(1).figureHandle');
        cv = evalin('base', 'Image(1).compression');
        sp = evalin('base', 'Resource.DisplayWindow(1).splitPalette');
        changeMap(handle,2*(cv-0.5),rej,sp);
        set(reject, 'Value', rej);
        evalin('base',['Image(1).reject = ',num2str(rej),';']);
    else
        rej = get(reject,'Value');
        set(src,'String',num2str(rej,'%3.0f'));
    end
end

% Persist Callback
function persist_Callback(src,eventdata)
    prst = get(src,'Value');
    assignin('base','persist',prst);
    assignin('base','action','persist');
    set(persValue,'String',num2str(prst,'%3.0f')); 
end

% PersistValue Callback
function persistValue_Callback(src,eventdata)
    prst = str2num(get(src,'String'));
    if (0<=prst) && (prst<=100)
        assignin('base', 'persist', prst);
        assignin('base', 'action', 'persist');
        set(persist,'Value', prst);
    else
        prst = get(persist,'Value');
        set(src,'String',num2str(prst,'%3.0f'));
    end
end         

% PGain Slider Callback
function pgainSldr_Callback(src,eventdata)
    pv = get(src,'Value');
    assignin('base', 'pgain', pv);
    assignin('base', 'action', 'pgain');
    set(pgainValue,'String',num2str(pv,'%1.3f'));    
end

% pgainValue Callback
function pgainValue_Callback(src,eventdata,pgainSldr)
    pv = str2double(get(src,'String'));
    if (0.1<=pv) && (pv<=10.0)
        assignin('base', 'pgain', pv);
        assignin('base', 'action', 'pgain');
        set(pgainSldr,'Value', pv);
    else
        pv = get(pgainSldr,'Value');
        set(pgainValue,'String',num2str(pv,'%1.3f'));
    end
end

% High Voltage 1 Slider Callback
function hv1Sldr_Callback(source,eventdata,rangeGranularity)
    if (VDAS~=1)||(simMode~=0) || trackP5==1 % disable updates in these cases
        hv = str2double(get(hv1Value,'String'));
        set(hv1Sldr,'Value',hv);
        return; 
    end
    hv = get(hv1Sldr,'Value');
    evalin('base','tStartHvSldr = tic;'); % set time the slider was moved for error suppression.
    % If profile 5 active and we have a pre-rev D TPC, don't allow setting hv below profile 5 value.
    if (hv2==5)&&(enforceVLim==1)
        if (hv < get(findobj('Tag','hv2Sldr'),'Value') + rangeGranularity)
            hv = get(findobj('Tag','hv2Sldr'),'Value') + rangeGranularity;
        end
    end
    % Attempt to set high voltage.  On error, setTpcProfileHighVoltage() returns voltage range minimum.
    [result, hvset] = setTpcProfileHighVoltage(45,1);
    if ~strcmpi(result, 'Success') && ~strcmpi(result, 'Hardware Not Open')
        % ERROR!  Failed to set high voltage.
        error('ERROR!  Failed to set Verasonics TPC high voltage for profile 1 because \"%s\".', result);
    end
    set(hv1Value,'String',num2str(hvset,'%.1f'));
    % Since requested value is not necessarily the value that was obtained,
    % we set the slider to the resulting value.
    set(hv1Sldr,'Value',hv);
end

% High Voltage 1 Value Callback
function hv1Value_Callback(source,eventdata,hv1Sldr,rangeGranularity)
    if (VDAS~=1)||(simMode~=0) || trackP5==1 % disable updates in these cases
        hv = get(hv1Sldr,'Value');
        set(hv1Value,'String',num2str(hv,'%.1f'));
        return; 
    end
    hv = str2double(get(hv1Value,'String'));
    % Protect against bad user input.  e.g."1.6.6"
    if(isnan(hv))
        hv = get(hv1Sldr,'Value');
    end
    evalin('base','tStartHvSldr = tic;'); % set time the slider was moved for error suppression.
    % If profile 5 active and we have a pre-rev D TPC, don't allow setting hv below profile 5 value.
    if (hv2==5)&&(enforceVLim==1)
        if (hv < get(findobj('Tag','hv2Sldr'),'Value') + rangeGranularity)
            hv = get(findobj('Tag','hv2Sldr'),'Value') + rangeGranularity;
        end
    end
    % Don't allow setting hv outside slider's Min/Max range.  This range
    % should be a subset of the range that setTpcProfileHighVoltage() uses.
    sliderMin = get(hv1Sldr, 'Min');
    sliderMax = get(hv1Sldr, 'Max');
    if hv < sliderMin, hv = sliderMin; end
    if hv > sliderMax, hv = sliderMax; end
    % Attempt to set high voltage.  On error, setTpcProfileHighVoltage() returns voltage range minimum.
    [result, hvset] = setTpcProfileHighVoltage(45,1);
    if ~strcmpi(result, 'Success') && ~strcmpi(result, 'Hardware Not Open')
        % ERROR!  Failed to set high voltage.
        error('ERROR!  Failed to set Verasonics TPC high voltage for profile 1 because \"%s\".', result);
    end
    % Since hv value is supplied by HAL or hv2Sldr, num2str() below will always
    % succeed in conversion.
    set(hv1Value,'String',num2str(hvset,'%.1f'));
    set(hv1Sldr,'Value',hv);
end

% High Voltage 2 Slider Callback
function hv2Sldr_Callback(source,eventdata,rangeGranularity)
    if (VDAS~=1)||(simMode~=0), 
        hv = str2double(get(hv2Value,'String'));
        set(hv2Sldr,'Value',hv);
        return; 
    end
    hv = get(hv2Sldr,'Value');
    evalin('base','tStartHvSldr = tic;'); % set time the slider was moved for error suppression.
    if (hv2==5)&&(trackP5>0) % If profile 5 active check for tracking feature enabled
        hv = max(get(hv1Sldr, 'Min'), hv); % don't go below slider minimum
        [result, ~] = setTpcProfileHighVoltage(hv,trackP5);
        if ~strcmpi(result, 'Success') && ~strcmpi(result, 'Hardware Not Open')
            % ERROR!  Failed to set high voltage.
            error('ERROR!  Failed to set Verasonics TPC high voltage for profile %d because \"%s\".', int8(trackP5), result);
        end
        if trackP5 == 1 % only update hv1 gui if trackP5 points to it and not some other profile
            set(hv1Value,'String',num2str(hv,'%.1f'));
            set(hv1Sldr,'Value',hv);
        end
    elseif (hv2==5)&&(enforceVLim==1) % If profile 5 active and we have a pre-rev D TPC, don't allow setting hv below profile 5 value.
        if (hv > get(findobj('Tag','hv1Sldr'),'Value') - rangeGranularity)
            hv = get(findobj('Tag','hv1Sldr'),'Value') - rangeGranularity;
        end
    end
    % Attempt to set high voltage.  On error, setTpcProfileHighVoltage() returns voltage range minimum.
    if profile5ena ~= 2  % don't call func to set HV if using extenal supply
        [result, hvset] = setTpcProfileHighVoltage(hv,hv2);
        if ~strcmpi(result, 'Success') && ~strcmpi(result, 'Hardware Not Open')
            % ERROR!  Failed to set high voltage.
            error('ERROR!  Failed to set Verasonics TPC high voltage for profile %d because \"%s\".', int8(hv2), result);
        end
    else  % for external HIFU power supply, call the extPwrCtrl function to set voltage
        % call external power supply control function
        [PSerrorflag, ~] = extPwrCtrl('SETV', hv);
        if PSerrorflag
            error('VSX: Communication error with external power supply.');
        end
        hvset = hv;
    end
    % Since requested value is not necessarily the value that was set,
    % we set the slider to the resulting value.
    set(findobj('Tag','hv2Value'),'String',num2str(hvset,'%.1f'));
    set(hv2Sldr,'Value',hv);
end

% High Voltage 2 Value Callback
function hv2Value_Callback(source,eventdata,hv2Sldr,rangeGranularity)
    if (VDAS~=1)||(simMode~=0)
        hv = get(hv2Sldr,'Value');
        set(hv2Value,'String',num2str(hv,'%.1f'));
        return; 
    end
    hv = str2double(get(hv2Value,'String'));
    % Protect against bad user input.  e.g."1.6.6"
    if(isnan(hv))
        hv = get(hv2Sldr,'Value');
    end
    % Don't allow setting hv outside slider's Min/Max range.  This range
    % should be a subset of the range that setTpcProfileHighVoltage() uses.
    sliderMin = get(hv2Sldr, 'Min');
    sliderMax = get(hv2Sldr, 'Max');
    if hv < sliderMin, hv = sliderMin; end
    if hv > sliderMax, hv = sliderMax; end
    evalin('base','tStartHvSldr = tic;'); % set time the slider was moved for error suppression.
    if (hv2==5)&&(trackP5>0) % If profile 5 active check for tracking feature enabled
        hv = max(get(hv1Sldr, 'Min'), hv); % don't go below slider minimum
        [result, ~] = setTpcProfileHighVoltage(hv,trackP5);
        if ~strcmpi(result, 'Success') && ~strcmpi(result, 'Hardware Not Open')
            % ERROR!  Failed to set high voltage.
            error('ERROR!  Failed to set Verasonics TPC high voltage for profile %d because \"%s\".', int8(trackP5), result);
        end
        if trackP5 == 1
            set(hv1Value,'String',num2str(hv,'%.1f'));
            set(hv1Sldr,'Value',hv);
        end
    elseif (hv2==5)&&(enforceVLim==1) % If profile 5 active and we have a pre-rev D TPC, don't allow setting hv below profile 5 value.
        if (hv > get(findobj('Tag','hv1Sldr'),'Value') - rangeGranularity)
            hv = get(findobj('Tag','hv1Sldr'),'Value') - rangeGranularity;
        end
    end
    if profile5ena ~= 2
        % Attempt to set high voltage.  On error, setTpcProfileHighVoltage() returns voltage range minimum.
        [result, hvset] = setTpcProfileHighVoltage(hv,hv2);
        if ~strcmpi(result, 'Success') && ~strcmpi(result, 'Hardware Not Open')
            % ERROR!  Failed to set high voltage.
            error('ERROR!  Failed to set Verasonics TPC high voltage for profile %d because \"%s\".', int8(hv2), result);
        end
    else  % for external HIFU power supply, call the extPwrCtrl function to set voltage
        % call external power supply control function
        [PSerrorflag, ~] = extPwrCtrl('SETV', hv);
        if PSerrorflag
            error('VSX: Communication error with external power supply.');
        end
        hvset = hv;
    end
    % Since hv value is supplied by HAL or hv1Sldr, num2str() below will always
    % succeed in conversion.
    set(hv2Value,'String',num2str(hvset,'%.1f'));
    set(hv2Sldr,'Value',hv);
end

% Speed Slider Callback
function speedSldr_Callback(source,eventdata)
    sv = get(speedSldr,'Value');
    assignin('base', 'speedCorrect', sv);
    assignin('base', 'action', 'speed');
    set(speedValue,'String',num2str(sv,'%1.3f'));    
end

% Speed Value Callback
function speedValue_Callback(speedValue,eventdata,speedSldr)
    sv = str2num(get(speedValue,'String'));
    if (0.6<=sv) && (sv<=1.4)
        assignin('base', 'speedCorrect', sv);
        assignin('base', 'action', 'speed');
        set(speedSldr,'Value', sv);
    else
        sv = get(speedSldr,'Value');
        set(speedValue,'String',num2str(sv,'%1.3f'));
    end
end

% Cineloop Callback
function cine_Callback(src,eventdata)
    if get(freeze,'Value')==0    % no action if not in freeze
        set(src,'Value',nfrms);  % reset slider to end
        return
    end  
    cv = round(get(src,'Value'));
    rfrm = nfrms - cv;  % rfrm is the number of frames back from nfrms 
    ImageBuf = evalin('base', 'Resource.ImageBuffer(1)');
    handle = evalin('base', 'Resource.DisplayWindow.imageHandle');
    cfrm = ImageBuf.lastFrame - rfrm;
    if ImageBuf.firstFrame > ImageBuf.lastFrame  % has buffer wrapped?
        if cfrm < 1, cfrm = ImageBuf.numFrames + cfrm; end
    else
        if cfrm < 1, cfrm = 1; end
    end
    % Find all the Process structures with 'imageDisplay' method.
    m = 0; % m keeps track of how many imageDisplay methods are found.
    if evalin('base','exist(''Process'',''var'')')
        Process = evalin('base','Process');
        for kc = 1:size(Process,2)
            if (strcmp(Process(kc).classname,'Image'))&&(strcmp(Process(kc).method,'imageDisplay'))
                m = m + 1;
                Control(m).Command = 'imageDisplay';
                Control(m).Parameters = cell(size(Process(kc).Parameters));
                for jc = 1:2:size(Process(kc).Parameters,2)
                    Control(m).Parameters{jc} = Process(kc).Parameters{jc};
                    Control(m).Parameters{jc+1} = Process(kc).Parameters{jc+1};
                    if strcmp(Process(kc).Parameters{jc},'framenum')
                        Control(m).Parameters{jc+1} = round(cfrm);
                    end
                end
            end
        end
    end
    if (m == 0), return, end; % exit if no imageDisplay methods found.
    runAcq(Control);
    set(findobj('Tag','CLValue'),'String',num2str(cv,'%2.0f'));
end

% cineValue Callback
function cineValue_Callback(cineValue,eventdata,cine)
    if get(freeze,'Value')==0  % no action if not in freeze        
        return 
    end
    cv = str2double(get(cineValue,'String'));
    if (1<=cv) && (cv<=nfrms)
        set(cine, 'Value', cv);
        rfrm = nfrms - cv;  % rfrm is the number of frames back from nfrms 
        ImageBuf = evalin('base', 'Resource.ImageBuffer(1)');
        handle = evalin('base', 'Resource.DisplayWindow(1).imageHandle');
        cfrm = ImageBuf.lastFrame - rfrm;
        if ImageBuf.firstFrame > ImageBuf.lastFrame  % has buffer wrapped?
            if cfrm < 1, cfrm = ImageBuf.numFrames + cfrm; end
        else
            if cfrm < 1, cfrm = 1; end
        end
        % Find all the Process structures with 'imageDisplay' method.
        m = 0; % m keeps track of how many imageDisplay methods are found.
        if evalin('base','exist(''Process'',''var'')')
            Process = evalin('base','Process');
            for kc = 1:size(Process,2)
                if (strcmp(Process(kc).classname,'Image'))&&(strcmp(Process(kc).method,'imageDisplay'))
                    m = m + 1;
                    Control(m).Command = 'imageDisplay';
                    Control(m).Parameters = cell(size(Process(kc).Parameters));
                    for jc = 1:2:size(Process(kc).Parameters,2)
                        Control(m).Parameters{jc} = Process(kc).Parameters{jc};
                        Control(m).Parameters{jc+1} = Process(kc).Parameters{jc+1};
                        if strcmp(Process(kc).Parameters{jc},'framenum')
                            Control(m).Parameters{jc+1} = round(cfrm);
                        end
                    end
                end
            end
        end
        if (m == 0), return, end; % exit if no imageDisplay methods found.
        runAcq(Control);
    else
        cv = get(cine,'Value');
        set(cineValue,'String',num2str(cv,'%2.0f'));
    end
end

% cineSave Callback
function cineSave_Callback(src,eventdata)
    if get(freeze,'Value')==0   % no action if not in freeze
        set(src,'Value',0); 
        return 
    end 
    ImageBuf = evalin('base', 'Resource.ImageBuffer(1)');
    fig = evalin('base', 'Resource.DisplayWindow(1).figureHandle');
    handle = evalin('base','Resource.DisplayWindow(1).imageHandle');
    cfrm = ImageBuf.firstFrame;
    if ~evalin('base','exist(''Process'',''var'')'), return, end;
    Process = evalin('base','Process');
    n = 1; % n keeps track of frame no. from first to last.
    while n <= ImageBuf.numFrames
        % Find all the Process structures with 'imageDisplay' method.
        m = 0; % m keeps track of how many imageDisplay methods are found.
        for kc = 1:size(Process,2)
            if (strcmp(Process(kc).classname,'Image'))&&(strcmp(Process(kc).method,'imageDisplay'))
                m = m + 1;
                Control(m).Command = 'imageDisplay';
                Control(m).Parameters = cell(size(Process(kc).Parameters));
                for jc = 1:2:size(Process(kc).Parameters,2)
                    Control(m).Parameters{jc} = Process(kc).Parameters{jc};
                    Control(m).Parameters{jc+1} = Process(kc).Parameters{jc+1};
                    if strcmp(Process(kc).Parameters{jc},'framenum')
                        Control(m).Parameters{jc+1} = round(cfrm);
                    end
                end
            end
        end
        if (m == 0), return, end; % exit if no imageDisplay methods found.
        runAcq(Control);
        F(n) = getframe(fig);
        if cfrm == ImageBuf.lastFrame, break, end;
        n = n+1;
        cfrm = cfrm + 1;
        if cfrm > ImageBuf.numFrames, cfrm = 1; end
    end
    filename = datestr(now,'dd-mmmm-yyyy_HH-MM-SS');
    movie2avi(F,filename, 'compression', 'None');
    set(src,'Value',0);
end

% HV timer callback for actual push capacitor voltage; used for both
% internal and external profile 5 power supply configurations.
function hvTimerCallback(src,eventdata)
    [Result,extCapVoltage] = getHardwareProperty('TpcExtCapVoltage');
    if ~strcmp(Result,'Success')
        error('VSX: Error from getHardwareProperty call to read push capacitor Voltage.');
    end
    set(hv2Actual,'String',num2str(extCapVoltage,'%.1f'));
    if 1-(extCapVoltage/get(hv2Sldr,'Value')) > 0.20 % if 20% low
        set(hv2Actual,'BackgroundColor',[0.7,0.7,1.0]);
    else
        set(hv2Actual,'BackgroundColor',[0.8,0.8,0.8]);
    end
end


end