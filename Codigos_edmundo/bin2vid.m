%function bin2vid(binname,ImageBuffer)
load('E:/ADQUISITIONS/PNEUMONIA/HEAD73630243821TK24');

FILE.ImgHandle=fopen([FILE.Directory ...
     sprintf('IMG%dTK%dCL%d',FILE.PatientID,FILE.Take,FILE.Cluster)],'r');

[video, n]=fread(FILE.ImgHandle,Inf,'double=>double');
i=PData.Size(1);
j=PData.Size(2);
k=n/i/j;
sizes=[i j k];
               
video=reshape(video,sizes);
xl=linspace(-j/2*0.3,j/2*0.3,j);
l2mm=1540/(Trans.frequency*1e3);
zl=linspace(SFormat.startDepth*l2mm,SFormat.endDepth*l2mm,j);

aviobj = avifile([FILE.Directory ...
                  sprintf('VIDI%dTK%d',FILE.PatientID,FILE.Take)],...
                  'compression','None'); 
avioobj.Fps= 5;
CMAP= repmat(((0:1/64:1).^UIValues.gamma)',[1 3]);

for i=1:sizes(3)
figure(89);
imagesc(xl,zl,video(:,:,i));
xlabel('azimuth (mm)'),ylabel('depth (mm)')
title(sprintf('Paciente #%d, zona #%d',FILE.PatientID,UIValues.zone));
set(gcf,'Colormap',CMAP);
%set(gcf,'CDataMapping','scaled');
set(gca,'CLim',[UIValues.reject UIValues.DynRng]);
colorbar;
axis image;
colormap gray;
aviobj = addframe(aviobj,gcf);
pause(1/avioobj.Fps);
end 
viobj = close(aviobj); %Close avi object

fclose('all');



%end