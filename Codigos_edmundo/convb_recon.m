
c = 1540;%Resource.Parameters.speedOfSound;  
Fc=Trans.frequency*1e6; 
Fs=4*Fc;
rxlambda=Receive(1).samplesPerWave*2;
mm2smpl=1e-3*Fc/c*rxlambda; %constant to change from mm to samples

maxAprSz=TX.NumEl;
dB=55;
pitch = 0.3e-3;
n=128%;Resource.Parameters.numTransmit;
nr=SFormat.numRays;
txFNum=TX.FNum;
samples=Receive(1).endSample-Receive(1).startSample+1;
zstart=SFormat.startDepth*rxlambda;
zend=SFormat.endDepth*rxlambda;
valid_data=zend-zstart;


Z=((0:samples-1)+zstart)*c/Fs/2;             % in the axial direction [mm]
Z=repmat(Z',[1 n nr]);

%%delay_offset(focus)=  Fs/Fc*(sqrt(((txNumEl)*Trans.spacing)^2+txFocus(focus)^2)-txFocus(focus));
Zoffset=[1]*c/Fs;

%%%%%%%%%%%%%%%%%%%%%%Data Index formation %%%%%%%%%%%%%%%%%%%%%
index=repmat(1:n,[samples 1 nr]); 
transshift=permute(index,[1 3 2]);
X=(transshift-index)*pitch;
index_fixer=(floor((0:(samples*nr*n-1))'/samples)*samples);
%%%%%%%%%%%%%%%%%%%%%% Apodization  %%%%%%%%%%%%%%%%%%%%%%%%%
Xtrim=floor(Z(:,1,1)/txFNum/pitch*2/rxlambda*2);
Xtrim(Xtrim>maxAprSz/2)=maxAprSz/2;
XtrimM=repmat(Xtrim,[1 n nr]);
apodi=index-transshift;
apodi(apodi>XtrimM)=NaN;
apodi(apodi<-XtrimM)=NaN;
apodi=apodi+XtrimM+1;

H=0.5-0.5*cos(2*pi*apodi./(XtrimM*2+2));
H(isnan(H))=0;
H=H./(XtrimM+1);

%%%%%%%%%%Receive Data, Reshape and Parameters reading%%%%%%%%%%%%%%%%%%
for frame=1:size(Raw,3)
A=Raw(:,:,frame);
%A=RcvData{1}(:,:,frame);

A=A(1:samples*2*nr,:)';
B= reshape(A,Resource.Parameters.numRcvChannels,samples*2,nr);
B1=B(:,1:samples,:);
B2=B(:,samples+1:2*samples,:);
C1=permute(B1,[2 1 3]); 
C2=permute(B2,[2 1 3]);
C=[C1 C2];

%%%%%%%%%%%%%%%%%%%%%% Delay %%%%%%%%%%%%%%%%%%%%%%%%%
delays = ((Z+Zoffset) + sqrt( (Z).^2 + (X).^2  ) )*Fs/c;
delaysa=floor(delays);
q=delays-delaysa;
delaysa(delaysa>samples)=samples;
delaysa(delaysa<1)=0;
delaysb=delaysa+1;
delaysb(delaysb>samples)=samples;
%%%%%%%%%%%%%%Delay Application with linear interpolation%%%%%%%%%%%%%%%%%
Dd= (1-q(:)).*double(C(delaysa(:)+index_fixer))+q(:).*double(C(delaysb(:)+index_fixer));
Dd=double((reshape(Dd,size(delays))));
%%%%%%%%%%%%%%%%% B MODE FORMATION %%%%%%%%%%%%%%
Da=Dd.*H;
Rf(:,:,frame)=squeeze(sum(Da(1:valid_data,:,:),2));
US=(abs(hilbert(Rf(:,:,frame))));
%US=US-max(US(:));

video(:,:,frame)=US;
frame
end
xl=linspace(0,n*pitch*1e3,n);
zl=linspace(zstart/mm2smpl,zend/mm2smpl,valid_data);

aviobj = avifile([FILE.Directory ...
                  sprintf('VIDR%dTK%d',FILE.PatientID,FILE.Take)],...
                  'compression','None'); 
avioobj.Fps= 10;
CMAP= repmat(((0:1/64:1).^UIValues.gamma)',[1 3]);
for frame=1:sizes(3)
figure(45)
Bmode=video(:,:,frame);
Bmode=UIValues.pgain*Bmode*(2^15-1)/(2^13-1);
Bmode=(20*log10(Bmode)-UIValues.reject)/UIValues.DynRng;
Bmode(Bmode<0)=0;Bmode(Bmode>1)=1;
Bmode=medfilt2(Bmode,[8 1]);
Bmode=imfilter(Bmode,fspecial('average',[1 8]));
Bmode=(imsharpen(Bmode,'Amount',2,'Radius',0.1','Threshold',0.2));

imagesc(xl,zl,Bmode);
title('Conventional Beamforming & Processing')
set(gca,'clim',[0 1]);
xlabel('azimuth (mm)'),ylabel('depth (mm)')
set(gcf,'Colormap',CMAP);
axis image;
aviobj = addframe(aviobj,gcf);
pause(0.01)
end
viobj = close(aviobj);
