Receive = evalin('base', 'Receive');
Trans = evalin('base', 'Trans');
Resource=evalin('base', 'Resource');
nfocus=evalin('base','nfocus');
nr=evalin('base','nr');
txFocus=evalin('base','txFocus');
wlsPer128=evalin('base','wlsPer128');
Filter=evalin('base','Filter');

maxAcqLength = sqrt(SFormat.endDepth^2 + ((Trans.numelements-1)*Trans.spacing)^2) - SFormat.startDepth;
wlsPer128 = 128/(4*2); % wavelengths in 128 samples for 2 samplesPerWave
Receive = repmat(struct('Apod', zeros(1,Trans.numelements), ...
                        'startDepth', SFormat.startDepth, ...
                        'endDepth', SFormat.startDepth + wlsPer128*ceil(maxAcqLength/wlsPer128), ...
                        'TGC', 1, ...
                        'bufnum', 1, ...
                        'framenum', 1, ...
                        'acqNum', 1, ...
                        'samplesPerWave', 4,...
                        'mode', 0, ...
                        'InputFilter', Filter.Coefficients(1:2:11), ...[0.0036,0.0127,0.0066,-0.0881,-0.2595,0.6494], ...   
                        'callMediaFunc', 0), 1, 2*nr*nfocus*Resource.RcvBuffer(1).numFrames);
   %close('ver');                 
% - Set event specific Receive attributes.
for f = 1:Resource.RcvBuffer(1).numFrames
    k = 2*nr*nfocus*(f-1);
    Receive(k+1).callMediaFunc = 1;
    for i=1:nfocus
       
        if i==1, ini=SFormat.startDepth;
        else  ini=floor(sum(txFocus(i-1:i))/2); end
       
        if i==nfocus, fin=SFormat.endDepth; 
        else fin=floor(sum(txFocus(i:i+1))/2)-1; end
        
 maxAcqLength = sqrt(fin^2 + ((Trans.numelements-1)*Trans.spacing)^2) - ini;
  
    for j = 1:2*nfocus:2*nr*nfocus
      n= (2*i-2);
   
        Receive(j+n+k).Apod(1:Resource.Parameters.numRcvChannels) = 1.0;
        Receive(j+n+k).framenum = f;
        Receive(j+n+k).acqNum = j+n;      % two acquisitions per frame 
        Receive(j+n+k).startDepth= ini;
        Receive(j+n+k).endDepth= ini + wlsPer128*ceil(maxAcqLength/wlsPer128);
        
        Receive(j+n+k+1).Apod((Resource.Parameters.numRcvChannels+1):Trans.numelements) = 1.0;
        Receive(j+n+k+1).framenum = f;
        Receive(j+n+k+1).acqNum = j+n+1;  % two acquisitions per frame 
        Receive(j+n+k+1).startDepth= ini;
        Receive(j+n+k+1).endDepth= ini + wlsPer128*ceil(maxAcqLength/wlsPer128);
    end
    end
end
Msamples=0;
ends=0;
n=1;
for i=1:2:2*nfocus
Msamples=Msamples+Receive(i).endDepth-Receive(i).startDepth;
ends(n)=Receive(i).endDepth;
n=n+1;
end
Resource.RcvBuffer.rowsPerFrame = Msamples*4*2*2*nr;  % max depth of 384 * 2 for round trip * 2 smpls/wave * 2*nr acquisitions 

%assignin('base','Receive',Receive);
assignin('base','Resource',Resource);

evalin('base','TGC.rangeMax = SFormat.endDepth;');
evalin('base','TGC.Waveform = computeTGCWaveform(TGC);');
