% Copyright 2001-2013 Verasonics, Inc.  All world-wide rights and remedies under all intellectual property laws and industrial property laws are reserved.  Verasonics Registered U.S. Patent and Trademark Office.
%
% File name SetUpL11_4v_128RyLnsSA.m:
% Generate .mat Sequence Object file for L7-4 transducer using 128 ray lines 
% with 2-1 synthetic aperture (two pulses per line to acquire data from all
% 128 receive elements).  Of the 128 transmit channels, the active transmit 
% aperture is limited based on user-entered transmit focus and f-number.
%
% - The receive acquisitions use 200% bandwidth to improve DMA transfers.
%
% Last update 03-15-2013

clear all

% Define system parameters.
Resource.Parameters.numTransmit = 128;      % number of transmit channels.
Resource.Parameters.numRcvChannels = 64;    % number of receive channels.
Resource.Parameters.speedOfSound = 1540;    % set speed of sound in m/sec before calling computeTrans
Resource.Parameters.simulateMode = 0;
%  Resource.Parameters.simulateMode = 1 forces simulate mode, even if hardware is present.
%  Resource.Parameters.simulateMode = 2 stops sequence and processes RcvData continuously.

% Specify Trans structure array.
Trans.name = 'L11-4v';
Trans.frequency = 7.5;  % frequency in megaHertz
Trans = computeTrans(Trans);  % L11-4v transducer is 'known' transducer so we can use computeTrans.
Trans.maxHighVoltage = 45;  % set maximum high voltage limit for pulser supply.

l2mm=Resource.Parameters.speedOfSound/Trans.frequency/1e3;

% 
% Filter=designfilt('bandpassfir',...
%                   'filterorder',20,...
%                   'CutoffFrequency1',0.5*Trans.frequency,...
%                   'CutoffFrequency2',1.5*Trans.frequency,...
%                   'SampleRate',4*Trans.frequency);

% Specify SFormat structure array.
nr = 128;  % nr is number of raylines.
SFormat.transducer = 'L11-4v';     % 128 element linear array with 1.0 lambda spacing
SFormat.scanFormat = 'RLIN';       % rectangular linear array scan
SFormat.radius = 0;                % ROC for curved lin. or dist. to virt. apex
SFormat.theta = 0;
SFormat.numRays = nr;             % no. of Rays
SFormat.FirstRayLoc = [-(nr-1)/2*Trans.spacing,0,0]; % x,y,z
SFormat.rayDelta = Trans.spacing;  % increment between rays (wvlnghts)
SFormat.startDepth =0;
SFormat.endDepth = round(60/l2mm);            % Acquisition depth in wavelengths

%Region creation Data 
txFocus = round([12 24 36]/l2mm);  % Initial transmit focus.
nfocus= size(txFocus,2);

% Specify PData structure array.
PData.sFormat = 1;      % use first SFormat structure.
PData.pdeltaX = Trans.spacing;
PData.pdeltaZ = 0.125;
PData.Size(1) = ceil((SFormat.endDepth-SFormat.startDepth)/PData.pdeltaZ);
PData.Size(2) = ceil((Trans.numelements*Trans.spacing)/PData.pdeltaX);
PData.Size(3) = 1;      % single image page
PData.Origin = [-Trans.spacing*(nr-1)/2,0,SFormat.startDepth]; % x,y,z of upper lft crnr
PData.focus=ceil((txFocus-SFormat.startDepth)/PData.pdeltaZ);
PData.nfocus= nfocus;
[PData.Region,PData.numRegions] = createNewRegions(PData);
% Specify Media object.
pt1;
Media.function = 'movePoints';

% Specify Resources.
Resource.RcvBuffer= repmat(struct('datatype','int16',...
                                  'rowsPerFrame', 700*4*2*2*nr,... % max depth of 384 * 2 for round trip * 2 smpls/wave * 2*nr acquisitions 
                                  'colsPerFrame', Resource.Parameters.numRcvChannels,...
                                  'numFrames',8),1,2);

Resource.InterBuffer.datatype = 'complex';
Resource.InterBuffer.numFrames = 1;         % one intermediate buffer needed for SA.
Resource.InterBuffer.rowsPerFrame = 2400;
Resource.InterBuffer.colsPerFrame = PData.Size(2);
Resource.ImageBuffer.datatype = 'double';
Resource.ImageBuffer.rowsPerFrame = 2400;
Resource.ImageBuffer.colsPerFrame = PData.Size(2);
Resource.ImageBuffer.numFrames = 8;
Resource.DisplayWindow.Title = 'L7-4v_128RyLnsSA';
Resource.DisplayWindow.pdelta = 0.75;

ScrnSize = get(0,'ScreenSize');
DwWidth = ceil(PData.Size(2)*PData.pdeltaX/Resource.DisplayWindow(1).pdelta);
DwHeight = ceil(PData.Size(1)*PData.pdeltaZ/Resource.DisplayWindow(1).pdelta);
Resource.DisplayWindow(1).Position = [5,5, ...  % lower left corner position
                                      DwWidth, DwHeight];
Resource.DisplayWindow(1).ReferencePt = [PData.Origin(1),PData.Origin(3)];   % 2D imaging is in the X,Z plane
Resource.DisplayWindow.Colormap = gray(256);

% Specify TW structure array.
TW.type = 'parametric';
TW.Parameters = [24,23,2,1];

% Specify nr TX structure arrays. Transmit centered on element n in the array for event n.
TX = repmat(struct('waveform', 1, ...
                   'Origin', [0.0,0.0,0.0], ...
                   'focus', txFocus(1), ...
                   'Steer', [0.0,0.0], ...
                   'Apod', zeros(1,Trans.numelements), ...
                   'Delay', zeros(1,Trans.numelements)), 1, SFormat.numRays*nfocus);

% Determine TX aperture based on focal point and desired f number.
txFNum = 3;  % set to desired f-number value for transmit (range: 1.0 - 20)
for i =1:nfocus

   txNumEl=round((txFocus(i)/txFNum)/Trans.spacing/2); % no. of elements in 1/2 aperture.
if txNumEl > (Trans.numelements/2 - 1) 
   txNumEl = floor(Trans.numelements/2 - 1); 
end   
% - Set event specific TX attributes.
for j = 1:SFormat.numRays  % 128 transmit events
    n= (j-1)*nfocus+i;
    % Set transmit Origins to positions of elements.
    TX(n).Origin = SFormat.FirstRayLoc + (j-1)*Trans.spacing*[1,0,0];
    % Set transmit Apodization so (1 + 2*TXnumel) transmitters are active.
    lft = j - txNumEl; if lft < 1, lft = 1; end;
    rt = j + txNumEl;  if rt > Trans.numelements, rt = Trans.numelements; end;
    TX(n).Apod(lft:rt) = 1.0;
    TX(n).focus= txFocus(i);
    TX(n).Delay = computeTXDelays(TX(n));
    TX(n).NumEl=txNumEl;
end
end

% Specify Receive structure arrays. 
% - We need 2*nr Receives for every frame (synthetic aperture).
maxAcqLength = sqrt(SFormat.endDepth^2 + ((Trans.numelements-1)*Trans.spacing)^2) - SFormat.startDepth;
wlsPer128 = 128/(4*2); % wavelengths in 128 samples for 2 samplesPerWave
Receive = repmat(struct('Apod', zeros(1,Trans.numelements), ...
                        'startDepth', SFormat.startDepth, ...
                        'endDepth', SFormat.startDepth + wlsPer128*ceil(maxAcqLength/wlsPer128), ...
                        'TGC', 1, ...
                        'bufnum', 1, ...
                        'framenum', 1, ...
                        'acqNum', 1, ...
                        'samplesPerWave', 4,...
                        'mode', 0, ...
                        'InputFilter', [-0.0051, 0, 0.0419, 0, -0.2885, 0.4968],...%Filter.Coefficients(1:2:11), ...[0.0036,0.0127,0.0066,-0.0881,-0.2595,0.6494], ...   
                        'callMediaFunc', 0), 1, 2*nr*nfocus*(Resource.RcvBuffer(1).numFrames+1));
   %close('ver');                 
% - Set event specific Receive attributes.
for f = 1:Resource.RcvBuffer(1).numFrames+1
    k = 2*nr*nfocus*(f-1);
    Receive(k+1).callMediaFunc = 1;
    for i=1:nfocus
       
        if i==1, ini=SFormat.startDepth;
        else  ini=floor(sum(txFocus(i-1:i))/2); end
       
        if i==nfocus, fin=SFormat.endDepth; 
        else fin=floor(sum(txFocus(i:i+1))/2)-1; end
        
 maxAcqLength = sqrt(fin^2 + ((Trans.numelements-1)*Trans.spacing)^2) - ini;
  
    for j = 1:2*nfocus:2*nr*nfocus
      n= (2*i-2);
   
        Receive(j+n+k).Apod(1:Resource.Parameters.numRcvChannels) = 1.0;
        Receive(j+n+k).framenum = f;
        Receive(j+n+k).acqNum = j+n;      % two acquisitions per frame 
        Receive(j+n+k).startDepth= ini;
        Receive(j+n+k).endDepth= ini + wlsPer128*ceil(maxAcqLength/wlsPer128);
        
        Receive(j+n+k+1).Apod((Resource.Parameters.numRcvChannels+1):Trans.numelements) = 1.0;
        Receive(j+n+k+1).framenum = f;
        Receive(j+n+k+1).acqNum = j+n+1;  % two acquisitions per frame 
        Receive(j+n+k+1).startDepth= ini;
        Receive(j+n+k+1).endDepth= ini + wlsPer128*ceil(maxAcqLength/wlsPer128);
        
        if f==Resource.RcvBuffer(1).numFrames+1
            Receive(j+n+k).bufnum=2;
            Receive(j+n+k+1).bufnum=2;
        end
    end
    end
end
Msamples=0;
ends=0;
n=1;
for i=1:2:2*nfocus
Msamples=Msamples+Receive(i).endDepth-Receive(i).startDepth;
ends(n)=Receive(i).endDepth;
n=n+1;
end
Resource.RcvBuffer(1).rowsPerFrame = Msamples*4*2*2*nr; % max depth of 384 * 2 for round trip * 2 smpls/wave * 2*nr acquisitions 
Resource.RcvBuffer(2).rowsPerFrame = Msamples*4*2*2*nr; % max depth of 384 * 2 for round trip * 2 smpls/wave * 2*nr acquisitions 

% Specify TGC Waveform structure.
TGC.CntrlPts = [193,437,650,812,923,983,1023,1022];   %[500,590,650,710,770,830,890,950];% log10(linspace(2,1000,8))/3*1000;
TGC.rangeMax = SFormat.endDepth;
TGC.Waveform = computeTGCWaveform(TGC);

% Specify Recon structure array. 
Recon = struct('senscutoff', 0.25, ...
               'pdatanum', 1, ...
               'rcvBufFrame',-1, ...
               'IntBufDest', [1,1], ...
               'ImgBufDest', [1,-1], ...
               'RINums', zeros(2*nfocus,nr));
% - Set RINums values for frame.
Recon.RINums(:) = (1:2*nr*nfocus);  % 2*nr ReconInfos needed for nr rays, 2-1 synthetic aperture

% Define ReconInfo structures.
ReconInfo = repmat(struct('mode', 3, ...  % replace IQ data first half SA.
                   'txnum', 1, ...
                   'rcvnum', 1, ...
                   'regionnum', 0), 1, 2*nr);
% - Set specific ReconInfo attributes.
for j = 1:2:2*nr*nfocus  % For each row in the column
    ReconInfo(j).mode = 3;   % replace IQ data
    ReconInfo(j).txnum = (j+1)/2;
    ReconInfo(j).rcvnum =  j ;%2*nr*nfocus*Resource.RcvBuffer(1).numFrames+j;
    ReconInfo(j).regionnum = (j+1)/2;
    ReconInfo(j+1).mode = 5; % accum and detect
    ReconInfo(j+1).txnum = (j+1)/2;
    ReconInfo(j+1).rcvnum =  j+1;%2*nr*nfocus*Resource.RcvBuffer(1).numFrames+j+1;
    ReconInfo(j+1).regionnum = (j+1)/2;
end

 %Specify Process structure array.
 
Process =repmat(struct('classname','External',...
                       'method', 'StoreRaw',...
                       'Parameters',[]),1,Resource.RcvBuffer(1).numFrames+2);

Process(1).Parameters={'srcbuffer','receive',... % name of buffer to process.
                       'srcbufnum',1,...
                       'srcframenum',10,... % process  frame.
                       'dstbuffer','none'};   
                     
for i=2:Resource.RcvBuffer(1).numFrames
   
Process(i).Parameters={'srcbuffer','receive',... % name of buffer to process.
                       'srcbufnum',1,...
                       'srcframenum',i-1,... % process  frame.
                       'dstbuffer','none'};   

end
                           
Process(i+1).method = 'EDisplay';
Process(i+1).Parameters={'srcbuffer','image',... % name of buffer to process.
                         'srcbufnum',1,...
                         'srcframenum',-1,... % process last frame.
                         'dstbuffer','none'};   

Process(i+2).method = 'preprocessRaw';
Process(i+2).Parameters={'srcbuffer','receive',... % name of buffer to process.
                         'srcbufnum',1,...
                         'srcframenum',1,... % process last frame.
                         'dstbuffer','receive',...
                         'dstbufnum',2,...
                         'dstframenum',1};   
                    


% Specify SeqControl structure arrays.
%  - Time between acquisitions in usec
t1 = ceil(2*ends/(Trans.frequency)+20); % 44 msec at max Receive.endDepth.
for i=1:nfocus
SeqControl(i).command = 'timeToNextAcq';
SeqControl(i).argument = t1(i);
end
%  - Time between frames at 12 fps at max Receive.endDepth.
FPS=5;
t2=1e6/FPS +t1(nfocus)-sum(t1)*2*nr;
SeqControl(nfocus+1).command = 'timeToNextAcq';
SeqControl(nfocus+1).argument = t2;
%  - Return to Matlab
SeqControl(nfocus+2).command = 'returnToMatlab';
%  - Jump back to start.
SeqControl(nfocus+3).command = 'jump';
SeqControl(nfocus+3).argument = 1;
nsc = nfocus+4; % next SeqControl number

% Specify Event structure arrays.
n = 1;
for i = 1:Resource.RcvBuffer(1).numFrames
    
    for j = 1:2*nfocus:2*nr*nfocus % Acquire frame
        for f=0:2:2*nfocus-1
        Event(n).info = '1st half of aperture.';
        Event(n).tx = (j+1+f)/2;   % use next TX structure.
        Event(n).rcv = 2*nr*nfocus*(i-1)+j+f;   
        Event(n).recon = 0;      % no reconstruction.
        Event(n).process = 0;    % no processing
        Event(n).seqControl = f/2+1; % seqCntrl
        n = n+1;
        
        Event(n).info = '2nd half of aperture.';
        Event(n).tx = (j+1+f)/2;   % use next TX structure.
        Event(n).rcv = 2*nr*nfocus*(i-1)+j+1+f;   
        Event(n).recon = 0;      % no Recon structure array.
        Event(n).process = 0;    % no processing
        Event(n).seqControl = f/2+1; % seqCntrl
        n = n+1;
        end
    end
    % Replace last events SeqControl for inter-frame timeToNextAcq.
    Event(n-1).seqControl = nfocus+1;
    
    Event(n).info = 'Transfer frame to host.';
    Event(n).tx = 0;        % no TX
    Event(n).rcv = 0;       % no Rcv
    Event(n).recon = 0;     % no Recon
    Event(n).process = 0; 
    Event(n).seqControl = nsc; 
       SeqControl(nsc).command = 'transferToHost'; % transfer frame to host buffer
       nsc = nsc+1;
    n = n+1;

    Event(n).info = 'recon and process'; 
    Event(n).tx = 0;         % no transmit
    Event(n).rcv = 0;        % no rcv
    Event(n).recon = 1;      % reconstruction
    Event(n).process = Resource.RcvBuffer(1).numFrames+1;    % process
    Event(n).seqControl = 0;
   
    n= n+1; 
    Event(n).info = 'Store Raw'; 
    Event(n).tx = 0;         % no transmit
    Event(n).rcv = 0;        % no rcv
    Event(n).recon = 0;      % reconstruction
    Event(n).process = i;    % process
    Event(n).seqControl = 0;
    
    if floor(i/4) == i/4     % Exit to Matlab every 4th frame reconstructed 
        Event(n).seqControl = nfocus+2;
    else
        Event(n).seqControl = 0;
    end
    n = n+1;
end

Event(n).info = 'Jump back';
Event(n).tx = 0;        % no TX
Event(n).rcv = 0;       % no Rcv
Event(n).recon = 0;     % no Recon
Event(n).process = 0; 
Event(n).seqControl = nfocus+3;

DIR='E:/ADQUISITIONS/PNEUMONIA/';
FILE=struct('PatientID',0000,...
            'Directory',DIR,...
            'Date',[],...
            'RcvHandle',[],...
            'ImgHandle',[],...
            'Take',0,...
            'Cluster',0);
start_aq=0;
IQ_flag=0;

if ~exist('presets.mat','file')
UIValues=struct('start',SFormat.startDepth,'range',SFormat.endDepth,...
                'nfocus',nfocus,'focus_spacing',10,'zfocus',txFocus(1),...
                'DynRng',60,'gamma',1,'reject',40,'pgain',1,'zone',1);
else
    load('presets.mat');
end


% User specified UI Control Elements
% - Sensitivity Cutoff
% UI(1).Control =  {'UserB7','Style','VsSlider','Label','Sens. Cutoff',...
%                   'SliderMinMaxVal',[0,1.0,Recon(1).senscutoff],...
%                   'SliderStep',[0.025,0.1],'ValueFormat','%1.3f'};
% UI(1).Callback = text2cell('%-UI#1Callback');

% - Z Range Change
% UI(2).Control = {'UserA2','Style','VsSlider','Label','Start (mm)',...
%                  'SliderMinMaxVal',round([UIValues.start,10,UIValues.start]*l2mm),...
%                  'SliderStep',[0.1,0.2],'ValueFormat','%3.0f'};
% UI(2).Callback = text2cell('%-UI#2Callback');
% 
% UI(3).Control = {'UserA1','Style','VsSlider','Label','Range (mm)',...
%                  'SliderMinMaxVal',round([100,200,UIValues.range]*l2mm),...
%                  'SliderStep',[0.1,0.2],'ValueFormat','%3.0f'};
% UI(3).Callback = text2cell('%-UI#3Callback');

% - F number change
% UI(4).Control = {'UserB6','Style','VsSlider','Label','F Number',...
%                  'SliderMinMaxVal',[1,20,txFNum],'SliderStep',[0.1,0.2],'ValueFormat','%2.0f'};
% UI(4).Callback = text2cell('%-UI#4Callback');


% - Transmit focus change
% UI(5).Control = {'UserB5','Style','VsSlider','Label','# Focus',...
%                  'SliderMinMaxVal',[1,3,UIValues.nfocus],...
%                  'SliderStep',[0.5,0.5],'ValueFormat','%2.0f'};
% UI(5).Callback = text2cell('%-UI#5Callback');
% 
% UI(6).Control = {'UserB4','Style','VsSlider','Label','Focus depth (mm)',...
%                  'SliderMinMaxVal',round([11,199,UIValues.zfocus]*l2mm),...
%                  'SliderStep',[0.1,0.2],'ValueFormat','%3.0f'};
% UI(6).Callback = text2cell('%-UI#6Callback');
% 
% UI(7).Control = {'UserB3','Style','VsSlider','Label','Focus spacing (mm)',...
%                  'SliderMinMaxVal',[10,50,UIValues.focus_spacing],...
%                  'SliderStep',[0.1,0.2],'ValueFormat','%2.0f'};
% UI(7).Callback = text2cell('%-UI#7Callback');


% - Dynamic range display
% UI(8).Control = {'UserC8','Style','VsSlider','Label','Dyn. Range (dB)',...
%                  'SliderMinMaxVal',[15,145,UIValues.DynRng],...
%                  'SliderStep',[0.1,0.2],'ValueFormat','%2.0f'};
% UI(8).Callback = text2cell('%-UI#8Callback');
% 
% UI(9).Control = {'UserC7','Style','VsSlider','Label','Reject',...
%                  'SliderMinMaxVal',[0,100,UIValues.reject],'SliderStep',[0.1,0.2],...
%                  'ValueFormat','%3.0f'};
% UI(9).Callback = text2cell('%-UI#9Callback');
% 
% UI(10).Control = {'UserC6','Style','VsSlider','Label','Gamma',...
%                  'SliderMinMaxVal',[0.1,10,UIValues.gamma],'SliderStep',[0.01,0.02],...
%                  'ValueFormat','%2.2f'};
% UI(10).Callback = text2cell('%-UI#10Callback');
% 
% UI(11).Control = {'UserC5','Style','VsSlider','Label','Gain',...
%                  'SliderMinMaxVal',[1,100,UIValues.pgain],'SliderStep',[0.01,0.2],...
%                  'ValueFormat','%3.0f'};
% UI(11).Callback = text2cell('%-UI#11Callback');



% - Control Buttons

UI(12).Control = {'UserB2','Style','VsPushButton','Label','New'};
UI(12).Callback = text2cell('%-UI#12Callback');

UI(13).Control = {'UserB1','Style','VsToggleButton','Label','Acquire'};
UI(13).Callback = text2cell('%-UI#13Callback');

% UI(14).Control = {'UserC4','Style','VsPushButton','Label','Save Preset'};
% UI(14).Callback = text2cell('%-UI#14Callback');
% 
% UI(15).Control = {'UserC3','Style','VsPushButton','Label','Load Preset'};
% UI(15).Callback = text2cell('%-UI#15Callback');

UI(16).Control = {'UserC6','Style','VsButtonGroup','Title','Zona',...
                  'NumButtons',8,'Labels',{'1','2','3','4','5','6','7','8'}};                 
UI(16).Callback = text2cell('%-UI#16Callback');

% - External Function Definition
EF(1).Function = text2cell('%EF#1');
EF(2).Function = text2cell('%EF#2');




% Specify factor for converting sequenceRate to frameRate.
frameRateFactor = 4;

% Save all the structures to a .mat file.
save('L11-4v_128RyLnsSA'); 

return


% **** Callback routines to be converted by text2cell function. ****
% **** Callback routines to be converted by text2cell function. ****
%-UI#1Callback - Sensitivity cutoff change
ReconL= evalin('base', 'Recon');
for i = 1:size(ReconL,2)
    ReconL(i).senscutoff = UIValue;
end
assignin('base','Recon',ReconL);
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'Recon'};
assignin('base','Control', Control);
return
%-UI#1Callback

%-UI#2Callback - Start change
global first;
simMode = evalin('base','Resource.Parameters.simulateMode');
% No range change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','SFormat.endDepth'));
    return
end
l2mm=evalin('base','l2mm');
startDepth = round(UIValue/l2mm);
SFormat = evalin('base','SFormat');
SFormat.startDepth = startDepth;
assignin('base','SFormat',SFormat);
RangeChange;
first=0;
return
%-UI#2Callback

%-UI#3Callback - End change
global first;
simMode = evalin('base','Resource.Parameters.simulateMode');
% No range change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','SFormat.endDepth'));
    return
end
l2mm=evalin('base','l2mm');
endDepth = round(UIValue/l2mm);
SFormat = evalin('base','SFormat');
SFormat.endDepth = endDepth;
assignin('base','SFormat',SFormat);
RangeChange;
first=0;
return
%-UI#3Callback

%-UI#4Callback - F number change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No F number change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','txFNum'));
    return
end
txFNum = UIValue;
assignin('base','txFNum',txFNum);
UpdateTX;
% Set Control command to update TX
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'TX'};
assignin('base','Control', Control);
return
%-UI#4Callback

%-UI#5Callback - # focus change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No focus change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','txFocus'));
    return
end
l2mm=evalin('base','l2mm');

UIValues= evalin('base','UIValues');
UIValues.nfocus = round(UIValue);
assignin('base','UIValues',UIValues);
assignin('base','nfocus',UIValue);
txFocus=createFocus(UIValues.zfocus,UIValues.nfocus,UIValues.focus_spacing);
assignin('base','txFocus',txFocus);
UpdateTX;
UpdateReceive;
% Set Control command to update TX
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'TX','Receive'};
assignin('base','Control', Control);
return
%-UI#5Callback 

%-UI#6Callback - TX focus change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No focus change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','txFocus'));
    return
end
l2mm=evalin('base','l2mm');

UIValues= evalin('base','UIValues');
UIValues.zfocus = round(UIValue/l2mm);
assignin('base','UIValues',UIValues);
txFocus=createFocus(UIValues.zfocus,UIValues.nfocus,UIValues.focus_spacing);
assignin('base','txFocus',txFocus);
UpdateTX;
UpdateReceive;
% Set Control command to update TX
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'TX','Receive'};
assignin('base','Control', Control);
return
%-UI#6Callback

%-UI#7Callback - Focus Spacing change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No focus change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','txFocus'));
    return
end
l2mm=evalin('base','l2mm');

UIValues= evalin('base','UIValues');
UIValues.focus_spacing = round(UIValue/l2mm);
assignin('base','UIValues',UIValues);
txFocus=createFocus(UIValues.zfocus,UIValues.nfocus,UIValues.focus_spacing);
assignin('base','txFocus',txFocus);
UpdateTX;
UpdateReceive;
% Set Control command to update TX
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'TX','Receive'};
assignin('base','Control', Control);
assignin('base', 'action', 'displayChange');
return
%-UI#7Callback

%-UI#8Callback - Set Dynamic Range
UIValues= evalin('base','UIValues');
UIValues.DynRng=UIValue;
assignin('base', 'UIValues', UIValues); 
UpdateScreen('DynRng',UIValue);
return
%-UI#8Callback

%-UI#9Callback - reject change
UIValues= evalin('base','UIValues');
UIValues.reject=UIValue;
assignin('base', 'UIValues', UIValues);
UpdateScreen('reject',UIValue);
return
%-UI#9Callback

%-UI#10Callback - gamma change
UIValues= evalin('base','UIValues');
UIValues.gamma=UIValue;
assignin('base', 'UIValues', UIValues);  
UpdateScreen('gamma',UIValue);
return
%-UI#10Callback

%-UI#11Callback - pgain change
UIValues= evalin('base','UIValues');
UIValues.pgain=UIValue;
assignin('base', 'UIValues', UIValues);
%UpdateScreen('pgain',UIValue);
return
%-UI#11Callback


%-UI#12Callback - New Pacient
flag_start = evalin('base', 'start_aq');
if (flag_start==0)
FILE = evalin('base', 'FILE');
FILE.PatientID=round(now*100000);
FILE.Date=  datestr(now);
FILE.Take=0;   
FILE.Cluster=0;  
assignin('base','FILE', FILE);
msgbox(sprintf('El ID del nuevo Paciente es %d',FILE.PatientID),...
       'Pacient ID');
end    
return
%-UI#12Callback

%-UI#13Callback - Start acquisition
flag_start = evalin('base', 'start_aq');

if flag_start==0
   FILE = evalin('base', 'FILE');
   
   FILE.Take=FILE.Take+1;
   
   FILE.RcvHandle=fopen([FILE.Directory ...
       sprintf('RCV%dTK%dCL%d',FILE.PatientID,FILE.Take,FILE.Cluster)],'wb');
   FILE.ImgHandle=fopen([FILE.Directory ...
       sprintf('IMG%dTK%dCL%d',FILE.PatientID,FILE.Take,FILE.Cluster)],'wb');
   
   assignin('base','FILE', FILE); 
   save([FILE.Directory sprintf('HEAD%dTK%d.mat',FILE.PatientID,FILE.Take)],'FILE');
   flag_start=1;
else
    flag_start=0;
    fclose('all');
end
assignin('base','start_aq', flag_start);
return
%-UI#13Callback

%-UI#14Callback - save presets
evalin('base','save(''presets.mat'',''UIValues'');');
return
%-UI#14Callback

%-UI#15Callback - load presets
evalin('base','load(''presets.mat'',''UIValues'');');
%overrunear los UIControls y ponerles los valores.
return
%-UI#15Callback

%-UI#16Callback - zone change
UIValues= evalin('base','UIValues');
UIValues.zone=UIState;
assignin('base', 'UIValues', UIValues);  
return
%UpdateScreen('pgain',UIValue);
return
%-UI#16Callback

%EF#1
EDisplay(RData)
global first;
% persistent tici;
% 
% if ~isempty(tici),toc(tici);end; tici=tic;

    
if isempty(first)||(first==0)
    UpdateScreen('Initialize',1);
    first=0;
end


 Resource.DisplayWindow = evalin('base', 'Resource.DisplayWindow');
 PData.Size=evalin('base', 'PData.Size');
 SFormat=evalin('base', 'SFormat');
 Trans=evalin('base', 'Trans');
 UIValues=evalin('base','UIValues');
%     
Bmode=RData(1:PData.Size(1),1:PData.Size(2));
% 
Bmode=(20*log10(Bmode)+20*log10(UIValues.pgain*(2^15-1)/(2^22-1)));
%Bmode=imfilter(Bmode,fspecial('average',[6 4]));
Bmode=medfilt2(Bmode,[3 3]);
Bmode=imfilter(Bmode,fspecial('average',[10 5]));
%Bmode=imfilter(Bmode,fspecial('prewitt'));

Bmode=(imsharpen(Bmode,'Amount',2,'Radius',0.1','Threshold',0.2));

 set(Resource.DisplayWindow.imageHandle,'cdata',Bmode);
   drawnow;
% store image
 flag_start = evalin('base', 'start_aq'); 
 if flag_start==1
     FILE = evalin('base', 'FILE');
     Receive = evalin('base', 'Receive(1)');
     TX.NumEl= evalin('base', 'txNumEl');
     TX.FNum= evalin('base', 'txFNum');
     
    count= fwrite(FILE.ImgHandle,Bmode,'double');    
    save([FILE.Directory sprintf('HEAD%dTK%d.mat',FILE.PatientID,FILE.Take)],...
       'Resource',...
       'PData',...
       'SFormat',...
       'Receive',...
       'Trans',...
       'TX',...
       'UIValues',...
       '-append');
 end

return
%EF#1

%EF#2
StoreRaw(RData)

flag_start = evalin('base', 'start_aq');
if flag_start==1
    FILE = evalin('base', 'FILE'); 
       % tic
    count= fwrite(FILE.RcvHandle,RData,'int16');    
       % toc;
end

return
%EF#2



