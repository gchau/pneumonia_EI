% Copyright 2001-2013 Verasonics, Inc.  All world-wide rights and remedies under all intellectual property laws and industrial property laws are reserved.  Verasonics Registered U.S. Patent and Trademark Office.
%
% File name SetUpL11_4v_128RyLnsSA.m:
% Generate .mat Sequence Object file for L7-4 transducer using 128 ray lines 
% with 2-1 synthetic aperture (two pulses per line to acquire data from all
% 128 receive elements).  Of the 128 transmit channels, the active transmit 
% aperture is limited based on user-entered transmit focus and f-number.
%
% - The receive acquisitions use 100% bandwidth to improve DMA transfers.
%
% Last update 03-15-2013
% RF DATA MODIFICATIONS


clear all;
close all;


% Define system parameters.
Resource.Parameters.numTransmit = 128;      % number of transmit channels.
Resource.Parameters.numRcvChannels = 64;    % number of receive channels.
Resource.Parameters.speedOfSound = 1540;    % set speed of sound in m/sec before calling computeTrans
Resource.Parameters.simulateMode = 0;
%  Resource.Parameters.simulateMode = 1 forces simulate mode, even if hardware is present.
%  Resource.Parameters.simulateMode = 2 stops sequence and processes RcvData continuously.

% Specify Trans structure array.
Trans.name = 'L11-4v';
Trans.frequency = 5;  % frequency in megaHertz
Trans = computeTrans(Trans);  % L11-4v transducer is 'known' transducer so we can use computeTrans.
Trans.maxHighVoltage = 50;  % set maximum high voltage limit for pulser supply.

Filter=designfilt('bandpassfir',...
                  'filterorder',20,...
                  'CutoffFrequency1',0.8*Trans.frequency,...
                  'CutoffFrequency2',1.2*Trans.frequency,...
                  'SampleRate',4*Trans.frequency);

% lambda to mm converter
l2mm=Resource.Parameters.speedOfSound/Trans.frequency/1e3;

% Specify SFormat structure array.
nr = 128;                          % nr is number of raylines.
SFormat.transducer = 'L11-4v';     % 128 element linear array with 1.0 lambda spacing
SFormat.scanFormat = 'RLIN';       % rectangular linear array scan
SFormat.radius = 0;                % ROC for curved lin. or dist. to virt. apex
SFormat.theta = 0;
SFormat.numRays = nr;             % no. of Rays
SFormat.FirstRayLoc = [-Trans.spacing*Trans.numelements/2,0,0]; % x,y,z
SFormat.rayDelta = Trans.spacing;  % increment between rays (wvlnghts)
SFormat.startDepth =0;
SFormat.endDepth = 300;            % Acquisition depth in wavelengths

% Specify PData structure array.
PData.sFormat = 1;      % use first SFormat structure.
PData.pdeltaX = Trans.spacing;
PData.pdeltaZ = 0.25;
PData.Size(1) = ceil((SFormat.endDepth-SFormat.startDepth)/PData.pdeltaZ);
PData.Size(2) = ceil((Trans.numelements*Trans.spacing)/PData.pdeltaX);
PData.Size(3) = 1;      % single image page
PData.Origin = [-Trans.spacing*Trans.numelements/2,0,SFormat.startDepth]; % x,y,z of upper lft crnr

% Specify Media object.
pt1;
Media.function = 'movePoints';

% Specify Resources.
Resource.RcvBuffer.datatype = 'int16';
Resource.RcvBuffer.rowsPerFrame = 400*2*4*2*nr; % max depth of 384 * 2 for round trip * 4 smpls/wave * 2*nr acquisitions 
Resource.RcvBuffer.colsPerFrame = Resource.Parameters.numRcvChannels;
Resource.RcvBuffer.numFrames = 10;
Resource.InterBuffer.datatype = 'complex';
Resource.InterBuffer.numFrames = 1;         % one intermediate buffer needed for SA.
Resource.InterBuffer.rowsPerFrame = 2048;
Resource.InterBuffer.colsPerFrame = PData.Size(2);
Resource.ImageBuffer.datatype = 'double';
Resource.ImageBuffer.rowsPerFrame = 2048;
Resource.ImageBuffer.colsPerFrame = PData.Size(2);
Resource.ImageBuffer.numFrames = 10;
Resource.DisplayWindow.Title = 'Conventional Imaging';
Resource.DisplayWindow.pdelta = 0.5;
ScrnSize = get(0,'ScreenSize');
DwWidth = ceil(PData.Size(2)*PData.pdeltaX/Resource.DisplayWindow(1).pdelta);
DwHeight = ceil(PData.Size(1)*PData.pdeltaZ/Resource.DisplayWindow(1).pdelta);
Resource.DisplayWindow(1).Position = [250,0, ...  % lower left corner position
                                      DwWidth, DwHeight];
Resource.DisplayWindow(1).ReferencePt = [PData.Origin(1),PData.Origin(3)];   % 2D imaging is in the X,Z plane
Resource.DisplayWindow.Colormap = gray(256);
Resource.DisplayWindow.DynamicRange= 40; %dynamic range in dB

%Resource.VDAS.updateFunction ='updateVDASDumu';
% Specify TW structure array.
TW.type = 'parametric';
TW.Parameters = [18,17,2,1];
% Specify nr TX structure arrays. Transmit centered on element n in the array for event n.
txFocus = 50;  % Initial transmit focus.
TX = repmat(struct('waveform', 1, ...
                   'Origin', [0.0,0.0,0.0], ...
                   'focus', txFocus, ...
                   'NumEl', 0,...
                   'Steer', [0.0,0.0], ...
                   'Apod', zeros(1,Trans.numelements), ...
                   'Delay', zeros(1,Trans.numelements)), 1, size(txFocus,2)*nr);

% Determine TX aperture based on focal point and desired f number.
txFNum = 2;  % set to desired f-number value for transmit (range: 1.0 - 20)
   txNumEl=round((txFocus/txFNum)/Trans.spacing/2); % no. of elements in 1/2 aperture.
if txNumEl > (Trans.numelements/2 - 1) 
   txNumEl = floor(Trans.numelements/2 - 1); 
end   
% - Set event specific TX attributes.
for j = 1:SFormat.numRays  % 128 transmit events
    n= j;
    % Set transmit Origins to positions of elements.
    TX(n).Origin = SFormat.FirstRayLoc + (j-1)*Trans.spacing*[1,0,0];
    % Set transmit Apodization so (1 + 2*TXnumel) transmitters are active.
    lft = j - txNumEl; if lft < 1, lft = 1; end;
    rt = j + txNumEl;  if rt > Trans.numelements, rt = Trans.numelements; end;
    TX(n).Apod(lft:rt) = 1.0;
    TX(n).focus= txFocus;
    TX(n).Delay = computeTXDelays(TX(n));
    TX(n).NumEl=txNumEl;
end
% Specify Receive structure arrays. 
% - We need 2*nr Receives for every frame (synthetic aperture).
maxAcqLength = sqrt(SFormat.endDepth^2 + ((Trans.numelements-1)*Trans.spacing)^2) - SFormat.startDepth;
wlsPer128 = 128/(2*2); % wavelengths in 128 samples for 2 samplesPerWave
Receive = repmat(struct('Apod', zeros(1,Trans.numelements), ...
                        'startDepth', SFormat.startDepth, ...
                        'endDepth', SFormat.startDepth + wlsPer128*ceil(maxAcqLength/wlsPer128), ...
                        'TGC', 1, ...
                        'bufnum', 1, ...
                        'framenum', 1, ...
                        'acqNum', 1, ...
                        'samplesPerWave', 4,...
                        'mode', 0, ...
                        'InputFilter', Filter.Coefficients(1:2:11), ...  [0.0036,0.0127,0.0066,-0.0881,-0.2595,0.6494], ... 
                        'callMediaFunc', 0,...
                        'VDASClipMod',1), 1, 2*nr*Resource.RcvBuffer(1).numFrames);
                    
% - Set event specific Receive attributes.
for i = 1:Resource.RcvBuffer(1).numFrames
    k = 2*nr*(i-1);
    Receive(k+1).callMediaFunc = 1;
    for j = 1:2:2*SFormat.numRays
        Receive(k+j).Apod(1:Resource.Parameters.numRcvChannels) = 1.0;
        Receive(k+j).framenum = i;
        Receive(k+j).acqNum = j;      % two acquisitions per frame 
        Receive(k+j+1).Apod((Resource.Parameters.numRcvChannels+1):Trans.numelements) = 1.0;
        Receive(k+j+1).framenum = i;
        Receive(k+j+1).acqNum = j+1;  % two acquisitions per frame 
    end
end

% Specify TGC Waveform structure.
TGC.CntrlPts = log10(linspace(1,1000,8))/3*1000;%[500,590,650,710,770,830,890,950];
TGC.rangeMax = SFormat.endDepth;
TGC.Waveform = computeTGCWaveform(TGC);

% Specify Recon structure array. 
Recon = repmat(struct('senscutoff', 0.5, ...
               'pdatanum', 1, ...
               'rcvBufFrame',-1, ...
               'IntBufDest', [1,1], ...
               'ImgBufDest', [1,-1], ...
               'RINums', zeros(2,nr)),1 ,Resource.InterBuffer.numFrames);
% - Set RINums values for frame.

% Define ReconInfo structures.
ReconInfo = repmat(struct('mode', 3, ...  % replace IQ data first half SA.
                   'txnum', 1, ...
                   'rcvnum', 1, ...
                   'regionnum', 0), 1, 2*nr*Resource.InterBuffer.numFrames);
% - Set specific ReconInfo attributes.
for i = 1:Resource.InterBuffer.numFrames
    Recon(i).IntBufDest=[1 i];
    Recon(i).RINums(:)= (1:2*nr)+(i-1)*2*nr;
for j = 1:2:2*nr  % For each row in the column
    ReconInfo(j  +(i-1)*2*nr).mode = 3;   % replace IQ data
    ReconInfo(j  +(i-1)*2*nr).txnum = (j+1)/2;
    ReconInfo(j  +(i-1)*2*nr).rcvnum = j;
    ReconInfo(j  +(i-1)*2*nr).regionnum = (j+1)/2;
    ReconInfo(j  +(i-1)*2*nr).normWeight=0.5;
    ReconInfo(j+1+(i-1)*2*nr).mode = 5; % accum and detect
    ReconInfo(j+1+(i-1)*2*nr).txnum = (j+1)/2;
    ReconInfo(j+1+(i-1)*2*nr).rcvnum = j+1;
    ReconInfo(j+1+(i-1)*2*nr).regionnum = (j+1)/2;
    ReconInfo(j+1+(i-1)*2*nr).normWeight=0.5;
end
end
                     
Process =repmat(struct('classname','External',...
                       'method', 'StoreRaw',...
                       'Parameters',[]),Resource.RcvBuffer(1).numFrames+1,1);

Process(1).Parameters={'srcbuffer','receive',... % name of buffer to process.
                       'srcbufnum',1,...
                       'srcframenum',10,... % process  frame.
                       'dstbuffer','none'};   
                     
for i=2:Resource.RcvBuffer(1).numFrames
   
Process(i).Parameters={'srcbuffer','receive',... % name of buffer to process.
                       'srcbufnum',1,...
                       'srcframenum',i-1,... % process  frame.
                       'dstbuffer','none'};   

end
                           
Process(i+1).method = 'EDisplay';
Process(i+1).Parameters={'srcbuffer','image',... % name of buffer to process.
                         'srcbufnum',1,...
                         'srcframenum',-1,... % process last frame.
                         'dstbuffer','none'};                         
           
               
% Specify SeqControl structure arrays.
%  - Time between acquisitions in usec
t1 = 2*500/(Trans.frequency) + 20; % 44 msec at max Receive.endDepth.
%t1=120;
SeqControl(1).command = 'timeToNextAcq';
SeqControl(1).argument = t1;
%  - Time between frames at 10 fps at max Receive.endDepth.
FPS=8;
t2= round((1e+06-2*nr*t1*FPS)/FPS);
SeqControl(2).command = 'timeToNextAcq';
SeqControl(2).argument =t2;
%  - Return to Matlab
SeqControl(3).command = 'returnToMatlab';
%  - Jump back to start.
SeqControl(4).command = 'jump';
SeqControl(4).argument = 1;
nsc = 5; % next SeqControl number

% Specify Event structure arrays.
n = 1;
for i = 1:Resource.RcvBuffer(1).numFrames
    for j = 1:2:2*nr                      % Acquire frame
        Event(n).info = '1st half of aperture.';
        Event(n).tx = (j+1)/2;   % use next TX structure.
        Event(n).rcv = 2*nr*(i-1)+j;   
        Event(n).recon = 0;      % no reconstruction.
        Event(n).process = 0;    % no processing
        Event(n).seqControl = 1; % seqCntrl
        n = n+1;
        
        Event(n).info = '2nd half of aperture.';
        Event(n).tx = (j+1)/2;   % use next TX structure.
        Event(n).rcv = 2*nr*(i-1)+j+1;   
        Event(n).recon = 0;      % no Recon structure array.
        Event(n).process = 0;    % no processing
        Event(n).seqControl = 1; % seqCntrl
        n = n+1;
    end
    % Replace last events SeqControl for inter-frame timeToNextAcq.
    Event(n-2).process = i;
    Event(n-1).seqControl = 2;
    
    Event(n).info = 'Transfer frame to host.';
    Event(n).tx = 0;        % no TX
    Event(n).rcv = 0;       % no Rcv
    Event(n).recon = 0;     % no Recon
    Event(n).process = 0; 
    Event(n).seqControl = nsc; 
       SeqControl(nsc).command = 'transferToHost'; % transfer frame to host buffer
       nsc = nsc+1;
    n = n+1;

    Event(n).info = 'recon and EDisplay'; 
    Event(n).tx = 0;         % no transmit
    Event(n).rcv = 0;        % no rcv
    Event(n).recon = 1;      % reconstruction
    Event(n).process = Resource.RcvBuffer(1).numFrames+1;    % process
    Event(n).seqControl = 0;
    

    if floor(i/4) == i/4     % Exit to Matlab every 4th frame reconstructed 
        Event(n).seqControl = 3;
    else
        Event(n).seqControl = 0;
    end
    n = n+1;
end
 
Event(n).info = 'Jump back';
Event(n).tx = 0;        % no TX
Event(n).rcv = 0;       % no Rcv
Event(n).recon = 0;     % no Recon
Event(n).process = 0; 
Event(n).seqControl = 4;


% User specific variables

DIR='E:/ADQUISITIONS/PNEUMONIA/';
FILE=struct('PatientID',[],...
            'Directory',DIR,...
            'Date',[],...
            'RcvHandle',[],...
            'ImgHandle',[],...
            'Take',0,...
            'Cluster',0);
start_aq=0;
IQ_flag=0;

UIValues=struct('start',SFormat.startDepth,'range',SFormat.endDepth-SFormat.startDepth,...
                'nfocus',1,'focus_spacing',10,'zfocus',txFocus,...
                'DynRng',60,'Setpoint','max','pSetpoint',100,...
                'gamma',1,'reject',40,'pgain',1);


% User specified UI Control Elements
% - Sensitivity Cutoff
UI(1).Control =  {'UserB7','Style','VsSlider','Label','Sens. Cutoff',...
                  'SliderMinMaxVal',[0,1.0,Recon(1).senscutoff],...
                  'SliderStep',[0.025,0.1],'ValueFormat','%1.3f'};
UI(1).Callback = text2cell('%-UI#1Callback');

% - Z Range Change
UI(2).Control = {'UserA2','Style','VsSlider','Label','Start (mm)',...
                 'SliderMinMaxVal',round([UIValues.start,100,UIValues.start]*l2mm),...
                 'SliderStep',[0.1,0.2],'ValueFormat','%3.0f'};
UI(2).Callback = text2cell('%-UI#2Callback');

UI(3).Control = {'UserA1','Style','VsSlider','Label','Range (mm)',...
                 'SliderMinMaxVal',round([100,320,UIValues.range]*l2mm),...
                 'SliderStep',[0.1,0.2],'ValueFormat','%3.0f'};
UI(3).Callback = text2cell('%-UI#3Callback');

% - F number change
UI(4).Control = {'UserB6','Style','VsSlider','Label','F Number',...
                 'SliderMinMaxVal',[1,20,2],'SliderStep',[0.1,0.2],'ValueFormat','%2.0f'};
UI(4).Callback = text2cell('%-UI#4Callback');


% - Transmit focus change
UI(5).Control = {'UserB5','Style','VsSlider','Label','# Focus',...
                 'SliderMinMaxVal',[0,3,UIValues.nfocus],...
                 'SliderStep',[0.1,0.2],'ValueFormat','%2.0f'};
UI(5).Callback = text2cell('%-UI#5Callback');

UI(6).Control = {'UserB4','Style','VsSlider','Label','Focus depth (mm)',...
                 'SliderMinMaxVal',round([UIValues.start,UIValues.range,100]*l2mm),...
                 'SliderStep',[0.1,0.2],'ValueFormat','%3.0f'};
UI(6).Callback = text2cell('%-UI#6Callback');

UI(7).Control = {'UserB3','Style','VsSlider','Label','Focus spacing (mm)',...
                 'SliderMinMaxVal',[10,50,UIValues.focus_spacing],...
                 'SliderStep',[0.1,0.2],'ValueFormat','%2.0f'};
UI(7).Callback = text2cell('%-UI#7Callback');


% - Dynamic range display
UI(8).Control = {'UserC8','Style','VsSlider','Label','Dyn. Range (dB)',...
                 'SliderMinMaxVal',[15,145,UIValues.DynRng],...
                 'SliderStep',[0.1,0.2],'ValueFormat','%2.0f'};
UI(8).Callback = text2cell('%-UI#8Callback');

UI(9).Control = {'UserC7','Style','VsSlider','Label','Reject',...
                 'SliderMinMaxVal',[0,100,UIValues.reject],'SliderStep',[0.1,0.2],...
                 'ValueFormat','%3.0f'};
UI(9).Callback = text2cell('%-UI#9Callback');

% UI(9).Control = {'UserC7','Style','VsSlider','Label',' % of Set point',...
%                  'SliderMinMaxVal',[1,500,UIValues.pSetpoint],'SliderStep',[0.1,0.2],...
%                  'ValueFormat','%3.0f'};
% UI(9).Callback = text2cell('%-UI#9Callback');
% 
% UI(10).Control = {'UserC6','Style','VsButtonGroup','Title','Set point',...
%                   'NumButtons',3,'Labels',{'Max','Mean','Median'}};                 
% UI(10).Callback = text2cell('%-UI#10Callback');

UI(10).Control = {'UserC6','Style','VsSlider','Label','Gamma',...
                 'SliderMinMaxVal',[0.1,10,UIValues.gamma],'SliderStep',[0.01,0.02],...
                 'ValueFormat','%2.2f'};
UI(10).Callback = text2cell('%-UI#10Callback');


UI(11).Control = {'UserC5','Style','VsSlider','Label','Gain',...
                 'SliderMinMaxVal',[1,100,UIValues.pgain],'SliderStep',[0.01,0.2],...
                 'ValueFormat','%3.0f'};
UI(11).Callback = text2cell('%-UI#11Callback');

% - Control Buttons

UI(12).Control = {'UserB2','Style','VsPushButton','Label','New'};
UI(12).Callback = text2cell('%-UI#12Callback');

UI(13).Control = {'UserB1','Style','VsToggleButton','Label','Acquire'};
UI(13).Callback = text2cell('%-UI#13Callback');



% - External Function Definition
EF(1).Function = text2cell('%EF#1');
EF(2).Function = text2cell('%EF#2');
% Specify factor for converting sequenceRate to frameRate.
frameRateFactor = 4;

% Save all the structures to a .mat file.
save('RawData.mat'); 
%clear all;
return


% **** Callback routines to be converted by text2cell function. ****
%-UI#1Callback - Sensitivity cutoff change
ReconL= evalin('base', 'Recon');
for i = 1:size(ReconL,2)
    ReconL(i).senscutoff = UIValue;
end
assignin('base','Recon',ReconL);
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'Recon'};
assignin('base','Control', Control);
return
%-UI#1Callback

%-UI#2Callback - Start change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No range change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','SFormat.endDepth'));
    return
end
l2mm=evalin('base','l2mm');
start = UIValue/l2mm;
SFormat = evalin('base','SFormat');
range=SFormat.endDepth-SFormat.startDepth;
SFormat.startDepth = start;
SFormat.endDepth=SFormat.startDepth+range;
assignin('base','SFormat',SFormat);
evalin('base','PData.Size(1) = ceil((SFormat.endDepth-SFormat.startDepth)/PData.pdeltaZ);');
evalin('base','[PData.Region,PData.numRegions] = createRegions(PData);');
evalin('base','Resource.DisplayWindow(1).Position(4) = ceil(PData.Size(1)*PData.pdeltaZ/Resource.DisplayWindow(1).pdelta);');
Receive = evalin('base', 'Receive');
Trans = evalin('base', 'Trans');
maxAcqLength = sqrt(SFormat.endDepth^2 + (Trans.numelements*Trans.spacing)^2)-SFormat.startDepth;
wlsPer128 = 128/(4*2);
for i = 1:size(Receive,2)
    Receive(i).endDepth = SFormat.startDepth + wlsPer128*ceil(maxAcqLength/wlsPer128);
end
assignin('base','Receive',Receive);
evalin('base','TGC.rangeMax = SFormat.endDepth;');
evalin('base','TGC.Waveform = computeTGCWaveform(TGC);');
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'SFormat','PData','Receive','Recon','DisplayWindow','ImageBuffer'};
assignin('base','Control', Control);
assignin('base', 'action', 'displayChange');

return
%-UI#2Callback

%-UI#3Callback - Range change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No range change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','SFormat.endDepth'));
    return
end
l2mm=evalin('base','l2mm');
range = UIValue/l2mm;
%assignin('base','range',range);
SFormat = evalin('base','SFormat');
SFormat.endDepth = SFormat.startDepth+range;
assignin('base','SFormat',SFormat);
evalin('base','PData.Size(1) = ceil((SFormat.endDepth-SFormat.startDepth)/PData.pdeltaZ);');
evalin('base','[PData.Region,PData.numRegions] = createRegions(PData);');
evalin('base','Resource.DisplayWindow(1).Position(4) = ceil(PData.Size(1)*PData.pdeltaZ/Resource.DisplayWindow(1).pdelta);');
Receive = evalin('base', 'Receive');
Trans = evalin('base', 'Trans');
maxAcqLength = sqrt(SFormat.endDepth^2 + (Trans.numelements*Trans.spacing)^2)-SFormat.startDepth;
wlsPer128 = 128/(4*2);
for i = 1:size(Receive,2)
    Receive(i).endDepth = SFormat.startDepth + wlsPer128*ceil(maxAcqLength/wlsPer128);
end
assignin('base','Receive',Receive);
evalin('base','TGC.rangeMax = SFormat.endDepth;');
evalin('base','TGC.Waveform = computeTGCWaveform(TGC);');
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'SFormat','PData','Receive','Recon','DisplayWindow','ImageBuffer'};
assignin('base','Control', Control);
assignin('base', 'action', 'displayChange');

return
%-UI#3Callback

%-UI#4Callback - F number change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No F number change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','txFNum'));
    return
end
txFNum = UIValue;
assignin('base','txFNum',txFNum);
% Determine TX aperture based on focal point and desired f number.
Trans = evalin('base', 'Trans');
txFocus = evalin('base', 'txFocus'); % get txFocus value for transmit
% txNumEl is the number of elements to include on each side of the center element, for the specified
%    focus and sensitivity cutoff.  Thus the full transmit aperture will be 2*txNumEl + 1 elements.
txNumEl = round((txFocus/txFNum)/Trans.spacing/2); % no. of elements in 1/2 aperture.
if txNumEl > (Trans.numelements/2 - 1), txNumEl = floor(Trans.numelements/2 - 1); end  
assignin('base','txNumEl',txNumEl);
% - Redefine event specific TX attributes for the new focus.
TX = evalin('base', 'TX');
for n = 1:128   % 128 transmit events
    % Set Apod vector back to all zeros
    TX(n).Apod(:) = 0;
    % write new focus value to TX
    TX(n).focus = txFocus;
    % Set transmit Apodization so (1 + 2*TXnumel) transmitters are active.
    lft = n - txNumEl;
    if lft < 1, lft = 1; end;
    rt = n + txNumEl;
    if rt > Trans.numelements, rt = Trans.numelements; end;
    TX(n).Apod(lft:rt) = 1.0;
    TX(n).Delay = computeTXDelays(TX(n));
end
assignin('base','TX', TX);
% Set Control command to update TX
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'TX'};
assignin('base','Control', Control);
return
%-UI#4Callback

%-UI#5Callback - # focus change
return
%-UI#5Callback 

%-UI#6Callback - TX focus change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No focus change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','txFocus'));
    return
end
l2mm=evalin('base','l2mm');
txFocus = UIValue/l2mm;
assignin('base','txFocus',txFocus);
% Determine TX aperture based on focal point and desired f number.
Trans = evalin('base', 'Trans');
txFNum = evalin('base', 'txFNum'); % get f-number value for transmit (range: 1.0 - 20)
% txNumEl is the number of elements to include on each side of the center element, for the specified
%    focus and sensitivity cutoff.  Thus the full transmit aperture will be 2*txNumEl + 1 elements.
txNumEl = round((txFocus/txFNum)/Trans.spacing/2); % no. of elements in 1/2 aperture.
if txNumEl > (Trans.numelements/2 - 1), txNumEl = floor(Trans.numelements/2 - 1); end  
assignin('base','txNumEl',txNumEl);
% - Redefine event specific TX attributes for the new focus.
TX = evalin('base', 'TX');
for n = 1:128   % 128 transmit events
    % Set Apod vector back to all zeros
    TX(n).Apod(:) = 0;
    % write new focus value to TX
    TX(n).focus = txFocus;
    % Set transmit Apodization so (1 + 2*TXnumel) transmitters are active.
    lft = n - txNumEl;
    if lft < 1, lft = 1; end;
    rt = n + txNumEl;
    if rt > Trans.numelements, rt = Trans.numelements; end;
    TX(n).Apod(lft:rt) = 1.0;
    TX(n).Delay = computeTXDelays(TX(n));
end
assignin('base','TX', TX);
% Set Control command to update TX
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'TX'};
assignin('base','Control', Control);
return
%-UI#6Callback

%-UI#7Callback - Focus Spacing change
return
%-UI#7Callback

%-UI#8Callback - Set Dynamic Range
Resource= evalin('base', 'Resource');
UIValues= evalin('base','UIValues');
Resource.DisplayWindow.DynamicRange = UIValue;
UIValues.DynRng=UIValue;
assignin('base', 'UIValues', UIValues);  
assignin('base', 'Resource', Resource);  
return
%-UI#8Callback

%-UI#9Callback - pSetpoint change
UIValues= evalin('base','UIValues');
UIValues.pSetpoint=UIValue;
assignin('base', 'UIValues', UIValues);  
return
%-UI#9Callback

%-UI#10Callback - Setpoint change
UIValues= evalin('base','UIValues');
switch UIState
    case 1, UIValues.Setpoint='max';
    case 2, UIValues.Setpoint='mean';
    case 3, UIValues.Setpoint='median';
end;
assignin('base', 'UIValues', UIValues);  
return
%-UI#10Callback

%-UI#11Callback - New Pacient
flag_start = evalin('base', 'start_aq');
if (flag_start==0)
FILE = evalin('base', 'FILE');
FILE.PatientID=round(rem(now,1)*100000);
FILE.Date=  datestr(now);
FILE.Take=0;   
FILE.Cluster=0;  
assignin('base','FILE', FILE);
end    
return
%-UI#11Callback

%-UI#12Callback - Start acquisition
flag_start = evalin('base', 'start_aq');

if flag_start==0
   FILE = evalin('base', 'FILE');
   
   FILE.Take=FILE.Take+1;
   
   FILE.RcvHandle=fopen([FILE.Directory ...
       sprintf('RCV%dTK%dCL%d',FILE.PatientID,FILE.Take,FILE.Cluster)],'wb');
   FILE.ImgHandle=fopen([FILE.Directory ...
       sprintf('IMG%dTK%dCL%d',FILE.PatientID,FILE.Take,FILE.Cluster)],'wb');
   
   assignin('base','FILE', FILE); 
   save([FILE.Directory sprintf('HEAD%dTK%d.mat',FILE.PatientID,FILE.Take)],'FILE');
   flag_start=1;
else
    flag_start=0;
    fclose('all');
end
assignin('base','start_aq', flag_start);
return
%-UI#12Callback

%-UI#13Callback - gamma change
UIValues= evalin('base','UIValues');
UIValues.gamma=UIValue;
assignin('base', 'UIValues', UIValues);  
return
%-UI#13Callback

%-UI#14Callback - gamma change
UIValues= evalin('base','UIValues');
UIValues.reject=UIValue;
assignin('base', 'UIValues', UIValues);  
return
%-UI#14Callback

%-UI#15Callback - gamma change
UIValues= evalin('base','UIValues');
UIValues.pgain=UIValue;
assignin('base', 'UIValues', UIValues);  
return
%-UI#15Callback


%EF#1
EDisplay(RData)
persistent axesHandle
Resource = evalin('base', 'Resource');
PData=evalin('base', 'PData');
SFormat=evalin('base', 'SFormat');
Trans=evalin('base', 'Trans');
UIValues=evalin('base','UIValues');

l2mm=Resource.Parameters.speedOfSound/Trans.frequency/1e3;
nticks=5;

Bmode=UIValues.pgain*RData(1:PData.Size(1),1:PData.Size(2))*(2^15-1)/(2^22-1);

Bmode=(20*log10(Bmode)-UIValues.reject)/UIValues.DynRng;
Bmode(Bmode<0)=0;Bmode(Bmode>1)=1;
Bmode=medfilt2(Bmode,[8 1]);
Bmode=imfilter(Bmode,fspecial('average',[1 8]));
Bmode=(imsharpen(Bmode,'Amount',2,'Radius',0.1','Threshold',0.2));

CMAP= repmat(((0:1/64:1).^UIValues.gamma)',[1 3]);

dB=[0     1]*UIValues.DynRng;

% Create the figure if it doesn�t exist.
 if isempty(axesHandle)||~ishandle(axesHandle)
 axesHandle=imgca(Resource.DisplayWindow.figureHandle);
 end
% Plot the RF data.
set(Resource.DisplayWindow.imageHandle,'cdata',Bmode);
set(Resource.DisplayWindow.imageHandle,'CDataMapping','scaled');
set(Resource.DisplayWindow.figureHandle,'Colormap',CMAP);
set(axesHandle,'CLim',[0 1]);
Yticklabel=num2str(round(l2mm*get(axesHandle,'Ytick')'));
Xticklabel=num2str(round(l2mm*get(axesHandle,'Xtick')'));
set(axesHandle,'Yticklabel',Yticklabel);
set(axesHandle,'Xticklabel',Xticklabel);
ylabel(axesHandle,'depth (mm)');
xlabel(axesHandle,'Azimuth (mm)');


% store image
 flag_start = evalin('base', 'start_aq'); 
 if flag_start==1
     FILE = evalin('base', 'FILE');
     Receive = evalin('base', 'Receive(1)');
     TX.NumEl= evalin('base', 'txNumEl');
     TX.FNum= evalin('base', 'txFNum');
     
    count= fwrite(FILE.ImgHandle,Bmode,'double');    
    save([FILE.Directory sprintf('HEAD%dTK%d.mat',FILE.PatientID,FILE.Take)],...
       'Resource',...
       'PData',...
       'SFormat',...
       'Receive',...
       'Trans',...
       'TX',...
       'UIValues',...
       '-append');


 end
return
%EF#1

%EF#2
StoreRaw(RData)

flag_start = evalin('base', 'start_aq');
if flag_start==1
    FILE = evalin('base', 'FILE'); 
        tic
    count= fwrite(FILE.RcvHandle,RData,'int16');    
        toc;
end

return
%EF#2


