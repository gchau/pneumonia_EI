% Copyright 2001-2013 Verasonics, Inc.  All world-wide rights and remedies under all intellectual property laws and industrial property laws are reserved.  Verasonics Registered U.S. Patent and Trademark Office.
%
% File name SetUpP4_2_128RyLns.m:
% Generate .mat Sequence Object file for P4-2 phased array virtual apex format,
% focused transmit scan with multiple raylines.
%  - The P4-2 is a 64 element probe that is wired to the scanhead connector with
%    elements 0-31 connected to inputs 1-32, and elements 32-63 connected
%    to inputs 97-128.  We therefore need a Trans.Connector array to specify the
%    connector channels used, which will be defined by the computeTrans function.
% This sequence uses 100% bandwidth receive data to reduce DMA transfer time.
%
% Last update: 03/24/2013

clear all

m = 128; % no. of ray lines in scan.

% Specify system parameters.
Resource.Parameters.numTransmit = 128;  % number of transmit channels.
Resource.Parameters.numRcvChannels = 64;  % number of receive channels.
Resource.Parameters.speedOfSound = 1540;
Resource.Parameters.speedCorrectionFactor = 1.0;
Resource.Parameters.simulateMode = 0;
%  Resource.Parameters.simulateMode = 1 forces simulate mode, even if hardware is present.
%  Resource.Parameters.simulateMode = 2 stops sequence and processes RcvData continuously.

% Specify Trans structure array.
Trans.name = 'P4-2v';
Trans = computeTrans(Trans);
Trans.maxHighVoltage = 50;  % set maximum high voltage limit for pulser supply.


Filter=designfilt('bandpassfir',...
                  'filterorder',20,...
                  'CutoffFrequency1',0.8*Trans.frequency,...
                  'CutoffFrequency2',1.2*Trans.frequency,...
                  'SampleRate',4*Trans.frequency);
              
% Set up SFormat structure array.
aperture = 64*Trans.spacing; % aperture based on 64 elements
SFormat.transducer = 'P4-2';
SFormat.scanFormat = 'VAPX';
SFormat.theta = -pi/4;
SFormat.radius = (aperture/2)/tan(-SFormat.theta); % dist. to virt. apex
SFormat.numRays = m;      % no. of Rays (1 for Flash transmit)
SFormat.FirstRayLoc = Trans.ElementPos(1,1:3);   % x,y,z
SFormat.rayDelta = 2*(-SFormat.theta)/(m-1);  % spacing in radians(sector) or dist. between rays
SFormat.startDepth = 0;
SFormat.endDepth = 160;   % Acquisition depth in wavelengths

% Set up PData structure.
PData.sFormat = 1;
PData.pdeltaX = 0.875;
PData.pdeltaZ = 0.125;
PData.Size(1) = 10 + ceil((SFormat.endDepth-SFormat.startDepth)/PData.pdeltaZ);
PData.Size(2) = 10 + ceil(2*(SFormat.endDepth + SFormat.radius)*sin(-SFormat.theta)/PData.pdeltaX);
PData.Size(3) = 1;
PData.Origin = [-(PData.Size(2)/2)*PData.pdeltaX,0,0];

% Specify Media.  Use point targets in middle of PData.
% Set up Media points
pt1;
Media.function = 'movePoints';

% Specify Resources.
Resource.RcvBuffer(1).datatype = 'int16';
Resource.RcvBuffer(1).rowsPerFrame = 2*2048*m; % This is for the max range on range slider. 
Resource.RcvBuffer(1).colsPerFrame = Resource.Parameters.numRcvChannels;
Resource.RcvBuffer(1).numFrames = 8;  
Resource.InterBuffer(1).datatype = 'complex';
Resource.InterBuffer(1).numFrames = 1;  % 1 frame defined but no intermediate buffer needed.
Resource.InterBuffer(1).rowsPerFrame = 2048;
Resource.InterBuffer(1).colsPerFrame = PData.Size(2);
Resource.ImageBuffer(1).datatype = 'double';
Resource.ImageBuffer(1).rowsPerFrame = 2048; % this is for maximum depth
Resource.ImageBuffer(1).colsPerFrame = PData.Size(2);
Resource.ImageBuffer(1).numFrames = 10;
Resource.DisplayWindow(1).Title = 'Image Display';
Resource.DisplayWindow(1).pdelta = 0.35;
Resource.DisplayWindow(1).Position = [200,200, ...
    ceil(PData.Size(2)*(PData.pdeltaX/Resource.DisplayWindow(1).pdelta)), ...
    ceil(PData.Size(1)*PData.pdeltaZ/Resource.DisplayWindow(1).pdelta)];    % height
Resource.DisplayWindow(1).ReferencePt = [PData.Origin(1),PData.Origin(3)]; % 2D imaging is in the X,Z plane
Resource.DisplayWindow(1).Colormap = gray(256);

% Specify TW structure array.
TW.type = 'parametric';
TW.Parameters = [36,17,2,1];   % A, B, C, D
txFocus=300;
% Specify TX structure array.
TX = repmat(struct('waveform', 1, ...
                   'Origin', [0.0,0.0,0.0], ...
                   'focus', txFocus, ...
                   'Steer', [0.0,0.0], ...
                   'Apod', ones(1,64), ...  % set TX.Apod for 64 elements
                   'Delay', zeros(1,64)), 1, m);
% - Set event specific TX attributes.
Angles = SFormat.theta:SFormat.rayDelta:(SFormat.theta + (m-1)*SFormat.rayDelta);
TXorgs = SFormat.radius*tan(Angles);
for n = 1:m   % m transmit events
    TX(n).Origin = [TXorgs(n),0.0,0.0];
    TX(n).Steer = [Angles(n),0.0];
    TX(n).Delay = computeTXDelays(TX(n));
end
%clear Angles TXorgs

% Specify Receive structure arrays. 
% - We need m Receives for each frame.
% -- Compute the maximum receive path length, using the law of cosines.
maxAcqLength = sqrt(aperture^2 + SFormat.endDepth^2 - 2*aperture*SFormat.endDepth*cos(SFormat.theta-pi/2)) - SFormat.startDepth;
wlsPer128 = 128/(2*2); % wavelengths in 128 samples for 2 samplesPerWave
Receive = repmat(struct('Apod', ones(1,64), ...
                        'startDepth', SFormat.startDepth, ...
                        'endDepth', SFormat.startDepth + wlsPer128*ceil(maxAcqLength/wlsPer128), ...
                        'TGC', 1, ...
                        'bufnum', 1, ...
                        'framenum', 1, ...
                        'acqNum', 1, ...
                        'samplesPerWave', 4, ...
                        'mode', 0, ...
                        'InputFilter', Filter.Coefficients(1:2:11),...[0.0036,0.0127,0.0066,-0.0881,-0.2595,0.6494], ...
                        'callMediaFunc', 0),1,m*Resource.RcvBuffer(1).numFrames);
% - Set event specific Receive attributes.
for i = 1:Resource.RcvBuffer(1).numFrames
    Receive(m*(i-1)+1).callMediaFunc = 1;
    for j = 1:m
        Receive(m*(i-1)+j).framenum = i;
        Receive(m*(i-1)+j).acqNum = j; 
    end
end

% Specify TGC Waveform structure.
TGC.CntrlPts = [450,550,650,710,770,830,890,950];
TGC.rangeMax = SFormat.endDepth;
TGC.Waveform = computeTGCWaveform(TGC);

% Specify Recon structure array.
Recon = struct('senscutoff', 0.5, ...
               'pdatanum', 1, ...
               'rcvBufFrame',-1, ...
               'IntBufDest', [1,1], ...
               'ImgBufDest', [1,-1], ...
               'RINums', 1:m);

% Define ReconInfo structures.
ReconInfo = repmat(struct('mode', 0, ...  % replace data.
                   'txnum', 1, ...
                   'rcvnum', 1, ...
                   'regionnum', 0), 1, m);
% - Set specific ReconInfo attributes.
for i = 1:m
    ReconInfo(i).txnum = i;
    ReconInfo(i).rcvnum = i;
    ReconInfo(i).regionnum = i;
end
    

% Specify Process structure array.
% pers = 10;
% Process(1).classname = 'Image';
% Process(1).method = 'imageDisplay';
% Process(1).Parameters = {'imgbufnum',1,...   % number of buffer to process.
%                          'framenum',-1,...   % (-1 => lastFrame)
%                          'pdatanum',1,...    % number of PData structure to use
%                          'norm',1,...        % normalization method(1 means fixed)
%                          'pgain',0.5,...            % pgain is image processing gain
%                          'persistMethod','none',...
%                          'persistLevel',pers,...
%                          'interp',1,...      % method of interpolation (1=4pt interp)
%                          'compression',0.5,...      % X^0.5 normalized to output word size
%                          'reject',2,...
%                          'mappingMode','full',...
%                          'display',1,...     % display image after processing
%                          'displayWindow',1};
Process =repmat(struct('classname','External',...
                       'method', 'StoreRaw',...
                       'Parameters',[]),1,1);
Process(1).method = 'EDisplay';
Process(1).Parameters={'srcbuffer','image',... % name of buffer to process.
                         'srcbufnum',1,...
                         'srcframenum',-1,... % process last frame.
                         'dstbuffer','none'};   
% Specify SeqControl structure arrays.
t1 = 2*354*.4 + 20; % ray line acquisition time for worst case range in usec
t2 = round((1e+06-128*t1*25)/25);   % Time between frames at 30 fps.
SeqControl(1).command = 'jump'; %  - Jump back to start.
SeqControl(1).argument = 1;
SeqControl(2).command = 'timeToNextAcq';  % set time between rays
SeqControl(2).argument = t1; 
SeqControl(3).command = 'timeToNextAcq';  % set time between frames
SeqControl(3).argument = t2;
SeqControl(4).command = 'returnToMatlab';
nsc = 5;

% Specify Event structure arrays.
n = 1;
for i = 1:Resource.RcvBuffer(1).numFrames
    for j = 1:m                      % Acquire rays
        Event(n).info = 'Acquire ray line';
        Event(n).tx = j; 
        Event(n).rcv = m*(i-1)+j;   
        Event(n).recon = 0;      % no reconstruction.
        Event(n).process = 0;    % no processing
        Event(n).seqControl = 2; % time between rays
        n = n+1;
    end
    Event(n-1).seqControl = [3,nsc]; % Replace last event's seqControl for frame time and transferToHost.
       SeqControl(nsc).command = 'transferToHost'; % transfer frame to host buffer
       nsc = nsc+1;

    Event(n).info = 'recon and process'; 
    Event(n).tx = 0;         % no transmit
    Event(n).rcv = 0;        % no rcv
    Event(n).recon = 1;      % reconstruction
    Event(n).process = 1;    % process
    if floor(i/3) == i/3     % Exit to Matlab every 3rd frame reconstructed 
        Event(n).seqControl = 4;
    else
        Event(n).seqControl = 0;
    end
    n = n+1;
end

Event(n).info = 'Jump back';
Event(n).tx = 0;        % no TX
Event(n).rcv = 0;       % no Rcv
Event(n).recon = 0;     % no Recon
Event(n).process = 0; 
Event(n).seqControl = 1;

nfocus=1;
% User specified UI Control Elements
% - Sensitivity Cutoff
if ~exist('presets.mat','file')
UIValues=struct('start',SFormat.startDepth,'range',SFormat.endDepth,...
                'nfocus',nfocus,'focus_spacing',10,'zfocus',txFocus(1),...
                'DynRng',60,'gamma',1,'reject',40,'pgain',1);
else
    load('presets.mat');
end

l2mm=Resource.Parameters.speedOfSound/Trans.frequency/1e3;

% User specified UI Control Elements
% - Sensitivity Cutoff
UI(1).Control =  {'UserB7','Style','VsSlider','Label','Sens. Cutoff',...
                  'SliderMinMaxVal',[0,1.0,Recon(1).senscutoff],...
                  'SliderStep',[0.025,0.1],'ValueFormat','%1.3f'};
UI(1).Callback = text2cell('%-UI#1Callback');

% - Z Range Change
UI(2).Control = {'UserA2','Style','VsSlider','Label','Start (mm)',...
                 'SliderMinMaxVal',round([UIValues.start,100,UIValues.start]*l2mm),...
                 'SliderStep',[0.1,0.2],'ValueFormat','%3.0f'};
UI(2).Callback = text2cell('%-UI#2Callback');

UI(3).Control = {'UserA1','Style','VsSlider','Label','Range (mm)',...
                 'SliderMinMaxVal',round([100,320,UIValues.range]*l2mm),...
                 'SliderStep',[0.1,0.2],'ValueFormat','%3.0f'};
UI(3).Callback = text2cell('%-UI#3Callback');

% - F number change
UI(4).Control = {'UserB6','Style','VsSlider','Label','F Number',...
                 'SliderMinMaxVal',[1,20,2],'SliderStep',[0.1,0.2],'ValueFormat','%2.0f'};
UI(4).Callback = text2cell('%-UI#4Callback');


% - Transmit focus change
UI(5).Control = {'UserB5','Style','VsSlider','Label','# Focus',...
                 'SliderMinMaxVal',[0,3,UIValues.nfocus],...
                 'SliderStep',[0.1,0.2],'ValueFormat','%2.0f'};
UI(5).Callback = text2cell('%-UI#5Callback');

UI(6).Control = {'UserB4','Style','VsSlider','Label','Focus depth (mm)',...
                 'SliderMinMaxVal',round([UIValues.start,UIValues.range,100]*l2mm),...
                 'SliderStep',[0.1,0.2],'ValueFormat','%3.0f'};
UI(6).Callback = text2cell('%-UI#6Callback');

UI(7).Control = {'UserB3','Style','VsSlider','Label','Focus spacing (mm)',...
                 'SliderMinMaxVal',[10,50,UIValues.focus_spacing],...
                 'SliderStep',[0.1,0.2],'ValueFormat','%2.0f'};
UI(7).Callback = text2cell('%-UI#7Callback');


% - Dynamic range display
UI(8).Control = {'UserC8','Style','VsSlider','Label','Dyn. Range (dB)',...
                 'SliderMinMaxVal',[15,145,UIValues.DynRng],...
                 'SliderStep',[0.1,0.2],'ValueFormat','%2.0f'};
UI(8).Callback = text2cell('%-UI#8Callback');

UI(9).Control = {'UserC7','Style','VsSlider','Label','Reject',...
                 'SliderMinMaxVal',[0,100,UIValues.reject],'SliderStep',[0.1,0.2],...
                 'ValueFormat','%3.0f'};
UI(9).Callback = text2cell('%-UI#9Callback');

UI(10).Control = {'UserC6','Style','VsSlider','Label','Gamma',...
                 'SliderMinMaxVal',[0.1,10,UIValues.gamma],'SliderStep',[0.01,0.02],...
                 'ValueFormat','%2.2f'};
UI(10).Callback = text2cell('%-UI#10Callback');


UI(11).Control = {'UserC5','Style','VsSlider','Label','Gain',...
                 'SliderMinMaxVal',[1,100,UIValues.pgain],'SliderStep',[0.01,0.2],...
                 'ValueFormat','%3.0f'};
UI(11).Callback = text2cell('%-UI#11Callback');

% - Control Buttons

UI(12).Control = {'UserB2','Style','VsPushButton','Label','New'};
UI(12).Callback = text2cell('%-UI#12Callback');

UI(13).Control = {'UserB1','Style','VsToggleButton','Label','Acquire'};
UI(13).Callback = text2cell('%-UI#13Callback');

UI(14).Control = {'UserC4','Style','VsPushButton','Label','Save Preset'};
UI(14).Callback = text2cell('%-UI#14Callback');

UI(15).Control = {'UserC3','Style','VsPushButton','Label','Load Preset'};
UI(15).Callback = text2cell('%-UI#15Callback');


% - External Function Definition
EF(1).Function = text2cell('%EF#1');
EF(2).Function = text2cell('%EF#2');


% Specify factor for converting sequenceRate to frameRate.
frameRateFactor = 4;

% Save all the structures to a .mat file.
save('P4-2_128RyLns'); 

return


% **** Callback routines to be converted by text2cell function. ****
% **** Callback routines to be converted by text2cell function. ****
%-UI#1Callback - Sensitivity cutoff change
ReconL= evalin('base', 'Recon');
for i = 1:size(ReconL,2)
    ReconL(i).senscutoff = UIValue;
end
assignin('base','Recon',ReconL);
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'Recon'};
assignin('base','Control', Control);
return
%-UI#1Callback

%-UI#2Callback - Start change
global first;
simMode = evalin('base','Resource.Parameters.simulateMode');
% No range change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','SFormat.endDepth'));
    return
end
l2mm=evalin('base','l2mm');
startDepth = round(UIValue/l2mm);
SFormat = evalin('base','SFormat');
SFormat.startDepth = startDepth;
assignin('base','SFormat',SFormat);
RangeChange;
first=0;
return
%-UI#2Callback

%-UI#3Callback - End change
global first;
simMode = evalin('base','Resource.Parameters.simulateMode');
% No range change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','SFormat.endDepth'));
    return
end
l2mm=evalin('base','l2mm');
endDepth = round(UIValue/l2mm);
SFormat = evalin('base','SFormat');
SFormat.endDepth = endDepth;
assignin('base','SFormat',SFormat);
RangeChange;
first=0;
return
%-UI#3Callback

%-UI#4Callback - F number change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No F number change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','txFNum'));
    return
end
txFNum = UIValue;
assignin('base','txFNum',txFNum);
UpdateTX;
% Set Control command to update TX
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'TX'};
assignin('base','Control', Control);
return
%-UI#4Callback

%-UI#5Callback - # focus change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No focus change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','txFocus'));
    return
end
l2mm=evalin('base','l2mm');

UIValues= evalin('base','UIValues');
UIValues.nfocus = UIValue;
assignin('base','UIValues',UIValues);
assignin('base','nfocus',UIValue);
txFocus=createFocus(UIValues.zfocus,UIValues.nfocus,UIValues.focus_spacing);
assignin('base','txFocus',txFocus);
UpdateTX;
UpdateReceive;
% Set Control command to update TX
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'TX','Receive'};
assignin('base','Control', Control);
return
%-UI#5Callback 

%-UI#6Callback - TX focus change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No focus change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','txFocus'));
    return
end
l2mm=evalin('base','l2mm');

UIValues= evalin('base','UIValues');
UIValues.zfocus = round(UIValue/l2mm);
assignin('base','UIValues',UIValues);
txFocus=createFocus(UIValues.zfocus,UIValues.nfocus,UIValues.focus_spacing);
assignin('base','txFocus',txFocus);
UpdateTX;
UpdateReceive;
% Set Control command to update TX
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'TX','Receive'};
assignin('base','Control', Control);
return
%-UI#6Callback

%-UI#7Callback - Focus Spacing change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No focus change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','txFocus'));
    return
end
l2mm=evalin('base','l2mm');

UIValues= evalin('base','UIValues');
UIValues.focus_spacing = round(UIValue/l2mm);
assignin('base','UIValues',UIValues);
txFocus=createFocus(UIValues.zfocus,UIValues.nfocus,UIValues.focus_spacing);
assignin('base','txFocus',txFocus);
UpdateTX;
UpdateReceive;
% Set Control command to update TX
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'TX','Receive'};
assignin('base','Control', Control);
return
%-UI#7Callback

%-UI#8Callback - Set Dynamic Range
UIValues= evalin('base','UIValues');
UIValues.DynRng=UIValue;
assignin('base', 'UIValues', UIValues); 
UpdateScreen('DynRng',UIValue);
return
%-UI#8Callback

%-UI#9Callback - reject change
UIValues= evalin('base','UIValues');
UIValues.reject=UIValue;
assignin('base', 'UIValues', UIValues);
UpdateScreen('reject',UIValue);
return
%-UI#9Callback

%-UI#10Callback - gamma change
UIValues= evalin('base','UIValues');
UIValues.gamma=UIValue;
assignin('base', 'UIValues', UIValues);  
UpdateScreen('gamma',UIValue);
return
%-UI#10Callback

%-UI#11Callback - pgain change
UIValues= evalin('base','UIValues');
UIValues.pgain=UIValue;
assignin('base', 'UIValues', UIValues);
%UpdateScreen('pgain',UIValue);
return
%-UI#11Callback


%-UI#12Callback - New Pacient
flag_start = evalin('base', 'start_aq');
if (flag_start==0)
FILE = evalin('base', 'FILE');
FILE.PatientID=round(rem(now,1)*100000);
FILE.Date=  datestr(now);
FILE.Take=0;   
FILE.Cluster=0;  
assignin('base','FILE', FILE);
end    
return
%-UI#12Callback

%-UI#13Callback - Start acquisition
flag_start = evalin('base', 'start_aq');

if flag_start==0
   FILE = evalin('base', 'FILE');
   
   FILE.Take=FILE.Take+1;
   
   FILE.RcvHandle=fopen([FILE.Directory ...
       sprintf('RCV%dTK%dCL%d',FILE.PatientID,FILE.Take,FILE.Cluster)],'wb');
   FILE.ImgHandle=fopen([FILE.Directory ...
       sprintf('IMG%dTK%dCL%d',FILE.PatientID,FILE.Take,FILE.Cluster)],'wb');
   
   assignin('base','FILE', FILE); 
   save([FILE.Directory sprintf('HEAD%dTK%d.mat',FILE.PatientID,FILE.Take)],'FILE');
   flag_start=1;
else
    flag_start=0;
    fclose('all');
end
assignin('base','start_aq', flag_start);
return
%-UI#13Callback

%-UI#14Callback - save presets
evalin('base','save(''presets.mat'',''UIValues'');');
return
%-UI#14Callback

%-UI#15Callback - load presets
evalin('base','load(''presets.mat'',''UIValues'');');
%overrunear los UIControls y ponerles los valores.
return
%-UI#15Callback


%EF#1
EDisplay(RData)
global first;

    UIValues=evalin('base','UIValues');
 
if isempty(first)||(first==0)
    UpdateScreen('Initialize',1);
    first=0;
end


 Resource.DisplayWindow = evalin('base', 'Resource.DisplayWindow');
 PData.Size=evalin('base', 'PData.Size');
 SFormat=evalin('base', 'SFormat');
 Trans=evalin('base', 'Trans');

Bmode=RData(1:PData.Size(1),1:PData.Size(2));
% 
Bmode=(20*log10(Bmode)+20*log10(UIValues.pgain*(2^15-1)/(2^22-1)));
Bmode=medfilt2(Bmode,[4 2]);
Bmode=imfilter(Bmode,fspecial('average',[1 3]));
%Bmode=imfilter(Bmode,fspecial('prewitt'));

Bmode=(imsharpen(Bmode,'Amount',2,'Radius',0.1','Threshold',0.2));

 set(Resource.DisplayWindow.imageHandle,'cdata',Bmode);
   drawnow;
% store image
%  flag_start = evalin('base', 'start_aq'); 
%  if flag_start==1
%      FILE = evalin('base', 'FILE');
%      Receive = evalin('base', 'Receive(1)');
%      TX.NumEl= evalin('base', 'txNumEl');
%      TX.FNum= evalin('base', 'txFNum');
%      
%     count= fwrite(FILE.ImgHandle,Bmode,'double');    
%     save([FILE.Directory sprintf('HEAD%dTK%d.mat',FILE.PatientID,FILE.Take)],...
%        'Resource',...
%        'PData',...
%        'SFormat',...
%        'Receive',...
%        'Trans',...
%        'TX',...
%        'UIValues',...
%        '-append');
% end
return
%EF#1

%EF#2
StoreRaw(RData)

flag_start = evalin('base', 'start_aq');
if flag_start==1
    FILE = evalin('base', 'FILE'); 
       % tic
    count= fwrite(FILE.RcvHandle,RData,'int16');    
       % toc;
end

return
%EF#2


