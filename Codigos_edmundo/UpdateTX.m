% Determine TX aperture based on focal point and desired f number.
Trans = evalin('base', 'Trans');
txFNum = evalin('base', 'txFNum'); % get f-number value for transmit (range: 1.0 - 20)
nfocus = evalin('base','nfocus');
txFocus = evalin('base','txFocus');
SFormat = evalin('base','SFormat');
% - Redefine event specific TX attributes for the new focus.
TX = repmat(struct('waveform', 1, ...
                   'Origin', [0.0,0.0,0.0], ...
                   'focus', txFocus(1), ...
                   'Steer', [0.0,0.0], ...
                   'Apod', zeros(1,Trans.numelements), ...
                   'Delay', zeros(1,Trans.numelements)), 1, SFormat.numRays*nfocus);
for i =1:nfocus

   txNumEl=round((txFocus(i)/txFNum)/Trans.spacing/2); % no. of elements in 1/2 aperture.
if txNumEl > (Trans.numelements/2 - 1) 
   txNumEl = floor(Trans.numelements/2 - 1); 
end   
% - Set event specific TX attributes.
for j = 1:SFormat.numRays  % 128 transmit events
    n= (j-1)*nfocus+i;
    % Set transmit Origins to positions of elements.
    TX(n).Origin = SFormat.FirstRayLoc + (j-1)*Trans.spacing*[1,0,0];
    % Set transmit Apodization so (1 + 2*TXnumel) transmitters are active.
    lft = j - txNumEl; if lft < 1, lft = 1; end;
    rt = j + txNumEl;  if rt > Trans.numelements, rt = Trans.numelements; end;
    TX(n).Apod(lft:rt) = 1.0;
    TX(n).focus= txFocus(i);
    TX(n).Delay = computeTXDelays(TX(n));
    TX(n).NumEl=txNumEl;
end
end
assignin('base','TX', TX);
