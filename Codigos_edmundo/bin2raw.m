%clear all;
%load('E:/ADQUISITIONS/PNEUMONIA/HEAD73649646434TK1');
load('E:/ADQUISITIONS/PNEUMONIA/HEAD73645637074TK1');

FILE.RcvHandle=fopen([FILE.Directory ...
     sprintf('RCV%dTK%dCL%d',FILE.PatientID,FILE.Take,FILE.Cluster)],'r');
FILE.ImgHandle=fopen([FILE.Directory ...
     sprintf('IMG%dTK%dCL%d',FILE.PatientID,FILE.Take,FILE.Cluster)],'r');

[Raw, n]=fread(FILE.RcvHandle,Inf,'int16=>int16');

fclose(FILE.RcvHandle);
%Resource.RcvBuffer.rowsPerFrame=1179648;
%Resource.RcvBuffer.colsPerFrame=64;
i=Resource.RcvBuffer.rowsPerFrame;
j=Resource.RcvBuffer.colsPerFrame;
k=n/i/j;
sizes=[i j k];

Raw=reshape(Raw,sizes);

l2mm=1540/Trans.frequency/1e3;
%%
convb_recon

[video, n]=fread(FILE.ImgHandle,Inf,'double=>double');
i=(SFormat.endDepth-SFormat.startDepth)*4;
j=128;
k=n/i/j;
sizes=[i j k];              
video=reshape(video,sizes);

xl=linspace(-j/2*0.3,j/2*0.3,j);
zl=linspace(SFormat.startDepth*l2mm,SFormat.endDepth*l2mm,j);

aviobj = avifile([FILE.Directory ...
                  sprintf('VIDI%dTK%d',FILE.PatientID,FILE.Take)],...
                  'compression','None'); 
avioobj.Fps= 10;
CMAP= repmat(((0:1/64:1).^UIValues.gamma)',[1 3]);

for i=1:sizes(3)
figure(89);
imagesc(xl,zl,video(:,:,i));
title('Verasonics Beamforming & Processing')
set(gca,'clim',[0 1]);
xlabel('azimuth (mm)'),ylabel('depth (mm)')
set(gcf,'Colormap',CMAP);
axis image;
aviobj = addframe(aviobj,gcf);
pause(0.01);
end 
viobj = close(aviobj); %Close avi object

fclose('all');
