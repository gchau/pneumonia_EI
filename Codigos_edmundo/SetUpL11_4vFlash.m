% Copyright 2001-2013 Verasonics, Inc.  All world-wide rights and remedies under all intellectual property laws and industrial property laws are reserved.  Verasonics Registered U.S. Patent and Trademark Office.
%
% File name SetUpL11_4vFlash.m:
% Generate .mat Sequence Object file for L7-4 Linear array flash transmit.
% All 128 transmit channels are used, with synthetic aperture to allow 
%   receive on all 128 elements (64 per transmit/ receive event).
% This version does asynchronous acquisition and processing.
%
% Last update 04-01-2013

clear all

% Define system parameters.
Resource.Parameters.numTransmit = 128;      % number of transmit channels.
Resource.Parameters.numRcvChannels = 64;    % number of receive channels.
Resource.Parameters.speedOfSound = 1540;    % set speed of sound in m/sec before calling computeTrans
Resource.Parameters.simulateMode = 0;
%  Resource.Parameters.simulateMode = 1 forces simulate mode, even if hardware is present.
%  Resource.Parameters.simulateMode = 2 stops sequence and processes RcvData continuously.

% Specify Trans structure array.
Trans.name = 'L11-4v';
Trans.frequency = 5; % nominal frequency in megahertz
Trans = computeTrans(Trans);  % L11-4v transducer is 'known' transducer so we can use computeTrans.
Trans.maxHighVoltage = 50;  % set maximum high voltage limit for pulser supply.

% Specify SFormat structure array.
SFormat.transducer = 'L11-4v';   % 128 element linear array
SFormat.scanFormat = 'RLIN';     % rectangular linear array scan
SFormat.radius = 0;              % ROC for curved lin. or dist. to virt. apex
SFormat.theta = 0;
SFormat.numRays = 1;      % no. of Rays (1 for Flat Focus)
SFormat.FirstRayLoc = [0,0,0];   % x,y,z
SFormat.rayDelta = 128*Trans.spacing;  % spacing in radians(sector) or dist. between rays (wvlnghts)
SFormat.startDepth = 5;   % Acquisition depth in wavelengths
SFormat.endDepth = 192;   % This should preferrably be a multiple of 128 samples.

% Specify PData structure array.
PData.sFormat = 1;      % use first SFormat structure.
PData.pdeltaX = Trans.spacing;
PData.pdeltaZ = 0.5;
PData.Size(1) = ceil((SFormat.endDepth-SFormat.startDepth)/PData.pdeltaZ); % startDepth, endDepth and pdelta set PData.Size.
PData.Size(2) = ceil((Trans.numelements*Trans.spacing)/PData.pdeltaX);
PData.Size(3) = 1;      % single image page
PData.Origin = [-Trans.spacing*(Trans.numelements-1)/2,0,SFormat.startDepth]; % x,y,z of upper lft crnr.

% Specify Media object. 'pt1.m' script defines array of point targets.
pt1;
Media.function = 'movePoints';

% Specify Resources.
Resource.RcvBuffer(1).datatype = 'int16';
Resource.RcvBuffer(1).rowsPerFrame = 8192;   % this size allows for maximum range
Resource.RcvBuffer(1).colsPerFrame = Resource.Parameters.numRcvChannels;
Resource.RcvBuffer(1).numFrames = 100;       % 100 frames used for RF cineloop.
Resource.InterBuffer(1).datatype = 'complex';
Resource.InterBuffer(1).rowsPerFrame = 1024; % this is for greatest depth
Resource.InterBuffer(1).colsPerFrame = PData.Size(2);
Resource.InterBuffer(1).numFrames = 1;  % one intermediate buffer needed.
Resource.ImageBuffer(1).datatype = 'double';
Resource.ImageBuffer(1).rowsPerFrame = 1024;
Resource.ImageBuffer(1).colsPerFrame = PData.Size(2);
Resource.ImageBuffer(1).numFrames = 10;
Resource.DisplayWindow(1).Title = 'L11-4vFlash';
Resource.DisplayWindow(1).pdelta = 0.35;
ScrnSize = get(0,'ScreenSize');
DwWidth = ceil(PData.Size(2)*PData.pdeltaX/Resource.DisplayWindow(1).pdelta);
DwHeight = ceil(PData.Size(1)*PData.pdeltaZ/Resource.DisplayWindow(1).pdelta);
Resource.DisplayWindow(1).Position = [250,(ScrnSize(4)-(DwHeight+150))/2, ...  % lower left corner position
                                      DwWidth, DwHeight];
Resource.DisplayWindow(1).ReferencePt = [PData.Origin(1),PData.Origin(3)];   % 2D imaging is in the X,Z plane
Resource.DisplayWindow(1).Colormap = gray(256);

% Specify Transmit waveform structure.  
TW.type = 'parametric';
TW.Parameters = [18,17,2,1];   % A, B, C, D

% Specify TX structure array.
txFocus = 40;  % Initial transmit focus.
txFNum = 2;  % set to desired f-number value for transmit (range: 1.0 - 20)
txNumEl=round((txFocus/txFNum)/Trans.spacing/2); % no. of elements in 1/2 aperture.
if txNumEl > (Trans.numelements/2 - 1), txNumEl = floor(Trans.numelements/2 - 1); end   


TX(1).waveform = 1;            % use 1st TW structure.
TX(1).Origin = [0.0,0.0,0.0];  % flash transmit origin at (0,0,0).
TX(1).focus = txFocus ;
TX(1).Steer = [0.0,0.0];       % theta, alpha = 0.
TX(1).Apod = zeros(1,Trans.numelements);
lft = 64 - txNumEl;
rt = 64 + txNumEl;
TX(1).Apod(lft:rt) = 1.0;
TX(1).Delay = computeTXDelays(TX(1));
% Specify TGC Waveform structure.
TGC.CntrlPts = [500,590,650,710,770,830,890,950];
TGC.rangeMax = SFormat.endDepth;
TGC.Waveform = computeTGCWaveform(TGC);

% Specify Receive structure arrays -
%   endDepth - add additional acquisition depth to account for some channels
%              having longer path lengths.
%   InputFilter - The same coefficients are used for all channels. The
%              coefficients below give a broad bandwidth bandpass filter.
maxAcqLength = sqrt(SFormat.endDepth^2 + (Trans.numelements*Trans.spacing)^2) - SFormat.startDepth;
wlsPer128 = 128/(4*2); % wavelengths in 128 samples for 4 samplesPerWave
Receive = repmat(struct('Apod', zeros(1,Trans.numelements), ...
                        'startDepth', SFormat.startDepth, ...
                        'endDepth', SFormat.startDepth + wlsPer128*ceil(maxAcqLength/wlsPer128), ...
                        'TGC', 1, ...
                        'bufnum', 1, ...
                        'framenum', 1, ...
                        'acqNum', 1, ...
                        'samplesPerWave', 4, ...
                        'mode', 0, ...
                        'InputFilter',[0.0036,0.0127,0.0066,-0.0881,-0.2595,0.6494], ...
                        'callMediaFunc', 0),1,2*Resource.RcvBuffer(1).numFrames);
% - Set event specific Receive attributes.
for i = 1:Resource.RcvBuffer(1).numFrames
    % -- 1st synthetic aperture acquisition for full frame.
    Receive(2*i-1).Apod(1:Resource.Parameters.numRcvChannels) = 1.0;
    Receive(2*i-1).framenum = i;
    Receive(2*i-1).acqNum = 1;
    Receive(2*i-1).callMediaFunc = 1;
    % -- 2nd synthetic aperture acquisition for full frame.
    Receive(2*i).Apod((Resource.Parameters.numRcvChannels+1):Trans.numelements) = 1.0;
    Receive(2*i).framenum = i;
    Receive(2*i).acqNum = 2;   % two acquisitions per frame 
end

% Specify Recon structure arrays.
Recon = struct('senscutoff', 0.6, ...
               'pdatanum', 1, ...
               'rcvBufFrame', -1, ...     % use most recently transferred frame
               'IntBufDest', [1,1], ...
               'ImgBufDest', [1,-1], ...  % auto-increment ImageBuffer each recon
               'RINums', [1;2]);

% Define ReconInfo structures.
ReconInfo = repmat(struct('mode', 3, ...  % replace IQ data.
                   'txnum', 1, ...
                   'rcvnum', 1, ...
                   'regionnum', 0), 1, 2);
% - Set specific ReconInfo attributes.
ReconInfo(1).mode = 3;
ReconInfo(1).rcvnum = 1;
ReconInfo(2).mode = 5; % accumulate and detect IQ data in output buffer.
ReconInfo(2).rcvnum = 2;

% Specify Process structure array.
pers = 20;
Process(1).classname = 'Image';
Process(1).method = 'imageDisplay';
Process(1).Parameters = {'imgbufnum',1,...   % number of buffer to process.
                         'framenum',-1,...   % (-1 => lastFrame)
                         'pdatanum',1,...    % number of PData structure to use
                         'norm',1,...        % normalization method(1 means fixed)
                         'pgain',1.0,...            % pgain is image processing gain
                         'persistMethod','simple',...
                         'persistLevel',pers,...
                         'interp',1,...      % method of interpolation (1=4pt interp)
                         'compression',0.5,...      % X^0.5 normalized to output word size
                         'reject',5,...      % reject level 
                         'mappingMode','full',...
                         'display',1,...      % display image after processing
                         'displayWindow',1};

% Specify SeqControl structure arrays.
SeqControl(1).command = 'jump'; % jump back to start.
SeqControl(1).argument = 1;
SeqControl(2).command = 'timeToNextAcq';  % time between synthetic aperture acquisitions
SeqControl(2).argument = 200;  % 200 usec
SeqControl(3).command = 'timeToNextAcq';  % time between frames
SeqControl(3).argument = 10000;  % 10 msec
SeqControl(4).command = 'returnToMatlab';
nsc = 5; % nsc is count of SeqControl objects

n = 1; % n is count of Events

% Acquire all frames defined in RcvBuffer
for i = 1:Resource.RcvBuffer(1).numFrames
    Event(n).info = '1st half of aperture.';
    Event(n).tx = 1;         % use 1st TX structure.
    Event(n).rcv = 2*i-1;    % use 1st Rcv structure.
    Event(n).recon = 0;      % no reconstruction.
    Event(n).process = 0;    % no processing
    Event(n).seqControl = 2; % time between syn. aper. acqs.
    n = n+1;

    Event(n).info = '2nd half of aperture.';
    Event(n).tx = 1;         % use 1st TX structure.
    Event(n).rcv = 2*i;      % use 2nd Rcv structure.
    Event(n).recon = 0;      % no reconstruction.
    Event(n).process = 0;    % no processing
    Event(n).seqControl = [3,nsc]; % time between frames, SeqControl struct defined below.
       SeqControl(nsc).command = 'transferToHost';
       nsc = nsc + 1;
    n = n+1;
    
    Event(n).info = 'Reconstruct'; 
    Event(n).tx = 0;         % no transmit
    Event(n).rcv = 0;        % no rcv
    Event(n).recon = 1;      % reconstruction
    Event(n).process = 1;    % processing
    if floor(i/5) == i/5     % Exit to Matlab every 5th frame 
        Event(n).seqControl = 4; % return to Matlab
    else
        Event(n).seqControl = 0;
    end
    n = n+1;
end

Event(n).info = 'Jump back to first event';
Event(n).tx = 0;        % no TX
Event(n).rcv = 0;       % no Rcv
Event(n).recon = 0;     % no Recon
Event(n).process = 0; 
Event(n).seqControl = 1; % jump command


% User specified UI Control Elements
% - Sensitivity Cutoff
UI(1).Control =  {'UserB7','Style','VsSlider','Label','Sens. Cutoff',...
                  'SliderMinMaxVal',[0,1.0,Recon(1).senscutoff],...
                  'SliderStep',[0.025,0.1],'ValueFormat','%1.3f'};
UI(1).Callback = text2cell('%-UI#1Callback');

% - Range Change
UI(2).Control = {'UserA1','Style','VsSlider','Label','Range',...
                 'SliderMinMaxVal',[64,320,SFormat.endDepth],'SliderStep',[0.1,0.2],'ValueFormat','%3.0f'};
UI(2).Callback = text2cell('%-UI#2Callback');

UI(3).Control = {'UserB4','Style','VsSlider','Label','TX Focus',...
                 'SliderMinMaxVal',[0,320,100],'SliderStep',[0.1,0.2],'ValueFormat','%3.0f'};
UI(3).Callback = text2cell('%-UI#3Callback');

UI(4).Control = {'UserB3','Style','VsSlider','Label','F Number',...
                 'SliderMinMaxVal',[1,20,2],'SliderStep',[0.1,0.2],'ValueFormat','%2.0f'};
UI(4).Callback = text2cell('%-UI#4Callback');

% - External Function Definition
EF(1).Function = text2cell('%EF#1');
EF(2).Function = text2cell('%EF#2');

% Specify factor for converting sequenceRate to frameRate.
frameRateFactor = 5;

% Save all the structures to a .mat file.
save('L11-4vFlash');

return


% **** Callback routines to be converted by text2cell function. ****
%-UI#1Callback - Sensitivity cutoff change
ReconL = evalin('base', 'Recon');
for i = 1:size(ReconL,2)
    ReconL(i).senscutoff = UIValue;
end
assignin('base','Recon',ReconL);
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'Recon'};
assignin('base','Control', Control);
return
%-UI#1Callback

%-UI#2Callback - Range change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No range change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','SFormat.endDepth'));
    return
end
range = UIValue;
assignin('base','range',range);
SFormat = evalin('base','SFormat');
SFormat.endDepth = range;
assignin('base','SFormat',SFormat);
evalin('base','PData.Size(1) = ceil((SFormat.endDepth-SFormat.startDepth)/PData.pdeltaZ);');
evalin('base','[PData.Region,PData.numRegions] = createRegions(PData);');
evalin('base','Resource.DisplayWindow(1).Position(4) = ceil(PData.Size(1)*PData.pdeltaZ/Resource.DisplayWindow(1).pdelta);');
Receive = evalin('base', 'Receive');
Trans = evalin('base', 'Trans');
maxAcqLength = sqrt(range^2 + (Trans.numelements*Trans.spacing)^2)-SFormat.startDepth;
wlsPer128 = 128/(4*2);
for i = 1:size(Receive,2)
    Receive(i).endDepth = SFormat.startDepth + wlsPer128*ceil(maxAcqLength/wlsPer128);
end
assignin('base','Receive',Receive);
evalin('base','TGC.rangeMax = SFormat.endDepth;');
evalin('base','TGC.Waveform = computeTGCWaveform(TGC);');
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'SFormat','PData','Receive','Recon','DisplayWindow','ImageBuffer'};
assignin('base','Control', Control);
assignin('base', 'action', 'displayChange');
return
%-UI#2Callback

%-UI#3Callback - TX focus changel
simMode = evalin('base','Resource.Parameters.simulateMode');
% No focus change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','txFocus'));
    return
end
txFocus = UIValue;
assignin('base','txFocus',txFocus);
% Determine TX aperture based on focal point and desired f number.
Trans = evalin('base', 'Trans');
txFNum = evalin('base', 'txFNum'); % get f-number value for transmit (range: 1.0 - 20)
% txNumEl is the number of elements to include on each side of the center element, for the specified
%    focus and sensitivity cutoff.  Thus the full transmit aperture will be 2*txNumEl + 1 elements.
txNumEl = round((txFocus/txFNum)/Trans.spacing/2); % no. of elements in 1/2 aperture.
if txNumEl > (Trans.numelements/2 - 1), txNumEl = floor(Trans.numelements/2 - 1); end  
assignin('base','txNumEl',txNumEl);
% - Redefine event specific TX attributes for the new focus.
TX = evalin('base', 'TX');
    % Set Apod vector back to all zeros
    TX(1).Apod(:) = 0;
    % write new focus value to TX
    TX(1).focus = txFocus;
    % Set transmit Apodization so (1 + 2*TXnumel) transmitters are active.
    lft = 64 - txNumEl;
    rt = 64 + txNumEl;
    TX(1).Apod(lft:rt) = 1.0;
    TX(1).Delay = computeTXDelays(TX(1));
    
assignin('base','TX', TX);
% Set Control command to update TX
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'TX'};
assignin('base','Control', Control);
return
%-UI#3Callback

%-UI#4Callback - F number change
simMode = evalin('base','Resource.Parameters.simulateMode');
% No F number change if in simulate mode 2.
if simMode == 2
    set(hObject,'Value',evalin('base','txFNum'));
    return
end
txFNum = UIValue;
assignin('base','txFNum',txFNum);
% Determine TX aperture based on focal point and desired f number.
Trans = evalin('base', 'Trans');
txFocus = evalin('base', 'txFocus'); % get txFocus value for transmit
% txNumEl is the number of elements to include on each side of the center element, for the specified
%    focus and sensitivity cutoff.  Thus the full transmit aperture will be 2*txNumEl + 1 elements.
txNumEl = round((txFocus/txFNum)/Trans.spacing/2); % no. of elements in 1/2 aperture.
if txNumEl > (Trans.numelements/2 - 1), txNumEl = floor(Trans.numelements/2 - 1); end  
assignin('base','txNumEl',txNumEl);
% - Redefine event specific TX attributes for the new focus.
TX = evalin('base', 'TX');
TX = evalin('base', 'TX');
    % Set Apod vector back to all zeros
    TX(1).Apod(:) = 0;
    % write new focus value to TX
    TX(1).focus = txFocus;
    % Set transmit Apodization so (1 + 2*TXnumel) transmitters are active.
    lft = 64 - txNumEl;
    rt = 64 + txNumEl;
    TX(1).Apod(lft:rt) = 1.0;
    TX(1).Delay = computeTXDelays(TX(1));
 assignin('base','TX', TX);
% Set Control command to update TX
Control = evalin('base','Control');
Control.Command = 'update&Run';
Control.Parameters = {'TX'};
assignin('base','Control', Control);
return
%-UI#4Callback