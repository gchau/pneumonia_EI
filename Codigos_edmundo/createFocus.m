function txFocus=createFocus(start,nfocus,spacing)

txFocus=(0:spacing:(nfocus-1)*spacing)+start;

return