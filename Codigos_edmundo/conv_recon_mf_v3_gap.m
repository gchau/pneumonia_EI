
c = Resource.Parameters.speedOfSound;  
Fc=Trans.frequency*1e6; 
Fs=4*Fc;
rxlambda=Receive(1).samplesPerWave*2;
mm2smpl=1e-3*Fc/c*rxlambda; %constant to change from mm to samples

maxAprSz=TX.NumEl;
dB=55;
pitch = 0.3e-3;
n=Resource.Parameters.numTransmit;
nr=SFormat.numRays;
txFNum=TX.FNum;
clear samples;

samples(1,:)=[Receive(1).endSample Receive(1).startSample];
samples(2,:)=[Receive(2).endSample Receive(2).startSample];
samples(3,:)=[Receive(3).endSample Receive(3).startSample];
samples(4,:)=[Receive(4).endSample Receive(4).startSample];
samples(5,:)=[Receive(5).endSample Receive(5).startSample];
samples(6,:)=[Receive(6).endSample Receive(6).startSample];

samples(:,3)=samples(:,1)-samples(:,2)+1;

zstart=SFormat.startDepth*rxlambda;
zend=SFormat.endDepth*rxlambda;
valid_data=zend-zstart;


% Z1=((0:samples(1,3)-1)+zstart)*c/Fs/2;             % in the axial direction [mm]
% Z1=repmat(Z1',[1 n nr]);
% 
% Z2=((samples(1,3):samples(3,3)+samples(1,3)-1)+zstart)*c/Fs/2;             % in the axial direction [mm]
% Z2=repmat(Z2',[1 n nr]);
% 
% Z3=((samples(3,3)+samples(1,3):samples(6,3)+samples(3,3)+samples(1,3)-1)+zstart)*c/Fs/2;             % in the axial direction [mm]
% Z3=repmat(Z3',[1 n nr]);


Z1=(((Receive(1).startDepth)*2*4:(Receive(1).endDepth)*2*4-1))*c/Fs/2;             % in the axial direction [mm]
Z1=repmat(Z1',[1 n nr]);

Z2=(((Receive(3).startDepth)*2*4:(Receive(3).endDepth)*2*4-1))*c/Fs/2;             % in the axial direction [mm]
Z2=repmat(Z2',[1 n nr]);

Z3=(((Receive(5).startDepth)*2*4:(Receive(5).endDepth)*2*4-1))*c/Fs/2;             % in the axial direction [mm]
Z3=repmat(Z3',[1 n nr]);



%%delay_offset(focus)=  Fs/Fc*(sqrt(((txNumEl)*Trans.spacing)^2+txFocus(focus)^2)-txFocus(focus));
Zoffset=[1]*c/Fs;
% Zoffset1=(1)*c/Fs;
% Zoffset2=(samples(3,3)+1)*c/Fs;
% Zoffset3=(samples(6,3)+1)*c/Fs;

%%%%%%%%%%%%%%%%%%%%%%Data Index formation %%%%%%%%%%%%%%%%%%%%%
index1=repmat(1:n,[samples(1,3) 1 nr]); 
transshift1=permute(index1,[1 3 2]);
X1=(transshift1-index1)*pitch;
%index_fixer1=(floor(((Receive(1).startDepth)*2*4:(Receive(1).endDepth)*2*4*nr*n-1)'/samples(1,3))*samples(1,3));
index_fixer1=(floor((0:(samples(1,3)*nr*n-1))'/samples(1,3))*samples(1,3));


index2=repmat(1:n,[samples(3,3) 1 nr]); 
transshift2=permute(index2,[1 3 2]);
X2=(transshift2-index2)*pitch;
%index_fixer2=(floor(((Receive(3).startDepth)*2*4:(Receive(3).endDepth)*2*4*nr*n-1)'/samples(3,3))*samples(3,3));
index_fixer2=(floor((0:(samples(3,3)*nr*n-1))'/samples(3,3))*samples(3,3));


index3=repmat(1:n,[samples(6,3) 1 nr]); 
transshift3=permute(index3,[1 3 2]);
X3=(transshift3-index3)*pitch;
%index_fixer3=(floor(((Receive(5).startDepth)*2*4:(Receive(5).endDepth)*2*4*nr*n-1)'/samples(5,3))*samples(5,3));
index_fixer3=(floor((0:(samples(5,3)*nr*n-1))'/samples(5,3))*samples(5,3));
    
  
%%%%%%%%%%%%%%%%%%%%%% Apodization  %%%%%%%%%%%%%%%%%%%%%%%%%
% Xtrim=floor(Z(:,1,1)/txFNum/pitch*2/rxlambda*2);
% Xtrim(Xtrim>maxAprSz/2)=maxAprSz/2;
% XtrimM=repmat(Xtrim,[1 n nr]);
% apodi=index-transshift;
% apodi(apodi>XtrimM)=NaN;
% apodi(apodi<-XtrimM)=NaN;
% apodi=apodi+XtrimM+1;
% 
% H=0.5-0.5*cos(2*pi*apodi./(XtrimM*2+2));
% H(isnan(H))=0;
% H=H./(XtrimM+1);

%%%%%%%%%%Receive Data, Reshape and Parameters reading%%%%%%%%%%%%%%%%%%
for frame=1:size(Raw,3)
A=Raw(:,:,frame);
%A=RcvData{1}(:,:,frame);

A=A(1:sum(samples(:,3))*nr,:)';
B= reshape(A,Resource.Parameters.numRcvChannels,sum(samples(:,3)),nr);
B1=B(:,samples(1,2):samples(1,1),:);
B2=B(:,samples(2,2):samples(2,1),:);
B3=B(:,samples(3,2):samples(3,1),:);
B4=B(:,samples(4,2):samples(4,1),:);
B5=B(:,samples(5,2):samples(5,1),:);
B6=B(:,samples(6,2):samples(6,1),:);

C1=permute(B1,[2 1 3]); 
C2=permute(B2,[2 1 3]);
C3=permute(B3,[2 1 3]); 
C4=permute(B4,[2 1 3]);
C5=permute(B5,[2 1 3]); 
C6=permute(B6,[2 1 3]);


C=[C1 C2;C3 C4; C5 C6];

CFoco1 = [C1 C2];
CFoco2 = [C3 C4];
CFoco3 = [C5 C6];


%%%%%%%%%%%%%%%%%%%%%% Delay %%%%%%%%%%%%%%%%%%%%%%%%%

delays1 = ((Z1+Zoffset) + sqrt((Z1).^2 + (X1).^2))*Fs/c;
delaysa1=floor(delays1);
q1=delays1-delaysa1;
delaysa1(delaysa1>(Receive(1).endDepth)*2*4)=(Receive(1).endDepth)*2*4;
delaysa1(delaysa1<1)=0;
delaysb1=delaysa1+1;
delaysb1(delaysb1>(Receive(1).endDepth)*2*4)=(Receive(1).endDepth)*2*4;



delays2 = ((Z2+Zoffset) + sqrt((Z2).^2 + (X2).^2))*Fs/c;
delaysa2=floor(delays2);
q2=delays2-delaysa2;
delaysa2(delaysa2>(Receive(3).endDepth)*2*4)=(Receive(3).endDepth)*2*4;
delaysa2(delaysa2<1)=0;
delaysb2=delaysa2+1;
delaysb2(delaysb2>(Receive(3).endDepth)*2*4)=(Receive(3).endDepth)*2*4;


delays3 = ((Z3+Zoffset) + sqrt((Z3).^2 + (X3).^2))*Fs/c;
delaysa3=floor(delays3);
q3=delays3-delaysa3;
delaysa3(delaysa3>(Receive(5).endDepth)*2*4)=(Receive(5).endDepth)*2*4;
delaysa3(delaysa3<1)=0;
delaysb3=delaysa3+1;
delaysb3(delaysb3>(Receive(5).endDepth)*2*4)=(Receive(5).endDepth)*2*4;

%%%%%%%%%%%%%%Delay Application with linear interpolation%%%%%%%%%%%%%%%%%
Dd1= (1-q1(:)).*double(CFoco1(delaysa1(:)+index_fixer1))+q1(:).*double(CFoco1(delaysb1(:)+index_fixer1));
Dd1=double((reshape(Dd1,size(delays1))));


Dd2= (1-q2(:)).*double(CFoco2(delaysa2(:)+index_fixer2-Receive(3).startDepth*2*4))+q2(:).*double(CFoco2(delaysb2(:)+index_fixer2-Receive(3).startDepth*2*4));
Dd2=double((reshape(Dd2,size(delays2))));


Dd3= (1-q3(:)).*double(CFoco3(delaysa3(:)+index_fixer3-Receive(5).startDepth*2*4))+q3(:).*double(CFoco3(delaysb3(:)+index_fixer3-Receive(5).startDepth*2*4));
Dd3=double((reshape(Dd3,size(delays3))));


%%%%%%%%%%%%%%%%% B MODE FORMATION %%%%%%%%%%%%%%
%Da=Dd.*H;
Rf1(:,:,frame)=squeeze(sum(Dd1(1:end,:,:),2));
Rf2(:,:,frame)=squeeze(sum(Dd2(1:end,:,:),2));
Rf3(:,:,frame)=squeeze(sum(Dd3(1:end,:,:),2));

US1=(abs(hilbert(Rf1(:,:,frame))));
US2=(abs(hilbert(Rf2(:,:,frame))));
US3=(abs(hilbert(Rf3(:,:,frame))));
%US=US-max(US(:));

video1(:,:,frame)=US1;
video2(:,:,frame)=US2;
video3(:,:,frame)=US3;


frame
end


% figure(1),imagesc(20*log(abs(hilbert(double(C(:,:,1)))))),colormap(gray)
% figure(2),imagesc(20*log(abs(hilbert(double(C(:,:,5)))))),colormap(gray)
% figure(3),imagesc(20*log(abs(hilbert(double(C(:,:,10)))))),colormap(gray)
% figure(4),imagesc(20*log(abs(hilbert(double(C(:,:,15)))))),colormap(gray)
% figure(5),imagesc(20*log(abs(hilbert(double(C(:,:,20)))))),colormap(gray)
% figure(6),imagesc(20*log(abs(hilbert(double(C(:,:,end)))))),colormap(gray)
% 
% 
% figure(1),imagesc(20*log(video(:,:,1))),colormap(gray)
% figure(2),imagesc(20*log(video(:,:,5))),colormap(gray)
% figure(3),imagesc(20*log(video(:,:,10))),colormap(gray)
% figure(4),imagesc(20*log(video(:,:,15))),colormap(gray)
% figure(5),imagesc(20*log(video(:,:,20))),colormap(gray)
% figure(6),imagesc(20*log(video(:,:,end))),colormap(gray)


figure(1),imagesc(20*log(video1(:,:,1)))%,colormap(gray)
figure(2),imagesc(20*log(video2(:,:,1)))%,colormap(gray)
figure(3),imagesc(20*log(video3(:,:,1)))%,colormap(gray)


% xl=linspace(0,n*pitch*1e3,n);
% zl=linspace(zstart/mm2smpl,zend/mm2smpl,valid_data);
% 
% aviobj = avifile([FILE.Directory ...
%                   sprintf('VIDR%dTK%d',FILE.PatientID,FILE.Take)],...
%                   'compression','None'); 
% avioobj.Fps= 10;
% CMAP= repmat(((0:1/64:1).^UIValues.gamma)',[1 3]);
% for frame=1:sizes(3)
% figure(45)
% Bmode=video(:,:,frame);
% Bmode=UIValues.pgain*Bmode*(2^15-1)/(2^13-1);
% Bmode=(20*log10(Bmode)-UIValues.reject)/UIValues.DynRng;
% Bmode(Bmode<0)=0;Bmode(Bmode>1)=1;
% Bmode=medfilt2(Bmode,[8 1]);
% Bmode=imfilter(Bmode,fspecial('average',[1 8]));
% Bmode=(imsharpen(Bmode,'Amount',2,'Radius',0.1','Threshold',0.2));
% 
% imagesc(xl,zl,Bmode);
% title('Conventional Beamforming & Processing')
% set(gca,'clim',[0 1]);
% xlabel('azimuth (mm)'),ylabel('depth (mm)')
% set(gcf,'Colormap',CMAP);
% axis image;
% aviobj = addframe(aviobj,gcf);
% pause(0.01)
% end
% viobj = close(aviobj);