function UpdateScreen(parameter,UIvalue)
global axesHandle;
l2mm=evalin('base','l2mm');
Resource = evalin('base', 'Resource');

% Create the handle if it doesn�t exist.
 axesHandle=imgca(Resource.DisplayWindow.figureHandle);
 
if (~isempty(parameter))&& (~isempty(UIvalue))

switch parameter

    case 'Initialize'
    
    UIValues=evalin('base','UIValues');
    SFormat=evalin('base','SFormat');
    PData.Size=evalin('base','PData.Size');
    
    %Z=linspace(SFormat.startDepth,SFormat.endDepth,PData.Size(1));
    Yticklabel=num2str(round(0.1*l2mm*(get(axesHandle,'Ytick')+SFormat.startDepth)'));
    Xticklabel=num2str(round(0.1*l2mm*get(axesHandle,'Xtick')'));
    set(axesHandle,'Yticklabel',Yticklabel);
    set(axesHandle,'Xticklabel',Xticklabel);
    ylabel(axesHandle,'Profundidad (cm)');
    xlabel(axesHandle,'Elevacion (cm)');
    
    CMAP= repmat(((0:1/64:1).^UIValues.gamma)',[1 3]);
    set(Resource.DisplayWindow.figureHandle,'Colormap',CMAP);
    set(Resource.DisplayWindow.imageHandle,'CDataMapping','scaled');
    
    set(axesHandle,'CLim',[UIValues.reject UIValues.DynRng]);
    
    UI=evalin('base','UI');
    if ishghandle(UI(16).handle(1))
    set(UI(16).handle(1),'Position',[0.6625 0.04 0.3 0.4])
    end
    
    
    
    case 'gamma',
        
    CMAP= repmat(((0:1/64:1).^UIvalue)',[1 3]);
    set(Resource.DisplayWindow.figureHandle,'Colormap',CMAP);
    set(Resource.DisplayWindow.imageHandle,'CDataMapping','scaled');
    
    case 'DynRng'
    
    DR=get(axesHandle,'CLim');
    set(axesHandle,'CLim',[DR(1) UIvalue]);
    
    case 'reject'

    DR=get(axesHandle,'CLim');
    set(axesHandle,'CLim',[UIvalue DR(2)]);
    
end %switch
end %parameter and value
return