Exer.V=15.8e-3;
Exer.Fc=5;
Exer.z=40*1540/1e4
Exer.VuPa=10^(-282/20);
Exer.uPa= Exer.V/Exer.VuPa;
Exer.MPa=Exer.uPa*1e-12;
Exer.Pr3=Exer.MPa*exp(-0.0345*Exer.Fc*Exer.z);
Exer.MI= (Exer.Pr3)/sqrt(Exer.Fc);
Exer