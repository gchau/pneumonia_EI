function [Region,numRegions]=createNewRegions(PData)

numRegions= PData.Size(2)*PData.nfocus;
Region= repmat(struct('sectionNum',int32(1),...
                            'startRow',  int32(1)),1,numRegions);

for i=1:PData.nfocus
    
    if i==1, ini=1;
    else  ini=floor(sum(PData.focus(i-1:i))/2); end
       
    if i==PData.nfocus, fin=PData.Size(1)-1; 
    else fin=floor(sum(PData.focus(i:i+1))/2)-1; end
      
        szrgn=size(ini:fin,2);
    for j=0:PData.Size(2)-1   
    
        n=i+j*PData.nfocus;
        Region(n).startRow=int32(ini);
        Region(n).numRows=int32(szrgn);
        Region(n).RowData=int32([ones(szrgn,1)*[j 1] (0:(szrgn-1))']);
        
    end
    
end

return