clear all
close all
v=VideoReader('LP22 14-54-41.avi')

root=pwd

for i=1:(v.NumberOfFrames)-1
%      i
video(:,:,i)= rgb2gray(read(v,i));
f=video(:,:,i);
fc=f(85:546, 136:586);
imagen=fc; %
end

% mat_total=[];
% mat_cero=[];

cero2=zeros(size(imagen,1),size(imagen,2));
mat_total=im2uint8(cero2);

for i=1:(v.NumberOfFrames)-1
     i
video(:,:,i)= rgb2gray(read(v,i));
f=video(:,:,i);
fc=f(85:546, 136:586);
imagen=fc; %PRIMERA IMAGEN, SOLO RECORTADA
lol(:,:,i)=fc;
x=[];
for i = 1:size(imagen,1)
    x = [x i];
end

nfc=fc/max(max(fc)); %UMBRALIZACI�N. NUEVA IMAGEN SOLO CON INTENSIDADES MAYORES
lol(:,:,i)=fc;

imwrite(fc,['D:\Users\Oz\Documents\MATLAB\Pneumonia\Sample\Enfermos\frame' num2str(i) '.png']);

imagen=nfc; %NUEVA IMAGEN SOLO CON INTENSIDADES MAYORES
alto_1=size(imagen,1);
largo_1=size(imagen,2);

acum_columna=sum(imagen);
transpuesta=imagen';
acum_fila=sum(transpuesta);

[curfitted, a,b,c]=fit(x',acum_fila', 'gauss2'); %POLINOMIO APROXIMADOR DE 
                                                  %SUMA POR FILAS

cur=curfitted.a1.*exp(-((x-curfitted.b1)/curfitted.c1).^2) + curfitted.a2.*exp(-((x-curfitted.b2)/curfitted.c2).^2);

y2=round(cur); %CURVA APROXIMADA PARA ELIMINAR DECIMALES
% plot(y2)
f1=find(y2==0); %ENCONTRAR VALORES DE CERO
der=diff(cur,2);

vec=[];
for i = 1:size(f1,2)
      vec=[vec f1(i)];
end
% plot(vec)
n_vec=[];
for i = 1:size(f1,2)-1
if vec(i+1)-vec(i)>1
    n_vec=[n_vec f1(i)];
end;
end
max_vec=max(n_vec);

imagen=fc;
alt=size(imagen,1);
lar=size(imagen,2);
% imagen2=[];
% if max_vec~=[]
    if max_vec>=50 & max_vec<=150
        img=imagen(max_vec:alt, 1:lar);
        imagen2=[[zeros(max_vec-1,lar)];img];
        n_im2=imagen2/max(max(imagen2));
    else
        n_im2=zeros(alt,lar);
        
    end
%     imagesc(imagen2)

    x=[];
    for i = 1:size(imagen,1)
        x = [x i];
    end
    
    [L, NUM] = bwlabel(n_im2);
    ar = regionprops(L, 'Area');
    areas = [];
    for index = 1:NUM
        areas = [areas ar(index).Area];
    end
    
    alto_n_mat=size(n_im2,1);
    largo_n_mat=size(n_im2,2);
    % Umbralizacion por areas (Observe las areas detectadas)
    aut = find( areas > 200); %& areas<1000
       
    cero=zeros(alto_n_mat,largo_n_mat);
    mat_cero=im2uint8(cero);
    
    a = 1; %indice para matriz final
%     new_mat=[];
    for k = 1:NUM
        if(areas(k) > 200 )% & areas<2000 Para descartar los tama�os peque�os y muy grandes
            zona = L==k;
            mat=im2uint8(zona);
            mat_cero=plus(mat_cero,mat);
            
        end
%         imagesc(mat_cero);
    end
    mat_total=plus(mat_total, mat_cero) ;
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 maximo=max(max(mat_total))   
 if maximo==0
        for i = 1:size(f1,2)-1
            if vec(i+1)-vec(i)>1
                n_vec=[n_vec f1(i+1)];
            end;
        end
        max_vec=max(n_vec);
        
        imagen=fc;
        alt=size(imagen,1);
        lar=size(imagen,2);
        % imagen2=[];
        % if max_vec~=[]
        if max_vec>=50 & max_vec<=150
            img=imagen(max_vec:alt, 1:lar);
            imagen2=[[zeros(max_vec-1,lar)];img];
            n_im2=imagen2/max(max(imagen2));
        else
            n_im2=zeros(alt,lar);
            
        end
        %     imagesc(imagen2)
        
        x=[];
        for i = 1:size(imagen,1)
            x = [x i];
        end
        
        [L, NUM] = bwlabel(n_im2);
        ar = regionprops(L, 'Area');
        areas = [];
        for index = 1:NUM
            areas = [areas ar(index).Area];
        end
        
        alto_n_mat=size(n_im2,1);
        largo_n_mat=size(n_im2,2);
        % Umbralizacion por areas (Observe las areas detectadas)
        aut = find( areas > 200); %& areas<1000
        
        cero=zeros(alto_n_mat,largo_n_mat);
        mat_cero=im2uint8(cero);
        
        a = 1; %indice para matriz final
        %     new_mat=[];
        for k = 1:NUM
            if(areas(k) > 200 )% & areas<2000 Para descartar los tama�os peque�os y muy grandes
                zona = L==k;
                mat=im2uint8(zona);
                mat_cero=plus(mat_cero,mat);
                
            end
            imagesc(mat_cero);
        end
        mat_total=plus(mat_total, mat_cero) ;
        
    end
    
    
%     figure(1),imagesc(mat_total)
    close all
    % end
    
end
figure
% imagesc(mat_total); axis image

[L, NUM] = bwlabel(mat_total);
ar = regionprops(L, 'Area');
lar=regionprops(L,'BoundingBox');

areas = [];
for index = 1:NUM
areas = [areas ar(index).Area];
end

% figure(2), stem( areas ), grid
prom=mean(areas);

largo=[];
for i=1:NUM
    largo=[largo lar(i).BoundingBox(3)]
end

% figure(3), stem( largo ), grid
prom_largo=mean(largo);

alto_n_mat=size(mat_total,1);
largo_n_mat=size(mat_total,2);

% Umbralizacion por areas (Observe las areas detectadas)
% aut = find( areas > prom); %& areas<1000

[L, NUM] = bwlabel(mat_total);
a_o = regionprops(L, 'Area');

cero2=zeros(alto_n_mat,largo_n_mat);
mat_cero2=im2uint8(cero2);
for k = 1:NUM
    area = a_o(k).Area(1);
    
    if area>=prom
%     largo=c_o(k).BoundingBox(3);
%     area = a_o(k).Area(1);
         zona = L==k;
         mat=im2uint8(zona);
         mat_cero2=plus(mat_cero2,mat);
    end
end
    
new_mat=mat_cero2;

[L2, NUM2] = bwlabel(new_mat); % etiquetado
objetos = regionprops(L2,'BoundingBox');

cero3=zeros(alto_n_mat,largo_n_mat);
m_final=im2uint8(cero3);

for i=1:NUM2
    largo=objetos(i).BoundingBox(3);
    if largo>=prom_largo
        zona_2=L2==i;
        mat_2=im2uint8(zona_2);
        m_final=plus(m_final,mat_2);
    end
end
% imagesc(m_final)
[L3, NUM3] = bwlabel(m_final); % etiquetado
coorden = regionprops(L3,'BoundingBox');

cero3=zeros(size(m_final,1),size(m_final,2));
m=im2uint8(cero3);

for i=1:NUM3
     y=coorden(i).BoundingBox(2);
    if y>=50 & y<=150
        zona_3=L2==i;
        mat_3=im2uint8(zona_3);
        m=plus(m,mat_3);
    end
end

% imagesc(m)


for i=1:(v.NumberOfFrames)-1
%      i
video(:,:,i)= rgb2gray(read(v,i));
f=video(:,:,i);
fc=f(85:546, 136:586);
imagen=fc;
lol(:,:,i)=fc;

nfc=fc/max(max(fc));
gg(:,:,i)=nfc;
end

for i=1:size(gg,3)
figure(64);
subplot(211),imagesc(gg(:,:,i).*uint8(m));axis image;
subplot(212),imagesc(lol(:,:,i));axis image;
pause(0.01)
end
