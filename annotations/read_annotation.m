clc
clear
close all;

annotation_name = 'annotation_file_121316';
load(annotation_name)

Tabla{1,1} = 'Patient number';
Tabla{1,2} = 'Take number';
Tabla{1,3} = 'Frame number';
Tabla{1,4} = 'Zone';
Tabla{1,5} = 'State';
Tabla{1,6} = 'Comments';
Tabla{1,7} = 'initial position';
Tabla{1,8} = 'final position';

contador=2;

for ii=1:size(annotation_file,1)
        regiones = annotation_file{ii}.regions;

    Tabla{contador,1} = annotation_file{ii}.patient;
    Tabla{contador,2} = annotation_file{ii}.take;
    Tabla{contador,3} = annotation_file{ii}.frame;
    Tabla{contador,4} = ['Zone ' annotation_file{ii}.zone];
    
    if (annotation_file{ii}.state == 0)
        Tabla{contador,5} = 'Normal';
    else
        if (annotation_file{ii}.state == -1)
            Tabla{contador,5} = 'Not enough quality';
        else
            if ((annotation_file{ii}.state == 1) && (size(regiones,1)>0))
                Tabla{contador,5} = 'Structure detected';
            else
                Tabla{contador,5} = 'Normal';

            end
        end
    end
    Tabla{contador,6} = annotation_file{ii}.comment;
    
    
    
    if (size(regiones,1)>0)
    
    for jj=1:size(regiones,1)
        Tabla{contador,7} = regiones(jj,1);
        Tabla{contador,8} = regiones(jj,1)+regiones(jj,2);
        contador  = contador+1;
    end
    
    else
    contador  = contador+1;
    end
    
    
    
end

xlswrite([annotation_name], Tabla)