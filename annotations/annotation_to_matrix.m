clc
clear
close all;

annotation_name = 'annotation_file_manual';
load(annotation_name)


num_lines = 128;
positions = linspace (-19.2,19.2,num_lines);

nombre_anterior = '';

for ii=1:size(annotation_file,1)
    
    if (strcmp(annotation_file{ii}.name_video,nombre_anterior)~=1) %if new video
        GT = [];
        contador = 1;
        nombre_file = ['Zone' annotation_file{ii}.zone '/' annotation_file{ii}.patient 'TK' annotation_file{ii}.take ];
        mkdir(['Zone' annotation_file{ii}.zone])
    end
    
    regiones = annotation_file{ii}.regions;
    
    if (annotation_file{ii}.state == 0)
        GT(contador,:) = zeros(1,num_lines);
    else
        if (annotation_file{ii}.state == -1)
            GT(contador,:) = -ones(1,num_lines);
        else
            if ((annotation_file{ii}.state == 1) && (size(regiones,1)>0))
               fila = zeros(1,num_lines);
               for jj=1:size(regiones,1)         %iterate over regions
                    inicio = regiones(jj,1);
                    fin = regiones(jj,1)+regiones(jj,3);
                    fila((positions > inicio) & (positions < fin)) = 1;
               end
               GT(contador,:) = fila;
            else
                GT(contador,:) = zeros(1,num_lines);

            end
        end
    end
    contador = contador+1;
    nombre_anterior = annotation_file{ii}.name_video;
    save (nombre_file, 'GT');
end