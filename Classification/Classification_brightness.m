clc;
clear;
close all;


%% Cargar datos y ordenarlos

% folder_resultados = '/home/gustavo/Documents/MATLAB/pneumonia_EI/neumonia sergio/Resultados/Output 14-Nov-2016/';
folder_resultados = '/home/gustavo/Documents/MATLAB/pneumonia_EI/neumonia sergio/Results-Bright-algorithm/'
folder_gt = '/home/gustavo/Documents/MATLAB/pneumonia_EI/Sonix_data/Ground Truth/'; %folder con el groundtruth

lista_archivos = dir(folder_resultados);
lista_archivos = lista_archivos(3:end); %Eliminar .. y .

features  = cell(length(lista_archivos),1);
true_labels = cell(length(lista_archivos),1);

for ll=1:length(lista_archivos)
    
    % Leer y poner los features en una estructura
    load ([folder_resultados  lista_archivos(ll).name],'finalt');
%     features{ll}=  (medfilt1(Mask,5,[],2));
    features{ll}= finalt(1:end,1:end);
    % Formar estructura con las etiquetas reales
    nombre_recortado = lista_archivos(ll).name(5:end); % nombre sin el prefijo output
    if ( strcmp('Sano',lista_archivos(ll).name(1:4))) % si es sano
        true_labels{ll} =  features{ll}*0; %el groundtruth es todo sano
    else
        % leer groundtruth
        load ([folder_gt nombre_recortado]);
        true_labels{ll} = GT(1:end,1:end);
    end
    
    clear finalt
    clear GT
end


possible_thresholds = 0:0.02:1.5;

metrics = zeros(length(possible_thresholds),2);  % sensitivity, specificity


label_pneumonia = 1;
label_healthy = 0;

% primera version. Sobre toda la muestra para comprobar resultados de Omar,
% posteriormente se agregará cross validation
num_patients = length(lista_archivos);
for pp=1:length(possible_thresholds)
    estim=[];
    Y=[];
    %% clasificar
    for ii=1:num_patients
        class_pat = double(features{ii}>possible_thresholds(pp));
%         class_pat = (medfilt1(class_pat,5,[],1)); %"suavizado"
        estim = [estim ;class_pat(:)];
        Y = [Y ;(true_labels{ii}(:))]; %0 sano, 1 enfermo
    end
    
    %% calcular especificidad y sensitividad
    
    TP = sum((estim==label_pneumonia) & (Y==label_pneumonia));  %true positives
    TN = sum((estim==label_healthy) & (Y==label_healthy));  %true negatives
    FN = sum((estim==label_healthy) & (Y==label_pneumonia));  %false negatives
    FP = sum((estim==label_pneumonia) & (Y==label_healthy));  %true negatives
    
    metrics(pp,1) = TP/(TP+FN);
    metrics(pp,2) = TN/(TN+FP);
end

ejex = 1-metrics(:,2);
ejey = metrics(:,1);
figure
plot(ejex,ejey)
xlabel('1-specificity') 
ylabel('sensitivity') 





%% Entrenamiento del modelo
num_patients = length(lista_archivos);
X=[];
Y=[];
for ii=1:num_patients
    X = [X ;features{ii}(:)];
    Y = [Y ;true_labels{ii}(:)]; %1 sano, 2 enfermo
end
Y=Y+1;


% B = mnrfit(X,Y);
bins = linspace(min(X),max(X),100);
h1 = hist(X(Y==1),bins);
h1=h1/sum(h1);
h2 = hist(X(Y==2),bins);
h2=h2/sum(h2);
figure, bar(bins,h1)
hold on, bar(bins,h2,'r')


%% Testeo
distancias = sqrt((ejex).^2+(1-ejey).^2);
[~,pos_min]=min(distancias);
thresh_opt = possible_thresholds(pos_min)
specificity = metrics(pos_min,2)
sensitivity = metrics(pos_min,1)
class_pat = double(features{ii}>thresh_opt);
TP = sum((estim==label_pneumonia) & (Y==label_pneumonia));  %true positives
TN = sum((estim==label_healthy) & (Y==label_healthy));  %true negatives
FN = sum((estim==label_healthy) & (Y==label_pneumonia));  %false negatives
FP = sum((estim==label_pneumonia) & (Y==label_healthy));  %true negatives