function [ X,Y ] = selectSample(X,Y,label_healthy,label_pneumonia,num_per_class)
%SELECTSAMPLE Summary of this function goes here
%   Detailed explanation goes here
   

    if nargin < 5
        num_per_class = sum(Y==label_pneumonia);
    end

    X_sanos = X(Y==label_healthy,:);
    X_enfermos = X(Y==label_pneumonia,:);

    rng(10) % results replicable
    permutation = randperm(size(X_sanos,1));
    permutation = permutation(1:num_per_class);
    X_sanos = X_sanos(permutation,:);
    
    permutation = randperm(size(X_enfermos,1));
    permutation = permutation(1:num_per_class);

    X_enfermos = X_enfermos(permutation,:);
    
    X=[X_sanos; X_enfermos];
    Y=[label_healthy*ones(size(X_sanos,1),1); label_pneumonia*ones(size(X_enfermos,1),1)];



end

