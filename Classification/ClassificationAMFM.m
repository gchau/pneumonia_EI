clc;
clear;
close all;

sigmoid = @(x) 1.0 ./ ( 1.0 + exp(-x) );

%% Cargar datos y ordenarlos
label_pneumonia = 1;
label_healthy = 0;

% folder_resultados = '/media/gustavo/A076C2B676C28C8A/Output_AMFM_sinsegm/';
folder_resultados = '/media/gustavo/A076C2B676C28C8A/Output_AMFM1/'
folder_gt = '/home/gustavo/Documents/MATLAB/pneumonia_EI/Sonix_data/Ground Truth/'; %folder con el groundtruth

lista_archivos = dir(folder_resultados);
lista_archivos = lista_archivos(3:end); %Eliminar .. y .

features  = cell(length(lista_archivos),1);
true_labels = cell(length(lista_archivos),1);

for ll=1: length(lista_archivos)
    
    lista_archivos(ll).name
    
    nombre_recortado = lista_archivos(ll).name(6:end); % nombre sin el prefijo de sano o caso
    
    
    
    if ( strcmp('Sano',lista_archivos(ll).name(1:4))) % si es sano
        disp('sano')
        % Leer y poner los features en una estructura
        load ([folder_resultados  lista_archivos(ll).name],'Mask1','Mask2');
        features{ll}=  cat(3,Mask1,Mask2) ;
        
        true_labels{ll} =  features{ll}(:,:,1)*0; %el groundtruth es todo sano
        
        clear Mask
        clear GT
    else %enfermo
        disp('enfermo')
        nombre_gt = [folder_gt nombre_recortado];
        
        if ~(exist(nombre_gt,'file')==2);
            disp('No hay GT de este archivo')
        else
   
            % Leer y poner los features en una estructura
            load ([folder_resultados  lista_archivos(ll).name],'Mask1','Mask2');
            features{ll}=  cat(3,Mask1,Mask2) ;
            
            % leer groundtruth
            load ([folder_gt nombre_recortado]);
            true_labels{ll} = GT(1:end,1:end);
            clear Mask
            clear GT
        end
        
    end
    
    
    
    
    
    
end


possible_thresholds = 0:0.02:1;

metrics = zeros(length(possible_thresholds),2);  % sensitivity, specificity


label_pneumonia = 1;
label_healthy = 0;

% primera version. Sobre toda la muestra para comprobar resultados de Omar,
% posteriormente se agregará cross validation
num_patients = length(lista_archivos);
X=[];
Y=[];
%% clasificar
for ii=1:num_patients

    [axial_dim, lat_dim, num_feat] = size(features{ii});
%     actual = zeros(axial_dim*lat_dim,num_feat);
    actual=[];
    for zz=1:num_feat
        cur_feat = features{ii}(:,:,zz);
        actual(:,zz) = cur_feat(:);
    end
    X = [X; actual];


    Y = [Y ;(true_labels{ii}(:))]; %0 sano, 1 enfermo
end

% Seleccionar muestra y reordernar data de manera aleatoria
num_folds=20;

num_per_class = floor(sum(Y==1)/num_folds)*num_folds;
[ X,Y ] = selectSample(X,Y,label_healthy,label_pneumonia,num_per_class);
X = X(:,[2 8 11 17]);

%% Entrenamiento del modelo
[ X_folds, Y_fold ] = divide_in_folds( X,Y,label_healthy,label_pneumonia,num_folds ) ;%división en folds


%% cross-validation scheme

% num_folds = length(X_folds);
train_metrics = zeros (10,2);
cv_metrics = zeros (10,2); % 1st column sensitivity, 2nd column specificity


for cc=1:num_folds
    
    disp(['Training fold ' num2str(cc) '/' num2str(num_folds)])
    
    X_train = [];
    Y_train = [];
    for xx=1:num_folds
        if (xx ~= cc)
            X_train = [X_train; X_folds{xx}];
            Y_train = [Y_train; Y_fold];
        else
            X_test = X_folds{cc};
            Y_test = Y_fold;
        end
    end
    
    % train logistic regression model
    Y_mod = Y_train + 1; % compatible con logistic regression function
    maximos = max(abs(X_train),[],1);
%     X = X ./ repmat(maximos,[size(X,1) 1]);
    B = mnrfit(X_train,Y_mod);

    X_mod = [ones(size(X_train,1),1) X_train];
    Y_pred = sigmoid(X_mod*B);
    
    %% ROC Curve analysis
    
    possible_thresholds = 0:0.01:1;
    metrics_roc = zeros(length(possible_thresholds),1);
    for pp=1:length(possible_thresholds)
        [ metrics_roc(pp,1), metrics_roc(pp,2), metrics_roc(pp,3) ] = treshold_metrics( Y_pred,Y_train, label_healthy, label_pneumonia, possible_thresholds(pp));
    end
    
    ejex = 1-metrics_roc(:,2);
    ejey = metrics_roc(:,1);
    distancias = sqrt((ejex).^2+(1-ejey).^2);
    [~,pos_min]=min(distancias);
    thresh_opt = possible_thresholds(pos_min);
    
    
%     figure
%     plot(ejex,ejey)
%     xlabel('1-specificity') 
%     ylabel('sensitivity') 
%     
    roc_curves{cc} = metrics_roc;
    thresholds(cc) = thresh_opt;
    
    %% final metrics
    train_metrics(cc,1) = metrics_roc(pos_min,1); %sensitivity
    train_metrics(cc,2) = metrics_roc(pos_min,2); %specificity
    train_metrics(cc,3) = metrics_roc(pos_min,3); %specificity

    X_cv = [ones(size(X_test,1),1) X_test];
    Y_cv = sigmoid(X_cv*B); 
    [ cv_metrics(cc,1), cv_metrics(cc,2), cv_metrics(cc,3) ] = treshold_metrics( Y_cv,Y_test, label_healthy, label_pneumonia, thresh_opt);
    
    
end

save('amfm_sonix','cv_metrics','train_metrics','roc_curves','ejex','ejey')





% 
% Y_mod = Y + 1; % compatible con logistic regression function
% X = X(:,[2,8]);
% maximos = max(abs(X),[],1);
% X = X ./ repmat(maximos,[size(X,1) 1]);
% 
% B = mnrfit(X,Y_mod);
% 
% X_mod = [ones(size(X,1),1) X];
% Y_pred = sigmoid(X_mod*B);
% 
% %% ROC CURVE
% 
% possible_thresholds = 0:0.01:1;
% metrics = zeros(length(possible_thresholds),2);  % sensitivity, specificity
% 
% 
% num_patients = length(lista_archivos);
% for pp=1:length(possible_thresholds)
%     estim= Y_pred<possible_thresholds(pp);
%     
%     TP = sum((estim==label_pneumonia) & (Y==label_pneumonia));  %true positives
%     TN = sum((estim==label_healthy) & (Y==label_healthy));  %true negatives
%     FN = sum((estim==label_healthy) & (Y==label_pneumonia));  %false negatives
%     FP = sum((estim==label_pneumonia) & (Y==label_healthy));  %true negatives
%     
%     metrics(pp,1) = TP/(TP+FN);
%     metrics(pp,2) = TN/(TN+FP);
% end
% 
% %% Draw curve
% 
% ejex = 1-metrics(:,2);
% ejey = metrics(:,1);
% figure
% plot(ejex,ejey)
% xlim([0 1])
% ylim([0 1])
% xlabel('1-specificity') 
% ylabel('sensitivity') 
% 
% 
% distancias = sqrt((ejex).^2+(1-ejey).^2);
% [~,pos_min]=min(distancias);
% thresh_opt = possible_thresholds(pos_min)
% specificity = metrics(pos_min,2)
% sensitivity = metrics(pos_min,1)
