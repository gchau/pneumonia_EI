function [ sensitivity, specificity, F1 ] = treshold_metrics( Y_pred,Y, label_healthy, label_pneumonia, treshold)
%TRESHOLD_METRICS Summary of this function goes here
%   Detailed explanation goes here
estim = (Y_pred<treshold);
%% calcular especificidad y sensitividad

TP = sum((estim==label_pneumonia) & (Y==label_pneumonia));  %true positives
TN = sum((estim==label_healthy) & (Y==label_healthy));  %true negatives
FN = sum((estim==label_healthy) & (Y==label_pneumonia));  %false negatives
FP = sum((estim==label_pneumonia) & (Y==label_healthy));  %true negatives

sensitivity = TP/(TP+FN);
specificity = TN/(TN+FP);

recall = sensitivity;
precision = TP/(TP+FP);
F1 = 2/((1/recall)+(1/precision));

end

