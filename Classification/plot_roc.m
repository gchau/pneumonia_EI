clc;
clear
close all;

num_roc = 5;

% amfm_name = 'amfm_sonix.mat';
% spect_name = 'spect_sonix.mat';
amfm_name = 'amfm_vera.mat';
spect_name = 'spect_vera.mat';

load(amfm_name)
amfm_rocs = roc_curves{num_roc}(:,1:2);
load(spect_name)
spect_rocs = roc_curves{num_roc}(:,1:2);

sens_spect = spect_rocs(:,1);
spec_spect = spect_rocs(:,2);

sens_amfm = amfm_rocs(:,1);
spec_amfm = amfm_rocs(:,2);

figure
plot(1-spec_spect,sens_spect,'b','lineWidth',1.5)
hold on
plot(1-spec_amfm,sens_amfm,'r','lineWidth',1.5)
legend('SF','AM-FM')
xlim([0,1])
ylim([0,1])


for ii=1:length(roc_curves)
load(amfm_name)
    amfm_rocs = roc_curves{ii}(:,1:2);
load(spect_name)
    spect_rocs = roc_curves{ii}(:,1:2);
    
    sens_spect = spect_rocs(:,1);
    spec_spect = spect_rocs(:,2);
    diffs = abs(diff([0;1-spec_spect]));
    spect_auc(ii) = sum(sens_spect.*diffs);

    
    sens_amfm = amfm_rocs(:,1);
    spec_amfm = amfm_rocs(:,2);
    diffs = abs(diff([0;1-spec_amfm]));
    amfm_auc(ii) = sum(sens_amfm.*diffs);
end

mean(spect_auc)
mean(amfm_auc)
