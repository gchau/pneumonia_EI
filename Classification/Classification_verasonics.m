clc;
clear;
close all;


%% Cargar datos y ordenarlos

% folder_resultados = '/home/gustavo/Documents/MATLAB/pneumonia_EI/neumonia sergio/Resultados/Output 14-Nov-2016/';
folder_resultados = '/home/gustavo/Documents/pneumo_verasonics_slope';
folder_gt = '/home/gustavo/Documents/MATLAB/pneumonia_EI/annotations/Zone1'; %folder con el groundtruth

lista_gt = dir( [folder_gt '/*.mat']);

features  = cell(length(lista_gt),1);
true_labels = cell(length(lista_gt),1);

for ll=1: length(lista_gt)
    lista_gt(ll).name
    
    nombre_output = [folder_resultados '/output_' lista_gt(ll).name ];
    
    if ~(exist(nombre_output,'file')==2)
        disp('No hay data de este archivo')
    else
        load ([folder_gt '/' lista_gt(ll).name]); % load groundtruth
        load (nombre_output,'Mask');
        true_labels{ll} = GT';
        features{ll}=  medfilt1(Mask,3,[],2);
        
%         num_min = min([size(true_labels{ll},2),size(features{ll},2)]);
%         true_labels{ll} = true_labels{ll}(:,1:num_min);
%         features{ll} = features{ll}(:,1:num_min);
        
        true_labels{ll} = true_labels{ll};
        features{ll} = features{ll};

    end
    
end


label_pneumonia = 1;
label_healthy = 0;


%% Entrenamiento del modelo
num_patients = length(features);
X=[];
Y=[];
for ii=1:num_patients

    [axial_dim, lat_dim, num_feat] = size(features{ii});
%     actual = zeros(axial_dim*lat_dim,num_feat);
    actual=[];
    for zz=1:num_feat
        cur_feat = features{ii}(:,:,zz);
        actual(:,zz) = cur_feat(:);
    end
    X = [X; actual];


    Y = [Y ;(true_labels{ii}(:))]; %0 sano, 1 enfermo
end

% Seleccionar muestra y reordernar data de manera aleatoria
num_folds=10;
num_per_class = floor(sum(Y==1)/num_folds)*num_folds;
[ X,Y ] = selectSample(X,Y,label_healthy,label_pneumonia,num_per_class);

%% Exploratory analysis

% bins = linspace(min(X),max(X),100);
% h1 = hist(X(Y==label_healthy),bins);
% h1=h1/sum(h1);
% h2 = hist(X(Y==label_pneumonia),bins);
% h2=h2/sum(h2);
% figure, bar(bins,h1)
% hold on, bar(bins,h2,'r')


% %% Exploratory analysis
% 
% bins = linspace(min(X),max(X),100);
% h1 = hist(X(Y==label_healthy),bins);
% h1=h1/sum(h1);
% h2 = hist(X(Y==label_pneumonia),bins);
% h2=h2/sum(h2);
% figure, bar(bins,h1)
% hold on, bar(bins,h2,'r')

%% Entrenamiento del modelo
[ X_folds, Y_fold ] = divide_in_folds( X,Y,label_healthy,label_pneumonia,num_folds ) ;%división en folds


%% cross-validation scheme

num_folds = length(X_folds);
train_metrics = zeros (10,2);
cv_metrics = zeros (10,2); % 1st column sensitivity, 2nd column specificity


for cc=1:num_folds
    
    disp(['Training fold ' num2str(cc) '/' num2str(num_folds)])
    
    X_train = [];
    Y_train = [];
    for xx=1:num_folds
        if (xx ~= cc)
            X_train = [X_train; X_folds{xx}];
            Y_train = [Y_train; Y_fold];
        else
            X_test = X_folds{cc};
            Y_test = Y_fold;
        end
    end
    
    % train logistic regression model
    Y_mod = Y_train + 1; % compatible con logistic regression function
    maximos = max(abs(X_train),[],1);
%     X = X ./ repmat(maximos,[size(X,1) 1]);
    B = mnrfit(X_train,Y_mod);

    X_mod = [ones(size(X_train,1),1) X_train];
    Y_pred = sigmoid(X_mod*B);
    
    %% ROC Curve analysis
    
    possible_thresholds = 0:0.01:1;
    metrics_roc = zeros(length(possible_thresholds),1);
    for pp=1:length(possible_thresholds)
        [ metrics_roc(pp,1), metrics_roc(pp,2), metrics_roc(pp,3) ] = treshold_metrics( Y_pred,Y_train, label_healthy, label_pneumonia, possible_thresholds(pp));
    end
    
    ejex = 1-metrics_roc(:,2);
    ejey = metrics_roc(:,1);
    distancias = sqrt((ejex).^2+(1-ejey).^2);
    [~,pos_min]=min(distancias);
    thresh_opt = possible_thresholds(pos_min);
%     
%     
%     figure
%     plot(ejex,ejey)
%     xlabel('1-specificity') 
%     ylabel('sensitivity') 
%     
    roc_curves{cc} = metrics_roc;
    thresholds(cc) = thresh_opt;
    
    %% final metrics
    train_metrics(cc,1) = metrics_roc(pos_min,1); %sensitivity
    train_metrics(cc,2) = metrics_roc(pos_min,2); %specificity
    train_metrics(cc,3) = metrics_roc(pos_min,3); %specificity


    X_cv = [ones(size(X_test,1),1) X_test];
    Y_cv = sigmoid(X_cv*B); 
    [ cv_metrics(cc,1), cv_metrics(cc,2), cv_metrics(cc,3) ] = treshold_metrics( Y_cv,Y_test, label_healthy, label_pneumonia, thresh_opt);
    
    
end

save('spect_vera','cv_metrics','train_metrics','roc_curves','ejex','ejey')

%% Entrenamiento del modelo
[ X_folds, Y_fold ] = divide_in_folds( X,Y,label_healthy,label_pneumonia,num_folds ) ;%división en folds


%% cross-validation scheme

num_folds = length(X_folds);
train_metrics = zeros (10,2);
cv_metrics = zeros (10,2); % 1st column sensitivity, 2nd column specificity


for cc=1:num_folds
    
    disp(['Training fold ' num2str(cc) '/' num2str(num_folds)])
    
    X_train = [];
    Y_train = [];
    for xx=1:num_folds
        if (xx ~= cc)
            X_train = [X_train; X_folds{xx}];
            Y_train = [Y_train; Y_fold];
        else
            X_test = X_folds{cc};
            Y_test = Y_fold;
        end
    end
    
    % train logistic regression model
    Y_mod = Y_train + 1; % compatible con logistic regression function
    maximos = max(abs(X_train),[],1);
%     X = X ./ repmat(maximos,[size(X,1) 1]);
    B = mnrfit(X_train,Y_mod);

    X_mod = [ones(size(X_train,1),1) X_train];
    Y_pred = sigmoid(X_mod*B);
    
    %% ROC Curve analysis
    
    possible_thresholds = 0:0.01:1;
    metrics_roc = zeros(length(possible_thresholds),1);
    for pp=1:length(possible_thresholds)
        [ metrics_roc(pp,1), metrics_roc(pp,2), metrics_roc(pp,3) ] = treshold_metrics( Y_pred,Y_train, label_healthy, label_pneumonia, possible_thresholds(pp));
    end
    
    ejex = 1-metrics_roc(:,2);
    ejey = metrics_roc(:,1);
    distancias = sqrt((ejex).^2+(1-ejey).^2);
    [~,pos_min]=min(distancias);
    thresh_opt = possible_thresholds(pos_min);
    
    
    figure
    plot(ejex,ejey)
    xlabel('1-specificity') 
    ylabel('sensitivity') 
    
    roc_curves{cc} = metrics_roc;
    thresholds(cc) = thresh_opt;
    
    %% final metrics
    train_metrics(cc,1) = metrics_roc(pos_min,1); %sensitivity
    train_metrics(cc,2) = metrics_roc(pos_min,2); %specificity
    train_metrics(cc,3) = metrics_roc(pos_min,3); %specificity


    X_cv = [ones(size(X_test,1),1) X_test];
    Y_cv = sigmoid(X_cv*B); 
    [ cv_metrics(cc,1), cv_metrics(cc,2), cv_metrics(cc,3) ] = treshold_metrics( Y_cv,Y_test, label_healthy, label_pneumonia, thresh_opt);
    
    
end
