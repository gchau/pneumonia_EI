function [ X_folds, Y ] = divide_in_folds( X,Y,label_healthy,label_pneumonia, num_folds )
%DIVIDE_IN_FOLDS Divide la data en 10 grupos para realizar la validación
%cruzada. X_fold es una celda donde cada elemento consiste de una partición
% de X. Y es el mismo para todas las particiones

X_sanos = X(Y==label_healthy);
X_enfermos = X(Y==label_pneumonia);

% with 10-fold CV 
size_fold1 = length(X_sanos)/num_folds;
size_fold2 = length(X_enfermos)/num_folds;

% Dividir en 10 secciones
for cc=1:num_folds
    sanos = X_sanos((cc-1)*size_fold1+1:cc*size_fold1,:);
    enfermos = X_enfermos((cc-1)*size_fold2+1:cc*size_fold2,:);
    X_folds{cc}=[sanos; enfermos];
end

Y = [zeros(size(sanos,1),1); ones(size(enfermos,1),1)];

end

