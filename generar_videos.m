clear;
close all;
home;
folder_videos = '/home/gustavo/Documents/MATLAB/pneumonia_EI/videos';
% folder_rf_data = '/media/gustavo/TOSHIBA EXT/ADQUISITIONS/PNEUMONIA';
% folder_rf_data = '/media/gustavo/TOSHIBA EXT/REAL_RAW_DATA_FG10S';

list_directories = dir('/media/gustavo/TOSHIBA EXT/Beamformed Data');
list_directories=list_directories(3:end);

for iii=1:length(list_directories)
% folder_rf_data =['/media/gustavo/TOSHIBA EXT/REAL_RAW_DATA_first'];


folder_rf_data =['/media/gustavo/TOSHIBA EXT/Beamformed Data/' list_directories(iii).name];
% folder_rf_data = '/media/gustavo/TOSHIBA EXT/Beamformed Data/prueba';

% folder_headers = '/home/gustavo/Documents/MATLAB/pneumonia_EI/HEAD';
folder_headers = '/home/gustavo/Documents/MATLAB/pneumonia_EI/HEADS_8dic';


N = 128;
overlap=10;
umbral = -20;

%cd('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pacientes_Verasonics\HEAD_G')
% cd('/home/gustavo/Documents/MATLAB/pneumonia_EI/HEAD')
% folder_head=dir;
% folder_head=struct2cell(folder_head);
% folder_HEAD=folder_head(1,3:end)'; % obviar folder . y ..
% folder= cell2mat(folder_HEAD);


% %cd('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pacientes_Verasonics\DATA_G')
% cd()
% folder_data=dir;
% folder_data=struct2cell(folder_data);
% folder_DATA=folder_data(1,3:end)';
% folder_DATA= cell2mat(folder_DATA);
%
% number_patients=size(folder_DATA,1);
% CMAP= repmat(((0:1/128:1).^2)',[1 3]);
lista_archivos = dir ([folder_rf_data]);
lista_archivos=struct2cell(lista_archivos);
lista_archivos=lista_archivos(1,3:end)';
pitch=0.3e-3;
n=128;
xl = linspace (-19.2,19.2,n);
zl=linspace(0,292*1540/(7.5*10^3),2336);%2366 = size(video_final) 292*2*4

for aa = 1:length(lista_archivos)
    
    file = lista_archivos{aa,1};
    disp([file])
        file_trimmed = file(1:11);

    
    header_full_path = [folder_headers '/HEAD' file];
    
    check_file = exist(header_full_path,'file');
    
    
    if check_file == 2
            load ([folder_rf_data '/' file])

        mkdir(['videos/' file_trimmed])
        load(header_full_path);
        nombre_preli = ['videos/' file_trimmed '/VIRF_zone' num2str(UIValues.zone) '_' file(1:end-4) ];
       
        writerObj = VideoWriter(nombre_preli);
        writerObj.FrameRate = 1;
        open(writerObj)
        
      
       
        for frame=1:size(video_final,3)
            figure(1)
            Bmode=video_final(:,:,frame);
            Bmode=abs(hilbert(Bmode));
            Bmode = Bmode/(max(Bmode(:)));
            aa=fliplr([2650,2650]);
            xtgc = linspace(0,292*1540/(7.5*10^3),2);
            tgc = interp1(xtgc,aa,zl);
            
            matrix_tgc = repmat(tgc(:),[1 128]);
            
            Bmode = Bmode.*matrix_tgc;
            Bmode=(20*log10(Bmode)+20*log10(UIValues.pgain*(2^16-1)/(2^15-1)));
%             Bmode=UIValues.pgain*Bmode*(2^15-1)/(2^13-1);
%             Bmode=(20*log10(Bmode)-UIValues.reject)/UIValues.DynRng;
%             Bmode(Bmode<0)=0;Bmode(Bmode>1)=1;
           %Bmode=imfilter(Bmode,fspecial('average',[6 4]));
Bmode=medfilt2(Bmode,[3 3]);
Bmode=imfilter(Bmode,fspecial('average',[10 5]));
%Bmode=imfilter(Bmode,fspecial('prewitt'));

Bmode=(imsharpen(Bmode,'Amount',2,'Radius',0.1','Threshold',0.2));
            CMAP= repmat(((0:1/64:1).^UIValues.gamma)',[1 3]);
%             subplot(1,2,1),
            im = imagesc(xl,zl,Bmode);
            title(['Zone ' num2str(num2str(UIValues.zone)) ' - Frame ' num2str(frame)])
            set(gca,'clim',[0 1]);
            xlabel('azimuth (mm)'),ylabel('depth (mm)')
            set(gcf,'Colormap',CMAP);
            set(im,'CDataMapping','scaled');

%             set(gcf,'CLim',[UIValues.reject UIValues.DynRng]);
            set(gca,'clim',[UIValues.reject UIValues.DynRng]);
            axis image;
            secsPerImage = [5 10 15];
            %open(writerObj);
%             subplot(1,2,2), plot(Mask(:,frame))
%             xlim([1 128])
%             ylim([-1.5 1.5])
            a=figure(1);
            h = getframe(a);
            writeVideo(writerObj,h);
%             pause(0.01)
        end
        
        close(writerObj);
        clear('writerObj')
    else
        disp(['No se encontró header de ' file])
    end
    
end
%                 toc
%                 filename = strcat(output_folder,folder_DATA(id,1:end));
% %                save(filename,'Imagen','Mask','ini','fin','BW2aux');
%                 save(filename','Mask','ini','fin');
%                 clear Mask;
%                 %clear Imagen;
%                 clear ini;
%                 clear fin;
%                 %clear BW;

%end


end