%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETECCION DE NEUMONIA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;
home;
code = pwd;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPARACION DE LA MUESTRA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

scanner = input('Select scanner: \n(1)Ultrasonix\n(2)Verasonix\n(3)Siemens\n ');

% Interfaz de lectura de datos
input_folder = uigetdir;
cd(input_folder)

% Deteccion de la ruta de los archivos
cd ..


output_folder = [code '\Output ' date '\ ']
cd(input_folder);

if exist(output_folder,'dir')==7;
else
    mkdir(output_folder);
end

folder = dir;
directorio = {folder.isdir};
directorio = find(cell2mat(directorio));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PROGRAMA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for id = directorio(3:end)
    
    % lectura del archivo
    cd(code);
    folder_loop = strcat(input_folder,'\',folder(id).name,'\');
    cd(folder_loop);
    switch scanner
        case 1
    images  = dir('*.rf');
        case 3
    images  = dir('*.rfd');
    end
    
    for fileid = 6%1:length(images)
        
        
        switch scanner
            case 1
                filename=images(fileid).name;
                fileCompleto = strcat(folder_loop,filename);
                cd(code);
                [RF,head] = RPread(fileCompleto, '6.0.5');
                xw=linspace(0,0.038,head.w);
            case 2
                preVisual
            case 3
                filename=images(fileid).name;
                fileCompleto = strcat(folder_loop,filename);
                cd(code);
                 
                [signal,head] = readFile(fileCompleto);
                RF = double(signal.RfData);
               
                harmonic=input('Size of 2nd dimention of normal data: ');
                
                if harmonic==size(RF,2)
                    xw=linspace(0,0.038,size(RF,2));
                else
                                            RF=RF(:,1:2:end,:);
%                     RF=RF(:,1:2:end,:)+RF(:,2:2:end,:);
                xw=linspace(0,0.038,size(RF,2));
                
                end
                
                
            otherwise
                disp('other value')
        end
                
        tic
        
        % Analisis frame por frame:
        
        archivo=images(fileid).name;
        
        switch scanner
        case 1
        nombre=strcat(input_folder,'\Pleura\',archivo(1:end-3));
        case 3
        nombre=strcat(input_folder,'\Pleura\',archivo(1:end-4));
        head.sf=40e6;
        end
        
        load(nombre);
        
        for frame = 1:size(RF,3)
            
            frame
            
            iRF= RF(:,:,frame);
            % Filtrado de bajas frecuencias
            [B] = fir1(100,1.5e6/head.sf/2,'high');
            for indexFIR=1:size(iRF,2)
                iRF(:,indexFIR)=filter(B,1,iRF(:,indexFIR));
            end
            %Imagen en Modo-B
            iUS = 20*log10(abs(hilbert(iRF)));
            iUS = iUS - max(iUS(:));
            
            
            %Recorte de la imagen segun la segmentacion de la pleura
            for indice=1:size(iRF,2);
                iRF2(:,indice)= iRF(ini(indice,frame):fin(indice,frame),indice);
            end
            
            factor=10;
            
            parfor ind=1:size(iRF2,2)-factor+1;
                
                Region = iRF2(:,ind:ind+factor-1);
                
                [Angulo,I01,I2] = FeatureFourier2(Region,head);
                
                AngMask{ind}=Angulo;
                Ind01{ind}=I01;
                Ind02{ind}=I2;
            end
            
                Mascara=cell2mat(AngMask);
            
            Mascara=[mean(Mascara)*ones(1,ceil(factor/2)-1) Mascara mean(Mascara)*ones(1,ceil(factor/2))];
            
            Mask(:,frame)=Mascara;
            
            Imagen(:,:,frame)=iUS;
            
            clear iRF2;
            
        end
        
        toc
        
        switch scanner
        case 1
        filename = strcat(output_folder,folder(id).name,'-',...
            images(fileid).name(1:end-3));
        case 3
        filename = strcat(output_folder,folder(id).name,'-',...
            images(fileid).name(1:end-4));
        end
        
        save(filename,'Imagen','Mask','ini','fin');
        
        clear Mask;
        clear Imagen;
        clear ini;
        clear fin;
        
    end
    
    
end
