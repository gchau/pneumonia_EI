%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETECCION DE NEUMONIA VERASONICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;
home;
code = pwd;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPARACION DE LA MUESTRA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%scanner = input('Select scanner: \n(1)Ultrasonix\n(2)Verasonix\n(3)Siemens\n ');

% Interfaz de lectura de datos
input_folder = uigetdir;
cd(input_folder)

% Deteccion de la ruta de los archivos
cd ..

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 N = 128;
 overlap=10;
 umbral = -20;
 
     
cd('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pacientes_Verasonics\HEAD')
folder_head=dir;
folder_head=struct2cell(folder_head);
folder_HEAD=folder_head(1,3:end)';
folder_HEAD= cell2mat(folder_HEAD);


cd('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pacientes_Verasonics\Data')
folder_data=dir;
folder_data=struct2cell(folder_data);
folder_DATA=folder_head(1,3:end)';
folder_DATA= cell2mat(folder_DATA);

number_patients=size(folder_DATA,1);

% dim = 100;
% factor=10;

for pp=1:number_patients

for dim = 50%10:10:100
    
    for factor = 5%10:10:50


        output_folder = [code '\Output ' date '_' 'Verasonics_'  num2str(factor) '_'  num2str(dim) '_'  num2str(overlap) '_' num2str(N) '_' num2str(umbral) '_' '\ ']
        cd(input_folder);

        if exist(output_folder,'dir')==7;
        else
            mkdir(output_folder);
        end

        folder = dir;
        directorio = {folder.isdir};
        directorio = find(cell2mat(directorio));

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % PROGRAMA
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        for id = 3%directorio(3:end)

        % lectura del archivo
        cd(code);
        folder_loop = strcat(input_folder,'\',folder(id).name,'\Data\');
        cd(folder_loop);

        images  = dir('*.mat');
    %    end

        for fileid = 4 %:length(images)


        filename=images(fileid).name;
        fileCompleto = strcat(folder_loop,filename);
        RF = cell2mat(struct2cell(load(filename)));
        %RF=RF(1:1976,:,:);
        load('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pacientes_Verasonics\HEAD\HEAD73644744621TK1.mat')
        %load('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pacientes_Verasonics\HEAD\HEAD73644548761TK1.mat')
        %load('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pacientes_Verasonics\HEAD\HEAD73644550787TK1.mat')
        %load('HEAD73644744621TK1.mat');
      
        %folder_read = strcat(input_folder,'\',folder(5).name,'\'); 
        %cd(folder_read);
        %[signal,head] = readFile(fileCompleto);
        %RF = double(signal.RfData);


        harmonic = 128;

        % IMPORTANTE               harmonic=input('Size of 2nd dimention of normal data: ');

        if harmonic==size(RF,2)
            xw=linspace(0,0.038,size(RF,2));
        else
                                    RF=RF(:,1:2:end,:);
%                     RF=RF(:,1:2:end,:)+RF(:,2:2:end,:);
        xw=linspace(0,0.038,size(RF,2));

        end

        tic

        archivo=images(fileid).name;
        
        nombre=strcat(input_folder,'\Pleura_Verasonics\',archivo(1:end-4));
        freq = Trans.frequency*10^6;% head.sf=40e6;
        load(nombre);

                for frame = 1:size(RF,3)
                    frame
                    iRF= RF(:,:,frame);
                    % Filtrado de bajas frecuencias
                    [B] = fir1(100,1.5e6/freq/2,'high');
                    for indexFIR=1:size(iRF,2)
                        iRF(:,indexFIR)=filter(B,1,iRF(:,indexFIR));
                    end
                    %Imagen en Modo-B
                    iUS = 20*log10(abs(hilbert(iRF)));
                    iUS = iUS - max(iUS(:));


                    %Recorte de la imagen segun la segmentacion de la pleura
                    for indice=1:size(iRF,2);
                        iRF2(:,indice)= iRF(ini(indice,frame):fin(indice,frame),indice);
                    end

         %           factor=10;
                    cd(code);
                    parfor ind=1:size(iRF2,2)-factor+1;

                        Region = iRF2(:,ind:ind+factor-1);

                        [Angulo,I01,I2] = FeatureFourier2_Verasonics(Region,freq,N,dim,overlap,umbral);

                        AngMask{ind}=Angulo;
                        Ind01{ind}=I01;
                        Ind02{ind}=I2;
                    end

                        Mascara=cell2mat(AngMask);

                    Mascara=[mean(Mascara)*ones(1,ceil(factor/2)-1) Mascara mean(Mascara)*ones(1,ceil(factor/2)-1)];

                    Mask(:,frame)=Mascara;

                    Imagen(:,:,frame)=iUS;

                    clear iRF2;

                end

                toc

        %         switch scanner
        %         case 1
        %         filename = strcat(output_folder,folder(id).name,'-',...
        %             images(fileid).name(1:end-3));
        %         case 3
                filename = strcat(output_folder,folder(id).name,'-',...
                    images(fileid).name(1:end-4));
        %         end


                save(filename,'Imagen','Mask','ini','fin');

                clear Mask;
                clear Imagen;
                clear ini;
                clear fin;
         end


        end


        cd(code);
        
    end
end
end
