function [RFFinal] = DataUtil(RF,filename)

Nframe=size(RF,3);
i=1;

for ff=1:Nframe
   
   RFFrame = RF(:,:,ff); 
   mu = max(max(max(RFFrame)));
   
   if mu == 0
       ff;
   else
        RFFinal(:,:,i) = RF(:,:,ff); 
        i=i+1;
   end
    
 %  filename = strcat(output_folder,...
 %           images(fileid).name(1:end));  
end

       save(filename,'RFFinal');
end