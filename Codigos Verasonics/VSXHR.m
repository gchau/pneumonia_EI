% Copyright 2001-2013 Verasonics, Inc.  All world-wide rights and remedies under all intellectual property laws and industrial property laws are reserved.  Verasonics Registered U.S. Patent and Trademark Office.
% 
% VSX (Verasonics Script eXecute) for processing .mat file (filename).
% Usage: run VSX and input 'filename'
%   where filename is the name of a .mat file containing all the structures
%   needed.
%   
%   To allow automated scripts to launch VSX, such as HardwareTest scripts,
%   'filename' may be set before calling VSX.  To prevent subsequently run
%   scripts for inadvertantly using that same 'filename', VSX will use the
%   detected 'filename' once and then delete the variable.
% 
% Program steps;
%   1. Read in structures needed to set up for running sequence.
%      - Use Resource structure to set up RcvData, ImgData, and
%        IQData (if needed.)
%   2. Validate structures to see if consistant.
%   3. Add VDAS parameters if missing and running on VDAS hardware.
%   3. Open GUI window.
%   4. Set up figure window for output display.
%   5. Open VDAS hardware, if available. If hardware not available, use
%      simulate mode. To skip check for hardware, set Resource.Parameters.simulateMode=1.
%   6. Enter processing loop:
%      While exit==0
%         Call runAcq to process events in sequence.
%         Every jump command back to 1st event will return control to Matlab
%            to check for GUI actions.
%            - for Freeze action, stop sequencer and wait in Matlab for 
%              Freeze button to change state.
%   7. On exit, copy back processing buffers from C to Matlab.
%   8. Close hardware if previously opened.

result = hardwareClose(); % close the hardware, in case it was left open from previous run.

% Allow VSX to practically exit and start again if "relaunchVsx" gets set to
% a function name by some external means (like a GUI callback).
% 
% This allows programmatic ability to run VSX, keep it running, but have it load
% a new ".mat" script.  For example, a MATLAB Compiler application could launch
% VSX once and keep it running, yet switch to a new scanhead .mat script.
% 
% The callback hook mechanism allows a relaunch requester to have their
% pre-relaunch commands executed, synchronously, at a point where VSX decides
% it is safe to do so.
% 
% Note that the call to hardwareClose() is skipped on a relaunch as is
% the call to hardwareOpenFirstCall() so we can avoid the overhead of HAL
% hardware open and close on a relaunch.
% 
% There is also an optional "relaunchVsxComplete" callback called below
% after VSX has completed the reload and is about to start running.
relaunchVsx = '';
while(0 ~= exist('relaunchVsx', 'var'))
  % Enter master outer processing loop.
  if(0 == strcmp('', relaunchVsx))
    % OK, we DO have a callback hook to call.
    % 
    % First, close any Verasonics VSX figure windows that may be open
    % from a previous run.
    close(get(0, 'Children'))
    
    % Call caller's callback.
    functionHandle = str2func(relaunchVsx);
    functionHandle();
    % Relauch request is cleared below when VSX clears variables
  end
  
clear runAcq
% temporary cleanup of any spectral Doppler processing.
try
    spectralDoppler('cleanup');  
catch
end

% Clear all variables with a few exceptions.
% 
% NOTE:  "Mcr_" prefixed variables are designed for use by the MATLAB Compiler
% Runtime, though you don't have to use the MCR to use the "Mcr_" prefix.
vars = whos;
for i = 1:size(vars,1)
    if ~(strcmp(vars(i).name,'filename') || ...
         strcmp(vars(i).name,'isCalledHardwareOpenFirstCall') || ...
         ~isempty(strfind(vars(i).name,'Mcr_')) || ... % Name contains "Mcr_"
         strcmp(vars(i).name,'relaunchVsxComplete') || ... % Cleared later below
         strcmp(vars(i).name,'RcvData') || ...
         strcmp(vars(i).name,'vars') || ...
         strcmp(vars(i).name,'Vs_VdasDebugging'))
        clear(vars(i).name);
    end
end

% HAL Vs_HardwareOpen()/Vs_HardwareClose() optimization.
% 
% Calls to open and close the Verasonics VDAS hardware are costly time wise, so
% we want to minimize the calling.  VSX makes several ".mex" calls which all need
% Vs_HardwareOpen().  Instead of open/close in each mex function, we instead
% let each one assume a Vs_HardwareAlreadyOwned return from Vs_HardwareOpen() is
% a "Vs_Success" return and continue as if the mex function itself did the open
% call.
% 
% The open call is meant to be mutually exclusive to a single process.  All these
% mex functions are part of the same process, so it is safe for them to make this
% assumption and achieve this optimization.
% 
% We still want a full error reporting hardware open call, so we call a special
% version here, hardwareOpenFirstCall.  This one will report all desired errors,
% including Vs_HardwareAlreadyOwned, but not "hardware not found" as that condition
% is to be handled later in VSX for "use simulate mode" instead of given an error.
% 
% If this call detects Vs_HardwareAlreadyOwned, then it truely is an error.  Because
% Vs_HardwareClose() was called above, if the hardware is already owned, then
% another legitimate application has the hardware locked, such as someone running
% V1HwDiag in a shell, or perhaps a previous run of VSX crashed and has failed
% to free the lock.  In the later case, a "vdasReset" mex call can often save
% one from a computer reboot to clear the lock.
% 
% Some mex functions, like vdasReset, require a full open/close call, but if
% they are called between other mex functions optimized to allow
% success on Vs_HardwareAlreadyOwned, then they are unaffected as they are still
% making the "first" Vs_HardwareOpen() call (and instead get Vs_Success instead
% of Vs_HardwareAlreadyOwned).
% 
% We want to make the very first call to open the Verasonics VDAS hardware.
% We only want to make this call once though.  We do NOT want to call it on
% a "relaunchVsx".
if(1 ~= exist('isCalledHardwareOpenFirstCall', 'var'))
    % OK, we have NOT yet made this call.
    isCalledHardwareOpenFirstCall = 1;
    
    % This call will succeed, or abort MATLAB with an error message.
    hardwareOpenFirstCall();
end

% Default settings.
maxADRate = 45; % maximum A/D sample rate of 45MHz (temporarily lowered from 60MHz)

activeProfile = 0; % This is set to the profile number of 2nd HV slider when activated by vsx_gui.

% Read in structures from .mat file.
% 
% Use filename variable if it is given and not empty, but only once.
% Otherwise ask the user for the filename.
if(~exist('filename', 'var') || (exist('filename', 'var') && isempty(filename)))
    % OK, the filename variable does NOT exist, so ask the user for the
    % filename.
    %filename = input('Name of .mat file to process: ','s');
    filename = 'L11-4v_128RyLnsSA';
end
% Use a try-catch so if file does not exist we can clear 'filename' for
% next run of VSX.
try
    load(filename);
    displayWindowTitle = filename;
    clear filename;
catch
    % File does not exist.  Clear 'filename' and report error.
    fileNameTemp = filename;
    clear filename;
    error(['"' fileNameTemp '" not found.'])
end

% - Check for minimum required structures present.
vars = whos;
RequiredStructs = {'Trans','Resource','TW','TX','Event'};
n = 0;
for i = 1:size(vars,1)
    if any(strcmp(vars(i).name, RequiredStructs)), n=n+1; end
end
if (n~=5), error('VSX: Trans, Resource, TW, TX and Event structures required as minimum. Exiting...\n'); end

% If .mat file contains an External Function definition, create the function in the temp directory
% and add the directory to the path.  Decoding the function here means that it can be used for an
% updateVDAS or GUI function.
if exist('EF','var')
    for i = 1:size(EF,2)
        if ~isfield(EF(i),'Function') || isempty(EF(i).Function), continue, end
        n = strfind(EF(i).Function{1},'=');
        if ~isempty(n)
            fname = textscan(EF(i).Function{1},'%*s %s %*[^\n]','Delimiter',{'=','('});
        else
            fname = textscan(EF(i).Function{1},'%s %*[^\n]', 'Delimiter','(');
        end
        fid = fopen([tempdir,fname{1}{1},'.m'], 'w');
        fprintf(fid,'function %s\n', EF(i).Function{1});
        for j = 2:size(EF(i).Function,2)
            fprintf(fid,'%s\n', EF(i).Function{j});
        end
    end
    status = fclose(fid);
    addpath(tempdir);
    rehash path
end

% Determine mode of operation.
% - The variable VDAS is used to indicate presence of hardware.
% - When not in simulateMode = 1 (simulate acquisition data), the hardware is assumed to be present
%   (VDAS=1) until it's presence is detected in the hardwareOpen call below.
if isfield(Resource.Parameters,'simulateMode')
    switch Resource.Parameters.simulateMode
        case 0 % user wants to use hardware.
            VDAS = 1;  % Assume hardware is available (override below if not).
            rloopButton = 0;  % used to set state of rcv data loop button on GUI.
            simButton = 0;    % used to set state of simulate button on GUI.
        case 1 % user wants to simulate acquisition
            VDAS = 0;  % if simulate mode, no VDAS parameters are required.
            rloopButton = 0; 
            simButton = 1;
        case 2 % user wants to run script with existing RcvData array.
            VDAS = 1;  % Assume hardware is available (override below if not).
            rloopButton = 1;
            simButton = 0;
    end
else
    % For no Resource.Parameters.simulateMode, use state of VDAS (if it exists) to set simulate mode.
    % (The variable VDAS was provided in old scripts to specify whether to run with the hardware.)
    if exist('VDAS','var') 
        if (VDAS==0)
            Resource.Parameters.simulateMode = 1;
            rloopButton = 0;  % used to set state of rcv data loop button on GUI.
            simButton = 1;    % used to set state of simulate button on GUI.
        else
            Resource.Parameters.simulateMode = 0;
            rloopButton = 0;
            simButton = 0;
        end
    else
        % Assume running with hardware if no simulateMode and VDAS variable doesn't exist.
        Resource.Parameters.simulateMode = 0;
        VDAS = 1;
        rloopButton = 0;
        simButton = 0;
    end
end

% **** Check Resource.Parameters.numTransmit and .numRcvChannels for valid attributes ****
% - Get the number of boards in the VDAS system. If no hardware available, the numBoards = 0.
numBoards = vdasBoardsDetectedGet;
% - If not in simulate mode and vdas boards detected, validate numTransmit and numRcvChannels. When
%   running with the hardware, Resource.Parameters.numTransmit and Resource.Parameters.numRcvChannels
%   must be consistent with hardware resources; In simulateMode=1, the match with hardware is not enforced.
if (numBoards~=0)&&(Resource.Parameters.simulateMode~=1)
    if ~isfield(Resource.Parameters,'numTransmit') || ~isfield(Resource.Parameters,'numRcvChannels')
        error('VSX: Resource.Parameters.numTransmit and/or Resource.Parameters.numRcvChannels not found.\n');
    end
    switch numBoards
        case 1
            if (Resource.Parameters.numTransmit ~= 64)
                error('VSX: Resource.Parameters.numTransmit must equal 64 for single board system.'); 
            end
            if (Resource.Parameters.numRcvChannels ~= 32)
                error('VSX: Resource.Parameters.numRcvChannels must equal 32 for single board system.'); 
            end
        case 2
            if (Resource.Parameters.numTransmit ~= 128)
                error('VSX: Resource.Parameters.numTransmit must equal 128 for two board system.');
            end
            if (Resource.Parameters.numRcvChannels ~= 64)
                error('VSX: Resource.Parameters.numRcvChannels must equal 64 for two board system.');
            end
        case 4
            if (Resource.Parameters.numTransmit ~= 128)&&(Resource.Parameters.numTransmit ~= 256)
                error('VSX: Resource.Parameters.numTransmit must equal 128 or 256 for 4 board system.');
            end
            if (Resource.Parameters.numRcvChannels == 64)
                fprintf('VSX: Resource.Parameters.numRcvChannels = 64, but the hardware has 128 channels.\n');
                fprintf('Do you want VSX to modify Resource.Parameters.numRcvChannels to 128 and change\n');
                Reply = input('RcvBuffer.colsPerFrame to 128 and attempt to run? [y] or n :','s');
                if ~isempty(Reply)&&~strcmpi('Y',Reply), return, end
                Resource.Parameters.numRcvChannels = 128;
                for i = 1:size(Resource.RcvBuffer)
                    Resource.RcvBuffer(i).colsPerFrame = 128;
                end
            elseif (Resource.Parameters.numRcvChannels ~= 128)
                error('VSX: Resource.Parameters.numRcvChannels must equal 128 for 4 board system.'); 
            end
    end
end

% **** Initialize data buffers - RcvData, IQData (if needed), and ImgData ****.
global IQData;
global ImgData;
global ImgDataP;
global LogData;

% If RcvData already exists in workspace, don't initialize, but check for same dimensions
% specified in Resource structure.
initRcvData = 1; % set default, which is to initialize RcvData.
if exist('RcvData','var')
    initRcvData = 0;  % RcvData exists - don't initialize RcvData, if checks pass.
    if size(RcvData,2) ~= size(Resource.RcvBuffer,2)
        initRcvData = 1; 
    else
        for i = 1:size(RcvData,2)
            if size(RcvData{i},1) ~= Resource.RcvBuffer(i).rowsPerFrame
                initRcvData = 1;
            end
            if size(RcvData{i},2) ~= Resource.RcvBuffer(i).colsPerFrame
                initRcvData = 1;
            end
            if size(RcvData{i},3) ~= Resource.RcvBuffer(i).numFrames
                initRcvData = 1;
            end
            if class(RcvData{i}) ~= Resource.RcvBuffer(i).datatype
                initRcvData = 1;
            end
        end
    end
    if (initRcvData == 0)
        fprintf('RcvData in workplace matches Resource.RcvBuffer specification - reusing without clearing.\n');
    else
        clear('RcvData');
    end
end
if (initRcvData == 1) 
    global RcvData;
    numRcvBuffers = size(Resource.RcvBuffer,2);
    RcvData = cell(1,numRcvBuffers);
    for i = 1:numRcvBuffers
        RcvData{i} = zeros(Resource.RcvBuffer(i).rowsPerFrame, ...
                           Resource.RcvBuffer(i).colsPerFrame, ...
                           Resource.RcvBuffer(i).numFrames, ...
                           Resource.RcvBuffer(i).datatype);
    end
end

if isfield(Resource, 'InterBuffer')
    numInterBuffers = size(Resource.InterBuffer,2);
    if numInterBuffers ~= 0
        IQData = cell(1,numInterBuffers);
        for i = 1:numInterBuffers
            switch lower(Resource.InterBuffer(i).datatype)
                case {'complex double','double','complex'}
                    type = 'double';
                case {'complex single','single'}
                    type = 'single';
                otherwise
                    error('VSX: Resource.InterBuffer.datatype is not supported.');
            end
            if (~isfield(Resource.InterBuffer(i),'pagesPerFrame'))||isempty(Resource.InterBuffer(i).pagesPerFrame)
                if (~isfield(Resource.InterBuffer(i),'sectionsPerFrame'))||isempty(Resource.InterBuffer(i).sectionsPerFrame)
                    IQData{i} = complex(zeros(Resource.InterBuffer(i).rowsPerFrame, ...
                                              Resource.InterBuffer(i).colsPerFrame, ...
                                              Resource.InterBuffer(i).numFrames,type));
                else
                    IQData{i} = complex(zeros(Resource.InterBuffer(i).rowsPerFrame, ...
                                              Resource.InterBuffer(i).colsPerFrame, ...
                                              Resource.InterBuffer(i).sectionsPerFrame, ...
                                              Resource.InterBuffer(i).numFrames,type));
                end
            else
                if (~isfield(Resource.InterBuffer(i),'sectionsPerFrame'))||isempty(Resource.InterBuffer(i).sectionsPerFrame)
                    IQData{i} = complex(zeros(Resource.InterBuffer(i).rowsPerFrame, ...
                                              Resource.InterBuffer(i).colsPerFrame, ...
                                              Resource.InterBuffer(i).pagesPerFrame, ...
                                              Resource.InterBuffer(i).numFrames,type));
                else
                    IQData{i} = complex(zeros(Resource.InterBuffer(i).rowsPerFrame, ...
                                              Resource.InterBuffer(i).colsPerFrame, ...
                                              Resource.InterBuffer(i).sectionsPerFrame, ...
                                              Resource.InterBuffer(i).pagesPerFrame, ...
                                              Resource.InterBuffer(i).numFrames,type));
                end
            end
        end
    end
end

if isfield(Resource, 'ImageBuffer')
    numImageBuffers = size(Resource.ImageBuffer,2);
    ImgData = cell(1,numImageBuffers);
    ImgDataP = cell(1,numImageBuffers);
    for i = 1:numImageBuffers
        if (~isfield(Resource.ImageBuffer(i),'sectionsPerFrame'))||isempty(Resource.ImageBuffer(i).sectionsPerFrame)
            ImgData{i} = zeros(Resource.ImageBuffer(i).rowsPerFrame,...
                         Resource.ImageBuffer(i).colsPerFrame,...
                         Resource.ImageBuffer(i).numFrames,...
                         Resource.ImageBuffer(i).datatype);
            ImgDataP{i} = zeros(Resource.ImageBuffer(i).rowsPerFrame,...
                         Resource.ImageBuffer(i).colsPerFrame,...
                         Resource.ImageBuffer(i).numFrames,...
                         Resource.ImageBuffer(i).datatype);
        else
            ImgData{i} = zeros(Resource.ImageBuffer(i).rowsPerFrame,...
                         Resource.ImageBuffer(i).colsPerFrame,...
                         Resource.ImageBuffer(i).sectionsPerFrame,...
                         Resource.ImageBuffer(i).numFrames,...
                         Resource.ImageBuffer(i).datatype);
            ImgDataP{i} = zeros(Resource.ImageBuffer(i).rowsPerFrame,...
                         Resource.ImageBuffer(i).colsPerFrame,...
                         Resource.ImageBuffer(i).sectionsPerFrame,...
                         Resource.ImageBuffer(i).numFrames,...
                         Resource.ImageBuffer(i).datatype);
        end
    end
end

% - Set a connector default, if none provided.
if ~isfield(Resource.Parameters,'connector')
    Resource.Parameters.connector = 1; % default to connector #1 (left)
end
% - Check if missing Resource.Parameters.startEvent and supply default
if (~isfield(Resource.Parameters,'startEvent'))
    Resource.Parameters.startEvent = 1; % default to 1st event
end
% - Check for the number of LogData records specified.  These records are use for debugging
%   purposes and are optionally generated by the mex file runAcq.  Default number is 128 records.
%   Meaning of fields in a 4 uint LogData record:
%     int32    id        This number is an identifier for the record and is
%                         the same as 'id' in LOGTIME(id).
%     int32    datatype  Identifier for type of data (0 = time, 1 = int, 2 = double);
%     int32     data1     For a time record, this field is the hi value of
%                         an AbsoluteTime number; for data record, it is
%                         the an integer value (for datatype=1) or the integer
%                         representation of a double (hi 16 bits integer, 
%                         low 16 bits fraction) (for datatype=2).
%     int32     data2     For a time record, this field is the lo value of
%                         an AbsoluteTime number. 
if (isfield(Resource.Parameters,'numLogDataRecs'))
    LogData = zeros(4,Resource.Parameters.numLogDataRecs,'int32');
else
    LogData = zeros(4,128,'int32');  % default size of LogData
end

% **** If VDAS=1, check for presence of Resource.VDAS attributes. If not found set defaults. ****
% - If the user is asking for hardware (VDAS=1) but no hardware is present (numBoards=0), provide
%   VDAS attributes according to Resource.Parameters.
if VDAS == 1
    if numBoards~=0
        sequenceMem = vdasSequencerRamGet()/1024; % sequence memory size in KBytes.
        if (sequenceMem == 0), error('VSX: vdasSequencerRamGet returned 0, indicating hardware problem.'); end
    else
        sequenceMem = 128;
    end
    if ~isfield(Resource,'VDAS')
        % No VDAS attributes specified; set defaults.
        %   CG memory is 64MB, organized in two banks of 32MB. The default memory
        %   partition is as follows:
        %      0MB - 48MB   RF data storage
        %      48MB - 64MB  Descriptor storage
        %   Sequence/TGC memory is 128K bytes. The default partition is:
        %      0KB - 120KB   Sequence storage
        %      120KB - 128KB TGC storage
        Resource.VDAS = struct('numTransmit',Resource.Parameters.numTransmit,...
                               'numRcvChannels',Resource.Parameters.numRcvChannels,...
                               'bufferStart',0,...
                               'blockSize',2048,...  % 128 two byte samples for 8 channels
                               'numBlocks',24576,... % 3/4 of available memory
                               'exportDelta',2048,...
                               'descriptStart',50331648,... % start at 48MB
                               'descriptSize',16777216,...
                               'seqStart',0,...
                               'seqSize',(sequenceMem-8)*1024,...  % leave room for 8 TGCs
                               'tGCStart',(sequenceMem-8)*1024,...
                               'numTGC',8,...
                               'testPattern',0,...
                               'halDebugLevel',0,...
                               'UpdateFunction','updateVDAS',...
                               'hardwareBoardConfiguration',0);                           
        % Set VDAS numTransmit and numRcvChannels according to no. of boards in system.
        if numBoards ~= 0
            Resource.VDAS.numTransmit = 64 * numBoards;
            Resource.VDAS.numRcvChannels = 32 * numBoards;
        else  % if no boards detected (numBoards = 0) and user is asking for hardware (VDAS = 1) ...
            switch Resource.Parameters.numRcvChannels
                case {32, 64, 128}
                    Resource.VDAS.numRcvChannels = Resource.Parameters.numRcvChannels;
                otherwise
                    error('VSX: Resource.Parameters.numRcvChannels must be 32, 64, or 128, when using VDAS hardware.');
            end
            Resource.VDAS.numTransmit = 2*Resource.VDAS.numRcvChannels;
        end
    else
        % At least some VDAS attributes are specified.  Keep those specified and provide
        %   defaults for those not found.
        if ~isfield(Resource.VDAS,'numRcvChannels')
            if numBoards ~= 0 
                Resource.VDAS.numRcvChannels = 32 * numBoards;
            else
                switch Resource.Parameters.numRcvChannels
                    case {32, 64, 128}
                        Resource.VDAS.numRcvChannels = Resource.Parameters.numRcvChannels;
                    otherwise
                        error('VSX: Resource.Parameters.numRcvChannels must be 32 (1 brd), 64 (2 brds), or 128 (4 brds), when using VDAS hardware.');
                end
            end
        end
        if ~isfield(Resource.VDAS,'numTransmit')
            if numBoards ~= 0 
                Resource.VDAS.numTransmit = 64 * numBoards;
            else
                Resource.VDAS.numTransmit = 2 * Resource.VDAS.numRcvChannels;
            end
        end
        if ~isfield(Resource.VDAS,'bufferStart'), Resource.VDAS.bufferStart = 0; end
        if ~isfield(Resource.VDAS,'blockSize'), Resource.VDAS.blockSize = 2048; end
        if ~isfield(Resource.VDAS,'descriptStart'), Resource.VDAS.descriptStart = 50331648; end
        if ~isfield(Resource.VDAS,'descriptSize')
            Resource.VDAS.descriptSize = 67108864 - Resource.VDAS.descriptStart;
        end
        if ~isfield(Resource.VDAS,'numBlocks')
            Resource.VDAS.numBlocks = (Resource.VDAS.descriptStart - ...
                Resource.VDAS.bufferStart)/Resource.VDAS.blockSize;
        end
        if ~isfield(Resource.VDAS,'exportDelta'), Resource.VDAS.exportDelta = 2048; end
        if ~isfield(Resource.VDAS,'seqStart'), Resource.VDAS.seqStart = 0; end
        if ~isfield(Resource.VDAS,'seqSize'), Resource.VDAS.seqSize = (sequenceMem-8)*1024; end
        if ~isfield(Resource.VDAS,'tGCStart')
            Resource.VDAS.tGCStart = Resource.VDAS.seqStart + Resource.VDAS.seqSize; 
        end
        if ~isfield(Resource.VDAS,'numTGC')
            if Resource.VDAS.tGCStart < 131072 
                Resource.VDAS.numTGC = floor((131072 - Resource.VDAS.tGCStart)/1024);
            else 
                Resource.VDAS.numTGC = floor((524288 - Resource.VDAS.tGCStart)/1024);
            end
        end
        if ~isfield(Resource.VDAS,'testPattern'), Resource.VDAS.testPattern = 0; end
        if ~isfield(Resource.VDAS,'halDebugLevel'), Resource.VDAS.halDebugLevel = 0; end
        if ~isfield(Resource.VDAS,'UpdateFunction')||isempty(Resource.VDAS.UpdateFunction) 
            Resource.VDAS.UpdateFunction = 'updateVDAS'; 
        end
        if ~isfield(Resource.VDAS,'hardwareBoardConfiguration')
            Resource.VDAS.hardwareBoardConfiguration = 0;
        end
    end
    updateVDASh = str2func(Resource.VDAS.UpdateFunction); % create handle to update function.
end


% ***** Trans structure *****
% - Check for Trans.frequency supported with 4 x sample clock or 4/3 x sample clock; modify Trans.frequency if needed.
if VDAS==1
    if exist('Receive', 'var') && isfield(Receive, 'samplesPerWave') && abs(Receive(1).samplesPerWave-4/3) < .1
        SupportedCFs = [16.8750,13.5,11.25,9.643,8.438];
    else
        SupportedCFs = [11.25,9.0,7.5,6.4286,5.625,5.0,4.5,3.75,3.2143,3.0,2.813,2.5,2.25,2.143,1.875,...
            1.8,1.607,1.5,1.406,1.286,1.250,1.125,1.071,1.0,0.938,0.918,0.833,0.804,0.714,0.703,0.625];
    end
    % Note center frequencies of 16.875, 13.5, 9.643, and 8.438 are
    % supported only by 4/3 sampling.  11.25 is supported by both 4/3 and
    % 4X/2X/1X sampling; all others are 4X/2X/1X only.
    if ~any(abs(SupportedCFs - Trans.frequency) < 0.005)
        [Dummy,Indices] = min(abs(SupportedCFs - Trans.frequency)); % find nearest supported frequency.
        cfreq = SupportedCFs(Indices);  % set new Trans.frequency
        fprintf(2,'VSX:  Unsupported transducer center frequency specified. Modifying\n');
        fprintf(2,'      center frequency to nearest supported frequency of %3.3f.\n',cfreq);
        scaleFactor = cfreq/Trans.frequency;
        Trans.elementWidth = Trans.elementWidth * scaleFactor;
        Trans.spacing = Trans.spacing * scaleFactor;
        if isfield(Trans,'radius'), Trans.radius = Trans.radius * scaleFactor; end
        Trans.ElementPos(:,1) = Trans.ElementPos(:,1) * scaleFactor;
        Trans.ElementPos(:,2) = Trans.ElementPos(:,2) * scaleFactor;
        Trans.ElementPos(:,3) = Trans.ElementPos(:,3) * scaleFactor;
        if isfield(Trans,'lensCorrection'), Trans.lensCorrection = Trans.lensCorrection * scaleFactor; end
        Trans.frequency = cfreq;
    end
end    
% - Check for maximum high voltage limit provided in Trans structure.  If not given, set a default.
if ~isfield(Trans,'maxHighVoltage'), Trans.maxHighVoltage = 56.4; end  % default to TPC-L limit.
% - Check for a TPC profile structure with a TPC.maxHighVoltage higher than Trans.maxHighVoltage.
if exist('TPC','var')
    for i = 1:size(TPC,2)
        if (isfield(TPC(i),'maxHighVoltage'))&&(~isempty(TPC(i).maxHighVoltage))
            if TPC(i).maxHighVoltage > Trans.maxHighVoltage
                fprintf(2,'VSX WARNING: TPC(%d).maxHighVoltage reduced to Trans.maxHighVoltage limit of %.1f volts.\n',i,Trans.maxHighVoltage);
                TPC(i).maxHighVoltage = Trans.maxHighVoltage;
            end
            TPC(i).highVoltageLimit = TPC(i).maxHighVoltage;
        end
    end
else
    TPC.maxHighVoltage = Trans.maxHighVoltage;
    TPC.highVoltageLimit = Trans.maxHighVoltage;
end
% - Determine size of active transducer aperture.  For non-muxed transducers with <= 128 elements,
%   this is usually the number of elements in the transducer.  For transducers with HVMux, this is
%   the number of connector I/O channels.
if isfield(Trans,'HVMux'), sizeTransActive = 128;
elseif isfield(Trans,'Connector'), sizeTransActive = length(Trans.Connector);
elseif (isfield(Resource.Parameters,'connector'))&&(Resource.Parameters.connector==0), sizeTransActive = 256;
else sizeTransActive = Resource.Parameters.numTransmit;
end


% ***** Media structure *****
% - Check for existance of 'MP' structure array. If not found, set defaults.
%       Media points are specified by number, position(x,y,z), and reflectivity.
%       MP(1,:) = [0,0,20,1.0];
if ~exist('Media','var')
    Media.model = 'PointTargets1';
    pt1;
    Media.numPoints = size(Media.MP,1);
elseif (isfield(Media, 'model'))
    if (strcmp(Media.model, 'PointTargets1'))
        pt1;
    elseif (strcmp(Media.model, 'PointTargets2'))
        pt2;
    elseif (strcmp(Media.model, 'PointTargets3'))
        mpt3;
    else
        error('Unknown media model. Could not initialize MP array.\n');
    end
elseif (isfield(Media, 'program'))
    eval(Media.program);
    Media.numPoints = size(Media.MP,1);
elseif (~isfield(Media, 'MP'))
    error('MP array not found.\n');
elseif (~isfield(Media, 'numPoints'))
    Media.numPoints = size(Media.MP,1);
end


% ***** PData structure *****
if exist('PData','var')
    % - Check to see if the PData structure(s) specify all pdeltas and Regions.  If not, create them.
    for i = 1:size(PData,2)
        % If PData(i).pdelta provided, set all other dimensions to same.
        if isfield(PData(i),'pdelta')&&~isempty(PData(i).pdelta)
            PData(i).pdeltaX = PData(i).pdelta;
            PData(i).pdeltaY = PData(i).pdelta;
            PData(i).pdeltaZ = PData(i).pdelta;
        end
        if ~isfield(PData(i),'Region')||isempty(PData(i).Region)
            [PData(i).Region,PData(i).numRegions] = createRegions(PData(i));
        end
    end
    % - Capture original PData(1).pdeltas for zoom function.
    orgPdeltaX = PData(1).pdeltaX;
    orgPdeltaZ = PData(1).pdeltaZ;
end


% ***** HIFU Option Check *****
% Check for use of HIFU transmit profile 5 in setup script and initialize the workspace variable,
% 'profile5ena', which controls all HIFU or profile 5-related features in the system:
%     profile5ena = 0 (default) if the setup script does not make any use of Profile 5;
%     profile5ena = 1 for Extended Burst Option using internal auxiliary power supply for Profile 5 transmit.
%     profile5ena = 2 for HIFU semi-custom option using ext. power supply with remote control for Profile 5 transmit.
% Check SeqControl commands for a setTPCProfile command with an argument of 5.
profile5ena = 0; % profile5ena will be set to 1 if profile 5 is actually used.
if exist('SeqControl','var')
    for j=1:size(SeqControl,2)
        if strcmp(SeqControl(j).command,'setTPCProfile')
            if (SeqControl(j).argument == 5)
                profile5ena = 1;
            end
        end
    end
end
% If profile 5 is going to be used, check that the hardware has the appropriate HIFU option installed.
% Check for the Resource.Parameter attribute, 'externalHifuPwr' from the setup script indicating that
% the external power supply is to be used.  Also check and initialize other parameters related to use
% of profile 5.
if (profile5ena == 1)
    % check for a user-specified substitute for TXEventCheck function
    if ~isfield(Resource,'HIFU') || ~isfield(Resource.HIFU,'TXEventCheckFunction') || isempty(Resource.HIFU.TXEventCheckFunction)
        Resource.HIFU.TXEventCheckFunction = 'TXEventCheck';
    end
    TXEventCheckh = str2func(Resource.HIFU.TXEventCheckFunction); % create handle to update function.
    
    % Now check for the required TPC(5) max high voltage limit
    if ~exist('TPC','var')||size(TPC,2)<5||~(isfield(TPC(5),'maxHighVoltage'))||isempty(TPC(5).maxHighVoltage)
        error('VSX: A script using profile 5 transmit must specify TPC(5).maxHighVoltage.');
    end
    
    if isfield(Trans, 'HVMux')
        % if a probe with HV mux chips is being used, send a warning prompt
        % to the user
        fprintf(2, 'VSX WARNING: specified transducer uses HV mux chips.  Profile 5 transmit through an HV mux \n');
        fprintf(2, 'transducer can be destructive to the HV mux chips.  Extreme caution with transmit level is advised. \n');
        reply = input('   Enter "y" if you want to continue, or a return to exit: ','s');
        if ~strcmpi(reply, 'y')
            return
        end
    end
    
    % Now check actual HW configuration
    if (VDAS == 1) && (numBoards~=0) % only check if not in simulate mode and VDAS boards present
        % Check for a HIFU option present. This might be internal (hifuOptionPresent=1) or external
        % (hifuOptionPresent=2).
        [Result, hifuOptionPresent] = getHardwareProperty('HifuOptionPresent');
        if ~strcmp(Result,'Success') 
            error('VSX: Error from getHardwareProperty call to determine if HIFU option installed.\n');
        end
        if hifuOptionPresent == 0 
            error('VSX: Profile 5 transmit features can not be used since HIFU option is not installed in the system.'); 
        end
        % Check push capacitor version.
        [Result, Resource.HIFU.pushcapID] = getHardwareProperty('PushCapacitorVersion');
        if ~strcmp(Result,'Success')
            error('VSX: Error from getHardwareProperty call to determine push capacitor version.');
        end
        % Check gate driver supply capacity.
        [Result, Resource.HIFU.GateDriverSupplyCapacity] = getHardwareProperty('GateDriverSupplyCapacity');
        if ~strcmp(Result,'Success')
            error('VSX: Error from getHardwareProperty call to determine gate driver supply capacity.');
        end
        if hifuOptionPresent == 2
            if isfield(Resource.HIFU,'externalHifuPwr') && (Resource.HIFU.externalHifuPwr == 1)
                profile5ena = 2;
            else
                error(['VSX: System is configured for use with external HIFU power supply, ',...
                'but Resource.HIFU.externalHifuPwr has not been set in SetUp script.']);
            end
            % At this point, the system is set to use the external power supply option.  So we have to
            % initialize the external power supply and make sure it is connected and up to 1.6 Volts 
            % before we try to open the VDAS HW (TPC initialization is likely to fail if HIFU
            % capacitor voltage is sitting at zero).
            % - Initialize external supply communication and set it to 1.6 Volts, 2 Amps
            
            % As part of initializing the power supply, we also determine
            % whether it is configured for series or parallel connection of
            % the two outputs and configure the control function to match.
            % This also lets us determine whether the power supply is
            % actually connected to the system and working properly, since
            % we will read back the actual voltage from the TPC.
            
            Resource.HIFU.extPwrConnection = 'parallel'; % assume it is parallel to start
            % now try programming the supply
            [extPSstatus, ~] = extPwrCtrl('INIT',1.6); 
            if extPSstatus ~= 0
                error('VSX: Cannot initialize external power supply.  Make sure it is connected and turned on.');
            end
            pause(0.5); % wait 0.5 second for the power supply to come up
            % now read the actual voltage at push cap. through the TPC
            [Result,pushCapVoltage] = getHardwareProperty('TpcExtCapVoltage');
            if ~strcmp(Result,'Success')
                error('VSX: Error from getHardwareProperty call to read push capacitor Voltage.');
            end
            
            % now check the result:
            if pushCapVoltage < 1.1
                % less than 1.1 Volt means we have a fault or power supply
                % not connected;
                error('VSX: No output from external power supply, or it may be disconnected.');
            elseif pushCapVoltage >= 1.1 && pushCapVoltage < 2.3
                % between 1.1 and 2.3 Volts means supply is using parallel
                % connection and is working properly, so no need to do
                % anything else
            elseif pushCapVoltage >= 2.3 && pushCapVoltage < 4.5
                % between 2.3 and 4.5 Volts means we have a
                % series-connected supply, so reconfigure for series
                % operation and test again
                
                % but first we must disable supply so it can be
                % reprogrammed
                [~, ~] = extPwrCtrl('CLOSE',1.6);
                pause(0.3) % give it time to close
                Resource.HIFU.extPwrConnection = 'series'; % reconfigure for series
                % now try reprogramming the supply
                [extPSstatus, ~] = extPwrCtrl('INIT',1.6); 
                if extPSstatus ~= 0
                    error('VSX: Cannot initialize external power supply.  Make sure it is connected and turned on.');
                end
                pause(0.5); % wait 0.5 second for the power supply to come up
                % now read the actual voltage at push cap. through the TPC
                [Result,pushCapVoltage] = getHardwareProperty('TpcExtCapVoltage');
                if ~strcmp(Result,'Success')
                    error('VSX: Error from getHardwareProperty call to read push capacitor Voltage.');
                end
                if pushCapVoltage < 2.3 || pushCapVoltage > 4.5
                    error('VSX: Error while initializing external supply for series connection.');
                end
            else
                % any other voltage means we have a fault.
                error('VSX: Error while initializing external supply for parallel connection.');
            end
                
        else % system is configured for internal aux. supply; confirm that's what the user intended
            if isfield(Resource.HIFU,'externalHifuPwr') && (Resource.HIFU.externalHifuPwr == 1)
                error('VSX: System is not configured for use with external HIFU power supply.');
            end
        end
    else % VDAS = 0 (simulate mode) or no hardware present.
        if isfield(Resource.HIFU,'externalHifuPwr') && (Resource.HIFU.externalHifuPwr == 1)
            % set simulation defaults for external HIFU
            profile5ena = 2;
        end
        % If user did not specify HW parameters, set a default value
        if ~isfield(Resource.HIFU,'pushcapID') || isempty(Resource.HIFU.pushcapID)
            Resource.HIFU.pushcapID = 3;  % Assume newest version of push capacitor.
        end
        if ~isfield(Resource.HIFU,'GateDriverSupplyCapacity') || isempty(Resource.HIFU.GateDriverSupplyCapacity)
            Resource.HIFU.GateDriverSupplyCapacity = 7.5; % reduced capacity for older TPC's in extended burst option systems
        end 
    end
end


% ***** TW Structure *****
% Check for required user fields.
if ~isfield(TW,'type'), error('VSX: Required TW.type attribute is missing.\n'); end
% Add required fields that may not be specified by user.
if any(strcmp('parametric',{TW.type})) % if any 'parametric' types are specified.
    if ~isfield(TW,'Parameters'), error('VSX: TW.Parameters required with type = ''parametric''.\n'); end 
    if ~isfield(TW,'twClock'), [TW.twClock] = deal(180); end
    if ~isfield(TW,'equalize'), [TW.equalize] = deal(1); end
    if ~isfield(TW,'extendBL'), [TW.extendBL] = deal(0); end
end
% Convert any older VDASxxx parameters to newer non-VDAS parameters.
if isfield(TW,'VDAStwClock'), [TW.twClock] = TW(:).VDAStwClock; TW = rmfield(TW,'VDAStwClock'); end
if isfield(TW,'VDASequalize'), [TW.equalize] = TW(:).VDASequalize; TW = rmfield(TW,'VDASequalize'); end
if isfield(TW,'VDASextendBL'), [TW.extendBL] = TW(:).VDASextendBL; TW = rmfield(TW,'VDASextendBL'); end
% Add additional required fields and default to empty state.
if ~isfield(TW,'Waveform'), TW(1).Waveform = []; end  % adding field to first adds field to all
if ~isfield(TW,'peak'), TW(1).peak = []; end
if ~isfield(TW,'samplesPerWL'), TW(1).samplesPerWL = []; end
if ~isfield(TW,'numsamples'), TW(1).numsamples = []; end
% Check each TW structure for correct attributes.
for i = 1:size(TW,2)
    switch TW(i).type
        case 'parametric'
            % Extend a single TW.Parameters specification to all transmitters, if needed.
            if size(TW(i).Parameters,1) == 1  
                TW(i).Parameters = repmat(TW(i).Parameters,sizeTransActive,1);
            elseif size(TW(i).Parameters,1) ~= sizeTransActive
                error('VSX: Number of rows in TW(%d).Parameters not equal to no. of active transmitters.\n',i);
            end
            if isempty(TW(i).twClock), TW(i).twClock = 180;
            elseif (TW(i).twClock~=180)&&(TW(i).twClock~=90)&&(TW(i).twClock~=45)
                error('VSX: TW(%d).twClock not equal to 180, 90 or 45\n',i);
            end
            if isempty(TW(i).equalize), TW(i).equalize = 1; end
            if isempty(TW(i).extendBL), TW(i).extendBL = 0;
            elseif TW(i).extendBL==1 
                if (VDAS==1) && (profile5ena == 0)
                    error('VSX: TW(%d).extendBL=1 can only be used with profile 5 transmit events.\n',i); 
                end
            end
            % If Waveform not provided, compute sampled waveform for possible switch to simulation and to get TW.peak.
            if isempty(TW(i).Waveform)
                [TW(i).Waveform,TW(i).peak,TW(i).numsamples,rc] = computeTWWaveform(TW(i));
                if rc~=0, error('VSX: error in computeTWWaveform for TW(%d).\n',i); end
            end
            % Check other attributes, in case Waveform was provided.
            if isempty(TW(i).peak), error('VSX: TW(%d).peak not provided for waveform.\n',i); end
            if isempty(TW(i).samplesPerWL), TW(i).samplesPerWL=64; 
            elseif TW(i).samplesPerWL~=64, error('VSX: TW(%d).samplesPerWL not equal to 64, as required for simulation.\n',i); end
            if isempty(TW(i).numsamples) 
                error('VSX: TW(%d).numsamples at 64 samplesPerWL not provided for waveform.\n',i); 
            end
        case 'function'
            if ~isfield(TW(i),'Descriptors'), error('VSX: TW(%d).Descriptors required with type=''function''.\n',i); end
            % Compute waveform if not provided.
            if isempty(TW(i).Waveform)
                [TW(i).Waveform,TW(i).peak,TW(i).numsamples,rc] = computeTWWaveform(TW(i));
                if rc~=0, error('VSX: error in computeTWWaveform for TW(%d).\n',i); end
            end
            if isempty(TW(i).peak), error('VSX: TW(%d).peak not provided for ''function'' waveform.\n',i); end
            if isempty(TW(i).samplesPerWL), TW(i).samplesPerWL = 64;
            elseif TW(i).samplesPerWL~=64, error('VSX: TW(%d).samplesPerWL not equal to 64, as required for simulation.\n',i); end
            if isempty(TW(i).numsamples), error('VSX: TW(%d).numsamples at 64 samplesPerWL not provided for ''function'' waveform.\n',i); end
        case 'sampled'
            if isempty(TW(i).Waveform), error('VSX: TW(%d).Waveform at 64 samplesPerWL not provided for ''sampled'' waveform.\n',i); end
            if isempty(TW(i).peak), error('VSX: TW(%d).peak not provided for ''sampled'' waveform.\n',i); end
            if isempty(TW(i).samplesPerWL), TW(i).samplesPerWL = 64;
            elseif TW(i).samplesPerWL~=64, error('VSX: TW(%d).samplesPerWL not equal to 64, as required for simulation.\n',i); end
            if isempty(TW(i).numsamples), error('VSX: TW(%d).numsamples at 64 samplesPerWL not provided for ''sampled'' waveform.\n',i); end
        otherwise
            error('VSX: Unrecognized TW(%d).type.\n',i);
    end
end
% Add the VDASParameters if running with the hardware.
if VDAS == 1
    if ~isfield(TW,'VDASParameters') 
        updateVDASh('TW');
    else
        fprintf(2,'VSX: TW.VDASParameters cannot be included in SetUp file. To set VDAS parameters\n');
        fprintf(2,'manually, use Resource.VDAS.updateFunction to specify user provided function.\n');
        error('VSX exiting...');
    end
end


% ***** TX Structure *****
% - Check for correct TX.Apod and TX.Delay format and consistency with TW definition.
for i = 1:size(TX,2)
    if ~isfield(TX(i),'Apod')||isempty(TX(i).Apod)
        error('VSX: ''Apod'' field is missing or empty in TX(%d).\n',i);
    end
    % Verify correct size of TX.Apod array
    if size(TX(i).Apod,2) ~= sizeTransActive
        if isfield(Trans,'HVMux'), error('VSX: Size of TX(%d).Apod for muxed transducer must be equal to 128.\n',i);
        elseif isfield(Trans,'Connector')
            error('VSX: Size of TX(%d).Apod must equal length of Trans.Connector.\n',i);
        elseif (isfield(Resource.Parameters,'connector'))&&(Resource.Parameters.connector==0)
            error('VSX: Size of TX(%d).Apod for Resource.Parameters.connector = 0 must be equal to 256.\n',i);
        else error('VSX: Size of TX(%d).Apod must be equal to Resource.Parameters.numTransmit.\n',i);
        end
    end
    if ~isfield(TX(i),'Delay')||isempty(TX(i).Delay)
        error('VSX: ''Delay'' field is missing or empty in TX(%d).\n',i);
    end
    if size(TX(i).Delay,2) ~= size(TX(i).Apod,2)
        error('VSX: Size of TX(%d).Delay must equal size of TX(%d).Apod.\n',i,i);
    end
end
% Add the TX VDAS parameters if running with the hardware.
if VDAS == 1
    if ~any(isfield(TX,{'VDASApod','VDASDelay'})) 
        updateVDASh('TX');
    else
        fprintf(2,'VSX: Neither TX.VDASApod or TX.VDASDelay can be included in SetUp file. To set VDAS parameters\n');
        fprintf(2,'manually, use Resource.VDAS.updateFunction to specify user provided function.\n');
        error('VSX exiting...');
    end
end


% ***** Receive Structure *****
if exist('Receive','var')
    % Check for missing required fields.
    if ~isfield(Receive,'Apod'), error('VSX: Receive.Apod field missing.\n'); end
    % Verify proper size of Receive.Apod arrays in all Receive structures.  This also checks for empty fields.
    n=0;
    for i=1:size(Receive,2), n = n+size(Receive(i).Apod,2); end
    if n ~= sizeTransActive*size(Receive,2) % This line and the 2 above can be replaced for the following line with Matlab 2011A.
    %if size([Receive.Apod],2) ~= sizeTransActive*size(Receive,2)
        fprintf(2,'VSX: Receive.Apod arrays are not sized correctly. They should be row vectors with the\n');
        fprintf(2,'same size as the number of active transducer elements, %d.\n',sizeTransActive);
        error('VSX exiting...');
    end
    if isfield(Trans,'HVMux') && ~isfield(Receive,'aperture')
        error('VSX: Receive.aperture missing for transducer that has an HVMux attribute.');
    end
    if ~isfield(Receive,'startDepth'), error('VSX: Receive.startDepth field missing.\n'); end
    if ~isfield(Receive,'endDepth'), error('VSX: Receive.endDepth field missing.\n'); end
    if ~isfield(Receive,'TGC'), error('VSX: Receive.TGC field missing.\n'); end
    if ~isfield(Receive,'bufnum'), error('VSX: Receive.bufnum field missing.\n'); end
    if ~isfield(Receive,'framenum'), error('VSX: Receive.ramenum field missing.\n'); end
    if ~isfield(Receive,'acqNum'), error('VSX: Receive.acqNum field missing.\n'); end
    if ~isfield(Receive,'mode'), error('VSX: Receive.mode field missing.\n'); end
    Receive(1).startSample = []; % adding field to first adds to all.
    Receive(1).endSample = [];
    if ~isfield(Receive,'samplesPerWave'), Receive(1).samplesPerWave = []; end
    % Check attributes in each receive structure.
    for i = 1:size(Receive,2)
        if isempty(Receive(i).startDepth), error('VSX: Receive(%d).startDepth empty.\n',i); end
        if isempty(Receive(i).endDepth), error('VSX: Receive(%d).endDepth empty.\n',i); end
        if isempty(Receive(i).bufnum), error('VSX: Receive(%d).bufnum empty.\n',i); end
        if isempty(Receive(i).framenum), error('VSX: Receive(%d).framenum empty.\n',i); end
        if isempty(Receive(i).acqNum), error('VSX: Receive(%d).acqNum empty.\n',i); end
        if isempty(Receive(i).mode), error('VSX: Receive(%d).mode empty.\n',i); end
        % Check for samplesPerWave attribute provided; if not found, see if set in Resource.RcvBuffer.
        if isempty(Receive(i).samplesPerWave)
            if isfield(Resource.RcvBuffer(Receive(i).bufnum),'samplesPerWave')
                Receive(i).samplesPerWave = Resource.RcvBuffer(Receive(i).bufnum).samplesPerWave;
            else
                Receive(i).samplesPerWave = 4.0; % default, if not provided
            end
        end
    end
    % Set startSample and endSample attributes to correspond to startDepth and endDepth attributes.
    % - Force numRcvSamples to be a multiple of 128.  Use rounding to find nearest multiple,
    %   since user script should be targetting a multiple of 128 to begin with.
    i = 1;
    n = size(Receive,2);
    while i <= n
        % Set values for 1st Receive of frame
        if Receive(i).acqNum ~= 1, error('VSX: Receive(%d).acqNum for start of frame not equal to 1.\n',i); end
        maxAcqNum = 1;  % maxAcqNum keeps track of the largest acqNum for the frame.
        numRcvSamples = 128*round((2*(Receive(i).endDepth - Receive(i).startDepth)*Receive(i).samplesPerWave)/128);
        if numRcvSamples == 0 
            numRcvSamples = 128;
            fprintf('VSX: Setting Receive(%d).endDepth to acquire a minimum of 128 samples.\n',i);
        end
        Receive(i).endDepth = Receive(i).startDepth + (numRcvSamples/2)/Receive(i).samplesPerWave;
        Receive(i).startSample = 1;
        Receive(i).endSample = numRcvSamples;
        j = numRcvSamples + 1; % j is the next startSample to use
        i = i+1;
        while (i <= n) && (Receive(i).bufnum == Receive(i-1).bufnum) && (Receive(i).framenum == Receive(i-1).framenum)
            numRcvSamples = 128*round((2*(Receive(i).endDepth - Receive(i).startDepth)*Receive(i).samplesPerWave)/128);
            if numRcvSamples == 0 
                numRcvSamples = 128; % minimum no. of samples is 128
                fprintf('VSX: Setting Receive(%d).endDepth to acquire a minimum of 128 samples.\n',i);
            end
            Receive(i).endDepth = Receive(i).startDepth + (numRcvSamples/2)/Receive(i).samplesPerWave;
            if Receive(i).acqNum == maxAcqNum + 1
                maxAcqNum = maxAcqNum + 1;
                Receive(i).startSample = j;
                Receive(i).endSample = j + numRcvSamples - 1;
                j = Receive(i).endSample + 1;
            elseif Receive(i).acqNum <= maxAcqNum  % If acqNum has been previously used, overwrite previous acquisition.
                k = i-1;  % search backwards for a previous Receive with the same acqNum
                while (k > 0)
                    if Receive(k).acqNum == Receive(i).acqNum, break, end
                    k = k - 1;
                end
                if k == 0, error('VSX: Out of order Receive(%d).acqNum which doesn''t match previous acqNum.\n',i); end
                Receive(i).startSample = Receive(k).startSample;
                Receive(i).endSample = Receive(i).startSample + numRcvSamples - 1;
                if Receive(i).endSample > Receive(k).endSample
                    error('VSX: Receive(%d).endSample for overwrite acqNum larger than in previous Receive.\n',i);
                end
            else
                error('VSX: Receive(%d).acqNum is not in sequential order.\n',i);
            end
            i = i+1;
        end
    end
    % Add the Receive VDAS parameters if running with the hardware. Add VDAS parameters here so
    % VDASFilter and VDASQFilter can be used to select default filters below.
    if VDAS == 1
        if  any(isfield(Receive,{'VDASApod','VDASclipMod','VDASMux','VDASADRate','VDASFilter','VDASQFilter'}))
            fprintf('VSX: SetUp file contains Receive.VDASxxxx attributes that may be overwritten by the default\n');
            fprintf('     ''updateVDAS'' function.  To set Receive.VDASxxxx parameters manually, set a custom updateVDAS\n');
            fprintf('      function using Resource.VDAS.updateFunction.\n');
        end
        % For the first update to Receive, we will calculate DMA Control objects, which must not already exist.
        if exist('DMAControl', 'var'), error('VSX: No DMAControl objects are allowed in SetUp scripts.\n'); end
        % Determine if we have an older sequence with automatic SeqControl DMA commands in same event with Recon,
        %   or newer sequence with 'transferToHost' SeqControl commands.
        oldSeq = 1;
        if ~exist('SeqControl','var'), SeqControl(1).command = 'noop'; end  % create a single dummy SeqControl.
        for i = 1:size(SeqControl,2)
            if strcmp(SeqControl(i).command,'transferToHost')
                oldSeq = 0;
                break;
            end
        end    
        updateVDASh('Receive');
    end

    % Provide default input filter coefficients if the user has not specified any.  If running with the 
    %   hardware, the default filters will get set from the VDASFilter and VDASQFilter values; otherwise 
    %   they will default to pass through.  If the user defines filter coefficients, they will only get 
    %   set at initialization (a runtime update of Receive will not change the filter coefficients).
    for i = 1:size(Receive,2)
        % Set up the lowpass filter coefficient arrays.    
        % Specify default coefficients for use if user script does not provide
        %    an explicit definition
        DefaultLPFcoef =[[ +0.0000  +0.0000  +0.0000  +0.0000  +0.0000  +1.0000];...
                         [ +0.0063  -0.0205  -0.0542  +0.0474  +0.2988  +0.4434];...
                         [ -0.0234  -0.0269  +0.0225  +0.1309  +0.2476  +0.2983];...
                         [ -0.0269  +0.0029  +0.0635  +0.1406  +0.2051  +0.2300];...
                         [ -0.0171  +0.0254  +0.0811  +0.1367  +0.1772  +0.1924];...
                         [ +0.0034  +0.0425  +0.0869  +0.1274  +0.1562  +0.1665];...
                         [ +0.0264  +0.0581  +0.0903  +0.1177  +0.1362  +0.1426];...
                         [ +0.0547  +0.0742  +0.0918  +0.1055  +0.1143  +0.1177];...
                         [ -0.0250  -0.0180   0.0785   0.0267  -0.3091   0.4714]];

        if (~isfield(Receive(i),'LowPassCoef'))||isempty(Receive(i).LowPassCoef)
            % There is no VDASLowPassCoef array, so generate it from the
            % precomputed defaults
            if isfield(Receive(i),'VDASFilter') && ~isempty(Receive(i).VDASFilter)
                % check for special case of 4/3 sampling
                if Receive(i).VDASFilter == 2 && Receive(i).samplesPerWave == 4/3
                    Receive(i).LowPassCoef = DefaultLPFcoef(9,:);
                else
                    Receive(i).LowPassCoef = DefaultLPFcoef(Receive(i).VDASFilter,:);
                end
            else
                Receive(i).LowPassCoef = DefaultLPFcoef(1,:);
            end
        end
        % Set up the bandpass filter coefficient array.
        % Specify default coefficients for use if user script does not provide
        %    an explicit definition
        DefaultBPFcoef =[[ +0.0146  +0.0244  -0.0088  -0.1167  -0.2544  +0.6816];...
                         [ -0.0161  -0.0000  +0.0713  +0.0000  -0.3047  +0.4990];...
                         [ +0.0000  +0.0000  +0.0000  +0.0000  +0.0000  +1.0000];...
                         [ -0.0005  +0.0142  -0.0566  +0.1255  -0.1934  +0.2217];...
                         [ +0.0000  +0.0000  +0.0000  +0.0000  +0.0000  +1.0000];...
                         [ +0.0000  +0.0000  +0.0000  +0.0000  +0.0000  +1.0000];...
                         [ +0.0000  +0.0000  +0.0000  +0.0000  +0.0000  +1.0000];...
                         [ -0.0415  -0.0522  -0.0840  -0.1143  -0.1367  +0.8574]];

        if (isfield(Receive(i),'InputFilter') && ~isempty(Receive(i).InputFilter))
            % There is a Receive.InputFilter array so use as is unless it has multiple rows.
            if size(Receive(i).InputFilter,1) ~= 1 % if multiple rows, check to see if all are the same
                if max(Receive(i).InputFilter,[],1) == min(Receive(i).InputFilter,[],1)
                    % all rows identical, so copy row 1 into a new single-row
                    % vector
                    bpcoef = Receive(i).InputFilter(1,:);
                    clear Receive(i).InputFilter
                    Receive(i).InputFilter = bpcoef;
                    clear bpcoef
                else
                    error('VSX: Receive(%d).InputFilter has multiple rows that are not identical.\n',i);
                end
            end
            % Now we have a single-row array; ready to pass it to the HAL
            % Note Hal will check for required structure, range, and format so
            % we don't need to do that here
        else
            % No Input Filter array exists, so create default from
            % the DefaultBPFcoef array using VDASQFilter to select appropriate set.
            if isfield(Receive(i),'VDASQFilter') && ~isempty(Receive(i).VDASQFilter)
                % check for 4/3 sampling special case
                if Receive(i).VDASQFilter == 0 && Receive(i).samplesPerWave == 4/3
                    Receive(i).InputFilter = DefaultBPFcoef(8,:);
                else
                    Receive(i).InputFilter = DefaultBPFcoef(Receive(i).VDASQFilter+1,:);
                end
            else
                Receive(i).InputFilter = DefaultBPFcoef(8,:);
            end
        end
    end
end

% **** TGC Structure *****
% If a TGC(1) waveform exists, initialize the tgc slide pot variables.
if exist('TGC','var')
    tgc1 = double(TGC(1).CntrlPts(1));
    tgc2 = double(TGC(1).CntrlPts(2));
    tgc3 = double(TGC(1).CntrlPts(3));
    tgc4 = double(TGC(1).CntrlPts(4));
    tgc5 = double(TGC(1).CntrlPts(5));
    tgc6 = double(TGC(1).CntrlPts(6));
    tgc7 = double(TGC(1).CntrlPts(7));
    tgc8 = double(TGC(1).CntrlPts(8));
else
    tgc1=511; tgc2=511; tgc3=511; tgc4=511; tgc5=511; tgc6=511; tgc7=511; tgc8=511;
end


% **** ReconInfo structures ****
% - Check 'ReconInfo' structures for presence of ReconInfo.Aperture and add if not provided.
if exist('ReconInfo','var')
    for i = 1:size(ReconInfo,2)
        if ~isfield(ReconInfo(i),'Aperture') || isempty(ReconInfo(i).Aperture)
            % If we have an aperture definition, build a connector I/O channel to element map.
            if isfield(Receive(ReconInfo(i).rcvnum),'aperture')&&~isempty(Receive(ReconInfo(i).rcvnum).aperture)
                n = Receive(ReconInfo(i).rcvnum).aperture;
                [Indices,Dummy,Aper2Chnl] = find(Trans.HVMux.Aperture(:,n));
                Chnl2Ele(Aper2Chnl) = Indices-1;
            elseif isfield(Trans,'Connector')
                [Indices,Dummy2,Aper2Chnl] = find(Trans.Connector);
                Chnl2Ele(Aper2Chnl) = Indices-1;
            else
                Aper2Chnl = (1:Trans.numelements)';
                Chnl2Ele = 0:(Trans.numelements-1);
            end
            switch Resource.Parameters.numRcvChannels
                case 32  % single board system
                    ReconInfo(i).Aperture(1:32) = -1; % initialize Aperture array
                    for j = 1:64
                        if Receive(ReconInfo(i).rcvnum).Apod(j) > 0
                            if j < 33, ReconInfo(i).Aperture(j) = j-1;
                            else ReconInfo(i).Aperture(j-32) = j-1;
                            end
                        end
                    end                        
                case 64 % two board system
                    ReconInfo(i).Aperture(1:64) = -1; % initialize Aperture array
                    for j = 1:size(Aper2Chnl,1)  % for each apodization value
                        if Receive(ReconInfo(i).rcvnum).Apod(j) ~= 0
                            if Aper2Chnl(j) < 65, ReconInfo(i).Aperture(Aper2Chnl(j)) = Chnl2Ele(Aper2Chnl(j));
                            else ReconInfo(i).Aperture(Aper2Chnl(j)-64) = Chnl2Ele(Aper2Chnl(j));
                            end
                        end
                    end
                case 128 % four board system
                    ReconInfo(i).Aperture(1:128) = -1; % initialize Aperture array
                    if Resource.Parameters.connector == 0
                        for j = 1:256
                            if Receive(ReconInfo(i).rcvnum).Apod(j) > 0
                                if j < 129, ReconInfo(i).Aperture(j) = j-1;
                                else ReconInfo(i).Aperture(j-128) = j-1;
                                end
                            end
                        end
                    else
                        for j = 1:size(Aper2Chnl,1)  % for each apodization value
                            if Receive(ReconInfo(i).rcvnum).Apod(j) > 0
                                ReconInfo(i).Aperture(Aper2Chnl(j)) = Chnl2Ele(Aper2Chnl(j));
                            end
                        end
                    end
            end
        end
    end
end


% **** Recon Structure ****
% - Check 'Recon' structure array for compatible PData and destination buffer sizes, and number
%   of ReconInfos. 
%   If only one column and ReconInfo.regionnum = 0 or [], add ReconInfo structures for all regions.
if exist('Recon','var')
    nextRI = size(ReconInfo,2) + 1; % this will keep track of new ReconInfo structures created.
    for i = 1:size(Recon,2)
        j = Recon(i).pdatanum;
        if isfield(Recon(i),'IntBufDest')&&(~isempty(Recon(i).IntBufDest))&&(Recon(i).IntBufDest(1) > 0)
            k = Recon(i).IntBufDest(1); % get dest. buffer no.
            % Check for InterBuffer large enough for PData.
            if (PData(j).Size(1) > Resource.InterBuffer(k).rowsPerFrame) ||...
               (PData(j).Size(2) > Resource.InterBuffer(k).colsPerFrame)
                error('Resource.InterBuffer(%d) not large enough for PData(%d)\n',k,j);
            end
        else
            % If no IntBufDest, check to verify that no IQ reconstructions are specified.
            for k = 1:numel(Recon(i).RINums)
                if ReconInfo(Recon(i).RINums(k)).mode > 2
                    error('VSX: Recon(%d) specifies a ReconInfo.mode > 2, but no IntBufDest is given.\n',i);
                end
            end
        end
        if isfield(Recon(i),'ImgBufDest')&&(~isempty(Recon(i).ImgBufDest))
            k = Recon(i).ImgBufDest(1); % get dest. buffer no.
            if k ~= 0
                % Check for ImageBuffer large enough for PData.
                if (PData(j).Size(1) > Resource.ImageBuffer(k).rowsPerFrame) ||...
                   (PData(j).Size(2) > Resource.ImageBuffer(k).colsPerFrame)
                    error('Resource.ImageBuffer(%d) not large enough for PData(%d)\n',k,j);
                end
            end
        end
        % if Recon(i).numchannels not specified, set the same as Resource.Parameters.numRcvChannels.
        if ~isfield(Recon(i),'numchannels')||isempty(Recon(i).numchannels)
            Recon(i).numchannels = Resource.Parameters.numRcvChannels;
        end
        % check for only one column and all regionnums in specified ReconInfos set to zero.
        if size(Recon(i).RINums,2)==1  
            for j = 1:size(Recon(i).RINums,1) % set any missing or empty fields to zero.
                if ~isfield(ReconInfo(Recon(i).RINums(j,1)),'regionnum')||...
                        isempty(ReconInfo(Recon(i).RINums(j,1)).regionnum)
                    ReconInfo(Recon(i).RINums(j,1)).regionnum = 0;
                end
            end
            % check for all ReconInfos in col. set to specify all regions (0)       
            if ~any([ReconInfo(Recon(i).RINums(:,1)).regionnum])
                for j = 1:size(Recon(i).RINums,1) % set first col RIs to region 1
                    ReconInfo(Recon(i).RINums(j,1)).regionnum = 1;
                end
                % Create additional ReconInfo structures for all regions
                for k = 2:PData(Recon(i).pdatanum).numRegions
                    for j = 1:size(Recon(i).RINums,1)
                        Recon(i).RINums(j,k) = nextRI;
                        ReconInfo(nextRI) = ReconInfo(Recon(i).RINums(j,1)); % make copy from 1st
                        ReconInfo(nextRI).regionnum = k;
                        nextRI = nextRI + 1;
                    end
                end
            else
                if ~all([ReconInfo(Recon(i).RINums(:,1)).regionnum])
                    error('VSX: single col. Recon(%d).RINums must specify ReconInfo.regionnums that are all set or all missing or 0.\n',i)
                    return
                end
            end
        end
    end
end


% If not in simulate mode, try to open the hardware.
if VDAS ~= 0
    % Try to open the Verasonics hardware.
    % 
    % NOTE:  Textual output from compiled C code is not seen in MATLAB until
    % after the function returns.
    try
        Result = hardwareOpen();
    catch
        Result = 'FAIL';
    end
    if strcmpi(Result,'SUCCESS')
        % Hardware opened successfully.
        %   When using a debugger, inform the Verasonic HAL so that it can
        %   disable Watch Dog timout, and maximize timeouts, such as for DMA.
        if exist('Vs_VdasDebugging', 'var')
            % OK, the flag exists, so set state accordingly.
            if(1 == Vs_VdasDebugging)
              % Warn user that Watch Dog timer is disabled.
              display('WARNING:  Vs_VdasDebugging set to 1, so DISABLING the VDAS watch dog timer and PCI Ready Timeout.');
            end
            Result = vdasDebugging(Vs_VdasDebugging);
            if ~strcmpi(Result, 'SUCCESS')
              % ERROR!  Failed to set debugging state.
              error('ERROR!  Failed to set Verasonics VDAS debugging state.');
            end
        end
    else  % If hardware didn't open successfully, revert to simulate mode.
        VDAS = 0;
        if rloopButton == 0
            Resource.Parameters.simulateMode = 1;
        else
            Resource.Parameters.simulateMode = 2;
        end
        fprintf('Unable to open hardware or HAL not found.; using simulate mode.\n')
    end
end

% If hardware opened successfully, check if running on older two board chassis system.  If so
% skip tests for scanhead connectivity.
if VDAS == 1
    if logical(vdasShiPresenceGet()) % if we have a newer SHI, use connectivity checks.
        % Query the system for scanhead connectivity and select scanhead.
        [Presence, Selected, ID] = vdasScanheadQuery();
        % If fakeScanhead is not set, check if a scanhead is present and has correct ID on the desired connector.
        if ~(isfield(Resource.Parameters,'fakeScanhead')&&Resource.Parameters.fakeScanhead == 1)
            % Check for presence of transducer at the script specified connector.
            if (Resource.Parameters.connector == 0) % Check for transducers at both connectors.
                if (Presence(1) ~= 1.0)
                    error('VSX: No transducer detected at connector 1 for script that specifies both connectors (connector=0).');
                elseif (Presence(2) ~= 1.0)
                    error('VSX: No transducer detected at connector 2 for script that specifies both connectors (connector=0).');
                end
                if (~strcmpi('custom',Trans.name))
                    error('VSX: Transducer that uses both connectors must have Trans.name = ''custom''.');
                end
            elseif Presence(Resource.Parameters.connector) ~= 1.0 % Check for a single transducer connected
                error('VSX: No transducer detected at script specified connector number %d.\n',Resource.Parameters.connector);
            end
            % Check for ID that matches script.
            if (~strcmpi('custom',Trans.name))&&(ID(Resource.Parameters.connector) ~= Trans.id)
                error('VSX: ID of transducer specified by script doesn''t match ID of transducer at connector %d.\n',Resource.Parameters.connector);
            end
            % Select the connector for a valid ID or custom transducer.
            result = vdasScanheadSelect(Resource.Parameters.connector,0);
            if ~strcmpi(result, 'SUCCESS'), error('VSX: Failed to select scanhead because "%s".\n', result); end     
        else   % fakeScanhead is set - check for connector open before selecting connector
            if ((Resource.Parameters.connector==0)&&(Presence(1)==0.0)&&(Presence(2)==0.0))||...
                ((Resource.Parameters.connector~=0)&&(Presence(Resource.Parameters.connector)==0.0))
                % For connector open, select scanhead with inverted connectivity monitoring.
                result = vdasScanheadSelect(Resource.Parameters.connector,1); 
                if ~strcmpi(result, 'SUCCESS'), error('VSX: Failed to select scanhead because "%s".\n', result); end 
            else
                % For scanhead at connector, don't allow fake scanhead mode for muxed scanhead different
                % from the scanhead specified in user script.
                if Resource.Parameters.connector==0, n = 1; else n = Resource.Parameters.connector; end
                if ID(n)~=Trans.id
                    if isfield(Trans,'HVMux') && ~isempty(Trans.HVMux)
                        fprintf(2,'VSX: This script specifies an HVMux scanhead, and cannot be run in fake\n');
                        error('scanhead mode with a scanhead that doesn''t match the script.');
                    end
                    fprintf('A scanhead with an ID that doesn''t match your script is connected at connector %d\n',n);
                    fprintf('Do you want to continue to run in fake scanhead mode with this scanhead? (Y or N)\n');
                    Reply = input('(If answer is N, VSX will exit and you can remove the scanhead and run again.)', 's');
                    if ~isempty(Reply)&&~strcmpi('Y',Reply), return, end
                end
                result = vdasScanheadSelect(Resource.Parameters.connector,0);
            end 
            if ~strcmpi(result, 'SUCCESS'), error('VSX: Failed to select scanhead because "%s".\n', result); end    
        end

        % If this scanhead is a HVMux scanhead, set the HVMux attributes.  HVMux operation is
        % not permitted with Trans.name = 'custom'.
        % NOTE:  This call returns but the HvMux high voltage rails are still ramping to try and
        % achieve the specified levels.  When runAcq is called below, runAcq's call to
        % Vs_SequenceLoadFromMatlab() will call Vs_SequenceLoad() which will wait until the HvMux
        % high voltage rails have reached their specified levels, or timeout with an error.  Thus,
        % when runAcq calls Vs_SequenceStart(), the HvMux high voltage levels will be stable.
        if isfield(Trans,'HVMux')&&~(strcmpi('custom',Trans.name)||(Resource.Parameters.connector == 0))
            [result, tpcHvMuxHvRailsResulting, tpcHvMuxLogicRailResulting] = vdasScanheadHvMuxSet( ...
               Trans.HVMux.highVoltageRails, ... % tpcHvMuxHvRails
               Trans.HVMux.logicRail, ...   % tpcHvMuxLogicRail
               Trans.HVMux.polarity, ...    % isShiDataInvert
               Trans.HVMux.latchInvert, ... % isShiLatchInvert
               Trans.HVMux.clockInvert, ... % isShiClockInvert
               Trans.HVMux.clock); % shiClockRate
            if strcmpi(result, 'SHI HvMux Erratum')
              % A Verasonics ScanHead Interface (SHI) board was detected to
              % have a known erratum when it is used with HvMux.  The HAL
              % has performed normal logic and after all normal processing
              % sets the return code to indicate the erratum.  Since we want
              % existing customers' systems to continue working after upgrading
              % to this software, we choose to turn the error into a warning.
              % UPDATE:  At this time, we are choosing to not report this warning.
              %warning('Verasonics:VSX', ...
              %  ['The detected version of the Verasonics ScanHead Interface (SHI) ' ...
              %  'board has a known erratum when HvMux scanheads are used with it.  ' ...
              %  'Please contact Support@Verasonics.com to see if you need your ' ...
              %  'Verasonics SHI board repaired.' ...
              %  '\nYou may hide this message by commenting it out in VSX.m.']);
            elseif ~strcmpi(result, 'SUCCESS')
                error('VSX: Failed to set scanhead HvMux attributes because "%s".\n', result);
            end
        elseif strcmpi('custom',Trans.name) && isfield(Trans,'HVMux')
            error('VSX: HVMux operation with a transducer name of ''custom'' is not supported.');
        end
    else % SHI was not detected. Check for newer TPC available - if so, there must be a problem with the SHI.
        if logical(vdasTpcPresenceGet())
            error('VSX: SHI not detected in V3 VDAS system, which indicates system fault.  Try vdasReset.');
        end
    end
end

% Set up GUI
if ~isfield(Resource.Parameters, 'GUI'), Resource.Parameters.GUI = 'vsx_gui'; end
if strcmp(Resource.Parameters.GUI,'vsx_guiOld')
    if ispc
        Resource.Parameters.GUI = 'vsx_guiPC';
    else
        Resource.Parameters.GUI = 'vsx_guiOld';
    end
end    
vsxGUIh = str2func(Resource.Parameters.GUI);
vsxGUIh();

% Set up DisplayWindow(s) for output image. DisplayWindow(s) can be created for displaying
% processed ImageBuffers.  If no ImageBuffer is specified in the user script, this step
% is skipped.  If an ImageBuffer is specified, but no DisplayWindow is defined, a default
% DisplayWindow is created, and sized according to the first PData structure.
if isfield(Resource,'ImageBuffer')
    if ~isfield(Resource,'DisplayWindow')
        % if no DisplayWindow specified, use default window.
        Resource.DisplayWindow = struct('Title', displayWindowTitle, ...
                                        'Position', [250,150,PData(1).Size(2)*4,PData(1).Size(1)*4], ...
                                        'ReferencePt',[PData(1).Origin(1),PData(1).Origin(3)], ...
                                        'pdelta', PData(1).pdelta/4, ...
                                        'Colormap',gray(256), ...
                                        'splitPalette',0, ...
                                        'clrWindow',0, ...  
                                        'figureHandle', 0, ...
                                        'imageHandle', 0);
    end
    for i = 1:size(Resource.DisplayWindow,2)
        if (~isfield(Resource.DisplayWindow(i),'Title'))||isempty(Resource.DisplayWindow(i).Title)
            Resource.DisplayWindow(i).Title = displayWindowTitle;
        end
        if (~isfield(Resource.DisplayWindow(i),'Position'))||isempty(Resource.DisplayWindow(i).Position)
            Resource.DisplayWindow(i).Position = [250,150,PData(1).Size(2)*4,PData(1).Size(1)*4];
        end
        width = Resource.DisplayWindow(i).Position(3);
        height = Resource.DisplayWindow(i).Position(4);
        DisplayData = zeros(height,width,'uint8');
        % Hide display if requested
        visibility = 'on';
        if((true == exist('Mcr_DisplayHide', 'var')) && (1 == Mcr_DisplayHide))
            % Caller has requested that we do NOT show the display window.
            visibility = 'off';
        end
        Resource.DisplayWindow(i).figureHandle = figure( ...
                'Name',Resource.DisplayWindow(i).Title,...
                'NumberTitle','off',...
                'Position',[Resource.DisplayWindow(i).Position(1), ... % left edge
                            Resource.DisplayWindow(i).Position(2), ... % bottom
                            width + 100, height + 150], ...            % width, height
                'Renderer','painters', ...
                'Colormap',Resource.DisplayWindow(i).Colormap, ...
                'Visible',visibility);
        axes('Units','pixels','Position',[60,90,width,height]);
        set(gca, 'Units','normalized');  % restore normalized units for re-sizing window.
        if ~isfield(Resource.DisplayWindow(i),'splitPalette')||isempty(Resource.DisplayWindow(i).splitPalette)
            Resource.DisplayWindow(i).splitPalette = 0;
        end
        if ~isfield(Resource.DisplayWindow(i),'pdelta')||isempty(Resource.DisplayWindow(i).pdelta)
            Resource.DisplayWindow(i).pdelta = PData(1).pdelta/4; 
        end
        if ~isfield(Resource.DisplayWindow(i),'clrWindow')||isempty(Resource.DisplayWindow(i).clrWindow) 
            Resource.DisplayWindow(i).clrWindow = 0; 
        end
        % Determine whether DisplayWindow is x,z (normal 2D scan) or x,y (3D C-scan) oriented.  The 
        % DisplayWindow.ReferencePt is a point in the x,z plane for normal 2D scans, or a point in
        % the x,y plane for a C-scan in a 3D volume.
        if Trans.type < 2
            % DisplayWindow is x,z oriented
            if ~isfield(Resource.DisplayWindow(i),'ReferencePt')||isempty(Resource.DisplayWindow(i).ReferencePt)
                Resource.DisplayWindow(i).ReferencePt = [PData(1).Origin(1),PData(1).Origin(3)];
            end
            vmin = Resource.DisplayWindow(i).ReferencePt(2); % vertical minimum is DisplayWindow.ReferencePt(2).
            vmax = vmin + height*Resource.DisplayWindow(i).pdelta;
            set(gca,'YDir','reverse');
        else
            % DisplayWindow is x,y oriented
            if ~isfield(Resource.DisplayWindow(i),'ReferencePt')||isempty(Resource.DisplayWindow(i).ReferencePt)
                Resource.DisplayWindow(i).ReferencePt = [PData(1).Origin(1),PData(1).Origin(2)];
            end
            vmin = Resource.DisplayWindow(i).ReferencePt(2);
            vmax = vmin - height*Resource.DisplayWindow(i).pdelta;
            set(gca,'YDir','normal');
        end
        xmin = Resource.DisplayWindow(i).ReferencePt(1);
        xmax = xmin + width*Resource.DisplayWindow(i).pdelta;
        Resource.DisplayWindow(i).imageHandle = image('CData',DisplayData, ...
                'XData',[xmin,xmax], ...
                'YData',[vmin,vmax]);
        axis equal tight;
        drawnow
    end
    % Capture Resource.DisplayWindow(1).pdelta for use by zoom functions.
    orgDispPdelta = Resource.DisplayWindow(1).pdelta;

    % Modify a DisplayWindow's default grayscale map(s), if specified by a Process structure that
    % references the DisplayWindow.
    if exist('Process','var') 
        for i = 1:size(Process,2)
            if (strcmp(Process(i).classname,'Image'))&&(strcmp(Process(i).method,'imageDisplay'))
                n = 0;
                for j = 1:2:size(Process(i).Parameters,2)
                    if strcmp(Process(i).Parameters{j},'displayWindow')
                        n = Process(i).Parameters{j+1};
                    end
                end
                if n==0, error('VSX: Process(%d) structure for imageDisplay method has no ''DisplayWindow'' attribute.\n',i); end
                Image(n) = struct('compression',0.5,'reject',0,'displayWindow',n,'extDisplay',0); % set defaults
                for j = 1:2:size(Process(i).Parameters,2)
                    if strcmp(Process(i).Parameters{j},'compression')
                        Image(n).compression = Process(i).Parameters{j+1};
                    end
                    if strcmp(Process(i).Parameters{j},'reject')
                        Image(n).reject = Process(i).Parameters{j+1};
                    end
                    if strcmp(Process(i).Parameters{j},'extDisplay')
                        Image(n).extDisplay = Process(i).Parameters{j+1};
                    end
                end
                if Image(n).extDisplay == 0
                    changeMap(Resource.DisplayWindow(Image(n).displayWindow).figureHandle,...
                          2*(Image(n).compression-0.5),Image(n).reject,...
                          Resource.DisplayWindow(Image(n).displayWindow).splitPalette);
                end
            end
        end
    end
end

% ***** Initialize GUI and add UI objects. *****
% - Set state of simulate and rcvdataloop buttons (initial state is 0).
set(findobj('String','Rcv Data Loop'),'Value',rloopButton);
set(findobj('String','Simulate'),'Value',simButton);

% Make UI window the current figure, unless the caller has requested to hide it.
if((true == exist('Mcr_GuiHide', 'var')) && (1 == Mcr_GuiHide))
  % Caller has requested that we do NOT show the GUI window.
  display('NOTE:  Caller has requested that the VSX GUI window be hidden.  Use ctrl-c to abort VSX if necessary.');
else
  % OK, DO make the GUI window active.
  set(0, 'CurrentFigure', findobj('tag','UI'));
end

% - If .mat file contains a UI object, add it to UI window.
if exist('UI','var')
    if strcmp('vsx_gui',Resource.Parameters.GUI)
        % Get GUI background color.
        f = findobj('tag','UI');
        bkgrnd = get(f,'Color');
        % Define available user control labels and their positions.
        UserID = {'UserA1','UserA2','UserB1','UserB2','UserB3','UserB4','UserB5','UserB6','UserB7','UserB8',...
                  'UserC1','UserC2','UserC3','UserC4','UserC5','UserC6','UserC7','UserC8'};
        UserPos = [UIPos(2,:,1); UIPos(3,:,1);...
                   UIPos(2,:,2); UIPos(3,:,2); UIPos(4,:,2); UIPos(5,:,2); UIPos(6,:,2); UIPos(7,:,2); UIPos(8,:,2); UIPos(9,:,2);...
                   UIPos(2,:,3); UIPos(3,:,3); UIPos(4,:,3); UIPos(5,:,3); UIPos(6,:,3); UIPos(7,:,3); UIPos(8,:,3); UIPos(9,:,3)];
    end
    for i = 1:size(UI,2)
        if (isfield(UI(i),'Statement')&&~isempty(UI(i).Statement))
            eval(UI(i).Statement);
        end
        if isfield(UI(i),'Control') && ~isempty(UI(i).Control)
            if strcmp('vsx_gui',Resource.Parameters.GUI)
                if strncmp(UI(i).Control{1},'User',4) % Do we have a UserXX control?
                    L = strcmp('Style',UI(i).Control);
                    j = find(L,1);
                    if isempty(j), error('VSX: No ''Style'' defined for user UI(%d).Control.\n',i); end
                    UIStyle = UI(i).Control{j+1};
                    L = strcmp(UI(i).Control{1},UserID);
                    j = find(L,1);
                    if isempty(j), error('VSX: No definition exists for UI(%d).Control at %s.\n',i,UI(i).Control{1}); end
                    switch UIStyle
                        case 'VsSlider'
                            L = strcmpi('Label',UI(i).Control);
                            k = find(L,1);
                            if ~isempty(k), Txt = UI(i).Control{k+1}; else Txt = UserID(j); end
                            L = strcmpi('SliderMinMaxVal',UI(i).Control);
                            k = find(L,1);
                            if ~isempty(k), MMV = UI(i).Control{k+1}; else MMV = [1,100,1]; end
                            L = strcmpi('SliderStep',UI(i).Control);
                            k = find(L,1);
                            if ~isempty(k), SS = UI(i).Control{k+1}; else SS = [0.01,0.1]; end
                            L = strcmpi('ValueFormat',UI(i).Control);
                            k = find(L,1);
                            if ~isempty(k), VF = UI(i).Control{k+1}; else VF = '%3.0f'; end
                            UserT = {f,'Style','text','String',Txt,'Units','normalized',...
                                'Position',[(UserPos(j,:)+SG.TO) SG.TS],...
                                'FontUnits','normalized','FontSize',SG.TF,'FontWeight','bold'};
                            UserS = {f,'Style','slider','Min',MMV(1),'Max',MMV(2),'Value',MMV(3),...
                                'SliderStep',SS,'Units','normalized','Position',[UserPos(j,:)+SG.SO,SG.SS],...
                                'BackgroundColor',bkgrnd-0.05,'Tag',[UserID{j} 'Slider'],...
                                'Callback',{str2func([UserID{j} 'Callback'])}};
                            UserE = {f,'Style','edit','String',num2str(MMV(3),VF),'UserData',VF,...
                                'Units','normalized','Position',[(UserPos(j,:)+SG.EO) SG.ES],...
                                'BackgroundColor',bkgrnd+0.1,'Tag',[UserID{j} 'Edit'],...
                                'Callback',{str2func([UserID{j} 'Callback'])}};        
                            UI(i).handle = [0,0,0];
                            UI(i).handle(1) = uicontrol(UserT{:});
                            UI(i).handle(2) = uicontrol(UserS{:});
                            UI(i).handle(3) = uicontrol(UserE{:});
                            % Define UserXX.Callback preamble.
                            UserCBPre = {...
                                [UserID{j} 'Callback(hObject, eventdata);'],...
                                ' ',...
                                'Cntrl = get(hObject, ''Style'');',...
                                'if strcmp(Cntrl,''slider'')',...
                                '    UIValue = get(hObject,''Value'');',...
                                ['    h = findobj(''Tag'', ''' [UserID{j} 'Edit'] ''');'],...
                                '    set(h,''String'',num2str(UIValue,get(h,''UserData'')));',...
                                'else',...
                                '    UIValue = str2num(get(hObject,''String''));',...
                                ['    h = findobj(''Tag'', ''' [UserID{j} 'Slider'] ''');'],...
                                '    mx = get(h,''Max'');',...
                                '    mn = get(h,''Min'');',...
                                '    if (UIValue > mx)',...
                                '        UIValue = mx;',...
                                '        set(hObject,''String'',num2str(UIValue,get(hObject,''UserData'')));',...
                                '    end',...
                                '    if (UIValue < mn)',...
                                '        UIValue = mn;',...
                                '        set(hObject,''String'',num2str(UIValue,get(hObject,''UserData'')));',...
                                '    end',...
                                '    set(h,''Value'',UIValue);',...
                                'end'};
                        case 'VsPushButton'
                            L = strcmpi('Label',UI(i).Control);
                            k = find(L,1);
                            if ~isempty(k), Txt = UI(i).Control{k+1}; else Txt = UserID(j); end
                            UI(i).handle = uicontrol(f,'Style','pushbutton','String',Txt,....
                                'Units','normalized','Position',[UserPos(j,:)+PB.BO,PB.BS],...
                                'FontUnits','normalized','FontSize',PB.FS,...
                                'BackgroundColor',bkgrnd+0.05,...
                                'Callback',{str2func([UserID{j} 'Callback'])});                               
                            % Define UserXX.Callback preamble.
                            UserCBPre = {...
                                [UserID{j} 'Callback(hObject, eventdata);'],...
                                ' '};
                        case 'VsToggleButton'
                            L = strcmpi('Label',UI(i).Control);
                            k = find(L,1);
                            if ~isempty(k), Txt = UI(i).Control{k+1}; else Txt = UserID(j); end
                            UI(i).handle = uicontrol(f,'Style','togglebutton','String',Txt,...
                                'Units','normalized','Position',[UserPos(j,:)+PB.BO,PB.BS],...
                                'FontUnits','normalized',...    %'FontSize',PB.FS,...
                                'BackgroundColor',bkgrnd+0.05,'tag',[UserID{j} 'ToggleButton'],...
                                'Callback',{str2func([UserID{j} 'Callback'])});                               
                            % Define UserXX.Callback preamble.
                            UserCBPre = {...
                                [UserID{j} 'Callback(hObject, eventdata);'],...
                                ' ',...
                                'button_state = get(hObject,''Value'');',...
                                'if button_state == get(hObject,''Max'')',...
                                '   % Toggle button is pressed.',...
                                '   UIState = 1;',...
                                'elseif button_state == get(hObject,''Min'')',...
                                '   % Toggle button is not pressed.',...
                                '   UIState = 0;',...
                                'end'};
                        case 'VsButtonGroup'
                            L = strcmpi('Title',UI(i).Control);
                            k = find(L,1);
                            if ~isempty(k), Txt = UI(i).Control{k+1}; else Txt = UserID(j); end
                            L = strcmpi('NumButtons',UI(i).Control);
                            k = find(L,1);
                            if ~isempty(k), nbuttons = UI(i).Control{k+1}; else nbuttons = 2; end
                            L = strcmpi('Labels',UI(i).Control);
                            k = find(L,1);
                            if ~isempty(k), Labels = UI(i).Control{k+1};
                            else for k=1:nbuttons, Labels(k) = num2str(k); end
                            end
                            % Modify sizes for no. of buttons.
                            BG.BGS(2) = (nbuttons+1)*BG.BI;
                            BG.BGO(2) = 0.12 - BG.BGS(2);
                            BG.BS(2) = 1/(nbuttons+1);
                            BG.BO(2) = 1-2*BG.BS(2);
                            BG.TFS = 1/(nbuttons+2.5);
                            UI(i).handle = zeros(1,nbuttons);
                            UI(i).handle(1) = uibuttongroup('Title',Txt,'Units','normalized',...
                                'Position',[UserPos(j,:)+BG.BGO,BG.BGS],...
                                'FontUnits','normalized','FontSize',BG.TFS,'FontWeight','bold',...
                                'BackgroundColor',bkgrnd+0.05,'SelectionChangeFcn',{str2func([UserID{j} 'Callback'])});
                            for k=1:nbuttons
                                UI(i).handle(k+1) = uicontrol('Style','radiobutton','Parent',UI(i).handle(1),...
                                    'Units','normalized',...
                                    'Position',[BG.BO(1),BG.BO(2)-(k-1)*(1/(nbuttons+1)),BG.BS],...
                                    'FontUnits','normalized','FontSize',BG.BFS,'String',Labels(k),...    
                                    'Tag',[UserID{j} 'RadioButton' num2str(k)]);
                            end
                            % Define UserXX.Callback preamble.
                            UserCBPre = {...
                                [UserID{j} 'Callback(hObject, eventdata);'],...
                                ' ',...
                                'S = get(eventdata.NewValue,''Tag'');',...
                                'UIState = str2double(S(18));',...
                                };
                        otherwise
                            error('VSX: Unrecognized style for user UI(%d).Control.',i);
                    end
                else
                    UI(i).handle = uicontrol(UI(i).Control{:});
                end
            else
                if strcmp('vsx_guiPC',Resource.Parameters.GUI)
                % Adjust Windows UIControl parameters to match Unix values
                    L = strcmpi('Style',UI(i).Control);
                    index = find(L,1);
                    if ~isempty(index)
                        switch UI(i).Control{index+1}
                            case 'text'
                                L = strcmpi('Position',UI(i).Control);
                                index = find(L,1);
                                if ~isempty(index) 
                                    UI(i).Control{index+1}(2) = UI(i).Control{index+1}(2)-4;
                                end                            
                            case 'slider'
                                L = strcmpi('Position',UI(i).Control);
                                index = find(L,1);
                                if ~isempty(index) 
                                    UI(i).Control{index+1}(4) = UI(i).Control{index+1}(4)-10;
                                end
                            case 'edit'
                                L = strcmpi('Position',UI(i).Control);
                                index = find(L,1);
                                if ~isempty(index) 
                                    UI(i).Control{index+1}(1) = UI(i).Control{index+1}(1)+5;
                                    UI(i).Control{index+1}(2) = UI(i).Control{index+1}(2)-12;
                                end
                        end
                    end
                end
                UI(i).handle = uicontrol(UI(i).Control{:});
            end
        end 
        if (isfield(UI(i),'Callback')&&~isempty(UI(i).Callback))
            % Decode callback function.
            % 
            % WARNING:  MCR cannot use dynamically generated .m scripts.  They
            % must exist at the time MATLAB Compiler was run.
            % 
            % Thus, use "ismcc"/"isdeployed" to avoid dynamically generated .m
            % files.  For Verasonics SetUpXxx.m calls, this means the
            % Verasonics UI.Callback entries that use tempdir dynamically
            % generated scripts.
            if(false == isdeployed)
                % OK, we are NOT under MCR, so DO make these calls.
                if (isfield(UI(i),'Control')&&~isempty(UI(i).Control))&&strncmp(UI(i).Control{1},'User',4) % Do we have a UserXX control?
                    fname = textscan(UserCBPre{1},'%s %*[^\n]', 'Delimiter','(');
                    fid = fopen([tempdir,fname{1}{1},'.m'], 'w');
                    fprintf(fid,'function %s\n', UserCBPre{1});
                    for j = 2:size(UserCBPre,2)
                        fprintf(fid,'%s\n', UserCBPre{j});
                    end
                    for j = 1:size(UI(i).Callback,2)
                        fprintf(fid,'%s\n', UI(i).Callback{j});
                    end                
                else
                    % Check for older format that required callback name on first line.
                    S = deblank(UI(i).Callback{1});
                    S = S((length(S)-1):length(S));  % if older format, S will get '.m'
                    if strcmp(S,'.m') % old format
                        fid = fopen([tempdir,UI(i).Callback{1}], 'w');
                        for j = 2:size(UI(i).Callback,2)
                            fprintf(fid,'%s\n', UI(i).Callback{j});
                        end
                    else % new format that has function prototype as first line.
                        n = strfind(UI(i).Callback{1},'=');
                        if ~isempty(n)
                            fname = textscan(UI(i).Callback{1},'%*s %s %*[^\n]','Delimiter',{'=','('});
                        else
                            fname = textscan(UI(i).Callback{1},'%s %*[^\n]', 'Delimiter','(');
                        end
                        fid = fopen([tempdir,fname{1}{1},'.m'], 'w');
                        fprintf(fid,'function %s\n', UI(i).Callback{1});
                        for j = 2:size(UI(i).Callback,2)
                            fprintf(fid,'%s\n', UI(i).Callback{j});
                        end                        
                    end
                end
                status = fclose(fid);
                addpath(tempdir);
                rehash path
            end
        end
    end
end

% ***** HIFU Limits Check ***** 
% If profile 5 is to be used, run the TXEventCheck routine before we actually start running to see
% if the setup script imposes any overlimit conditions that could lead to HW damage.
if profile5ena > 0
    % Note that TPC(5) has already been initialized with the maxHighVoltage values that 
    % represent the limits in the gui hv control sliders.
    [TXerr, TXEvent] = TXEventCheckh();
    if TXerr
        if TXerr == 2
            fprintf(2,'VSX: Transmit profile 5 overlimit conditions detected that could be damaging to system.\n')
            fprintf(2,' Reduce frequency, duty cycle, or make other changes as appropriate, and then run the SetUp script and VSX again.\n');
        else
            fprintf(2,'VSX: An illegal operating state or command has been detected.\n')
        end
        error(' TXEvent structure in workspace may help identify error.');
    end
end


% ***** Start of loop for continuously calling 'runAcq'. *****
% 'runAcq' is called with the single input, 'Control', which is a two attribute structure 
%    array where the first attribute, 'Command', is a command string,  and the second attribute,
%    'Parameters', is a cell array whose interpretation depends on the command given. 
% Valid commands are:
%    Control.Command = 
%      'set' - set an attribute of an existing object (structure) and return.
%          Control.Parameters = {'Object', objectNumber, 'attribute', value, 'attribute', value, ...}
%      'set&Run' - set an attribute of an existing object (structure) and run sequence.
%          Control.Parameters = {'Object', objectNumber, 'attribute', value, 'attribute', value, ...}
%      'update&Run' - update the entire object(s) by re-reading from Matlab, then run sequence.
%          Control.Parameters = {'Resource','ImageBuffer','DisplayWindow','Parameters', ...
%                                'Trans','Media','SFormat','PData','TW','TX','Receive', ...
%                                'TGC','Recon','Process','SeqControl','Event'}*
%                                * provide one or more of this list of objects
%      'copyBuffers' - Copies back to Matlab IQData,ImgData,ImgDataP and LogData
%          buffers, without running the Event sequence.
%      'setBFlag' - set the BFlag used for triggering the conditional branch sequence control.
%      'imageDisplay' - display an image using the specified Image parameters.
%          Control.Parameters = {'Image attribute, value, ...}
%      'debug' = turns on or off debug output.
Control.Command = [];  % Set Control.Command to 'empty' for no control action.
%Control.Command = 'debug';  % Control.Command to turn on or off debug output.
%Control.Parameters = {'on'};
action = 'none';  % First main loop action must be 'none'.
exit = 0;   % exit will get set to 1 when UI window is closed.
freeze = 0; % freeze will get set to 1 when 'Freeze' togglebutton is pressed.
sequencePeriod = 1;   % set initial frame period to 1 second
tElapsed = 0;

% If a caller has set "relaunchVsxComplete" callback, then call it now.
% VSX has finished reloading and is about to start running.  The caller
% may need to refresh its state based on items generated above and must
% do so before the sequence starts.
if(true == exist('relaunchVsxComplete', 'var'))
    % OK, we DO have a callback hook to call.
    functionHandle = str2func(relaunchVsxComplete);
    functionHandle();
    clear relaunchVsxComplete;
end

% Run simulation.
while exit==0
    drawnow
    if freeze == 1
        % If freeze is on, copy buffers back to Matlab and wait for freeze
        % togglebutton to be pressed off. copyBuffers does a sequenceStop if running
        % with the hardware.
        Control.Command = 'copyBuffers'; 
        runAcq(Control); % NOTE:  If runAcq() has an error, it reports it then exits MATLAB.
        
        % We are about to enter a wait-for-unfreeze.  During that time, no
        % commands besides callbacks are available.  For callers that require
        % the ability to send a command during the time VSX is frozen, we
        % implement a MATLAB Timer.
        % 
        % If the caller sets the variable Mcr_FreezeTimerFunction to
        % the name of their callback function, then a timer will be created
        % to periodically call that callback, thus allowing the injection of
        % commands while frozen, such as unfreeze.  (This would be the case
        % for a caller who has hidden the VSX GUI control panel, and thus the
        % unfreeze button, with their own custom GUI control panel that uses
        % only "pull" technology rather than VSX GUI control panel's "push"
        % technology.  "Pull" here means that case where VSX is running
        % separately from a custom control panels and VSX is asking the
        % custom control panel for any commands to execute, as is the case
        % for the "Custom App" example in "MATLAB Compiler/Windows 64/R2011a/dot NET".)
        % 
        % NOTE:  Because the timer works within the MATLAB single-threaded
        % environment, it cannot guarantee execution times or execution rates.
        if((true == exist('Mcr_FreezeTimerFunction', 'var')) && (0 == strcmp('', Mcr_FreezeTimerFunction)))
          % OK, we DO have a callback hook to call from a Timer.
          % 
          % We want to call the user's callback SCRIPT, but Timers want to
          % call a FUNCTION that accepts (object,event).  So, we make an
          % anonymous function here that calls the user's script.
          functionHandle = @(object, event)evalin('base', Mcr_FreezeTimerFunction);
          freezeTimer = timer ...
          ( ...
            'BusyMode',      'queue', ...        % So communication is not lost
            'ExecutionMode', 'fixedRate', ...
            'StartDelay',    0, ...              % Call callback immediately
            'Period',        0.25, ...           % Call again every 1/4 seconds
            'TimerFcn',      functionHandle ... % User's callback
          );
          
          start(freezeTimer);
        end
        
        % Wait for unfreeze
        waitfor(findobj('String','Freeze'),'Value',0);
        if exit==1 % exit might get set while in freeze.
            break;
        end
        
        % If we used a timer above, stop it and delete it.
        if(true == exist('freezeTimer', 'var'))
          % OK, we DO have a timer to stop and delete.
          stop(freezeTimer);
          delete(freezeTimer);
          clear freezeTimer;
        end
        
        % Reset startEvent, in case sequence contains return-to-Matlab points prior to the
        % end of sequence, where 'freeze' may have been pressed.  Sequence will be re-started
        % after updating startEvent.
        if isempty(Control(1).Command), n=1; else n=length(Control)+1; end 
        Control(n).Command = 'set&Run';
        Control(n).Parameters = {'Parameters',1,'startEvent',Resource.Parameters.startEvent};
    end
    % The following switch statement implements actions set by the default GUI controls.
    switch action
        case 'none'
        case 'tgc'
            TGC(1).CntrlPts = [tgc1,tgc2,tgc3,tgc4,tgc5,tgc6,tgc7,tgc8];
            TGC(1).Waveform = computeTGCWaveform(TGC(1));
            if (VDAS==1)&&(Resource.Parameters.simulateMode==0) 
                Result = loadTgcWaveform(1); 
                if ~strcmpi(Result, 'SUCCESS'), break; end
            end
        case 'zoomin'
            % 'zoomin' keeps the size of the DisplayWindow constant, but decreases both 
            % DisplayWindow.pdelta and PData(1).pdelta. The DisplayWindow.refPt also is changed to
            % zoom from the center. Note: 'zoomin' only works on DisplayWindow 1, which is assumed
            % to be the main image display window.
            cx = Resource.DisplayWindow(1).ReferencePt(1) + Resource.DisplayWindow(1).pdelta*...
                (Resource.DisplayWindow(1).Position(3)-1)/2;
            cz = Resource.DisplayWindow(1).ReferencePt(2) + Resource.DisplayWindow(1).pdelta*...
                (Resource.DisplayWindow(1).Position(4)-1)/2;
            Resource.DisplayWindow(1).pdelta = Resource.DisplayWindow(1).pdelta * 0.8;
            if Resource.DisplayWindow(1).pdelta < orgDispPdelta * 0.2621
                Resource.DisplayWindow(1).pdelta = orgDispPdelta * 0.2621;
            end
            Resource.DisplayWindow(1).ReferencePt(1) = cx - Resource.DisplayWindow(1).pdelta*...
                (Resource.DisplayWindow(1).Position(3)-1)/2;
            Resource.DisplayWindow(1).ReferencePt(2) = cz - Resource.DisplayWindow(1).pdelta*...
                (Resource.DisplayWindow(1).Position(4)-1)/2;
            PData(1).Origin(1) = Resource.DisplayWindow(1).ReferencePt(1);
            PData(1).Origin(3) = Resource.DisplayWindow(1).ReferencePt(2);
            PData(1).pdeltaX = PData(1).pdeltaX * 0.8;
            if PData(1).pdeltaX < orgPdeltaX * 0.2621, PData(1).pdeltaX = orgPdeltaX * 0.2621; end
            PData(1).pdeltaZ = PData(1).pdeltaZ * 0.8;
            if PData(1).pdeltaZ < orgPdeltaZ * 0.2621, PData(1).pdeltaZ = orgPdeltaZ * 0.2621; end
            axesHandle = get(Resource.DisplayWindow(1).figureHandle,'CurrentAxes');
            cla(axesHandle); % this clears any objects attached to the old axes.
            ymin = Resource.DisplayWindow(1).ReferencePt(2);
            ymax = ymin + Resource.DisplayWindow(1).Position(4)*Resource.DisplayWindow(1).pdelta;
            xmin = Resource.DisplayWindow(1).ReferencePt(1);
            xmax = xmin + Resource.DisplayWindow(1).Position(3)*Resource.DisplayWindow(1).pdelta;
            DisplayData = zeros(Resource.DisplayWindow(1).Position(4),Resource.DisplayWindow(1).Position(3),'uint8');
            Resource.DisplayWindow(1).imageHandle = image(DisplayData,...
                    'Parent',axesHandle, ...
                    'Xdata',[xmin,xmax], ...
                    'YData',[ymin,ymax]);
            Resource.DisplayWindow(1).clrWindow = 1;
            Resource.ImageBuffer(1).lastFrame = 0; % this clears the ImageBuffer
            [PData(1).Region,PData(1).numRegions] = createRegions(PData(1));
            if isempty(Control(1).Command), n=1; else n=length(Control)+1; end 
            Control(n).Command = 'update&Run';
            Control(n).Parameters = {'PData','Recon','DisplayWindow','ImageBuffer'};
        case 'zoomout'
            cx = Resource.DisplayWindow(1).ReferencePt(1) + Resource.DisplayWindow(1).pdelta*...
                (Resource.DisplayWindow(1).Position(3)-1)/2;
            cz = Resource.DisplayWindow(1).ReferencePt(2) + Resource.DisplayWindow(1).pdelta*...
                (Resource.DisplayWindow(1).Position(4)-1)/2;
            Resource.DisplayWindow(1).pdelta = Resource.DisplayWindow(1).pdelta * 1.25;
            if Resource.DisplayWindow(1).pdelta > orgDispPdelta * 3.8147
                Resource.DisplayWindow(1).pdelta = orgDispPdelta * 3.8147;
            end
            Resource.DisplayWindow(1).ReferencePt(1) = cx - Resource.DisplayWindow(1).pdelta*...
                (Resource.DisplayWindow(1).Position(3)-1)/2;
            Resource.DisplayWindow(1).ReferencePt(2) = cz - Resource.DisplayWindow(1).pdelta*...
                (Resource.DisplayWindow(1).Position(4)-1)/2;
            PData(1).Origin(1) = Resource.DisplayWindow(1).ReferencePt(1);
            PData(1).Origin(3) = Resource.DisplayWindow(1).ReferencePt(2);
            PData(1).pdeltaX = PData(1).pdeltaX * 1.25;
            if PData(1).pdeltaX > orgPdeltaX * 3.8147, PData(1).pdeltaX = orgPdeltaX * 3.8147; end
            PData(1).pdeltaZ = PData(1).pdeltaZ * 1.25;
            if PData(1).pdeltaZ > orgPdeltaZ * 3.8147, PData(1).pdeltaZ = orgPdeltaZ * 3.8147; end
            axesHandle = get(Resource.DisplayWindow(1).figureHandle,'CurrentAxes');
            cla(axesHandle); % this clears any objects attached to the old axes.
            ymin = Resource.DisplayWindow(1).ReferencePt(2);
            ymax = ymin + Resource.DisplayWindow(1).Position(4)*Resource.DisplayWindow(1).pdelta;
            xmin = Resource.DisplayWindow(1).ReferencePt(1);
            xmax = xmin + Resource.DisplayWindow(1).Position(3)*Resource.DisplayWindow(1).pdelta;
            DisplayData = zeros(Resource.DisplayWindow(1).Position(4),Resource.DisplayWindow(1).Position(3),'uint8');
            Resource.DisplayWindow(1).imageHandle = image(DisplayData,...
                    'Parent',axesHandle, ...
                    'Xdata',[xmin,xmax], ...
                    'YData',[ymin,ymax]);
            Resource.DisplayWindow(1).clrWindow = 1;
            Resource.ImageBuffer(1).lastFrame = 0; % this clears the ImageBuffer
            [PData(1).Region,PData(1).numRegions] = createRegions(PData(1));
            if isempty(Control(1).Command), n=1; else n=length(Control)+1; end
            Control(n).Command = 'update&Run';
            Control(n).Parameters = {'PData','Recon','DisplayWindow','ImageBuffer'};
        case 'panlft'
            % pan 10% of image width.
            PData(1).Origin(1) = PData(1).Origin(1) - 0.1*PData(1).pdeltaX*PData(1).Size(2);
            Resource.DisplayWindow(1).ReferencePt(1) = PData(1).Origin(1);
            Resource.DisplayWindow(1).clrWindow = 1;
            Resource.ImageBuffer(1).lastFrame = 0;
            [PData(1).Region,PData(1).numRegions] = createRegions(PData(1));
            if isempty(Control(1).Command), n=1; else n=length(Control)+1; end
            Control(n).Command = 'update&Run';
            Control(n).Parameters = {'PData','Recon','DisplayWindow','ImageBuffer'};
        case 'panrt'
            % pan 10% of image width.
            PData(1).Origin(1) = PData(1).Origin(1) + 0.1*PData(1).pdeltaX*PData(1).Size(2);
            Resource.DisplayWindow(1).ReferencePt(1) = PData(1).Origin(1);
            Resource.DisplayWindow(1).clrWindow = 1;
            Resource.ImageBuffer(1).lastFrame = 0;
            [PData(1).Region,PData(1).numRegions] = createRegions(PData(1));
            if isempty(Control(1).Command), n=1; else n=length(Control)+1; end
            Control(n).Command = 'update&Run';
            Control(n).Parameters = {'PData','Recon','DisplayWindow','ImageBuffer'};
        case 'panup'
            % pan 10% of image height.
            PData(1).Origin(3) = PData(1).Origin(3) - 0.1*PData(1).pdeltaZ*PData(1).Size(1);
            Resource.DisplayWindow(1).ReferencePt(2) = PData(1).Origin(3);
            Resource.DisplayWindow(1).clrWindow = 1;
            Resource.ImageBuffer(1).lastFrame = 0;
            [PData(1).Region,PData(1).numRegions] = createRegions(PData(1));
            if isempty(Control(1).Command), n=1; else n=length(Control)+1; end
            Control(n).Command = 'update&Run';
            Control(n).Parameters = {'PData','Recon','DisplayWindow','ImageBuffer'};
        case 'pandn'
            % pan 10% of image height.
            PData(1).Origin(3) = PData(1).Origin(3) + 0.1*PData(1).pdeltaZ*PData(1).Size(1);
            Resource.DisplayWindow(1).ReferencePt(2) = PData(1).Origin(3);
            Resource.DisplayWindow(1).clrWindow = 1;
            Resource.ImageBuffer(1).lastFrame = 0;
            [PData(1).Region,PData(1).numRegions] = createRegions(PData(1));
            if isempty(Control(1).Command), n=1; else n=length(Control)+1; end
            Control(n).Command = 'update&Run';
            Control(n).Parameters = {'PData','Recon','DisplayWindow','ImageBuffer'};
        case 'persist'
            % Set the value of persistence for a Process structure with class = 'image, method = 
            %  'imageDisplay' and 'displayWindow' = 1;
            if exist('Process','var')
                % Find the 'Image/imageDisplay' Process structure for displayWindow 1
                for i = 1:size(Process,2)
                    if (strcmp(Process(i).classname,'Image'))&&(strcmp(Process(i).method,'imageDisplay'))
                        for j = 1:2:size(Process(i).Parameters,2)
                            if (strcmp(Process(i).Parameters{j},'displayWindow'))&&(Process(i).Parameters{j+1}==1)
                                for k = 1:2:size(Process(i).Parameters,2)
                                    if strcmp(Process(i).Parameters{k},'persistLevel')
                                        Process(i).Parameters{k+1} = persist;
                                        if isempty(Control(1).Command), n=1; else n=length(Control)+1; end
                                        Control(n).Command = 'set&Run';
                                        Control(n).Parameters = {'Process',i,...
                                                              'persistLevel',persist};
                                        break
                                    end
                                end
                            end
                        end
                    end
                end
            end  
        case 'pgain'
            % change processing gain factor for 1st 'image' Process structure with method "imageDisplay".
            for i = 1:size(Process,2);
                if (strcmp(Process(i).classname,'Image'))&&(strcmp(Process(i).method,'imageDisplay'))
                    break;
                end
            end
            if i <= size(Process,2)
                if isempty(Control(1).Command), n=1; else n=length(Control)+1; end
                Control(n).Command = 'set&Run';
                Control(n).Parameters = {'Process',i,...
                                         'pgain',pgain};
            end
        case 'speed'
            % change speed of sound correction factor.
            Resource.Parameters.speedCorrectionFactor = speedCorrect;
            if isempty(Control(1).Command), n=1; else n=length(Control)+1; end
            Control(n).Command = 'set&Run';
            Control(n).Parameters = {'Parameters',1,...
                                  'speedCorrectionFactor',speedCorrect};
        case 'displayChange'
            % adjust display window for size, position or aspect ratio change.
            axesHandle = get(Resource.DisplayWindow(1).figureHandle,'CurrentAxes');
            cla(axesHandle); % this clears any objects attached to the old axes.
            set(Resource.DisplayWindow(1).figureHandle, ...
                'Position',[Resource.DisplayWindow(1).Position(1), ... % left edge
                            Resource.DisplayWindow(1).Position(2), ... % bottom
                            Resource.DisplayWindow(1).Position(3) + 100, ... % width
                            Resource.DisplayWindow(1).Position(4) + 150]);    % height
            set(axesHandle, 'Units','pixels', ...
                            'Position',[60,90,Resource.DisplayWindow(1).Position(3),Resource.DisplayWindow(1).Position(4)]);
            set(axesHandle, 'Units','normalized');  % restore normalized units for re-sizing window.
            ymin = Resource.DisplayWindow(1).ReferencePt(2);
            ymax = ymin + Resource.DisplayWindow(1).Position(4)*Resource.DisplayWindow(1).pdelta;
            xmin = Resource.DisplayWindow(1).ReferencePt(1);
            xmax = xmin + Resource.DisplayWindow(1).Position(3)*Resource.DisplayWindow(1).pdelta;
            DisplayData = zeros(Resource.DisplayWindow(1).Position(4),Resource.DisplayWindow(1).Position(3),'uint8');
            Resource.DisplayWindow(1).imageHandle = image(DisplayData,...
                    'Parent',axesHandle, ...
                    'Xdata',[xmin,xmax], ...
                    'YData',[ymin,ymax]);
            Resource.DisplayWindow(1).clrWindow = 1;
            Resource.ImageBuffer(1).lastFrame = 0; % this clears the ImageBuffer
            axis(axesHandle, 'equal', 'tight');
       
        case 'rcvloop'
            % switch to/from receive data loop mode (Resource.Parameters.simulateMode = 2)
            if rloopButton == 1
                if isempty(Control(1).Command), n=1; else n=length(Control)+1; end
                Control(n).Command = 'copyBuffers'; % copyBuffers does a sequenceStop
                runAcq(Control); % NOTE:  If runAcq() has an error, it reports it then exits MATLAB.
                Resource.Parameters.simulateMode = 2;
                if isempty(Control(1).Command), n=1; else n=length(Control)+1; end
                Control(n).Command = 'set&Run';
                Control(n).Parameters = {'Parameters',1,'simulateMode',2,'startEvent',Resource.Parameters.startEvent};
                simButton = 0;
                set(findobj('String','Simulate'),'Value',0);
            else
                if (VDAS==1) % restart sequence
                    Resource.Parameters.simulateMode = 0;
                    if isempty(Control(1).Command), n=1; else n=length(Control)+1; end
                    Control(n).Command = 'set&Run';
                    Control(n).Parameters = {'Parameters',1,'simulateMode',0,'startEvent',Resource.Parameters.startEvent};
                    simButton = 0;
                    set(findobj('String','Simulate'),'Value',0);
                else % if no hardware, go back to simulate mode 1
                    Resource.Parameters.simulateMode = 1;
                    if isempty(Control(1).Command), n=1; else n=length(Control)+1; end
                    Control(n).Command = 'set&Run';
                    Control(n).Parameters = {'Parameters',1,'simulateMode',1,'startEvent',Resource.Parameters.startEvent};
                    simButton = 1;
                    set(findobj('String','Simulate'),'Value',1);
                end
            end
        case 'simulate'
            % switch mode to/from simulate.
            if (simButton == 1)
                Resource.Parameters.simulateMode = 1;
                if isempty(Control(1).Command), n=1; else n=length(Control)+1; end
                Control(n).Command = 'set&Run';
                Control(n).Parameters = {'Parameters',1,'simulateMode',1,'startEvent',Resource.Parameters.startEvent};
                if rloopButton == 1
                    rloopButton = 0;
                    set(findobj('String','Rcv Data Loop'),'Value',0);
                end
            else
                if (VDAS==1)
                    Resource.Parameters.simulateMode = 0;
                    if isempty(Control(1).Command), n=1; else n=length(Control)+1; end
                    Control(n).Command = 'set&Run';
                    Control(n).Parameters = {'Parameters',1,'simulateMode',0,'startEvent',Resource.Parameters.startEvent};
                else
                    simButton = 1; % if no hardware, change state back to simulate.
                    set(findobj('String','Simulate'),'Value',1);
                    fprintf('VSX: Entering simulate mode since hardware is not present.\n');
                end
            end
    end
    
    action = 'none';  % this prevents continued execution of same action.
    % if ~isempty(Control(n).Command), disp(Control), drawnow, end % Uncomment to print out Control changes.
        
    % HIFU Limits Check: If profile 5 is in use, we will check here to see if the Control structure
    % will trigger an update; if so, call TXEventCheck before proceeding to call runAcq.
    if profile5ena > 0
        if any(strcmp({Control.Command}, 'update&Run'))
            [TXerr, TXEvent] = TXEventCheckh();
            if TXerr == 1
                fprintf(2,'Profile 5 limit check: Illegal or overlimit conditions detected that could be damaging to system.\n');
                fprintf(2,' - TXEvent structure in workspace may help identify error.\n');
                break; % Force VSX to exit gracefully if a limit error is found
            elseif TXerr == 2
                Control.Command(1) = [];  % Set Control.Command to 'empty' for no control action.
                fprintf(2,'Profile 5 limit check: The GUI command that created this condition has been blocked.');
                fprintf(2,'Re-adjust transmit frequency, duty cycle, or make other changes as appropriate.\n\n');
            end
        end
    end
    
    % Perform low pass filter on sequencePeriod times.
    sequencePeriod = sequencePeriod * 0.8 + tElapsed * 0.2;
    % Call runAcq and time its execution.
    tStartRunAcq = tic;
    % NOTE:  If runAcq() has an error, it reports it then exits to MATLAB.
    runAcq(Control);
    tElapsed = toc(tStartRunAcq);
    if freeze==1    % if freeze got set by runAcq, set state of togglebutton.
        set(findobj('String','Freeze'),'Value',1);
    end
end

% Copy buffers back to Matlab space. copyBuffers also stops the hardware sequencer.
Control(1).Command = 'copyBuffers';
runAcq(Control); % NOTE: If runAcq() has an error, it reports it then exits MATLAB.
if (VDAS == 1)
    % HIFU: before closing HW, check for use of external power supply and disable it.
    switch profile5ena
        case 1
            % Internal HIFU supply was in use so we need to set voltage back to minimum
            [result,~] = setTpcProfileHighVoltage(1.6, 5);
        case 2
            % External supply was in use so now we have to disable its output and set voltage and
            % current back to minimum levels of 1.6 V., 2 A.
            [extPSstatus, ~] = extPwrCtrl('CLOSE',1.6); % disable and close
    end
    % Close hardware.
    Result = hardwareClose();
end
% Print out frame rate estimate.
if ~exist('frameRateFactor','var')
    fprintf('Sequence rate = %3.2f\n', 1/sequencePeriod)
else
    fprintf('Frame rate = %3.2f\n', frameRateFactor/sequencePeriod)
end
% If a LogData record size was specified, convert the LogData file.
% *** Note: The called routine is platform specific.
if (ismac && isfield(Resource.Parameters,'numLogDataRecs')), convertLogData; end

end % This ends the master outer while looping construct for "relaunchVsx".
quit
%return