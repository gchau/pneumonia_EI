
folder_head= ['/home/gabrielamamani/HEAD/'];
cd(folder_head);
folder_head = dir;
aux0=struct2cell(folder_head);
aux_head=aux0(1,3:29)';
head = cell2mat(aux_head);

folder_data= ['/home/gabrielamamani/DATA/'];
cd(folder_data);
folder_data = dir;
aux1=struct2cell(folder_data);
aux_data=aux1(1,3:29)';
data = cell2mat(aux_data);

cd('/home/gabrielamamani/');

    for id = 4:size(folder_head,1)-2
        id
        head_i= head(id,:);
        data_i=data(id,:);
        file_headi=strcat('/home/gabrielamamani/HEAD/',head_i);
        load(file_headi)
        clear Receive
        load('Receive.mat')
        load('TX.mat')
        file_datai=fopen(strcat('/home/gabrielamamani/DATA/',data_i),'r');
        [Raw, n]=fread(file_datai,Inf,'int16=>int16');
        fclose('all');
        i=Resource.RcvBuffer.rowsPerFrame;
        j=Resource.RcvBuffer.colsPerFrame;
        k=n/i/j;
        sizes=[i j k];
        Raw=reshape(Raw,sizes);
        l2mm=1540/Trans.frequency/1e3;
        
        %conv_recon_mf_v3_gap
         [video_final] = conv_recon_mf_v3_gap_function(Resource,Trans,Receive,TX,...
                 SFormat,Raw);
%         [video_final] = conv_recon_mf_parfor(Resource,Trans,Receive,TX,...
%                 SFormat,Raw);

        name_i=head(id,5:end);
        savefile=strcat('/home/gabrielamamani/REAL_RAWDATA/P',name_i);
        save(savefile,'video_final') 
        %clear Raw
        %clear videom
        clear video_final
        
    end
    
