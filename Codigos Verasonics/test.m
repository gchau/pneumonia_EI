cd('D:\Users\electronica\Desktop\Pneumonia\Output 05-Jul-2016_Verasonics_5_50_10_128_-20_');
folder_data = dir;
folder_data=struct2cell(folder_data);
folder_DATA=folder_data(1,3:end)';
folder_DATA= cell2mat(folder_DATA);

pneumonia = ones(size(folder_DATA,1),1);
pneumonia(10,1)=0;
pneumonia(14:18,1)=zeros(5,1);
pneumonia(23:25,1)=zeros(3,1);

for id = 1:size(folder_DATA,1)
    
    load(folder_DATA(id,:))
    ma = mean(Mask,2);
    mean_mask(:,id)=ma;
        
end

mu = mean(mean_mask)';