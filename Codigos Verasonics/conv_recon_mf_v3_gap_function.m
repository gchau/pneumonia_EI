
function [video_final] = conv_recon_mf_v3_gap_function(Resource,Trans,Receive,TX,...
                SFormat,Raw)

%load('TX.mat')

c = Resource.Parameters.speedOfSound;  
Fc=Trans.frequency*1e6; 
Fs=4*Fc;
rxlambda=Receive(1).samplesPerWave*2;
mm2smpl=1e-3*Fc/c*rxlambda; %constant to change from mm to samples

maxAprSz=TX.NumEl;
dB=55;
pitch = 0.3e-3;
n=Resource.Parameters.numTransmit;
nr=SFormat.numRays;
%txFNum=TX.FNum;
txFNum=3;
clear samples;

samples(1,:)=[Receive(1).endSample Receive(1).startSample];
samples(2,:)=[Receive(2).endSample Receive(2).startSample];
samples(3,:)=[Receive(3).endSample Receive(3).startSample];
samples(4,:)=[Receive(4).endSample Receive(4).startSample];
samples(5,:)=[Receive(5).endSample Receive(5).startSample];
samples(6,:)=[Receive(6).endSample Receive(6).startSample];

samples(:,3)=samples(:,1)-samples(:,2)+1;

zstart=SFormat.startDepth*rxlambda;
zend=SFormat.endDepth*rxlambda;
valid_data=zend-zstart;


Z1=(((Receive(1).startDepth)*2*4:(Receive(1).endDepth)*2*4-1))*c/Fs/2;             % in the axial direction [mm]
Z1=repmat(Z1',[1 n nr]);

Z2=(((Receive(3).startDepth)*2*4:(Receive(3).endDepth)*2*4-1))*c/Fs/2;             % in the axial direction [mm]
Z2=repmat(Z2',[1 n nr]);

Z3=(((Receive(5).startDepth)*2*4:(Receive(5).endDepth)*2*4-1))*c/Fs/2;             % in the axial direction [mm]
Z3=repmat(Z3',[1 n nr]);



%%delay_offset(focus)=  Fs/Fc*(sqrt(((txNumEl)*Trans.spacing)^2+txFocus(focus)^2)-txFocus(focus));
Zoffset=[1]*c/Fs;
% Zoffset1=(1)*c/Fs;
% Zoffset2=(samples(3,3)+1)*c/Fs;
% Zoffset3=(samples(6,3)+1)*c/Fs;

%%%%%%%%%%%%%%%%%%%%%%Data Index formation %%%%%%%%%%%%%%%%%%%%%
index1=repmat(1:n,[samples(1,3) 1 nr]); 
transshift1=permute(index1,[1 3 2]);
X1=(transshift1-index1)*pitch;
%index_fixer1=(floor(((Receive(1).startDepth)*2*4:(Receive(1).endDepth)*2*4*nr*n-1)'/samples(1,3))*samples(1,3));
index_fixer1=(floor((0:(samples(1,3)*nr*n-1))'/samples(1,3))*samples(1,3));


index2=repmat(1:n,[samples(3,3) 1 nr]); 
transshift2=permute(index2,[1 3 2]);
X2=(transshift2-index2)*pitch;
%index_fixer2=(floor(((Receive(3).startDepth)*2*4:(Receive(3).endDepth)*2*4*nr*n-1)'/samples(3,3))*samples(3,3));
index_fixer2=(floor((0:(samples(3,3)*nr*n-1))'/samples(3,3))*samples(3,3));


index3=repmat(1:n,[samples(6,3) 1 nr]); 
transshift3=permute(index3,[1 3 2]);
X3=(transshift3-index3)*pitch;
%index_fixer3=(floor(((Receive(5).startDepth)*2*4:(Receive(5).endDepth)*2*4*nr*n-1)'/samples(5,3))*samples(5,3));
index_fixer3=(floor((0:(samples(5,3)*nr*n-1))'/samples(5,3))*samples(5,3));
    

for hh=1:nr
    
    aux1 = TX(1+txFNum*(hh-1)).Apod;
    aux1(aux1~=0)=hanning(sum(aux1(aux1~=0),2));
    aux2 = TX(2+txFNum*(hh-1)).Apod;
    aux2(aux2~=0)=hanning(sum(aux2(aux2~=0),2));
    aux3 = TX(3+txFNum*(hh-1)).Apod;
    aux3(aux3~=0)=hanning(sum(aux3(aux3~=0),2));    
    H11(:,:,hh)=aux1;   
    H22(:,:,hh)=aux2;
    H33(:,:,hh)=aux3;    
    
  
end

H1 = repmat(H11,[samples(1,3) 1 1]);
H2 = repmat(H22,[samples(3,3) 1 1]);
H3 = repmat(H33,[samples(5,3) 1 1]);

%videom = zeros(Receive(6).endDepth*2*4,nr,size(Raw,3));

%%%%%%%%%%Receive Data, Reshape and Parameters reading%%%%%%%%%%%%%%%%%%
for frame=1:size(Raw,3)
A=Raw(:,:,frame);
%A=RcvData{1}(:,:,frame);

A=A(1:sum(samples(:,3))*nr,:)';
B= reshape(A,Resource.Parameters.numRcvChannels,sum(samples(:,3)),nr);
B1=B(:,samples(1,2):samples(1,1),:);
B2=B(:,samples(2,2):samples(2,1),:);
B3=B(:,samples(3,2):samples(3,1),:);
B4=B(:,samples(4,2):samples(4,1),:);
B5=B(:,samples(5,2):samples(5,1),:);
B6=B(:,samples(6,2):samples(6,1),:);

C1=permute(B1,[2 1 3]); 
C2=permute(B2,[2 1 3]);
C3=permute(B3,[2 1 3]); 
C4=permute(B4,[2 1 3]);
C5=permute(B5,[2 1 3]); 
C6=permute(B6,[2 1 3]);


C=[C1 C2;C3 C4; C5 C6];

CFoco1 = [C1 C2];
CFoco2 = [C3 C4];
CFoco3 = [C5 C6];


%%%%%%%%%%%%%%%%%%%%%% Delay %%%%%%%%%%%%%%%%%%%%%%%%%

delays1 = ((Z1+Zoffset) + sqrt((Z1).^2 + (X1).^2))*Fs/c;
delaysa1=floor(delays1);
q1=delays1-delaysa1;
delaysa1(delaysa1>(Receive(1).endDepth)*2*4)=(Receive(1).endDepth)*2*4;
delaysa1(delaysa1<1)=0;
delaysb1=delaysa1+1;
delaysb1(delaysb1>(Receive(1).endDepth)*2*4)=(Receive(1).endDepth)*2*4;



delays2 = ((Z2+Zoffset) + sqrt((Z2).^2 + (X2).^2))*Fs/c;
delaysa2=floor(delays2);
q2=delays2-delaysa2;
delaysa2(delaysa2>(Receive(3).endDepth)*2*4)=(Receive(3).endDepth)*2*4;
delaysa2(delaysa2<(Receive(3).startDepth)*2*4)=(Receive(3).startDepth)*2*4;
delaysb2=delaysa2+1;
delaysb2(delaysb2>(Receive(3).endDepth)*2*4)=(Receive(3).endDepth)*2*4;


delays3 = ((Z3+Zoffset) + sqrt((Z3).^2 + (X3).^2))*Fs/c;
delaysa3=floor(delays3);
q3=delays3-delaysa3;
delaysa3(delaysa3>(Receive(5).endDepth)*2*4)=(Receive(5).endDepth)*2*4;
delaysa3(delaysa3<(Receive(5).startDepth)*2*4)=(Receive(5).startDepth)*2*4;
delaysb3=delaysa3+1;
delaysb3(delaysb3>(Receive(5).endDepth)*2*4)=(Receive(5).endDepth)*2*4;

%%%%%%%%%%%%%%Delay Application with linear interpolation%%%%%%%%%%%%%%%%%
Dd1= (1-q1(:)).*double(CFoco1(delaysa1(:)+index_fixer1))+q1(:).*double(CFoco1(delaysb1(:)+index_fixer1));
Dd1=double((reshape(Dd1,size(delays1))));


Dd2= (1-q2(:)).*double(CFoco2(delaysa2(:)+index_fixer2-Receive(3).startDepth*2*4))+q2(:).*double(CFoco2(delaysb2(:)+index_fixer2-Receive(3).startDepth*2*4));
Dd2=double((reshape(Dd2,size(delays2))));


Dd3= (1-q3(:)).*double(CFoco3(delaysa3(:)+index_fixer3-Receive(5).startDepth*2*4))+q3(:).*double(CFoco3(delaysb3(:)+index_fixer3-Receive(5).startDepth*2*4));
Dd3=double((reshape(Dd3,size(delays3))));


%%%%%%%%%%%%%%%%% B MODE FORMATION %%%%%%%%%%%%%%


Da1=Dd1.*H1;
Da2=Dd2.*H2;
Da3=Dd3.*H3;

Rf1(:,:,frame)=squeeze(sum(Da1(1:end,:,:),2));
Rf2(:,:,frame)=squeeze(sum(Da2(1:end,:,:),2));
Rf3(:,:,frame)=squeeze(sum(Da3(1:end,:,:),2));


US1=Rf1(:,:,frame);
US2=Rf2(:,:,frame);
US3=Rf3(:,:,frame);


video1(:,:,frame)=US1;
video2(:,:,frame)=US2;
video3(:,:,frame)=US3;


frame
end
                                                  
                                                        
% PROMEDIO POR PESOS                                                        
Foco1=TX(1).focus*2*4;
Foco2=TX(2).focus*2*4;
Foco3=TX(3).focus*2*4;

ZInterp1=Receive(3).startDepth*2*4:Receive(5).startDepth*2*4-1;
fauxF11=repmat(abs(ZInterp1-Foco1)./(abs(ZInterp1-Foco1)+abs(ZInterp1-Foco2)),[nr 1])';
fauxF21=repmat(abs(ZInterp1-Foco2)./(abs(ZInterp1-Foco1)+abs(ZInterp1-Foco2)),[nr 1])';
Parte1F1=video1(Receive(3).startDepth*2*4:Receive(5).startDepth*2*4-1,:,:);
Parte1F2=video2(1:Receive(5).startDepth*2*4-Receive(3).startDepth*2*4,:,:);
Interpolacion11=Parte1F1.*repmat((1-fauxF11),[1 1 frame])+ ...
                 Parte1F2.*repmat((1-fauxF21),[1 1 frame]);


ZInterp2=Receive(5).startDepth*2*4:Receive(1).endDepth*2*4;
fauxF12=repmat(abs(ZInterp2-Foco1)./(abs(ZInterp2-Foco1)+abs(ZInterp2-Foco2)+abs(ZInterp2-Foco3)),[nr 1])';
fauxF22=repmat(abs(ZInterp2-Foco2)./(abs(ZInterp2-Foco1)+abs(ZInterp2-Foco2)+abs(ZInterp2-Foco3)),[nr 1])';
fauxF32=repmat(abs(ZInterp2-Foco3)./(abs(ZInterp2-Foco1)+abs(ZInterp2-Foco2)+abs(ZInterp2-Foco3)),[nr 1])';
Parte2F1=video1(Receive(5).startDepth*2*4:end,:,:);
Parte2F2=video2(Receive(5).startDepth*2*4-Receive(3).startDepth*2*4: ...
                Receive(1).endDepth*2*4-Receive(3).startDepth*2*4,:,:);
Parte2F3=video3(1:Receive(1).endDepth*2*4-Receive(5).startDepth*2*4+1,:,:);
Interpolacion22=Parte2F1.*repmat((1-fauxF12),[1 1 frame])+...
                Parte2F2.*repmat((1-fauxF22),[1 1 frame])+...
                Parte2F3.*repmat((1-fauxF32),[1 1 frame]);

            
fauxab1=repmat(abs((Foco3:Receive(1).endDepth*2*4)-Foco2),[nr 1])';
fauxb1=repmat(abs((Foco3:Receive(1).endDepth*2*4)-Foco3),[nr 1])'; 
fauxa1=472*ones(size(fauxb1)); 
Interpolacion22(end-264:end,:,:)=(Parte2F3(end-264:end,:,:).*repmat(fauxab1,[1 1 frame])-Parte2F2(end-264:end,:,:).*repmat(fauxb1,[1 1 frame]))./ ...
                repmat(fauxa1,[1 1 frame]);   



ZInterp3=Receive(1).endDepth*2*4:Receive(3).endDepth*2*4-1;
fauxab=repmat(abs(ZInterp3-Foco2),[nr 1])';
fauxb=repmat(abs(ZInterp3-Foco3),[nr 1])';    

fauxa=472*ones(size(fauxb)); 
Parte3F2=video2((Receive(1).endDepth*2*4-Receive(3).startDepth*2*4+1):end,:,:);
Parte3F3=video3((Receive(1).endDepth*2*4-Receive(5).startDepth*2*4)+1 ...
                :Receive(3).endDepth*2*4-1-Receive(5).startDepth*2*4+1,:,:);
            
Interpolacion33=(Parte3F3.*repmat(fauxab,[1 1 frame])-Parte3F2.*repmat(fauxb,[1 1 frame]))./ ...
                repmat(fauxa,[1 1 frame]);         

videom(1:Receive(3).startDepth*2*4-1,:,:)=video1(1:Receive(3).startDepth*2*4-1,:,:);
videom(Receive(3).startDepth*2*4:Receive(5).startDepth*2*4-1,:,:)=Interpolacion11;
videom(Receive(5).startDepth*2*4:Receive(1).endDepth*2*4,:,:)=Interpolacion22;
videom(Receive(1).endDepth*2*4:Receive(3).endDepth*2*4-1,:,:)=Interpolacion33;
videom(Receive(3).endDepth*2*4:SFormat.endDepth*2*4,:,:)=2.96*video3(Receive(3).endDepth*2*4-...
                                                            Receive(5).startDepth*2*4+1:...
                                                            Receive(3).endDepth*2*4-...
                                                            Receive(5).startDepth*2*4+1+ ...
                                                            SFormat.endDepth*2*4 - ...
                                                            Receive(3).endDepth*2*4,:,:);                                                                                                         
                                                       
                                                        
[video_final] = DataUtil(videom);
 
%save('/home/gabrielamamani')
end