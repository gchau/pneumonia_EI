
function Bmode=imgUS(rf,x,z,dB)

% Conversion de unidades:
x=x*1e3;z=z*1e3;

% Proceso:
iUS = 20*log10(abs(hilbert(rf))); 
iUS = iUS - max(iUS(:));

% figure;

imagesc(x,z,iUS); 
Bmode=iUS;

xlabel('Samples','FontSize', 20)
ylabel('Samples','FontSize', 20)

hb=colorbar;
ylabel(hb,'dB','FontSize', 20)
axis normal
set(gca, 'clim', [-dB 0])
set(gca,'FontSize',20)
colormap gray

end