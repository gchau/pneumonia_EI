%clear all;
%load('E:/ADQUISITIONS/PNEUMONIA/HEAD73616943906TK1');
load('HEAD73616883113TK1')
%fi=fopen('RCV73616955706TK3CL0');
% fi=fopen('RCV73638641221TK1CL0');
% [Raw,n]=fread(fi,Inf,'int16=>int16');


%FILE.RcvHandle=fopen([FILE.Directory ...
%     sprintf('RCV%dTK%dCL%d',FILE.PatientID,FILE.Take,FILE.Cluster)],'r');
FILE.ImgHandle=fopen(['H:/ADQUISITIONS/PNEUMONIA/' ...
    sprintf('IMG%dTK%dCL%d',FILE.PatientID,FILE.Take,FILE.Cluster)],'r');

%fi=fopen('RCV73616955706TK3CL0');
%[Raw,n]=fread(fi,'int16');

%fclose(FILE.RcvHandle);
%Resource.RcvBuffer.rowsPerFrame=1179648;
%Resource.RcvBuffer.colsPerFrame=64;
i=Resource.RcvBuffer.rowsPerFrame;%Resource.RcvBuffer.rowsPerFrame;
j=Resource.RcvBuffer.colsPerFrame;%Resource.RcvBuffer.colsPerFrame;
k=n/i/j;
sizes=[i j k];

Raw2=reshape(Raw,sizes);

l2mm=1540/Trans.frequency/1e3;
% convb_recon_gapli

[video, n]=fread(FILE.ImgHandle,Inf,'double=>double');
i=(SFormat.endDepth-SFormat.startDepth)*4;
j=128;
k=n/i/j;
sizes=[i j k];              
video=reshape(video,sizes);

xl=linspace(-j/2*0.3,j/2*0.3,j);
zl=linspace(SFormat.startDepth*l2mm,SFormat.endDepth*l2mm,j);

aviobj = avifile(['H:/ADQUISITIONS/PNEUMONIA/' ...
                  sprintf('VIDI%dTK%d',FILE.PatientID,FILE.Take)],...
                  'compression','None'); 
aviobj.Fps= 10;
CMAP= repmat(((0:1/64:1).^UIValues.gamma)',[1 3]);

for i=1:sizes(3)
figure(89);
imagesc(xl,zl,video(:,:,i));
title('Verasonics Beamforming & Processing')
set(gca,'clim',[0 30]);
xlabel('azimuth (mm)'),ylabel('depth (mm)')
set(gcf,'Colormap',CMAP);
axis image;
aviobj = addframe(aviobj,gcf);
pause(0.01);
end 
viobj = close(aviobj); %Close avi object

fclose('all');