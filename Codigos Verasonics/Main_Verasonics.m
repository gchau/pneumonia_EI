%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETECCION DE NEUMONIA VERASONICS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;
home;
code = pwd;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPARACION DE LA MUESTRA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%scanner = input('Select scanner: \n(1)Ultrasonix\n(2)Verasonix\n(3)Siemens\n ');

% Interfaz de lectura de datos
input_folder = uigetdir;
cd(input_folder)

% Deteccion de la ruta de los archivos
cd ..

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 N = 128;
 overlap=10;
 umbral = -20;
 
     
%cd('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pacientes_Verasonics\HEAD_G')
cd('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pacientes_Verasonics\HEAD_FG10S')
folder_head=dir;
folder_head=struct2cell(folder_head);
folder_HEAD=folder_head(1,3:end)';
folder_HEAD= cell2mat(folder_HEAD);


%cd('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pacientes_Verasonics\DATA_G')
cd('F:\REAL_RAW_DATA_FG10S')
folder_data=dir;
folder_data=struct2cell(folder_data);
folder_DATA=folder_data(1,3:end)';
folder_DATA= cell2mat(folder_DATA);

number_patients=size(folder_DATA,1);

for dim = 50%10:10:100
    
    for factor = 5%10:10:50


        output_folder = [code '\Output ' date '_' 'Verasonics_'  num2str(factor) '_'  num2str(dim) '_'  num2str(overlap) '_' num2str(N) '_' num2str(umbral) '_' '\ ']
        cd(input_folder);

        if exist(output_folder,'dir')==7;
        else
            mkdir(output_folder);
        end

        folder = dir;
        directorio = {folder.isdir};
        directorio = find(cell2mat(directorio));

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % PROGRAMA
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        for id = 1:number_patients%directorio(3:end)
%       aux_folder=strcat('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pacientes_Verasonics\HEAD_G\',folder_HEAD(id,:));
        aux_folder=strcat('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pacientes_Verasonics\HEAD_FG10S\',folder_HEAD(id,:));
        load(aux_folder)
        aux_folder=strcat('F:\REAL_RAW_DATA_FG10S\',folder_DATA(id,:));
        load(aux_folder)
        RF=video_final;
        harmonic = 128;

        % IMPORTANTE               harmonic=input('Size of 2nd dimention of normal data: ');

        if harmonic==size(RF,2)
            xw=linspace(0,0.038,size(RF,2));
        else
           RF=RF(:,1:2:end,:);
            xw=linspace(0,0.038,size(RF,2));
        end

        tic
        pleura=strcat('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pleura_Verasonics_FG10S\' ...
                      ,folder_DATA(id,1:15),'\',folder_DATA(id,:));
        load(pleura)
       
        freq = Trans.frequency*10^6;% head.sf=40e6;
        load(pleura);

                for frame = 1:size(RF,3)
                    frame
                    iRF= RF(:,:,frame);
                    % Filtrado de bajas frecuencias
                    [B] = fir1(100,1.5e6/freq/2,'high');
                    for indexFIR=1:size(iRF,2)
                        iRF(:,indexFIR)=filter(B,1,iRF(:,indexFIR));
                    end
                    %Imagen en Modo-B
                    %iUS = 20*log10(abs(hilbert(iRF)));
%                                 iUS=abs(hilbert(iRF));
%                                 iUS=UIValues.pgain*iUS*(2^15-1)/(2^13-1); % agregado dsps
%                                 iUS=(20*log10(iUS)-UIValues.reject)/UIValues.DynRng; % agregado dsps 
%                                 iUS(iUS<0)=0;iUS(iUS>1)=1;
                    %iUS = iUS - max(iUS(:));
                    
                    %Recorte de la imagen segun la segmentacion de la pleura
                    for indice=1:size(iRF,2);
                        iRF2(:,indice)= iRF(ini(indice,frame):fin(indice,frame),indice);
                    end

         %           factor=10;
                    cd(code);
                    parfor ind=1:size(iRF2,2)-factor+1;

                        Region = iRF2(:,ind:ind+factor-1);
                        [Angulo,I01,I2,BW2] = FeatureFourier2_Verasonics(Region,freq,N,dim,overlap,umbral);
                        AngMask{ind}=Angulo;
                        Ind01{ind}=I01;
                        Ind02{ind}=I2;
                        BW2aux{ind}=BW2;
                    end

                    Mascara=cell2mat(AngMask);
                    Mascara=[mean(Mascara)*ones(1,ceil(factor/2)-1) Mascara mean(Mascara)*ones(1,ceil(factor/2)-1)];
                    Mask(:,frame)=Mascara;
                    %Imagen(:,:,frame)=iUS;
                    clear iRF2;

                end

                toc
                filename = strcat(output_folder,folder_DATA(id,1:end));
%                save(filename,'Imagen','Mask','ini','fin','BW2aux');
                save(filename','Mask','ini','fin');
                clear Mask;
                %clear Imagen;
                clear ini;
                clear fin;
                %clear BW;
        end
    end
        cd(code);       
end
%end


