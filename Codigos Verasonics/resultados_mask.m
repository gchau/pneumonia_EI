cd('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pacientes_Verasonics\DATA10S3');
folder_data = dir;
folder_data=struct2cell(folder_data);
folder_DATA=folder_data(1,3:end)';
folder_DATA= cell2mat(folder_DATA);

pneumonia = ones(size(folder_DATA,1),1);
pneumonia(10,1)=0;
pneumonia(14:18,1)=zeros(5,1);
pneumonia(23:25,1)=zeros(3,1);


UIValues.pgain=2;
UIValues.reject=20;
UIValues.DynRng=85;
pitch=0.3e-3;
n=128;
xl=linspace(0,n*pitch*1e3,n);
zl=linspace(0,292*1540/(7.5*10^3),2336);%2366 = size(video_final) 292*2*4
%CMAP= repmat(((0:1/128:1).^UIValues.gamma)',[1 3]);
CMAP= repmat(((0:1/128:1).^2)',[1 3]);

   for id = 1:size(folder_DATA,1)
    cd('D:\Users\electronica\Desktop\Pneumonia\Verasonics_Pacientes_Puno\Pacientes_Verasonics\DATA10S3')
    data = folder_DATA(id,:);
    load(data)
    
    cd('D:\Users\electronica\Desktop\Pneumonia\3Output 20-Jul-2016_Verasonics_5_50_10_128_-20_')
    data = folder_DATA(id,:);
    load(data)    
    
    
    cd('D:\Users\electronica\Desktop\Pneumonia\Videos_Output20Jul') 
    
    name = folder_DATA(id,1:end-4);
    name = strcat(name,'.avi');
    writerObj = VideoWriter(name);
    writerObj.FrameRate = 1;
    open(writerObj)
 

    for frame=1:size(video_final,3)
        figure(1)
        Bmode=video_final(:,:,frame);
        Bmode=abs(hilbert(Bmode));
        Bmode=UIValues.pgain*Bmode*(2^15-1)/(2^13-1);
        Bmode=(20*log10(Bmode)-UIValues.reject)/UIValues.DynRng;
        Bmode(Bmode<0)=0;Bmode(Bmode>1)=1;
        Bmode=medfilt2(Bmode,[8 1]);
        Bmode=imfilter(Bmode,fspecial('average',[1 8]));
        Bmode=(imsharpen(Bmode,'Amount',2,'Radius',0.1','Threshold',0.2));
        subplot(1,2,1),imagesc(xl,zl,Bmode);
        title('Conventional Beamforming')
        set(gca,'clim',[0 1]);
        xlabel('azimuth (mm)'),ylabel('depth (mm)')
        set(gcf,'Colormap',CMAP);
        axis image;
        secsPerImage = [5 10 15];
        %open(writerObj);
         subplot(1,2,2), plot(Mask(:,frame))
         xlim([1 128])
         ylim([-1.5 1.5]) 
        a=figure(1); 
        h = getframe(a);
        writeVideo(writerObj,h);
        pause(0.01)
    end
    close(writerObj);
    clear('writerObj')
   end

