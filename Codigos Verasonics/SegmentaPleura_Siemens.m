%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETECCION DE NEUMONIA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;
home;
code = pwd;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPARACION DE LA MUESTRA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Interfaz de lectura de datos
input_folder = uigetdir;
cd(input_folder)


% Deteccion de la ruta de los archivos
output_folder=pwd;
output_folder = [output_folder '\Pleura\ ']
cd(input_folder);

if exist(output_folder,'dir')==7;
else
    mkdir(output_folder);
end

folder = dir;
directorio = {folder.isdir};
directorio = find(cell2mat(directorio));

for id = directorio(3:end)
    
    % lectura del archivo
    cd(code);
    folder_loop = strcat(input_folder,'\',folder(id).name,'\');
    cd(folder_loop);
    images  = dir('*.rfd');
    
    mu=strcat(input_folder,'\','Reader','\');
    
    for fileid = 1:length(images) 
        
        fileCompleto = strcat(folder_loop,'\',images(fileid).name);
        cd(mu);
        RF_all = readFile(fileCompleto);
        RF = RF_all.RfData;
        RF = double(RF);
        
        for frame = 1:size(RF,3)
        frame
            
            F2p= RF(:,:,frame);
            
            iUS = 20*log10(abs(hilbert(F2p))); 
            iUS = iUS - max(iUS(:)); 

        figure(96);imagesc(iUS);colormap gray;set(gca, 'clim', [-60 0]);
        
        [x, y] = getpts;
        
        p = polyfit(x,y,1);
        yfit =  round(p(1) .* (1:size(RF,2)) + p(2));
        
        hold on
        
       
        
        if p(1)<0
        yfit2=yfit + (length(iUS)-yfit(1));
        else
        yfit2=yfit + (length(iUS)-yfit(end));
        end
            
            plot(yfit,'r');
            plot(yfit2,'r');
            
        ini(:,frame)=yfit;
        fin(:,frame)=yfit2;

        hold off
        
        end
        
        filename = strcat(output_folder,...
            images(fileid).name(1:end-3));
        save(filename,'ini','fin');
        close all;
        
    end
end
