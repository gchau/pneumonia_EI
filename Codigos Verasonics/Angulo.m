function [Perio] = Angulo(Region)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%% Features Inclinacion %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

N = 256;
dim = 50;
overlap=25;

tam = 1:ceil(dim*overlap/100):size(Region,1)-ceil(dim*overlap/100);

I1 = zeros(length(tam),N/2);
cnt = 1;

for ky = tam                        % numero de muestra
    Reg1 = Region(ky:min(ky+dim-1,size(Region,1)),:);
    Reg1a = bsxfun(@times,Reg1,hamming(size(Reg1,1)));
    FReg = abs(fft(Reg1a,N,1));
    FReg2 = sum(FReg,2)/size(Region,2);
    I1(cnt,:) = FReg2(1:N/2)';
    cnt = cnt+1;
end

Perio = bsxfun(@rdivide,I1,max(I1,[],2));

end