%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DETECCION DE NEUMONIA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all;
close all;
home;
code = pwd;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PREPARACION DE LA MUESTRA
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Interfaz de lectura de datos
input_folder = uigetdir;
cd(input_folder)


% Deteccion de la ruta de los archivos
output_folder=pwd;
output_folder = [output_folder '\Verasonics_Pacientes_Puno\Pleura_Verasonics10SF\ ']
%output_folder = [output_folder '\Verasonics_Pacientes_Puno\Pleura_Verasonics_G\ ']
cd(input_folder);

if exist(output_folder,'dir')==7;
else
    mkdir(output_folder);
end

input_folder_pacientes = [input_folder '\Verasonics_Pacientes_Puno\Pacientes_Verasonics\REAL_RAW_DATA10S']
%input_folder_pacientes = ['F:\REAL_RAW_DATA_G2']
cd(input_folder_pacientes);
folder_data = dir;
folder_data=struct2cell(folder_data);
folder_DATA=folder_data(1,3:end)';
folder_DATA= cell2mat(folder_DATA);


UIValues.pgain=2;
UIValues.reject=20;
UIValues.DynRng=85;
pitch=0.3e-3;
n=128;
xl=linspace(0,n*pitch*1e3,n);
zl=linspace(0,292*1540/(7.5*10^3),2336);
CMAP= repmat(((0:1/128:1).^2)',[1 3]);


for id = 126:size(folder_DATA,1)% id=42:64
    
    % lectura del archivo
        output_folder_patient =  strcat(output_folder,folder_DATA(id,1:end-4));
        if exist(output_folder_patient,'dir')==7;
        else
        mkdir(output_folder_patient);
        end

        fileCompleto = strcat(input_folder_pacientes,'\',folder_DATA(id,:));
        RF= cell2mat(struct2cell(load(fileCompleto)));
       % cd(code);
        
       %filenameData = strcat(output_folder,...
       %     images(id-2).name(1:end));
       % RF = DataUtil(RF0,filenameData);
        
        for frame = 1:size(RF,3)
        frame
            
            Bmode=RF(:,:,frame);
            Bmode=abs(hilbert(Bmode));
            Bmode=UIValues.pgain*Bmode*(2^15-1)/(2^13-1);
            Bmode=(20*log10(Bmode)-UIValues.reject)/UIValues.DynRng;
            Bmode(Bmode<0)=0;Bmode(Bmode>1)=1;
            Bmode=medfilt2(Bmode,[8 1]);
            Bmode=imfilter(Bmode,fspecial('average',[1 8]));
            Bmode=(imsharpen(Bmode,'Amount',2,'Radius',0.1','Threshold',0.2));
            figure(1),imagesc(Bmode)
            %figure(1),imagesc(xl,zl,Bmode);
            %title('Conventional Beamforming')
            %set(gca,'clim',[0 1]);
            %xlabel('azimuth (mm)'),ylabel('depth (mm)')
            set(gcf,'Colormap',CMAP);
            %axis image;
            
            
%             ORIGINAL  
%             F2p= RF(:,:,frame);
%             iUS = 20*log10(abs(hilbert(F2p))); 
%             iUS = iUS - max(iUS(:)); 
%             figure(1);imagesc(iUS);colormap gray;set(gca, 'clim', [-85 0]);
        
        [x, y] = getpts;
        
        p = polyfit(x,y,1);
        yfit =  round(p(1) .* (1:size(RF,2)) + p(2));
        
        hold on
        
       
        
        if p(1)<0
        %yfit2=yfit + (length(iUS)-yfit(1));
        yfit2=yfit + (length(Bmode)-yfit(1));
        else
        yfit2=yfit + (length(Bmode)-yfit(end));
        end
            
            plot(yfit,'r');
            plot(yfit2,'r');
            
        ini(:,frame)=yfit;
        fin(:,frame)=yfit2;

        hold off
        
        end
        
        cd(output_folder_patient)
        filename = strcat(output_folder_patient,'\',folder_DATA(id,:));
        save(filename,'ini','fin');
        close all;
        cd(code)
 %   end
end
